#include <iostream>
using namespace std;
int main()
{
    cin.tie(0)->sync_with_stdio(0);
    int n;
    cin >> n;
    bool a[100];
    fill(a, a + 100, false);
    for (int i = 0; i < n; i++)
    {
        int cur;
        cin >> cur;
        a[cur - 1] = true;
    }
    int m;
    int cnt = 0;
    cin >> m;
    for (int i = 0; i < m; i++) 
    {
        bool add = true;
        int k;
        cin >> k;
        for (int j = 0; j < k; j++)
        {
            int cur;
            cin >> cur;
            if (a[cur-1])
            {
                add = false;
                cin.ignore(100, '\n');
                break;
            }
        }
        if (add)
            cnt++;
    }
    cout << cnt;
    return 0;
}