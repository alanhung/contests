#include <bits/stdc++.h>
using namespace std;
// 0 index
long long query(long long tree[], int n, int a, int b) {
    long long ans = 0;
    a += n;
    b += n;
    while(a<=b) {
        if (a&1) ans += tree[a++];
        if (!(b&1)) ans += tree[b--];
        a/=2;
        b/=2;
    }
    return ans;
}
// 0 index
void update(long long tree[], int n, int i, long long val) {
    i += n;
    tree[i] = val;
    for (i/=2; i >= 1; i/=2) {
        tree[i] = tree[2 * i] + tree[2 * i + 1];
    }
}

// 0 index
long long odd[2000000];
long long even[2000000];
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, q;
    cin >> n >> q;
    // 0 index
    int odd_size = n / 2;
    int even_size = n / 2 + n % 2;
    for (int i = 0; i < n; i++) {
        long long x;
        cin >> x;
        if (i%2)
            update(odd, odd_size, i/2 , x);
        else
            update(even, even_size,i/2, x);
    }
    /* cout << "EVEN: " << even_size << '\n'; */
    /* for (int i = even_size; i < 2 * even_size; i++) */
    /*     cout << even[i] << ' '; */
    /* cout << '\n'; */
    /* cout << "ODD: " << odd_size << '\n'; */
    /* for (int i = odd_size; i < 2 * odd_size; i++) */
    /*     cout << odd[i] << ' '; */
    /* cout << '\n'; */
    while(q--) {
        int type;
        cin >> type;
        if (type == 1) {
            int i;
            long long j;
            cin >> i >> j;
            i--;
            if (i%2)
                update(odd, odd_size, i/2 , j);
            else
                update(even, even_size,i/2, j);
        } else {
            int l, r;
            cin >> l >> r;
            l--;
            r--;
            if (l%2) {
                if (!(r%2)) r--;
                cout << query(odd, odd_size, l/2, r/2) << '\n';
            } else {
                cout << query(even, even_size, l/2, r/2) << '\n';
            }
        }
    }
    return 0;
}