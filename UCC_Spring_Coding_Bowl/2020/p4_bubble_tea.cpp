#include <bits/stdc++.h>
using namespace std;
int main() {
    int N;
    cin >> N;
    int val[N + 1];
    val[0] = 0;
    for (int i = 1; i <= N; i++)
        cin >> val[i];
    int dp[N + 1];
    dp[0] = 0;
    dp[1] = val[1];
    if (N >= 2)
        dp[2] = min(val[2] + dp[1], val[2] + val[1] - (int)(min(val[2], val[1]) * 0.25));
    for (int i = 3; i <= N; i++) {
        dp[i] = min(min(val[i] + dp[i - 1], val[i] + val[i - 1] - (int)(min(val[i], val[i - 1]) * 0.25) + dp[i - 2]), val[i] + val[i - 1] + val[i - 2] - (int)(min(min(val[i], val[i - 1]), val[i - 2]) * 0.5  ) + dp[i - 3]);
    }
    cout << dp[N];
    return 0;
}

