// CCC '09 S1 - Cool Numbers
// https://dmoj.ca/problem/ccc09s1
#include <bits/stdc++.h>
using namespace std;
int main() {
    int a, b;
    cin >> a >> b;
    int f = pow(a, 1.0/6) + 1e-9;
    int s = pow(b, 1.0/6) + 1e-9;
    if (f * f * f * f * f * f == a)
        f--;
    cout << s - f << '\n';
    return 0;
}