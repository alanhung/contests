#include <bits/stdc++.h>
using namespace std;
int main()
{
    cin.tie(0)->sync_with_stdio(0);
    int N;
    unsigned long long f;
    cin >> N >> f;
    stack<pair<unsigned long long, int>> st;
    st.push({f, 1});
    unsigned long long cnt = 0;
    for (int i = 1; i < N; i++)
    {
        unsigned long long c;
        cin >> c;
        if (c < st.top().first)
        {
            cnt++;
            st.push({c, 1});
        }
        else if (c == st.top().first)
        {
            cnt += st.top().second;
            st.top().second++;
            if (st.size() >= 2)
                cnt++;
        }
        else
        {
            while (!st.empty() && c > st.top().first)
            {
                cnt += st.top().second;
                st.pop();
            }
            if (!st.empty())
            {
                if (c == st.top().first)
                {
                    cnt += st.top().second;
                    st.top().second++;
                    if (st.size() >= 2)
                        cnt++;
                }
                else
                {
                    cnt++;
                    st.push({c, 1});
                }
            }
            else
                st.push({c, 1});
        }
    }
    cout << cnt;
    return 0;
}