#include <bits/stdc++.h>
using namespace std;
int find(int x, int parent[])
{
    while (x != parent[x])
        x = parent[x];
    return x;
}

bool same(int a, int b, int parent[])
{
    return find(a, parent) == find(b, parent);
}

void unite(int a, int b, int parent[], int size[], vector<int> adj[])
{
    a = find(a, parent);
    b = find(b, parent);
    if (size[a] > size[b])
        swap(a, b);
    size[a] += size[b];
    parent[b] = a;
    adj[a].push_back(b);
}

int main()
{
    cin.tie(0)->sync_with_stdio(0);
    int N, M;
    cin >> N >> M;
    vector<int> adj[N];
    int parent[N];
    int size[N];
    for (int i = 0; i < N; i++)
    {
        parent[i] = i;
        size[i] = 1;
    }
    for (int i = 0; i < M; i++)
    {
        int K;
        cin >> K;
        if (K)
        {
            int f;
            cin >> f;
            f--;
            for (int j = 1; j < K; j++)
            {
                int s;
                cin >> s;
                s--;
                if (!same(f, s, parent))
                    unite(f, s, parent, size, adj);
            }
        }
    }
    vector<int> result;
    int root = find(0, parent);
    cout << size[root] << '\n';
    queue<int> q;
    q.push(root);
    result.push_back(root + 1);
    while (!q.empty())
    {
        int u = q.front();
        q.pop();
        for (int nei : adj[u])
        {
            result.push_back(nei + 1);
            q.push(nei);
        }
    }
    sort(result.begin(), result.end());
    for (int item : result)
        cout << item << ' ';
    return 0;
}