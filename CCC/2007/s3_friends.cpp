// CCC '07 S3 - Friends
#include <iostream>
#include <map>
#include <string>

class Graph
{
public:
  Graph()
  {
  }
  void addEdge(int n, int m)
  {
    l.insert({n, m});
  }

  std::string search(int a, int b)
  {
    int seperation = -1;
    int start = a;

    while (true)
    {
      if (l.find(a) != l.end())
      {
        a = l[a];
        seperation++;
        if (a == b)
        {
          return "Yes " + std::to_string(seperation);
        }

        if (start == a)
        {
          break;
        }
      }
      else
      {
        break;
      }
    }

    return "No";
  }

private:
  std::map<int, int> l;
};

int main()
{
  int numberOfStudent;
  std::cin >> numberOfStudent;

  Graph g = Graph();

  for (int i = 0; i < numberOfStudent; i++)
  {
    int a, b;
    std::cin >> a >> b;
    g.addEdge(a, b);
  }

  while (true)
  {
    int a, b;
    std::cin >> a >> b;

    if (!a && !b)
    {
      break;
    }
    else
    {
      std::cout << g.search(a, b) << '\n';
    }
  }
  return 0;
}