// Summing a Sequence
#include <iostream>

int main()
{
	std::ios::sync_with_stdio(0);
	std::cin.tie(0);
	int N;
	std::cin >> N;
	long long dp[N];

	int f;
	std::cin >> f;
	dp[0] = (f < 0 ? 0 : f);

	if (N >= 2)
	{
		int s;
		std::cin >> s;
		dp[1] = (s < 0 ? 0 : s);
	}

	if (N >= 3)
	{
		int t;
		std::cin >> t;
		dp[2] = dp[0] + (t < 0 ? 0 : t);
	}

	for (int i = 3; i < N; i++)
	{
		int c;
		std::cin >> c;

		dp[i] = std::max(dp[i - 2], dp[i - 3]) + (c < 0 ? 0 : c);
	}

	if (N == 1)
	{
		std::cout << dp[0];
	}
	else
	{
		std::cout << std::max(dp[N - 1], dp[N - 2]);
	}
	return 0;
}

