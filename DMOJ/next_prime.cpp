// Next Prime
// https://dmoj.ca/problem/bf3
#include <bits/stdc++.h>
using namespace std;

bool is_prime(int n) {
  for (int i = 2; i * i <= n; i++) {
    if (!(n % i))
      return false;
  }
  return true;
}

int main() {
  int n;
  cin >> n;
  if (n == 1 || n == 2) {
    cout << 2;
    return 0;
  }
  if (!(n & 1))
    n++;
  while (true) {
    if (is_prime(n)) {
      cout << n;
      break;
    } else
      n += 2;
  }
  return 0;
}
