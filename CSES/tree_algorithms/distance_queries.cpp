// Distance Queries
// https://cses.fi/problemset/task/1135
#include <bits/stdc++.h>
using namespace std;
int ancestor[200001][21];
vector<int> adj[200001];
int depth[200001];
void dfs(int u, int p) {
    for (int v : adj[u]) {
        if (v != p) {
            ancestor[v][0] = u;
            depth[v] = depth[u] + 1;
            dfs(v, u);
        }
    }
}
int raise(int a, int d) {
    int z = 0;
    while (d > 0) {
        if (d & 1) a = ancestor[a][z];
        z++;
        d >>= 1;
    }
    return a;
}
int lca(int a, int b) {
    if (depth[a] > depth[b]) {
        swap(a, b);
    }
    b = raise(b, depth[b] - depth[a]);
    if (a == b) return a;
    for (int i = 20; i >= 0; i--) {
        if (ancestor[a][i] != ancestor[b][i]) {
            a = ancestor[a][i];
            b = ancestor[b][i];
        }
    }
    return ancestor[a][0];
}
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, q;
    cin >> n >> q;
    for (int i = 0; i < n-1;i++) {
        int a, b;
        cin >> a >> b;
        adj[a].push_back(b);
        adj[b].push_back(a);
    }
    dfs(1, 0);
    for (int i = 1; i <= 20; i++) {
        for (int j = 1; j <= n; j++) {
            ancestor[j][i] = ancestor[ancestor[j][i-1]][i-1];
        }
    }
    while(q--) {
        int a, b;
        cin >> a >> b;
        cout << depth[a] + depth[b] - 2 * depth[lca(a, b)] << '\n';
    }
    return 0;
}
