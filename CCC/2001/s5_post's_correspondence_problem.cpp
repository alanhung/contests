// CCC '01 S5 - Post's Correspondence Problem
// https://dmoj.ca/problem/ccc01s5
#include <bits/stdc++.h>
using namespace std;
/* pair<int, int> f(string const &s, vector<string> &arr) { */
/*     vector<string>::iterator l = arr.begin(); */
/*     vector<string>::iterator r = arr.end(); */
/*     for (int i = 0; i < s.size(); i++) { */
/*         vector<string>::iterator nl = lower_bound(l, r, s[i], [&](string const &e, char val) { return i < e.size() && e[i] < val; }); */
/*         vector<string>::iterator nr = upper_bound(l, r, s[i], [&](char val, string const &e) { return i < e.size() && val < e[i]; }); */
/*         l = nl; */
/*         r = nr; */
/*     } */
/*     return {l - arr.begin(), r - arr.begin()}; */
/* } */
bool match(string a, string b, string &ra, string &rb) {
    int i = 0;
    int n =  min(a.size(), b.size());
    for (;i < n; i++) {
        if (a[i] != b[i])
            break;
    }
    if (i == n) {

        if (a.size() > b.size())
            ra = a.substr(i);
        else if (b.size() > a.size())
            rb = b.substr(i);

        return a.size() == b.size();
    }
    return false;
}


/* bool ma(string const &sa, string const &sb, string const &la, string const &lb, string &ra, string &rb) { */
/*     if (sa.empty()) { */
/*         if (la.size() <= sb.size()) { */
/*             rb = sb.substr(la.size() - sb.size()) + lb; */
/*             return false; */
/*         } */
/*         int i = 0; */
/*         int n = min(lb.size(), la.size() - sb.size()); */
/*         for (int i = 0; i < n; i++) { */
/*             if (lb[i] != la[sb.size() + i]) */
/*                 break; */
/*         } */
/*         if (i == n) { */
/*             if (lb.size() + sb.size() < la.size()) */
/*                 ra = la.substr(lb.size() + sb.size()); */
/*             else if (lb.size() + sb.size() > la.size()) */
/*                 rb = lb.substr(la.size() - sb.size()); */
/*             return lb.size() + sb.size() == la.size(); */
/*         } */
/*     } else { */
/*         if (lb.size() <= sa.size()) { */
/*             ra = sa.substr(lb.size() - sa.size()) + la; */
/*             return false; */
/*         } */
/*         int i = 0; */
/*         int n = min(la.size(), lb.size() - sa.size()); */
/*         for (int i = 0; i < n; i++) { */
/*             if (la[i] != lb[sa.size() + i]) */
/*                 break; */
/*         } */
/*         if (i == n) { */
/*             if (la.size() + sa.size() < lb.size()) */
/*                 rb = lb.substr(la.size() + sa.size()); */
/*             else if (la.size() + sa.size() > lb.size()) */
/*                 ra = la.substr(lb.size() - sa.size()); */
/*             return la.size() + sa.size() == lb.size(); */
/*         } */
/*     } */
/*     return false; */
/* } */

bool solve(string sa, string sb, vector<string> &a, vector<string> &b, vector<int> &res, int m) {
    if (res.size() >= m)
        return false;
    /* int l, r; */
    /* if (sa.empty()) */
    /*     tie(l, r) = f(sb, a); */
    /* else */
    /*     tie(l, r) = f(sa, b); */
    /* cout << l << ' ' << r << '\n'; */
    /* return false; */
    /*  */
    /* for (int i = l; i < r; i++) { */
    /*     for (int j = l; j < r; j++) { */
    /*         string ra; */
    /*         string rb; */
    /*         if (ma(sa, sb, a[i], b[i], ra, rb)) { */
    /*             res.push_back(i); */
    /*             return true; */
    /*         } else if (!ra.empty() || !rb.empty()) { */
    /*             res.push_back(i); */
    /*             if (solve(ra, rb, a, b, res, m)) */
    /*                 return true; */
    /*             res.pop_back(); */
    /*         } */
    /*     } */
    /* } */

    int n = a.size();
    for (int i = 0; i < n; i++) {
        string ra;
        string rb;
        if (match(sa + a[i], sb + b[i], ra, rb)) {
            res.push_back(i);
            return true;
        } else if (!ra.empty() || !rb.empty()) {
            res.push_back(i);
            if (solve(ra, rb, a, b, res, m)) {
                return true;
            }
            res.pop_back();
        }
    }
    return false;
}

int main() {
    int m, n;
    cin >> m >> n;
    vector<string> a(n);
    vector<string> b(n);
    for (int i = 0; i < n; i++)
        cin >> a[i];
    for (int i = 0; i < n; i++)
        cin >> b[i];
    vector<int> res;
    if (solve("", "", a, b, res, m)) {
        cout << res.size() << '\n';
        for (int &i : res)
            cout << i + 1 << '\n';
    }
    else
        cout << "No solution.";
    return 0;
}