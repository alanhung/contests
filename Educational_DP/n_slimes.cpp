#include <bits/stdc++.h>
using namespace std;
int main() {
    int n;
    cin >> n;
    unsigned long long pre[n + 1];
    pre[0] = 0;
    for (int i = 1; i <= n; i++) {
        cin >> pre[i];
        pre[i] += pre[i - 1];
    }
    unsigned long long dp[n][n];
    for (int i = 0; i < n; i++)
        dp[i][i] = 0;
    for (int gi = 1; gi < n; gi++) {
        for (int i = 0; i < n - gi; i++) {
            dp[i][i + gi] = numeric_limits<unsigned long long>::max();
            for (int gj = 0; gj < gi; gj++)
                dp[i][i + gi] = min(dp[i][i + gi], dp[i][i + gj] + dp[i + gj + 1][i + gi]);
            dp[i][i + gi] += pre[i + gi + 1] - pre[i];
        }
    }
    cout << dp[0][n-1] << '\n';
    return 0;
}