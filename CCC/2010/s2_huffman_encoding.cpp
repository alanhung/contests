#include <iostream>
#include <map>
#include <string>
using namespace std;
int main(){
    cin.tie(0)->sync_with_stdio(0);
    map<int, char> m;
    int k;
    cin >> k;
    for (int i = 0; i < k; i++)
    {
        char c;
        string s;
        cin >> c >> s;
        int cnt = 0;
        for (int j = 0; j < s.length(); j++)
        {
            if(s[j] == '0')
                cnt = 2 * cnt + 1;
            else 
                cnt = 2 * cnt + 2;
        }
        m.insert({cnt, c});
    }
    string t;
    cin >> t;
    int cnt = 0;
    for (int i = 0; i < t.length(); i++) {
        if (t[i] == '0')
            cnt = 2 * cnt + 1;
        else 
            cnt = 2 * cnt + 2;
        
        if (m.find(cnt) != m.end()) {
            cout << m[cnt];
            cnt = 0;
        }
    }
    return 0;
}