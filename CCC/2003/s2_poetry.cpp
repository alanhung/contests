// CCC '03 S2 - Poetry
// https://dmoj.ca/problem/ccc03s2
#include <bits/stdc++.h>
using namespace std;

bool isvowel(char a) {
    return a == 'a' || a == 'e' || a == 'i' || a == 'o' || a == 'u';
}

string syllable(string line) {
    stringstream ss(line);
    string l;
    while(ss >> l);

    string res;
    int i = l.size() - 1;
    while (i >= 0 && !isvowel(tolower(l[i]))) {
        res.push_back(tolower(l[i]));
        i--;
    }
    if (i >= 0)
        res.push_back(tolower(l[i]));
    reverse(res.begin(), res.end());
    return res;
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    int t;
    cin >> t;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');
    while(t--) {
        string a, b, c, d;
        getline(cin, a);
        getline(cin, b);
        getline(cin, c);
        getline(cin, d);
        string sa = syllable(a);
        string sb = syllable(b);
        string sc = syllable(c);
        string sd = syllable(d);
        if (sa == sb && sa == sc && sa == sd)
            cout << "perfect\n";
        else if (sa == sb && sc == sd)
            cout << "even\n";
        else if (sa == sc && sb == sd)
            cout << "cross\n";
        else if (sa == sd && sb == sc)
            cout << "shell\n";
        else
            cout << "free\n";
    }
    return 0;
}