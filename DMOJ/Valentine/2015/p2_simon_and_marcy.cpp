// Simon and Marcy
#include <iostream>

int main()
{
	std::ios::sync_with_stdio(0);
	std::cin.tie(0);
	int C, N;
	std::cin >> C >> N;

	int dp[N + 1];
	std::fill(dp, dp + N + 1, 0);
	int val[C];
	int wt[C];

	for (int i = 0; i < C; i++)
	{
		std::cin >> val[i] >> wt[i];
	}

	for (int i = 0; i < C; i++)
	{
		for (int j = N; j >= wt[i]; j--)
		{
			dp[j] = std::max(dp[j], dp[j - wt[i]] + val[i]);
		}
	}
	
	std::cout << dp[N];

	return 0;
}

