// Download Speed
// https://cses.fi/problemset/task/1694
#include <bits/stdc++.h>
using namespace std;
long long bfs(int s, int t, vector<int> &parent, vector<vector<int>> const &adj, vector<vector<long long>> const &capacity) {
    fill(parent.begin(), parent.end(), -1);
    parent[s]=-2;
    queue<pair<int,long long>> q;
    q.push({s, LONG_LONG_MAX});
    while(!q.empty()) {
        int u = q.front().first;
        long long flow = q.front().second;
        q.pop();

        for (int v : adj[u]) {
            if (parent[v] == -1 && capacity[u][v]) {
                parent[v] = u;
                long long new_flow = min(flow, capacity[u][v]);
                if (v == t)
                    return new_flow;
                q.push({v, new_flow});
            }
        }
    }
    return 0;
}
long long maxflow(int s, int t, vector<vector<int>> const &adj, vector<vector<long long>> &capacity, int n) {
    long long flow = 0;
    vector<int> parent(n);
    long long new_flow;
    while ((new_flow = bfs(s, t, parent, adj, capacity))) {
        flow += new_flow;
        int cur = t;
        while(cur != s) {
            int prev = parent[cur];
            capacity[prev][cur] -= new_flow;
            capacity[cur][prev] += new_flow;
            cur = prev;
        }
    }
    return flow;
}
int main() {
    int n, m;
    cin >> n >> m;
    vector<vector<int>> adj(n);
    vector<vector<long long>> capacity(n, vector<long long>(n));
    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            capacity[i][j]=0;
    for (int i = 0; i < m; i++) {
        int a, b;
        long long c;
        cin >> a >> b >> c;
        a--;
        b--;
        if (!capacity[a][b]) {
            adj[a].push_back(b);
            adj[b].push_back(a);
        }
        capacity[a][b] += c;
    }
    cout << maxflow(0, n-1, adj, capacity, n);
    return 0;
}
