// Coin Combinations 2
// https://cses.fi/problemset/task/1636
#include <bits/stdc++.h>
using namespace std;
#define MOD 1000000007
int main() {
    int n, x;
    cin >> n >> x;
    int w[n];
    for (int i = 0; i < n; i++) 
        cin >> w[i];
    int dp[x + 1];
    fill(dp, dp + x + 1, 0);
    dp[0]=1;
    for (int i = 0; i < n; i++) {
        for (int j = w[i]; j <= x; j++) {
            dp[j] += dp[j - w[i]];
            dp[j] %= MOD;
        }
    }
    cout << dp[x];
    return 0;
}
