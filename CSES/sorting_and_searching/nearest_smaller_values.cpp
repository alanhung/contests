// Nearest Smaller Values
// https://cses.fi/problemset/task/1645/
#include <bits/stdc++.h> 
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n;
    cin >> n;
    stack<pair<int,int>> stk;
    for (int i = 1; i  <= n; i++) {
        int x;
        cin >> x;
        while(!stk.empty() && stk.top().first >= x)
            stk.pop();
        if (stk.empty())
            cout << 0;
        else
            cout << stk.top().second;
        stk.push({x, i});
        cout << ' ';
    }
    return 0;
}
