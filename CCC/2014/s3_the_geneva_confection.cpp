#include <iostream>
#include <stack>

int main()
{
  int number_of_tests;

  std::cin >> number_of_tests;

  for (int i = 0; i < number_of_tests; i++)
  {
    std::stack<int> mountain;
    std::stack<int> branch;
    branch.push(0);

    int number_of_cars;
    std::cin >> number_of_cars;

    for (int j = 0; j < number_of_cars; j++)
    {
      int current_car;
      std::cin >> current_car;
      mountain.push(current_car);
    }

    int count = 1;

    while (true)
    {
      if (!mountain.empty())
      {
        if (mountain.top() == count)
        {
          mountain.pop();
          count++;
        }
        else if (branch.top() == count)
        {
          branch.pop();
          count++;
        }
        else
        {
          branch.push(mountain.top());
          mountain.pop();
        }
      }
      else
      {
        if (branch.top() == 0)
        {
          std::cout << "Y" << std::endl;
          break;
        }
        else if (branch.top() == count)
        {
          branch.pop();
          count++;
        }
        else
        {
          std::cout << "N" << std::endl;
          break;
        }
      }
    }
  }
}