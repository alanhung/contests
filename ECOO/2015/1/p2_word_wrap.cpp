#include <iostream>
#include <sstream>
#include <string>

using namespace std;

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  for (int t = 0; t < 10; t++) {
    int W;
    string line;
    cin >> W;
    cin.ignore();
    getline(cin, line);

    stringstream ss(line);
    string token;

    int w = W;

    while (ss >> token) {
      if (token.size() > W) {
        int size = token.size();
        int offset = 0;
        if (w != W) {
          cout << '\n';
        }
        while (size > W) {
          cout << token.substr(offset, W) << '\n';
          size -= W;
          offset += W;
        }
        cout << token.substr(offset);
        w = W - (token.size() - offset);

        if (size == W) {
          cout << '\n';
          w = W;
        }
      } else if (token.size() < w) {
        if (w == W) {
          cout << token;
          w -= token.size();
        } else {
          cout << ' ' << token;
          w = w - token.size() - 1;
        }
      } else if (token.size() == w) {
        if (w == W) {
          cout << token << '\n';
        } else {
          cout << '\n' << token;
          w = W - token.size();
        }
      } else if (token.size() > w) {
        cout << '\n' << token;
        w = W - token.size();
      }
    }
    if (w != W) {
      cout << '\n';
    }
    cout << "=====" << '\n';
  }
  return 0;
}
