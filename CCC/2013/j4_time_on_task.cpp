import java.util.Scanner;
import java.util.Arrays;

public class Main {

  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int t = sc.nextInt();
    int c = sc.nextInt();
    int[] chores = new int[c];
    int result = 0;
    for (int i = 0; i < c; i++) {
      chores[i] = sc.nextInt();
    }
    Arrays.sort(chores);
    for (int i = 0; i < chores.length; i++) {
      if (t - chores[i] >= 0) {
        result++;
        t -= chores[i];
      } else {
        break;
      }
    }
    System.out.println(result);
  }
}