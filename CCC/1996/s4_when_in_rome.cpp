// CCC '96 S4 - When in Rome
// https://dmoj.ca/problem/ccc96s4
#include <bits/stdc++.h>
using namespace std;
int parse(int &i, string const &s, char term, int const val[]) {
    int res = 0;
    while(s[i] != term) {
        int a = val[(int)s[i]];
        int b = val[(int)s[i + 1]];
        /* cout << s[i] << ' ' << s[i + 1] << '\n'; */
        /* cout << a << ' ' << b << '\n'; */
        if (!b) {
            res += a;
            i++;
            break;
        }
        else if (b <= a) {
            res += a;
            i++;
        } else {
            res += (b - a);
            i+=2;
        }
    }
    return res;
}
string tostr(int d) {
    if (d > 1000)
        return "CONCORDIA CUM VERITATE";
    string res;
    while (d > 0) {
        if (d >= 1000) {
            d -= 1000;
            res.push_back('M');
        } else if (d >= 900) {
            d -= 900;
            res.push_back('C');
            res.push_back('M');
        } else if (d >= 500) {
            d -= 500;
            res.push_back('D');
        } else if (d >= 400) {
            d -= 400;
            res.push_back('C');
            res.push_back('D');
        } else if (d >= 100) {
            d -= 100;
            res.push_back('C');
        } else if (d >= 90) {
            d -= 90;
            res.push_back('X');
            res.push_back('C');
        } else if (d >= 50) {
            d -= 50;
            res.push_back('L');
        } else if (d >= 40) {
            d-=40;
            res.push_back('X');
            res.push_back('L');
        } else if (d >= 10) {
            d -= 10;
            res.push_back('X');
        } else if (d >= 9) {
            d -= 9;
            res.push_back('I');
            res.push_back('X');
        } else if (d >= 5) {
            d -= 5;
            res.push_back('V');
        } else if (d >= 4) {
            d -= 4;
            res.push_back('I');
            res.push_back('V');
        } else {
            d--;
            res.push_back('I');
        }
    }
    return res;
}
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int val[256];
    fill(val, val + 256, 0);
    val['I']=1;
    val['V']=5;
    val['X']=10;
    val['L']=50;
    val['C']=100;
    val['D']=500;
    val['M']=1000;
    int t;
    cin >> t;
    while(t--) {
        string s;
        cin >> s;
        int i = 0;
        int a = parse(i, s, '+', val);
        i++;
        int b = parse(i, s, '=', val);
        /* cout << a << " " << b << '\n'; */
        cout << s << tostr(a + b) << '\n';
    }
    return 0;
}