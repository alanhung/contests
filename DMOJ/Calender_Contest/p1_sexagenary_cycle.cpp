#include <bits/stdc++.h>
using namespace std;
int main()
{
    cin.tie(0)->sync_with_stdio(0);
    string table[60];
    string stems[] = {"甲", "乙", "丙", "丁", "戊", "己", "庚", "辛", "壬", "癸"};
    string branches[] = {"子", "丑", "寅", "卯", "辰", "巳", "午", "未", "申", "酉", "戌", "亥"};
    int f, s;
    f = s = 0;
    for (int i = 0; i < 60; i++)
    {
        table[i] = stems[f] + branches[s];
        f = (f + 1) % 10;
        s = (s + 1) % 12;
    }
    int N;
    cin >> N;
    for (int i = 0; i < N; i++)
    {
        int c;
        cin >> c;
        cout << table[(((c - 1984) % 60) + 60) % 60] << '\n';
    }
    return 0;
}

