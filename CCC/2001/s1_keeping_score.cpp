// CCC '01 S1 - Keeping Score
// https://dmoj.ca/problem/ccc01s1
#include <bits/stdc++.h>
using namespace std;
int main() {
    string s;
    cin >> s;

    vector<char> arr[4];
    int k = 0;
    for (int i = 0; i < s.size(); i++) {
        int j = i + 1;
        while (j < s.size() && (s[j] != 'C' && s[j] != 'D' && s[j] != 'H' && s[j] != 'S')) {
            arr[k].push_back(s[j]);
            j++;
        }
        i = j - 1;
        k++;
    }

    int t = 0;
    cout << "Cards Dealt " << "Points\n";
    for (int i = 0; i < 4; i++) {
        if (i == 0)
            cout << "Clubs ";
        else if (i == 1)
            cout << "Diamonds ";
        else if (i == 2)
            cout << "Hearts ";
        else 
            cout << "Spades ";
        int l = 0;
        for (int j = 0; j < arr[i].size(); j++) {
            if (arr[i][j] == 'A')
                l += 4;
            else if (arr[i][j] == 'K')
                l+= 3;
            else if (arr[i][j] == 'Q')
                l += 2;
            else if (arr[i][j] == 'J')
                l += 1;
            cout << arr[i][j] << ' ';
        }
        if (arr[i].size() == 0)
            l += 3;
        else if (arr[i].size() == 1)
            l += 2;
        else if (arr[i].size() == 2)
            l += 1;
        t += l;
        cout << l << '\n';
    }
    cout << " Total " << t;
    return 0;
}