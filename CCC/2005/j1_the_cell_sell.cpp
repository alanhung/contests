// CCC '05 J1 - The Cell Sell
// https://dmoj.ca/problem/ccc05j1
#include <bits/stdc++.h>
using namespace std;
int pa(int d, int e, int w) {
    return (d - 100 > 0 ? (d - 100) * 25 : 0) + 15 * e + 20 * w;
}
int pb(int d, int e, int w) {
    return (d - 250 > 0 ? (d - 250) * 45 : 0) + 35 * e + 25 * w;
}
int main() {
    int d, e, w;
    cin >> d >> e >> w;
    int a = pa(d, e, w);
    int b = pb(d, e, w);
    cout << "Plan A costs " << fixed << setprecision(2) << (double)a/100 << '\n';
    cout << "Plan B costs " << fixed << setprecision(2) << (double)b/100 << '\n';
    if (b > a)
        cout << "Plan A is cheapest.\n";
    else if (a > b)
        cout << "Plan B is cheapest.\n";
    else
        cout << "Plan A and B are the same price.\n";
    return 0;
}