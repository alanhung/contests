// Another Contest 7 Problem 1 - Lonely Users
// https://dmoj.ca/problem/acc7p1
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int t;
    cin >> t;
    while(t--) {
        int n;
        cin >> n;
        if (n == 1)
            cout << 0;
        else if (n == 2)
            cout << 2;
        else
            cout << n - 1;
        cout << '\n';
    }
    return 0;
}