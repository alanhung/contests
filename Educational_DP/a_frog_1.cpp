#include <iostream>

using namespace std;
int main() {
    int N;
    cin >> N;
    int height[N + 1];
    for(int i = 1; i <= N; i++)
        cin >> height[i];
    int dp[N + 1];
    dp[1] = 0;
    dp[2] = abs(height[2] - height[1]);
    for (int i = 3; i <= N; i++) {
        dp[i] = min(dp[i-1] + abs(height[i] - height[i-1]), dp[i-2] + abs(height[i] - height[i-2]));
    }
    cout << dp[N] << '\n';
    return 0;
}