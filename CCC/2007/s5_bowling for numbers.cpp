#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int t;
    cin >> t;
    while(t--) {
        int n, k, w;
        cin >> n >> k >> w;
        int val[n + 1];
        val[0] = 0;
        for (int i = 1; i <= n; i++) {
            cin >> val[i];
            val[i] += val[i - 1];
        }
        int dp[k + 1][n + 1];
        fill(*dp, *dp + (k + 1) * (n + 1), 0);
        for (int i = 1; i <= k; i++) {
            for (int j = 1; j <= n; j++) {
                int f = (j - w >= 0) ? val[j] - val[j - w] + dp[i - 1][j - w] : val[j];
                int s = dp[i][j - 1];
                dp[i][j] = max(f, s);
            }
        }
        cout << dp[k][n] << '\n';
    }
    return 0;
}