#include <iostream>
#include <string>

int main()
{
  int number_of_lines;
  std::cin >> number_of_lines;

  for (int i = 0; i < number_of_lines; i++)
  {
    int num;
    char c;
    std::cin >> num >> c;

    std::string result;

    for (int i = 0; i < num; i++) {
      result += c;
    }

    std::cout << result << std::endl;
  }
  return 0;
}