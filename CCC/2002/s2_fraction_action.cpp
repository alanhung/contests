#include <iostream>

int get_gcd(int numerator, int denominator)
{
  int gcd = numerator % denominator;

  while (gcd > 0)
  {
    numerator = denominator;
    denominator = gcd;
    gcd = numerator % denominator;
  }

  return numerator == denominator ? 1 : denominator;
}

int main()
{

  int numerator, denominator;
  std::cin >> numerator >> denominator;

  int num = numerator / denominator;
  int frac = numerator % denominator;

  int gcd = get_gcd(frac, denominator);

  if (num == 0 && frac == 0)
  {
    std::cout << 0 << std::endl;
  }
  else if (num == 0 && frac != 0)
  {
    std::cout << frac / gcd << '/' << denominator / gcd << std::endl;
  }
  else if (num != 0 && frac == 0)
  {
    std::cout << num << std::endl;
  }
  else
  {
    std::cout << num << ' ' << frac / gcd << '/' << denominator / gcd << std::endl;
  }

  return 0;
}