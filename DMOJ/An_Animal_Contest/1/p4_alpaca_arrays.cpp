// An Animal Contest 1 P4 - Alpaca Arrays
// https://dmoj.ca/problem/aac1p4
#include <bits/stdc++.h>
using namespace std;

bool check(int l, int x, int t[]) {
    for (int i = 1; i * i < x; i++) {
        if (!(x % i)) {
            if (t[i] >= l && t[x / i] >= l) {
                return true;
            }
        }
    }
    return false;
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, q;
    cin >> n >> q;
    int arr[n];
    for (int i = 0; i < n; i++) cin >> arr[i];
    vector<pair<tuple<int, int, int>, int>> queries(q);
    vector<bool> res(q);
    for (int i = 0; i < q; i++) {
        int l, r, x;
        cin >> l >> r >> x;
        queries[i] = {{l-1, r-1, x}, i};
    }
    int t[100001];
    fill(t, t + 100001, -1);
    sort(queries.begin(), queries.end(), [](pair<tuple<int,int,int>, int> i, pair<tuple<int,int,int>, int> j ) { return get<1>(i.first) < get<1>(j.first) ;});
    int r = 0;
    for (pair<tuple<int,int,int>, int> &q : queries) {
        while (r <= get<1>(q.first)) {
            t[arr[r]] = r;
            r++;
        }
        res[q.second] = check(get<0>(q.first), get<2>(q.first), t);
    }
    for (bool b : res)
        if (b) cout << "YES\n";
        else cout << "NO\n";
    return 0;
}