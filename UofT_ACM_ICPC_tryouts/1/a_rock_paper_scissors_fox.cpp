import java.util.Scanner;
import java.util.HashMap;

public class Main {

  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    HashMap<String, String> rpsf = new HashMap<String, String>();
    rpsf.put("Rock", "Paper");
    rpsf.put("Paper", "Scissors");
    rpsf.put("Scissors", "Rock");
    rpsf.put("Fox", "Foxen");


    int n = sc.nextInt();

    for (int i = 0; i < n; i++) {
      String e = sc.next();
      if (e.equals("Foxen")) break;
      System.out.println(rpsf.get(e));
      
    }

  }
}