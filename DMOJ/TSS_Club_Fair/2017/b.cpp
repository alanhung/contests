#include <cmath>
#include <iostream>
#include <vector>

using namespace std;

int main()
{
    ios::sync_with_stdio(0);
    cin.tie(0);
    int N, Q;
    cin >> N >> Q;

    int ans[1414215];
    fill(ans, ans + 1414215, 0);

    for (int i = 0; i < N; i++)
    {
        int f, s;
        cin >> f >> s;
        ans[(int)ceil(sqrt(pow(f, 2) + pow(s, 2)))] += 1;
    }

    for (int i = 1; i < 1414215; i++)
    {
        ans[i] += ans[i - 1];
    }

    for (int i = 0; i < Q; i++)
    {
        int f;
        cin >> f;
        cout << ans[f] << '\n';
    }

    return 0;
}
