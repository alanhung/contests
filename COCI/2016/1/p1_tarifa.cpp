#include <bits/stdc++.h>
using namespace std;
int main(){
    int X, N;
    cin >> X >> N;
    int sum = 0;
    while(N--) {
        sum += X;
        int p;
        cin >> p;
        sum -= p;
    }
    cout << sum + X << '\n';
    return 0;
}