// An Animal Contest 5 P1 - Bamboo Cookies
// https://dmoj.ca/problem/aac5p1
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n;
    cin >> n;
    int o = 0;
    int e = 0;
    for (int i = 0; i < n; i++) {
        int x;
        cin >> x;
        if (x & 1) o++;
        else e++;
    }
    cout << o / 2 + e / 2 << '\n';
    return 0;
}