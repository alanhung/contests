// Segment Tree Test

#include <iostream>
#include <vector>

class ST {
public:
  int n;
  std::vector<int> m;
  std::vector<int> g;
  std::vector<int> q;

  ST(const std::vector<int> &a) {
    n = a.size();
    m.assign(2 * n, 0);
    g.assign(2 * n, 0);
    q.assign(2 * n, 0);

    for (int i = a.size() - 1; i >= 0; i--) {
      q[i + n] = 1;
      update(i, a[i]);
    }
  }

  int gcd(int a, int b) {
    if (b == 0)
      return a;
    return gcd(b, a % b);
  }

  void update(int k, int x) {
    k += n;
    g[k] = x;
    m[k] = x;

    for (k /= 2; k >= 1; k /= 2) {
      m[k] = std::min(m[2 * k], m[2 * k + 1]);

      g[k] = gcd(g[2 * k], g[2 * k + 1]);

      q[k] = 0;

      if (g[k] == g[2 * k])
        q[k] += q[2 * k];
      if (g[k] == g[2 * k + 1])
        q[k] += q[2 * k + 1];
    }
  }

  int getM(int l, int r) {
    l += n;
    r += n;

    int s = 1000000001;

    while (l <= r) {
      if (l % 2 == 1)
        s = std::min(s, m[l++]);
      if (r % 2 == 0)
        s = std::min(s, m[r--]);
      l /= 2;
      r /= 2;
    }

    return s;
  }

  int getG(int l, int r) {
    l += n;
    r += n;

    int s = 0;

    while (l <= r) {
      if (l % 2 == 1)
        s = gcd(g[l++], s);
      if (r % 2 == 0)
        s = gcd(g[r--], s);
      l /= 2;
      r /= 2;
    }

    return s;
  }

  int getQ(int l, int r) {
    int gcd = getG(l, r);
    l += n;
    r += n;

    int s = 0;

    while (l <= r) {
      if (l % 2 == 1) {
        if (gcd == g[l])
          s += q[l];
        l++;
      }

      if (r % 2 == 0) {
        if (gcd == g[r])
          s += q[r];
        r--;
      }

      l /= 2;
      r /= 2;
    }

    return s;
  }
};

// void debug(const std::vector<int> &a, const ST tree)
//{

// std::cout << "A: ";
// for (int i = 0; i < a.size(); i++)
//{
// std::cout << a[i] << ' ';
//}
// std::cout << '\n';

// std::cout << "M: ";
// for (int i = 0; i < tree.m.size(); i++)
//{
// std::cout << tree.m[i] << ' ';
//}
// std::cout << '\n';
// std::cout << "G: ";
// for (int i = 0; i < tree.g.size(); i++)
//{
// std::cout << tree.g[i] << ' ';
//}
// std::cout << '\n';
// std::cout << "Q: ";
// for (int i = 0; i < tree.q.size(); i++)
//{
// std::cout << tree.q[i] << ' ';
//}
// std::cout << '\n';
//}

int main() {
  std::ios::sync_with_stdio(0);
  std::cin.tie(0);
  int N, M;
  std::cin >> N >> M;

  std::vector<int> a;

  for (int i = 0; i < N; i++) {
    int c;
    std::cin >> c;
    a.push_back(c);
  }

  ST tree(a);

  for (int i = 0; i < M; i++) {
    char c;
    int l, r;
    std::cin >> c >> l >> r;

    if (c == 'C') {
      tree.update(l - 1, r);
      a[l - 1] = r;
    } else if (c == 'M') {
      std::cout << tree.getM(l - 1, r - 1) << '\n';
    } else if (c == 'G') {
      std::cout << tree.getG(l - 1, r - 1) << '\n';
    } else {
      std::cout << tree.getQ(l - 1, r - 1) << '\n';
    }
  }
  return 0;
}
