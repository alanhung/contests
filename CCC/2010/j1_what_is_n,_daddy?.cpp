import java.util.Scanner;
import java.util.HashMap;

public class Main {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int n = sc.nextInt();
    HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
    map.put(1, 1);
    map.put(2, 2);
    map.put(3, 2);
    map.put(4, 3);
    map.put(5, 3);
    map.put(6, 3);
    map.put(7, 2);
    map.put(8, 2);
    map.put(9, 1);
    map.put(10, 1);
    System.out.println(map.get(n));
  }
}