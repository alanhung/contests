// Counting Divisors
// https://cses.fi/problemset/task/1713

// https://www.geeksforgeeks.org/prime-factorization-using-sieve-olog-n-multiple-queries/ 
#include <bits/stdc++.h>
using namespace std;
void sieve(int spf[], int n) {
    for (int i = 1; i <= n; i++ )
        spf[i] = i;
    for (int p = 2; p * p <= n; p++) {
        if (spf[p] == p) {
            for (int i = p * p; i <= n; i += p) {
                if (spf[i] == i) spf[i] = p;
            }
        }
    }
}
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int spf[1000001];
    sieve(spf, 1000000);
    int n;
    cin >> n;
    while(n--) {
        int x;
        cin >> x;
        int res = 1;
        while(x != 1) {
            int y = spf[x];
            int c = 1;
            while(x%y == 0) {
                c++;
                x/=y;
            }
            res *= c;
        }
        cout << res << '\n';
    }
    return 0;
}
