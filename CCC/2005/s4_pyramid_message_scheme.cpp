// CCC '05 S4 - Pyramid Message Scheme
// https://dmoj.ca/problem/ccc05s4
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int t;
    cin >> t;
    while(t--) {
        int q;
        cin >> q;
        vector<string> message(q);
        for (int i = 0; i < q; i++)
            cin >> message[i];
        set<string> seen;
        seen.insert(message[q-1]);
        int depth = 0;
        int l = 0;
        for (int i = 0; i < q; i++) {
            if (seen.find(message[i]) != seen.end()) {
                l--;
            } else {
                l++;
                depth = max(depth, l);
                seen.insert(message[i]);
            }
        }
        cout << q * 10 - depth * 20 << '\n';
    }
    return 0;
}