#include <bits/stdc++.h>
using namespace std;

void f(int arr[], int l, int r) {
    if (r-l==1)
        return;
    int d = (r - l) / 3;
    f(arr,l, l+d);
    f(arr,l+d,l+2*d);
    f(arr,l+2*d,l+3*d);
    int left[d];
    for (int i = 0; i < d; i++)
        left[i]=arr[l+i];
    int mid[d];
    for (int i = 0; i < d; i++)
        mid[i]=arr[l+d+i];
    int right[d];
    for (int i = 0; i < d; i++)
        right[i]=arr[l+2*d+i];
    for(int i = 0; i < d; i++)
        arr[l+i] = right[i];
    for(int i = 0; i < d; i++)
        arr[l+d+i] = left[i];
    for(int i = 0; i < d; i++)
        arr[l+2*d+i] = mid[i];
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n;
    cin >> n;
    int arr[n];
    for (int i = 0; i < n; i++)
        cin >> arr[i];
    f(arr, 0, n);
    for (int i = 0; i < n; i++)
        cout << arr[i] << ' ';
    cout << '\n';
    return 0;
}

