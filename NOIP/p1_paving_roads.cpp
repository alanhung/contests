// NOIP '18 P1 - Paving Roads
// https://dmoj.ca/problem/noip18p1
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n;
    cin >> n;
    int cnt = 0;
    int l = 0;
    for (int i = 0; i < n; i++) {
        int x;
        cin >> x;
        if (x > l)
            cnt += x - l;
        l = x;
    }
    cout << cnt;
    return 0;
}

