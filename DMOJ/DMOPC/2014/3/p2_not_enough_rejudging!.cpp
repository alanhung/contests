// https://dmoj.ca/problem/dmopc14c3p2
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n;
    cin >> n;
    int wa = 0;
    string arr[n];
    for (int i = 0; i < n; i++) {
        string s;
        cin >> s;
        if (s == "WA")
            wa++;
        arr[i] = s;
    }
    int ir = 20;
    wa = floor(0.3*(double)wa);
    for (int i = 0; i < n; i++) {
        if (arr[i] == "WA") {
            if (wa) {
                cout << "AC";
                wa--;
            }else
                cout << "WA";
        } else if (arr[i] == "IR") {
            if (ir > 10) {
                cout << "AC";
                ir--;
            } else if (ir) {
                cout << "WA";
                ir--;
            }
            else
                cout << "IR";
        }else if (arr[i] == "TLE")
            cout << "WA";
        else
            cout << arr[i];
        cout << '\n';
    }
    return 0;
}