// Number Spiral
// https://cses.fi/problemset/task/1071
#include<bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int t;
    cin >> t;
    while(t--) {
        long long a, b;
        cin >> a >> b;
        if (a >b)
            if (a & 1)
                cout << a * a - (a - 1) - (a - b);
            else
                cout << a * a - (a - 1) + (a - b);
        else 
            if (b & 1)
                cout << b * b - (b - 1) + (b - a);
            else
                cout << b * b - (b - 1) - (b - a);
        cout << '\n';
    }
    return 0;
}
