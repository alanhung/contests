import java.util.Scanner;
import java.util.Arrays;

public class Main {

  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    String[] ch = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};

    int k = sc.nextInt();
    String word = sc.next();
    String result = "";

    for (int i = 0; i < word.length(); i++) {
      int s = 3 * (i + 1) + k;
      String current = String.valueOf(word.charAt(i));
      int currentIndex = Arrays.asList(ch).indexOf(current);
      if (currentIndex - s < 0) {
        result += ch[ch.length - (0 - (currentIndex - s))];
      } else {
        result += ch[currentIndex - s];
      }
    }
    System.out.println(result);

  }
}