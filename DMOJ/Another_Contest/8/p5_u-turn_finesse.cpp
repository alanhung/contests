// Another Contest 8 Problem 5 - U-Turn Finesse
// https://dmoj.ca/problem/acc8p5
#include <bits/stdc++.h>
using namespace std;
long long query(long long tree[], long long i, long long  m) {
    long long res= 0;
    while(i > 0) {
        res = (res + tree[i]) % m;
        i -= i & (-i);
    }
    return res;
}
void update(long long  tree[], long long i, long long x, long long n, long long m) {
    while(i <= n) {
        tree[i]  = (tree[i] + x) % m;
        i += i & (-i);
    }
}
int main() {
    cin.tie(0)->sync_with_stdio(0);
    long long m = 998244353;
    int t;
    cin >> t;
    while(t--) {
        int n;
        cin >> n;
        // val, pos
        long long arr[n];
        for (int i = 0; i < n; i++) cin >> arr[i];

        // store number of increasing subsequences that ends in number
        long long tree[n + 1];
        fill(tree, tree + n + 1, 0);
        long long dpa[n];
        for (int i = 0; i < n; i++) {
            if (arr[i] == 1)
                dpa[i] = 1;
            else
                dpa[i] = (1 + query(tree, arr[i]-1, m))%m;
            update(tree, arr[i], dpa[i], n, m);
        }
        /* cout << "A\n"; */
        /* for (int i = 0; i < n; i++) */
        /*     cout << dpa[i] << ' '; */
        /* cout << '\n'; */
        reverse(arr, arr + n);
        fill(tree, tree + n + 1, 0);
        long long dpb[n];
        for (int i = 0; i < n; i++) {
            if (arr[i] == 1)
                dpb[i] = 1;
            else
                dpb[i] = (1 + query(tree, arr[i]-1, m))%m;
            update(tree, arr[i], dpb[i], n, m);
        }
        /* cout << "B\n"; */
        /* for (int i = 0; i < n; i++) */
        /*     cout << dpb[i] << ' '; */
        /* cout << '\n'; */
        long long res = 0;
        for (int i = 0; i < n; i++)
            res = (res + dpa[i] * dpb[n - i - 1] % m) % m;
        cout << res << '\n';

    }
    return 0;
}