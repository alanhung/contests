// CCC '01 S2 - Spirals
#include <iostream>
#include <cmath>
#include <algorithm>
#include <tuple>
#include <string>

int main()
{
  int x, y;
  std::cin >> x >> y;
  int diff = y - x + 1;
  int side = std::ceil(std::sqrt(diff));

  if (x == 1 && y == 1)
  {
    side = 1;
  }

  int spiral[side][side];
  std::fill(*spiral, *spiral + side * side, -1);

  int xPos;
  int yPos;

  if (side % 2 == 0)
  {
    xPos = side / 2 - 1;
    yPos = side / 2 - 1;
  }
  else
  {
    xPos = side / 2;
    yPos = side / 2;
  }

  int standard = 2;
  int count = 0;
  int direction = 0;

  for (int num = x; num <= y; num++)
  {
    spiral[yPos][xPos] = num;

    count++;

    if (direction == 0)
    {
      yPos++;
    }
    else if (direction == 1)
    {
      xPos++;
    }
    else if (direction == 2)
    {
      yPos--;
    }
    else
    {
      xPos--;
    }

    if (count == standard / 2 || count == standard)
    {
      direction = (direction + 1) % 4;
    }

    if (count == standard)
    {
      standard += 2;
      count = 0;
    }
  }

  //for (int i = 0; i < side; i++)
  //{
  //for (int j = 0; j < side; j++)
  //{
  //std::cout << spiral[i][j] << ' ';
  //}
  //std::cout << '\n';
  //}
  //std::cout << '\n';
  for (int i = 0; i < side; i++)
  {
    for (int j = 0; j < side; j++)
    {
      if (spiral[i][j] != -1)
      {
        std::cout << spiral[i][j] << ' ';
      }
    }
    std::cout << '\n';
  }
}