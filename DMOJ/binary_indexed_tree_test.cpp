// Binary Indexed Tree Test

#include <iostream>
#include <vector>

class BIT {
public:
  std::vector<long long> sums;
  std::vector<int> values;

  BIT(const std::vector<int> &a, const std::vector<int> &b) {
    sums.assign(a.size() + 1, 0);
    values = b;

    for (int i = 1; i <= a.size(); i++) {
      updateSums(i, a[i - 1]);
    }

    for (int i = 1; i < b.size(); i++) {
      if (b[i]) {
        initValues(i, b[i]);
      }
    }
  }

  long long sum(int index) {
    long long sum = 0;

    while (index > 0) {
      sum += sums[index];

      index -= index & (-index);
    }

    return sum;
  }

  long long sum(int l, int r) { return sum(r) - sum(l - 1); }

  int query(int num) {
    int sum = 0;

    while (num > 0) {
      sum += values[num];
      num -= num & (-num);
    }

    return sum;
  }

  void updateSums(int index, int val) {
    while (index < sums.size()) {
      sums[index] += val;
      index += index & (-index);
    }
  }

  void updateValues(int index, int val) {
    while (index < values.size()) {
      values[index] += val;
      index += index & (-index);
    }
  }
  void initValues(int index, int val) {
    index += index & (-index);
    while (index < values.size()) {
      values[index] += val;
      index += index & (-index);
    }
  }
};

int main() {
  std::ios::sync_with_stdio(0);
  std::cin.tie(0);
  int N, M;
  std::cin >> N >> M;

  std::vector<int> a;
  std::vector<int> b(100001, 0);

  for (int i = 0; i < N; i++) {
    int c;
    std::cin >> c;
    a.push_back(c);
    b[c] += 1;
  }

  BIT tree(a, b);

  for (int i = 0; i < M; i++) {
    char c;
    std::cin >> c;

    if (c == 'C') {
      int f, s;
      std::cin >> f >> s;

      tree.updateSums(f, s - a[f - 1]);
      tree.updateValues(a[f - 1], -1);
      tree.updateValues(s, 1);
      a[f - 1] = s;
    } else if (c == 'Q') {
      int f;
      std::cin >> f;
      std::cout << tree.query(f) << '\n';
    } else {
      int f, s;
      std::cin >> f >> s;
      std::cout << tree.sum(f, s) << '\n';
    }
  }
  return 0;
}
