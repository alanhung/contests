// DMOPC '22 Contest 1 P2 - Hat Swap
// https://dmoj.ca/problem/dmopc22c1p2
#include <bits/stdc++.h>
using namespace std;

pair<vector<pair<int,int>>, vector<pair<int,int>>> bfs(int n, int root, vector<int> adj[], int color[]) {
    queue<int> q;
    q.push(root);
    int dist[n];
    fill(dist, dist + n, INT_MAX);
    dist[root] = 0;
    vector<pair<int,int>> fst(n, {INT_MAX, -1});
    vector<pair<int,int>> snd(n, {INT_MAX, -1});
    fst[color[root]].first = 0;
    fst[color[root]].second = root;
    while(!q.empty()) {
        int u = q.front();
        q.pop();
        for (int v : adj[u]) {
            if (dist[v] == INT_MAX) {
                dist[v] = dist[u] + 1;
                if (fst[color[v]].first > dist[v]) {
                    snd[color[v]].first = fst[color[v]].first;
                    snd[color[v]].second = fst[color[v]].second;
                    fst[color[v]].first = dist[v];
                    fst[color[v]].second = v;
                } else if (snd[color[v]].first > dist[v]) {
                    snd[color[v]].first = dist[v];
                    snd[color[v]].second = v;
                }
                q.push(v);
            }
        }
    }
    return {fst, snd};
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, m;
    cin >> n >> m;
    vector<int> adj[n];
    int color[n];
    for (int i = 0; i < n; i++) {
        cin >> color[i];
        color[i]--;
    }
    for (int i = 0; i < m; i++) {
        int f, s;
        cin >> f >> s;
        f--;
        s--;
        adj[f].push_back(s);
        adj[s].push_back(f);
    }

    vector<pair<int,int>> a1, a2, b1, b2;
    tie(a1, a2) = bfs(n, 0, adj, color);
    tie(b1, b2) = bfs(n, n-1, adj, color);
    long long ans = INT_MAX;
    for (int i = 0; i < n; i++) {
        if (a1[i].second != b1[i].second)
            ans = min(ans, (long long) a1[i].first + b1[i].first);
        else {
            ans = min(ans, (long long ) a1[i].first + b2[i].first);
            ans = min(ans, (long long ) a2[i].first + b1[i].first);
            if (a2[i].second != b2[i].second)
                ans = min(ans, (long long) a2[i].first + b2[i].first);
        }
    }
    if (ans == INT_MAX)
        cout << -1 << '\n';
    else
        cout << ans << '\n';
    return 0;
}