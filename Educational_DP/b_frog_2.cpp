// Fog 1
// time out
#include <iostream>
#include <vector>
#include <cmath>
#include <limits>

int search(const int &K, const std::vector<int> &stones)
{
  int dp[stones.size()];
  dp[0] = 0;

  for (int i = 1; i < stones.size(); i++)
  {
    int min = std::numeric_limits<int>::max();

    for (int j = 1; j <= K; j++)
    {
      if (i - j < 0)
      {
        break;
      }
      int current = dp[i - j] + std::abs(stones[i] - stones[i - j]);
      if (current < min)
      {
        min = current;
      }
    }

    dp[i] = min;
  }
  return dp[stones.size() - 1];
}

int main()
{
  std::ios::sync_with_stdio(0);
  std::cin.tie(0);
  int N, K;
  std::cin >> N >> K;
  std::vector<int> stones;
  for (int i = 0; i < N; i++)
  {
    int current;
    std::cin >> current;
    stones.push_back(current);
  }
  std::cout << search(K, stones);

  return 0;
}