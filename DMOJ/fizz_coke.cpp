#include <iostream>
#include <vector>
#include <string>
#include <utility>
#include <algorithm>

int main()
{
	std::ios::sync_with_stdio(0);
	std::cin.tie(0);
	int M, N;
	std::cin >> M >> N;

	std::vector<std::pair<int,std::string>> v;

	for (int i = 0; i < M; i++)
	{
		int c;
		std::string s;
		std::cin >> c >> s;
		v.push_back({c,s});
	}

	std::sort(v.begin(), v.end());

	std::string results[N+1];

	for (int i = 0; i < v.size(); i++)
	{
		for (int j = v[i].first; j <= N; j += v[i].first)
		{
			results[j] += v[i].second;
		}
	}

	for (int i = 1; i <= N; i++)
	{
		if (results[i] != "")
		{
			std::cout << results[i] << '\n';
		}
		else
		{
			std::cout << i << '\n';
		}
	}

	return 0;
}

