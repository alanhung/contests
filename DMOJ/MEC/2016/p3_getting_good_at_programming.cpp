// MEC '16 P3 - Getting Good at Programming
#include <iostream>
#include <vector>

int main()
{
    std::ios::sync_with_stdio(0);
    std::cin.tie(0);
    int N, T;
    std::cin >> N >> T;

    std::vector<std::vector<int>> wt;
    std::vector<std::vector<int>> val;

    for (int i = 0; i < N; i++)
    {
        int level;
        std::cin >> level;

        int lastT = 0;
        int lastX = 0;

		wt.push_back(std::vector<int>{});
		val.push_back(std::vector<int>{});

        for (int j = 0; j < level; j++)
        {
            int t, x;
            std::cin >> t >> x;
            lastT += t;
            lastX += x;
			wt[i].push_back(lastT);
			val[i].push_back(lastX);
			
        }
    }

    int dp[T + 1];
    std::fill(dp, dp + T + 1, 0);

    // for (int i = 0; i < wt.size(); i++)
    //{
    // std::cout << wt[i] << ' ' << val[i] << '\n';
    //}
	//

    for (int i = 0; i < wt.size(); i++)
    {
        for (int j = T; j >= wt[i][0]; j--)
        {

			for (int k = 0; k < wt[i].size(); k++)
			{
				if (wt[i][k] <= j)
				{
					dp[j] = std::max(dp[j], val[i][k] + dp[j - wt[i][k]]);
				}
			}
        }
    }

    std::cout << dp[T];
    return 0;
}


