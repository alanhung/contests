#include <bits/stdc++.h>
using namespace std;
long long dp[4][101];
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n;
    cin >> n;
    int arr[n + 1];
    for (int i = 1; i <= n ;i++)
        cin >> arr[i];
    dp[0][0] = 1;
    long long res = 0;
    for (int i = 1; i <= n; i++) {
        res += dp[3][arr[i]];
        for (int j = 3; j > 0 ; j--) {
            for (int k = 0; k <= 100; k++) {
                if (j >= 1 && k >= arr[i])
                    dp[j][k] += dp[j-1][k - arr[i]];
            }
        }
    }
    cout << res << '\n';
    return 0;
}