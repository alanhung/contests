// Another Contest 7 Problem 3 - Network Connections
// https://dmoj.ca/problem/acc7p3
#include <bits/stdc++.h>
using namespace std;
int find_set(int u, vector<int> &parent) {
    if (u == parent[u])
        return u;
    return parent[u] = find_set(parent[u], parent);
}
void unite_set(int a, int b, vector<int> &parent, vector<int> &size) {
    a = find_set(a, parent);
    b = find_set(b, parent);
    if (a != b) {
        if (size[a] < size[b])
            swap(a, b);
        parent[b] = a;
        size[a] += size[b];
    }
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, m;
    cin >> n >> m;
    vector<int> val(n);
    for (int i = 0; i < n; i++) cin >> val[i];
    vector<tuple<int,int,int>> edges;
    for (int i = 0; i < m; i++) {
        int a, b;
        cin >> a >> b;
        edges.push_back({a-1,b-1, 0});
    }
    for (int i = 0; i < n-1; i++)
        edges.push_back({i, i + 1, val[i + 1] - val[i]});
    vector<int> size(n);
    vector<int> parent(n);
    for (int i = 0; i < n; i++) {
        size[i] = 1;
        parent[i]=i;
    }
    long long cost=0;
    sort(edges.begin(), edges.end(), [](tuple<int,int,int> &i, tuple<int,int,int> &j) { return get<2>(i) < get<2>(j); } );
    for (tuple<int,int,int> &e : edges) {
        int a = find_set(get<0>(e), parent);
        int b = find_set(get<1>(e), parent);
        if (a != b) {
            unite_set(a, b, parent, size);
            cost += get<2>(e);
        }
    }
    cout << cost << '\n';
    return 0;
}