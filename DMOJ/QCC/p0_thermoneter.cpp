#include <bits/stdc++.h>

using namespace std;

int main()
{
	ios::sync_with_stdio(0);
	cin.tie(0);

	int N;
	cin >> N;

	for (int i =  0; i  < N; i++)
	{
		double t;
		cin >> t;

		if (t < 34)
		{
			cout << "Too cold, please try again." << '\n';
		}
		else if (t >= 34 && t <= 35.5)
		{
			cout << "Take a hot bath." << '\n';
		}
		else if (t > 35.5 && t <= 38)
		{
			cout << "Rest if feeling unwell." << '\n';
		}
		else if (t > 38 && t <= 39)
		{
			cout << "Take some medicine." << '\n';
		}
		else if (t > 39 && t <= 41)
		{
			cout << "Take a cold bath and some medicine." << '\n';
		}
		else if (t > 41 && t <= 46.1)
		{
			cout << "Go to the hospital." << '\n';
		}
		else if (t > 46.1 && t <= 50)
		{
			cout << "Congrats, you have a new world record!" << '\n';
		}
		else
		{
			cout << "Too hot, please try again." << '\n';
		}

	}
	return 0;
}

