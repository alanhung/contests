// An Animal Contest 1 P3 - Happy Alpacas
// https://dmoj.ca/problem/aac1p3
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int N, X;
    cin >> N >> X;
    if (N % 2 != X % 2)
        cout << -1 << '\n';
    else {
        int k = 0;
        int i = 1;
        while (k < X) {
            cout << i << " \n"[k==N-1];
            k++;
            i += 2;
        }
        int j = 2;
        while (k < N) {
            cout << j << " \n"[k==N-1];
            j += 2;
            k++;
            if (k >= N)
                break;
            cout << i << " \n"[k==N-1]; 
            i += 2;
            k++;
        }
    }
    return 0;
}