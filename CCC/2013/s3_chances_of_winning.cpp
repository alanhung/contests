// CCC '13 S3 - Chances of Winning
#include <iostream>
#include <vector>
#include <algorithm>
#include <utility>

class Team
{
public:
  int id;
  int points;
  Team(int id, int points)
  {
    this->id = id;
    this->points = points;
  }
};

enum Result
{
  WIN,
  TIE,
  LOSE,
  ALL,
};

std::vector<std::vector<Result>> findAll(int gamesToPlay)
{
  if (gamesToPlay <= 0)
  {
    std::vector<std::vector<Result>> v = {{}};
    return v;
  }
  std::vector<std::vector<Result>> result;

  for (int i = 0; i < ALL; i++)
  {
    if (i == WIN)
    {
      std::vector<std::vector<Result>> nextLevel = findAll(gamesToPlay - 1);
      for (int j = 0; j < nextLevel.size(); j++)
      {
        nextLevel[j].push_back(WIN);
        result.push_back(nextLevel[j]);
      }
    }
    else if (i == LOSE)
    {
      std::vector<std::vector<Result>> nextLevel = findAll(gamesToPlay - 1);
      for (int j = 0; j < nextLevel.size(); j++)
      {
        nextLevel[j].push_back(LOSE);
        result.push_back(nextLevel[j]);
      }
    }
    else
    {
      std::vector<std::vector<Result>> nextLevel = findAll(gamesToPlay - 1);
      for (int j = 0; j < nextLevel.size(); j++)
      {
        nextLevel[j].push_back(TIE);
        result.push_back(nextLevel[j]);
      }
    }
  }
  return result;
}

int main()
{
  int favouriteTeam, numberOfGamesPlayed;
  std::cin >> favouriteTeam >> numberOfGamesPlayed;
  std::vector<Team> teams;
  teams.push_back(Team(1, 0));
  teams.push_back(Team(2, 0));
  teams.push_back(Team(3, 0));
  teams.push_back(Team(4, 0));

  std::vector<std::pair<int, int>> gamesToPlay;
  gamesToPlay.push_back(std::make_pair(1, 2));
  gamesToPlay.push_back(std::make_pair(1, 3));
  gamesToPlay.push_back(std::make_pair(1, 4));
  gamesToPlay.push_back(std::make_pair(2, 3));
  gamesToPlay.push_back(std::make_pair(2, 4));
  gamesToPlay.push_back(std::make_pair(3, 4));

  for (int i = 0; i < numberOfGamesPlayed; i++)
  {
    int teamA, teamB, pointA, pointB;
    std::cin >> teamA >> teamB >> pointA >> pointB;

    for (int j = 0; j < gamesToPlay.size(); j++)
    {
      if ((teamA == gamesToPlay[j].first && teamB == gamesToPlay[j].second) || (teamA == gamesToPlay[j].second && teamB == gamesToPlay[j].first))
      {
        if (pointA > pointB)
        {
          teams[teamA - 1].points += 3;
        }
        else if (pointA < pointB)
        {
          teams[teamB - 1].points += 3;
        }
        else
        {
          teams[teamA - 1].points += 1;
          teams[teamB - 1].points += 1;
        }
        gamesToPlay.erase(gamesToPlay.begin() + j);
      }
    }
  }

  std::vector<std::vector<Result>> result = findAll(gamesToPlay.size());

  int T = 0;

  for (int i = 0; i < result.size(); i++)
  {
    std::vector<int> points;
    points.push_back(teams[0].points);
    points.push_back(teams[1].points);
    points.push_back(teams[2].points);
    points.push_back(teams[3].points);

    for (int j = 0; j < gamesToPlay.size(); j++)
    {
      if (result[i][j] == WIN)
      {
        points[gamesToPlay[j].first - 1] += 3;
      }
      else if (result[i][j] == LOSE)
      {
        points[gamesToPlay[j].second - 1] += 3;
      }
      else
      {
        points[gamesToPlay[j].first - 1] += 1;
        points[gamesToPlay[j].second - 1] += 1;
      }
    }
    int max = *std::max_element(points.begin(), points.end());
    if (points[favouriteTeam - 1] == max && std::count(points.begin(), points.end(), max) <= 1)
    {
      T++;
    }
  }
  std::cout << T;
  return 0;
}