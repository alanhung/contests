#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, m;
    cin >> n >> m;
    vector<int> adj[n];
    int i = 1;
    while(m && i < n) {
        adj[0].push_back(i);
        i++;
        m--;
    }
    bool b = false;
    for (int i = 1; i < n; i++) {
        for (int j = i + 1; j < n; j++) {
            if (!m) {
                b = true;
                break;
            }
            adj[i].push_back(j);
            m--;
        }
        if (b)
            break;
    }
    for (int i = 0; i < n; i++)
        for (int v : adj[i])
            cout << i + 1 << ' ' <<  v + 1 << '\n';
    return 0;
}