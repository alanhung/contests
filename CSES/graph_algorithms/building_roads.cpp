// Building Roads
// https://cses.fi/problemset/task/1666
#include <bits/stdc++.h>
using namespace std;
int main() {
    int n, m;
    cin >> n >> m;
    vector<int> adj[n];
    for (int i = 0; i < m; i++) {
        int a, b;
        cin >> a >> b;
        adj[a-1].push_back(b-1);
        adj[b-1].push_back(a-1);
    }
    bool v[n];
    fill(v, v + n, 0);
    vector<int> res;
    for (int i = 0; i <n ;i++) {
        if (!v[i]) {
            res.push_back(i);
            v[i]=1;
            queue<int> q;
            q.push(i);
            while(!q.empty()) {
                int u = q.front();
                q.pop();
                for (int i = 0; i < adj[u].size(); i++) {
                    if (!v[adj[u][i]]) {
                        v[adj[u][i]]=1;
                        q.push(adj[u][i]);
                    }
                }
            }
        }
    }
    cout << res.size() - 1 << '\n';
    for (int i = 1; i < res.size(); i++) {
        cout << res[0] + 1 << ' ' << res[i] + 1 << '\n';
    }
    return 0;
}
