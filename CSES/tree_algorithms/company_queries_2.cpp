// Company Queries 2
// https://cses.fi/problemset/task/1688
#include <bits/stdc++.h>
using namespace std;
int ancestor[2000001][21];
void dfs(int u, int p, vector<int> adj[], int depth[]) {
    for (int v : adj[u]) {
        if (v != p) {
            depth[v] = depth[u] + 1;
            dfs(v, u, adj, depth);
        }
    }
}

int raise(int a, int d, int depth[]) {
    int z = 0;
    while (d > 0) {
        if (d & 1) a = ancestor[a][z];
        d >>= 1;
        z++;
    }
    return a;
}

int lca(int a, int b, int depth[]) {
    if (depth[a] > depth[b])
        swap(a, b);
    b = raise(b, depth[b] - depth[a], depth);
    if (a == b) return a;
    for (int i = 20; i >= 0; i--) {
        if (ancestor[a][i] != ancestor[b][i]) {
            a = ancestor[a][i];
            b = ancestor[b][i];
        }
    }
    return ancestor[a][0];
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, q;
    cin >> n >> q;
    int depth[n + 1];
    vector<int> adj[n + 1];
    for (int i = 2; i <= n; i++) {
        cin >> ancestor[i][0];
        adj[ancestor[i][0]].push_back(i);
    }
    for (int i = 1; i <= 20; i++) {
        for (int j = 1; j <= n; j++) {
            ancestor[j][i] = ancestor[ancestor[j][i-1]][i-1];
        }
    }
    depth[1]=1;
    dfs(1, 0, adj, depth);
    while(q--) {
        int a, b;
        cin >> a >> b;
        cout << lca(a, b, depth) << '\n';
    }
    return 0;
}
