#include <iostream>
#include <string>
#include <map>

using namespace std;

int main()
{
  string plaintext, ciphertext, matchtext;
  getline(cin, plaintext);
  getline(cin, ciphertext);
  getline(cin, matchtext);

  map<char, char> m;

  for (int i = 0; i < plaintext.size(); i++)
  {
    m.insert({ciphertext[i], plaintext[i]});
  }

  string result;

  for (int i = 0; i < matchtext.size(); i++)
  {
    char current_letter = matchtext[i];
    if (m.find(current_letter) != m.end())
    {
      result += m[current_letter];
    }
    else
    {
      result += '.';
    }
  }

  cout << result << endl;
  return 0;
}