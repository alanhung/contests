// CCC '00 S3 - Surfing
// https://dmoj.ca/problem/ccc00s3
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n;
    cin>>n;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');
    map<string, set<string>> ma;
    for (int i = 0; i < n; i++) {
        string page;
        getline(cin, page);
        if (ma.find(page) != ma.end())
            ma.insert({page, set<string>()});
        while(true) {
            string line;
            getline(cin, line);
            if (line == "</HTML>")
                break;
            for (int i = 0; i < line.size() - 4; i++) {
                if (line[i] == 'H' && line[i + 1] == 'R' && line[i + 2] == 'E' && line[i + 3] == 'F' && line[i + 4] == '=') {
                    string url;
                    int k = i + 6;
                    while(line[k] != '"') {
                        url.push_back(line[k]);
                        k++;
                    }
                    ma[page].insert(url);
                    i = k;
                    cout << "Link from " << page << " to " << url << '\n';
                }
            }
        }
    }
    while(true) {
        string f;
        getline(cin, f);
        if (f == "The End")
            break;
        string s;
        getline(cin, s);
        queue<string> q;
        set<string> v;
        q.push(f);
        v.insert(f);
        bool found = false;
        while(!q.empty()) {
            string u = q.front();
            q.pop();
            for (set<string>::iterator it = ma[u].begin(); it != ma[u].end(); ++it) {
                if (v.find(*it) == v.end()) {
                    if(*it == s) {
                        found = true;
                        break;
                    }
                    v.insert(*it);
                    q.push(*it);
                }
            }
            if (found)
                break;
        }
        if (found) cout << "Can surf from " << f << " to " << s << ".\n";
        else cout << "Can't surf from " << f << " to " << s << ".\n";
    }
    return 0;
}