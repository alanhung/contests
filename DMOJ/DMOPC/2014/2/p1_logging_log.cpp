// Longging Log
// https://dmoj.ca/problem/dmopc14c2p1
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int d;
    cin >> d;
    for (int i = 1; i <= d; i++) {
        int t;
        cin >> t;
        int sum = 0;
        while(t--) {
            int x;
            cin >> x;
            sum += x;
        }
        if (sum) cout << "Day " << i << ": " << sum << '\n';
        else cout << "Weekend\n";
    }
    return 0;
}