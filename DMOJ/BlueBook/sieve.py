#!/usr/bin/env python
n = int(input())
prime = [1 for i in range(n)]
prime[0] = 0
p = 2
while (p**2 <= n):
  if (prime[p - 1] == 1):
    for i in range(p**2, n + 1, p):
      prime[i - 1] = 0
  p += 1

for i in range(n):
  print(prime[i])

