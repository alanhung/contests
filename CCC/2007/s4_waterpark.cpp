// CCC '07 S4 - Waterpark
#include <iostream>
#include <vector>

int main()
{
	std::ios::sync_with_stdio(0);
	std::cin.tie(0);
	int n;
	std::cin >> n;

	std::vector<int> adj[n];

	while (true)
	{
		int f, s;
		std::cin >> f >> s;

		if (!f)
		{
			break;
		}

		adj[s - 1].push_back(f - 1);
	}
	int dp[n];
	std::fill(dp, dp + n, 0);

	dp[0] = 1;

	for (int i = 1; i < n; i++)
	{
		for (std::vector<int>::iterator it = adj[i].begin(); it != adj[i].end(); ++it)
		{
			dp[i] += dp[*it];
		}
	}

	std::cout << dp[n - 1];

	return 0;
}