#include <bits/stdc++.h>
using namespace std;
int main()
{
    cin.tie(0)->sync_with_stdio(0);
    int N, Q;
    cin >> N >> Q;
    int prefix[N];
    fill(prefix, prefix + N, 0);
    cin >> prefix[0];
    for (int i = 1; i < N; i++)
    {
        cin >> prefix[i];
        prefix[i] += prefix[i - 1];
    }
    for (int i = 0; i < Q; i++)
    {
        int f, s;
        cin >> f >> s;
        f--;
        s--;
        if (f > 0)
            cout << prefix[N-1] - (prefix[s] - prefix[f-1]) << '\n';
        else
            cout << prefix[N-1] - prefix[s] << '\n';
    }
    return 0;
}

