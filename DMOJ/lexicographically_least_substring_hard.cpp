// Lexicographically Least Substring (Hard)
// https://dmoj.ca/problem/bf2hard
#include <bits/stdc++.h>
using namespace std;

vector<int> sc(string s) {
    s += "$";
    int n = s.size();
    vector<int> p(n), c(n);
    iota(p.begin(), p.end(), 0);
    sort(p.begin(), p.end(), [&](int i, int j) { return s[i] < s[j];});
    c[p[0]] = 0;
    for (int i = 1; i < n; i++)
        c[p[i]] = c[p[i-1]] + (s[p[i]] != s[p[i - 1]]);
    for (int h = 0; (1 << h) < n; h++) {
        vector<pair<int,int>> temp(n);
        for (int &i : p)
            temp[i] = {c[i], c[(i + (1 << h)) % n]};
        sort(p.begin(), p.end(), [&](int i, int j) { return temp[i] < temp[j]; });
        c[p[0]]=0;
        for (int i = 1; i < n; i++)
            c[p[i]] = c[p[i-1]] + (temp[p[i]] != temp[p[i - 1]]);
    }
    p.erase(p.begin());
    return p;
}

vector<int> sa(string s) {
    s += '`';
    int n = s.size();
    vector<int> p(n), c(n), cnt(max(27, n), 0);
    for (int i = 0; i < n; i++)
        cnt[s[i] - 96]++;
    for (int i = 1; i < 27; i++)
        cnt[i] += cnt[i-1];
    for (int i = 0; i < n; i++)
        p[--cnt[s[i]-96]]=i;
    c[p[0]]=0;
    int classes = 1;
    for (int i = 1; i < n; i++) {
        if (s[p[i]] != s[p[i-1]])
            classes++;
        c[p[i]] = classes - 1;
    }
    vector<int> pn(n), cn(n);
    for (int h = 0; (1 << h) < n; h++) {
        for (int i = 0; i < n; i++) {
            pn[i] = p[i] - (1 << h);
            if (pn[i] < 0)
                pn[i] += n;
        }
        fill(cnt.begin(), cnt.begin() + classes, 0);
        for (int i = 0; i < n; i++)
            cnt[c[pn[i]]]++;
        for (int i = 1; i < classes; i++)
            cnt[i] += cnt[i-1];
        for (int i = n-1; i >= 0; i--)
            p[--cnt[c[pn[i]]]] = pn[i];
        cn[p[0]]=0;
        classes = 1;
        for (int i = 1; i < n; i++) {
            pair<int,int> cur = {c[p[i]], c[(p[i] + (1 << h)) % n]};
            pair<int,int> prev = {c[p[i-1]], c[(p[i-1] + (1 << h)) % n]};
            if (cur != prev)
                classes++;
            cn[p[i]] = classes - 1;
        }
        c.swap(cn);
    }
    p.erase(p.begin());
    return p;
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    string s;
    int k;
    cin >> s >> k;
    vector<int> suffix = sc(s);
    for (int i = 0; i < (int)suffix.size(); i++) {
        if ((int)suffix.size() - suffix[i] >= k) {
            cout << s.substr(suffix[i], min((int)suffix.size() - suffix[i], k)) << '\n';
            return 0;
        }
    }
    return 0;
}
