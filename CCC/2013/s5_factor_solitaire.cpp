// CCC '13 S5 - Factor Solitaire
// https://dmoj.ca/problem/ccc13s5
#include <bits/stdc++.h>
using namespace std;
void sieve(int spf[], int n) {
    for (int i = 0; i <= n; i++)
        spf[i] = i;
    for (int p = 2; p * p <= n; p++) {
        if (spf[p] == p) {
            for (int i = p * p; i <= n; i += p)
                if (spf[i] == i) spf[i] = p;
        }
    }
}
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n;
    cin >> n;
    int spf[n + 1];
    sieve(spf, n);
    long long cnt = 0;
    while(n != 1) {
        if (spf[n] == n) {
            cnt += (n - 1);
            n--;
        } else {
            cnt += (spf[n] - 1);
            n -= (n / spf[n]);
        }
    }
    cout << cnt << '\n';
    return 0;
}