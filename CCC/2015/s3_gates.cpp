// CCC '00 S4 - Golf
#include <iostream>
#include <vector>
#include <algorithm>

int main()
{
  int numberOfGates, numberofPlanes;
  std::cin >> numberOfGates >> numberofPlanes;

  std::vector<int> gates;
  for (int i = 0; i < numberOfGates; ++i)
  {
    gates.push_back(0);
  }

  int maxPlanes = 0;

  for (int i = 0; i < numberofPlanes; ++i)
  {
    int currentPlane;
    std::cin >> currentPlane;

    std::vector<int>::iterator value = std::find(std::next(gates.begin(), numberOfGates - currentPlane), gates.end(), 0);

    if (value != gates.end())
    {
      maxPlanes++;
      *value = -1;
    }
    else
    {
      break;
    }
  }

  std::cout << maxPlanes;
  return 0;
}