// Collecting Numbers
// https://cses.fi/problemset/task/2216
#include <bits/stdc++.h>
using namespace std;
int main() {
    int n;
    cin >> n;
    int a[n];
    for (int i = 0; i < n; i++)
        cin >> a[i];
    int v[n + 1];
    fill(v, v + n + 1, 0);
    int cnt = 0;
    for (int i = 0; i < n; i++) {
        v[a[i]]=1;
        if (!v[a[i]-1])
            cnt++;
    }
    cout << cnt;
    return 0;
}
