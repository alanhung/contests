// CCC '00 S5 - Sheep and Coyotes
// https://dmoj.ca/problem/ccc00s5
#include <bits/stdc++.h>
using namespace std;
int main() {
    int n;
    cin >> n;
    vector<pair<double, double>> sheeps;
    for (int i = 0; i < n; i++) {
        double a, b;
        cin >> a >> b;
        sheeps.push_back({a*1000, b*1000});
    }
    vector<pair<double, double>> res;
    for(int i = 0; i < n; i++) {
        double l = 0;
        double r = 1000000;
        // cout << "SHEEP: " << sheeps[i].first << ' ' << sheeps[i].second << '\n';
        for (int j = 0; j < n; j++) {
            if (sheeps[j].first == sheeps[i].first) {
                if (sheeps[j].second >= sheeps[i].second)
                    continue;
                if (sheeps[j].second < sheeps[i].second) {
                    l = r + 1;
                    break;
                }
            }
            double c = ((sheeps[i].first + sheeps[j].first) + (sheeps[i].second - sheeps[j].second) * (sheeps[i].second + sheeps[j].second) / (sheeps[i].first - sheeps[j].first) ) / 2;
            /* cout << "S: " << sheeps[j].first << ' ' << sheeps[j].second << '\n'; */
            /* cout << l << ' ' << r << '\n'; */
            /* cout << c << '\n'; */
            if (sheeps[j].first > sheeps[i].first)
                r = min(r, c);
            else if (sheeps[j].first < sheeps[i].first)
                l = max(l, c);
        }
        if (l <= r)
            res.push_back(sheeps[i]);
    }
    cout << fixed << setprecision(2);
    for (pair<double,double> &s : res)
        cout << "The sheep at ("  << s.first / 1000 << ", " << s.second / 1000 << ") might be eaten.\n";
    return 0;
}