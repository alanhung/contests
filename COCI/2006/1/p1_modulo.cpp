#include <bits/stdc++.h>
using namespace std;
int main () {
    bool arr[42];
    fill(arr, arr + 42, 0);
    for (int i = 0; i < 10; i++) {
        int x;
        cin >> x;
        arr[x % 42] = 1;
    }
    int sum = 0;
    for (int i = 0; i < 42; i++)
        sum += arr[i];
    cout << sum << '\n';
    return 0;
}