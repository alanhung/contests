// Towers
// https://cses.fi/problemset/task/1073
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n;
    cin >> n;
    multiset<int> m;
    while(n--) {
        int k;
        cin >> k;
        multiset<int>::iterator it = m.upper_bound(k);
        if (it != m.end())
            m.erase(it);
        m.insert(k);
    }
    cout << m.size() << '\n';
    return 0;
}
