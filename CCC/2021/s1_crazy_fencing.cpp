// CCC '21 S1 - Crazy Fencing
#include <iostream>

int main()
{
  int N;
  std::cin >> N;
  int heights[N + 1];
  int widths[N];

  for (int i = 0; i < N + 1; ++i)
  {
    std::cin >> heights[i];
  }
  for (int i = 0; i < N; ++i)
  {
    std::cin >> widths[i];
  }

  long double area = 0;

  for (int i = 0; i < N; i++)
  {
    area += (long double)((heights[i] + heights[i + 1]) * widths[i]) / (long double)2.0;
  }
  std::cout << std::fixed << area << std::endl;

  return 0;
}