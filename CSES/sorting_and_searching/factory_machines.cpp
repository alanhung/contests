// Factory Machines
// https://cses.fi/problemset/task/1620
#include <bits/stdc++.h>
using namespace std;
bool valid(long long x, long long t, long long arr[], int n) {
    long long sum = 0;
    for (int i = 0; i < n; i++)
        sum += x / arr[i];
    return sum >= t;
}
int main() {
    int n; 
    long long t;
    cin >> n >> t;
    long long arr[n];
    long long smallest = INT_MAX;
    for (int i = 0; i < n; i++) {
        cin >> arr[i];
        smallest = min(smallest, arr[i]);
    }
    long long res = 0;
    for (long long b = smallest * t; b >= 1; b /= 2) {
        while(!valid(res + b, t, arr, n)) res += b;
    }
    cout << res + 1;
    return 0;
}
