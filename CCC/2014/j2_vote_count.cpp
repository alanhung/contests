import java.util.Scanner;

public class Main {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int n = sc.nextInt();
    String votes = sc.next();
    int aCount = 0;
    int bCount = 0;

    for (int i = 0; i < n; i++) {
      if(votes.charAt(i) == 'A') {
        aCount++;
      } else {
        bCount++;
      }
    }
    sc.close();
    System.out.println(aCount > bCount ? 'A' : aCount < bCount ? 'B' : "Tie");
  }
}