// IOI '01 P1 - Mobile Phones
// https://dmoj.ca/problem/ioi01p1
#include <bits/stdc++.h>
using namespace std;
int bit[1025][1025];
void update(int s, int x, int y, int val) {
    for (; y <= s; y += (y & - y)) {
        for (int j = x; j <= s; j += (j & -j))
            bit[y][j] += val;
    }
}
int query(int x, int y) {
    int sum = 0;
    for (; y > 0; y -= (y & -y)) {
        for (int j = x; j > 0; j -= (j & -j))
            sum += bit[y][j];
    }
    return sum;
}

int sum(int l, int b, int r, int t) {
    return query(r, t) + query(l - 1, b - 1) - query(r, b - 1) - query(l - 1, t);
}
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int s;
    cin >> s >> s;
    while (1) {
        int l;
        cin >> l;
        if (l == 3)
            break;
        if (l == 1) {
            int x, y, a;
            cin >> x >> y >> a;
            update(s, x + 1, y + 1, a);
        }
        if (l == 2) {
            int l, b, r, t;
            cin >> l >> b >> r >> t;
            cout << sum(l + 1, b + 1, r + 1, t + 1) << '\n';
        }
    }
    return 0;
}