// Binomial Coeficcients
// https://cses.fi/problemset/task/1079
#include <bits/stdc++.h>
using namespace std;
#define MOD 1000000007ll
long long fac[1000001];
long long inv[1000001];
long long modpow(long long base, long long exp, long long m) {
    long long res = 1;
    while (exp > 0) {
        if (exp & 1) res = (res * base) % m;
        base = (base * base) % m;
        exp >>= 1;
    }
    return res;
}
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n;
    cin >> n;
    fac[0] = 1;
    inv[0] = 1;
    for (int i = 1; i <= 1000000; i++) {
        fac[i] = fac[i-1]*i%MOD;
        inv[i] = modpow(fac[i], MOD-2, MOD);
    }
    while(n--) {
        long long a, b;
        cin >> a >> b;
        cout << fac[a] * inv[b] % MOD * inv[a-b]%MOD << '\n';
    }
    return 0;
}
