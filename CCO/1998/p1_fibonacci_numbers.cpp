// CCO '98 P1 - Fibonacci Numbers
#include <algorithm>
#include <iostream>
#include <string>

std::string findSum(std::string a, std::string b) {
  std::string result = "";

  int n1 = a.length();
  int n2 = b.length();
  int diff = n2 - n1;

  int carry = 0;

  for (int i = n1 - 1; i >= 0; i--) {
    int sum = ((a[i] - '0') + (b[i + diff] - '0') + carry);
    result.push_back(sum % 10 + '0');

    carry = sum / 10;
  }

  for (int i = diff - 1; i >= 0; i--) {
    int sum = ((b[i] - '0') + carry);
    result.push_back(sum % 10 + '0');
    carry = sum / 10;
  }

  if (carry) {
    result.push_back(carry + '0');
  }

  std::reverse(result.begin(), result.end());

  return result;
}

int main() {
  std::ios::sync_with_stdio(0);
  std::cin.tie(0);
  std::string fib[201];
  fib[0] = "0";
  fib[1] = "1";
  fib[2] = "1";

  for (int i = 3; i <= 200; i++) {
    fib[i] = findSum(fib[i - 2], fib[i - 1]);
  }

  int n;

  while (true) {
    std::cin >> n;

    if (n == 0) {
      break;
    }

    std::cout << fib[n] << '\n';
  }

  return 0;
}
