// An Animal Contest 4 P3 - Snowy Slopes
// https://dmoj.ca/problem/aac4p3
#include <bits/stdc++.h>
using namespace std;
int sign(int x) {
    return (x > 0) - (x < 0);
}
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, m;
    cin >> n >> m;
    // x, y
    pair<int,int> points[n];
    for (int i = 0; i  < n; i++) {
        int x, y;
        cin >> x >> y;
        points[i] = {x, y};
    }
    // k, d
    set<pair<int,int>> slopes;
    for (int i = 0; i < m; i++) {
        int k, d;
        cin >> k >> d;
        int g = gcd(abs(k), abs(d));
        slopes.insert({abs(k)/g*sign(k)*sign(d), abs(d)/g});
    }
    long long cnt = 0;
    for (set<pair<int,int>>::iterator slope = slopes.begin(); slope != slopes.end(); ++slope) {
        map<long long, long long> mp;
        for (int i = 0; i < n; i++) {
            long long id = (long long)slope->first * points[i].first - (long long)slope->second * points[i].second;
            map<long long,long long>::iterator it = mp.find(id);
            if (it == mp.end())
                mp.insert({id, 1});
            else {
                cnt += it->second;
                it->second++;
            }
        }
    }
    cout << cnt << '\n';
    return 0;
}