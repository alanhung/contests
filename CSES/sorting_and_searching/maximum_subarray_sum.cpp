// Maximum Subarray Sum
// https://cses.fi/problemset/task/1643
#include <bits/stdc++.h>
using namespace std;
int main() {
    int n;
    cin >>n;
    int a[n];
    for (int i = 0; i < n; i++)
        cin >> a[i];
    long long best = numeric_limits<int>::min();
    long long sum = numeric_limits<int>::min();
    for (int i = 0; i < n; i++) {
        sum = max((long long)a[i], sum + a[i]);
        best = max(sum, best);
    }
    cout << best;
    return 0;
}
