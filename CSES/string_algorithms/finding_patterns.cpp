// Finding Patterns
// https://cses.fi/problemset/task/2102
#include <bits/stdc++.h>
using namespace std;
int lower(int l, int r, vector<int> &suffix, string &text, char c, int i) {
    int n = r;
    while (l < r) {
        int mid = l + (r - l) / 2;
        if (suffix[mid] + i < suffix.size() && c <= text[suffix[mid] + i]) {
            r = mid;
        } else {
            l = mid + 1;
        }
    }
    if (l < n && (suffix[l] + i >= suffix.size() || text[suffix[l] + i] < c))
        l++;
    return l;
}
int upper(int l, int r, vector<int> &suffix, string &text, char c, int i) {
    int n = r;
    while (l < r) {
        int mid = l + (r - l) / 2;
        if (suffix[mid] + i >= suffix.size() || c >= text[suffix[mid] + i]) {
            l = mid + 1;
        } else {
            r = mid;
        }
    }
    if (l < n && ( suffix[l] + i >= suffix.size() || text[suffix[l] + i] <= c))
        l++;
    return l;
}

// bad implementation
// passed but doesn't work generally
vector<int> doubling(string &s) {
    int n = s.size();
    vector<pair<pair<int, int>, int>> il(n);
    vector<int> fl(n);
    for (int i = 0; i < n; i++) {
        fl[i] = s[i] - 97;
    }
    for (int i = 1; (1 << i ) <= n; i++) {
        for (int j = 0; j < n; j++) {
            il[j] = {{fl[j], j + (1 << (i - 1) ) < n ? fl[j + (1 << (i - 1))] : -1 }, j};
        }
        sort(il.begin(), il.end());
        fl[il[0].second] = 0;
        int cnt = 1;
        for (int j = 1; j < n; j ++) {
            if (il[j].first != il[j - 1].first)
                cnt++;
            fl[il[j].second] = cnt - 1;
        }

    }
    vector<int> res(n);
    for (int i = 0; i < n; i++)
        res[ fl[i] ] = i;
    return res;
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    string text;
    cin >> text;
    vector<int> suffix = doubling(text);
    for (int i = 0; i < suffix.size(); i++)
        cout << suffix[i] << ' ';
    cout << '\n';
    int n;
    cin >> n;
    while (n--) {
        string s;
        cin >> s;
        int l = 0;
        int r = text.size();
        bool bad = false;
        for (int i = 0; i < s.size(); i++) {
            int nl = lower(l, r, suffix, text, s[i], i);
            int nr = upper(l, r, suffix, text, s[i], i);
            cout << nl << ' ' << nr << '\n';
            bad = nl == nr;
            if (bad)
                break;
            l = nl;
            r = nr;
        }
        if (bad) cout << "NO\n";
        else cout << "YES\n";
    }
    return 0;
}
