// Foxhole
#include <iostream>

using namespace std;

int main()
{
  ios::sync_with_stdio(0);
  cin.tie(0);
  int T;
  cin >> T;

  for (int t = 0; t < T; t++)
  {
    int H, W, N;
    cin >> H >> W >> N;
    char grid[H + 1][W];

    int x = 0;
    int y = 0;

    int treasure = 0;

    for (int i = 0; i < W; i++)
    {
      grid[0][i] = 'E';
    }
    for (int i = 1; i <= H; i++)
    {
      for (int j = 0; j < W; j++)
      {
        char c;
        cin >> grid[i][j];
      }
    }

    for (int i = 0; i < N; i++)
    {
      char a;
      cin >> a;
      if (a == 'R')
      {
        if (x + 1 < W && grid[y][x + 1] != 'S')
        {
          x++;
        }
      }
      else if (a == 'L')
      {
        if (x - 1 >= 0 && grid[y][x - 1] != 'S')
        {
          x--;
        }
      }
      else
      {
        if (y + 1 <= H && grid[y + 1][x] != 'S')
        {
          y++;
        }
      }

      if (grid[y][x] == 'T')
      {
        treasure++;
        grid[y][x] = 'E';
      }
      else if (grid[y][x] == 'D')
      {
        grid[y][x] = 'E';
      }

      while (true)
      {
        if (y + 1 <= H)
        {
          if (grid[y + 1][x] == 'E')
          {
            y++;
          }
          else
          {
            break;
          }
        }
        else
        {
          break;
        }
      }
    }

    cout << treasure << '\n';
  }
  return 0;
}