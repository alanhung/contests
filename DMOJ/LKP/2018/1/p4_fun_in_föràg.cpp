// LKP '18 Contest 1 P4 - Fun in Föràg
// https://dmoj.ca/problem/lkp18c1p4
#include <bits/stdc++.h>
using namespace std;
int n, m, A, B, C;
// level minute node
vector<tuple<int, int, int>> adj[100000];
long long dist[100000];
bool visited[100000];
bool check(int x) {
    fill(dist, dist + n, LONG_LONG_MAX);
    fill(visited, visited + n, 0);
    priority_queue<pair<long long, int>> q;
    dist[A]=0;
    q.push({0, A});
    while(!q.empty()) {
        long long d;
        int u;
        tie(d, u) = q.top();
        d = -d;
        q.pop();
        if (visited[u])
            continue;
        visited[u]=1;
        if (u == B)
            break;
        for (const tuple<int,int,int> &node : adj[u]) {
            if (get<0>(node) <= x) {
                long long nd = d + get<1>(node);
                if (nd < dist[get<2>(node)]) {
                    dist[get<2>(node)] = nd;
                    q.push({-nd, get<2>(node)});
                }
            }
        }
    }
    return dist[B] < C;
}
int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin >> n >> m;
    // level minute node
    for (int i = 0; i < m; i++) {
        int a, b, c;
        cin >> a >> b >> c;
        a--;
        b--;
        adj[a].push_back({i + 1, c,  b});
        adj[b].push_back({i + 1, c, a});
    }
    cin >> A >> B >> C;
    A--;
    B--;
    bool top = check(m);
    if (top) {
        int k = 0;
        for (int b = m / 2; b >= 1; b /= 2)
            while(!check(k+b)) k += b;
        cout << k + 1 << '\n';
    } else {
        cout << -1 << '\n';
    }
    return 0;
}
