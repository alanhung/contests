// DWITE '09 R7 #2 - Sum of Primes
#include <iostream>

int main()
{
	std::ios::sync_with_stdio(0);
	std::cin.tie(0);

	int T = 100000;

	bool prime[T + 1];
	int sum[T + 1];

	for (int i = 0; i <= T; i++)
	{
		prime[i] = true;
		sum[i] = 0;
	}
	

	for (int i = 2; i * i <= T; i++)
	{
		if (prime[i])
		{
			for (int j = i * i; j <= T; j += i)
			{
				prime[j] = false;
			}
		}
	}

	for (int i = 2; i <= T; i++)
	{
		sum[i] += sum[i - 1];
		if (prime[i])
		{
			sum[i] += i;
		}

	}
	for (int i = 0; i < 5; i++)
	{
		int c;
		std::cin >> c;
		std::cout << sum[c] << '\n';
	}

	return 0;
}