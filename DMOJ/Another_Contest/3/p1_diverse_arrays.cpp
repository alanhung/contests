// Diverse Array
// https://dmoj.ca/problem/acc3p1
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    long long n; 
    int k;
    cin >> n >> k;
    int arr[n];
    for (int i = 0; i < n; i++) {
        cin >> arr[i];
        arr[i]--;
    }
    if (k == 1) {
        cout << n * (n + 1) / 2 << '\n';
        return 0;
    }
    int freq[n];
    fill(freq, freq + n, 0);
    int cnt = 1;
    long long res = 1;
    freq[arr[0]]=1;
    int j = 0;
    for (int i = 1; i < n; i++) {
        if (!freq[arr[i]])
            cnt++;
        freq[arr[i]]+=1;
        if (cnt >= k) {
            while(cnt >= k) {
                freq[arr[j]]--;
                if (!freq[arr[j]])
                    cnt--;
                j++;
            }
        }
        res += (i - j + 1);
    }
    cout << n * (n + 1) / 2 - res << '\n';
    return 0;
}