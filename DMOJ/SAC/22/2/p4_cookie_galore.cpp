#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int N, M;
    cin >> N >> M;
    char grid[N][M];
    for (int i = 0; i < N; i++)
        for (int j = 0; j < M; j++)
            cin >> grid[i][j];
    // node, weight
    vector<pair<int,int>> adj[N * M];
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < M; j++) {
            // top
            if (i > 0)
                adj[i * M + j].push_back({(i - 1) * M + j, grid[i-1][j] == 'C' });
            // right
            if (j < M - 1)
                adj[i * M + j].push_back({i * M + j + 1, grid[i][j+1] == 'C' });
            // down
            if (i < N - 1)
                adj[i * M + j].push_back({(i + 1) * M + j, grid[i+1][j] == 'C' });
            // left
            if (j >  0)
                adj[i * M + j].push_back({i * M + j - 1, grid[i][j-1] == 'C' });
        }
    }
    int dist[N * M];
    bool visited[N * M];
    fill(dist, dist + N * M, INT_MAX);
    fill(visited, visited + N * M, 0);
    // weight, node
    deque<int> q;
    q.push_front(0);
    dist[0] = grid[0][0] == 'C';
    while(!q.empty()) {
        int node = q.front();
        q.pop_front();
        if (visited[node])
            continue;
        visited[node]=1;
        for (pair<int,int> child : adj[node]) {
            if (dist[node] + child.second < dist[child.first]) {
                dist[child.first] = dist[node] + child.second;
                if (child.second)
                    q.push_back(child.first);
                else
                    q.push_front(child.first);
            }

        }
    }
    /* for (int i = 0; i < N; i++){ */
    /*     for (int j = 0; j < M; j++) */
    /*         cout << dist[i * M + j] << ' '; */
    /*     cout << '\n'; */
    /* } */
    cout << dist[N * M - 1] << '\n';
    return 0;
}