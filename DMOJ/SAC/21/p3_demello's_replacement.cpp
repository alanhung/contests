#include <bits/stdc++.h>

using namespace std;

long f(long M, long C, long E, long P)
{
    return floor(4 * sqrt(M) + 3 * pow(C, P) - 4 * E);
}

int main()
{
    ios::sync_with_stdio(0);
    cin.tie(0);
    int N, P;
    cin >> N >> P;

    auto cmp = [](pair<string, long> a, pair<string, long> b) {return a.second < b.second;};

    set<pair<string, long>, decltype(cmp)> s(cmp);

    for (int i = 0; i < N; i++)
    {
        string name;
        long M;
        long C;
        long E;
        cin >> name >> M >> C >> E;
        s.insert(make_pair(name, f(M, C, E, P)));
    }
    cout << s.rbegin()->first << " " << s.rbegin()->second << '\n';
    cout << s.begin()->first << " " << s.begin()->second;
    return 0;
}