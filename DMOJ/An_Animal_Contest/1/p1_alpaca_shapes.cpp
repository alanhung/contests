#include <bits/stdc++.h>
using namespace std;
int main() {
    int s, r;
    cin >> s >> r;
    cout << ((pow(s, 2) > 3.14 * pow(r, 2)) ? "SQUARE" : "CIRCLE") << '\n';
    return 0;
}