// Subordinates
// https://cses.fi/problemset/task/1674
#include <bits/stdc++.h>
using namespace std;
void dfs(int u, int p, vector<int> adj[], int dist[]) {
    dist[u]=0;
    for (int v : adj[u]) {
        if (v != p) {
            dfs(v, u, adj, dist);
            dist[u]+=1;
            dist[u] += dist[v];
        }
    }
}
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n;
    cin >> n;
    vector<int> adj[n];
    for (int i = 1; i < n; i++) {
        int x;
        cin >> x;
        adj[x-1].push_back(i);
    }
    int dist[n];
    dfs(0, -1, adj, dist);
    for (int i = 0; i < n; i++) 
        cout << dist[i] << ' ';
    return 0;
}
