// Another Contest 8 Problem 1 - Trash Push
// https://dmoj.ca/problem/acc8p1
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int t;
    cin >> t;
    while(t--) {
        int n, k;
        cin >> n >> k;
        cout << n / k + (n % k != 0) << '\n';
    }
    return 0;
}