// An Animal Contest 4 P2 - Lavish Lights
// https://dmoj.ca/problem/aac4p2
#include <bits/stdc++.h>
using namespace std;
long long lcm(long long a, long long b) {
    return a * b / gcd(a, b);
}
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, q;
    cin >> n >> q;
    int arr[n];
    for (int i=0;i<n;i++) cin >> arr[i];
    vector<long long> prefix;
    prefix.push_back(arr[0]);
    for (int i = 1; i < n; i++) {
        prefix.push_back(lcm(prefix[i-1], arr[i]));
        if (prefix[i] > 1e9)
            break;
    }
    /* cout << "LCM\n"; */
    /* for (int i = 0; i < (int)prefix.size(); i++) cout << prefix[i] << ' '; */
    /* cout << '\n'; */
    while(q--) {
        int t;
        cin >> t;
        vector<long long>::iterator it = upper_bound(prefix.begin(), prefix.end(),t, [](long long val, long long i) { return val % i  ;} );
        /* cout << "Q " << it - prefix.begin() << '\n'; */
        if (it == prefix.end()) cout << - 1;
        else cout << it - prefix.begin() + 1;
        cout << '\n';
    }
    return 0;
}