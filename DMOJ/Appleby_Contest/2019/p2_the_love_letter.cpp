// Appleby Contest '19 P2 - The Love Letter
// https://dmoj.ca/problem/ac19p2
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, l;
    cin >> n >> l;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');
    string s;
    getline(cin, s);
    for (int i = 0; i < s.size(); i++)
        cout << (s[i] == ' ' ? s[i] : (char)(((s[i] - 97) + l) % 26 + 97));
    return 0;
}