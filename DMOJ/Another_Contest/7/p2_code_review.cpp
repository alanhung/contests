// Another Contest 7 Problem 2 - Code Review
// https://dmoj.ca/problem/acc7p2
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n;
    cin >> n;
    int stu[n];
    for (int i = 0; i < n; i++)
        stu[i]=i;
    for (int i = 0; i < n; i++)
        cout << stu[i] + 1 << ' ';
    cout << '\n' << flush;
    while(true) {
        int a;
        cin >> a;
        if (a == 0 || a == -1)
            break;
        a--;
        swap(stu[(a + 1)%n], stu[a]);
        for (int i = 0; i < n; i++) cout << stu[i] + 1 << ' ';
        cout << '\n' << flush;
    }
    return 0;
}