// New Year's '17 P1 - Mr. N and Presents
// https://dmoj.ca/problem/year2017p1
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int q;
    cin >> q;
    map<int,int> mp;
    deque<pair<int,int>> stk;
    for (int i = 0; i < q; i++) {
        char c;
        int x;
        cin >> c >> x;
        if (c == 'F') {
            stk.push_front({x, i});
            mp.insert_or_assign(x, i);
        }
        else if (c == 'E') {
            stk.push_back({x, i});
            mp.insert_or_assign(x, i);
        }
        else
            mp.insert_or_assign(x, -1);
    }
    for (int i = 0; i < (int)stk.size(); i++) {
        map<int,int>::iterator it = mp.find(stk[i].first);
        if (it->second == stk[i].second )
            cout << stk[i].first << '\n';
    }
    return 0;
}

