#include <bits/stdc++.h>
using namespace std;
int main() {
    int N, M, X, Y;;
    cin >> N >> M;
    vector<int> adj[N];
    for (int i = 0; i < M; i++) {
        int x, y;
        cin >> x >> y;
        x--;
        y--;
        adj[x].push_back(y);
        adj[y].push_back(x);
    }
    cin >> X >> Y;
    X--;
    Y--;
    int dist[N];
    bool visited[N];
    fill(visited, visited + N, 0);
    fill(dist, dist + N, 0);
    queue<int> q;
    q.push(X);
    visited[X] = true;
    while(!q.empty()) {
        int u = q.front();
        q.pop();
        for (int v : adj[u]) {
            if (!visited[v]) {
                visited[v] = true;
                dist[v] = dist[u] + 1;
                q.push(v);
            }
            if (visited[Y])
                break;
        }
        if (visited[Y])
            break;
    }
    cout << M - dist[Y] << '\n';
    return 0;
}

