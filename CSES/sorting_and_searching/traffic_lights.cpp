// Traffic Lights
// https://cses.fi/problemset/task/1163
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int x, n;
    cin >> x >> n;
    set<int> s;
    multiset<int> m;
    s.insert(0);
    s.insert(x);
    m.insert(x);
    while(n--) {
        int k;
        cin >> k;
        set<int>::iterator fit = s.insert(k).first;
        set<int>::iterator sit = fit;
        fit--;
        sit++;

        m.erase(m.find(*sit - *fit));
        m.insert(*sit - k);
        m.insert(k - *fit);
        cout << *(--m.end()) << '\n';
    }
    return 0;
}
