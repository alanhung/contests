// Dynamic Range Minimum Queries
// https://cses.fi/problemset/task/1649
#include <bits/stdc++.h>
using namespace std;
int query(int tree[], int n, int a, int b) {
    a += n;
    b += n;
    int ans = INT_MAX;
    while(a <= b) {
        if (a&1) ans = min(ans, tree[a++]);
        if (!(b&1)) ans = min(ans, tree[b--]);
        a /= 2;
        b /= 2;
    }
    return ans;
}
void update(int tree[], int n, int i, int x) {
    i += n;
    tree[i] = x;
    for (i /= 2; i >= 1; i /= 2) {
        tree[i] = min(tree[2*i], tree[2*i+1]);
    }
}
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, q;
    cin >> n >> q;
    int tree[2 * n];
    fill(tree, tree + 2 * n, INT_MAX);
    for (int i = 0; i < n; i++) {
        int x;
        cin >> x;
        update(tree, n, i, x);
    }
    while(q--) {
        int a, b, c;
        cin >> a >> b >> c;
        if (a == 1)
            update(tree, n, b-1, c);
        else
            cout << query(tree, n, b-1, c-1) << '\n';
    }
    return 0;
}
