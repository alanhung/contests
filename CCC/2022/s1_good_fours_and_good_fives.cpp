#include <bits/stdc++.h>
using namespace std;
int a(int N, bool m[], int g[])
{
    int div = N / 20;
    int mod = N % 20;
    if (div == 0)
    {
        if (m[mod])
            return 1;
    }
    else
    {
        if (mod == 0 || m[mod])
            return div + 1;
        else
            for (int i = 0; i < 13; i++) {
                int r = a(N - g[i], m, g);
                if (r)
                    return r;
            }
    }
    return 0;
}
int main()
{
    cin.tie(0)->sync_with_stdio(0);
    int N;
    cin >> N;
    int g[] = {4, 5, 8, 9, 10, 12, 13, 14, 15, 16, 17, 18, 19};
    bool m[21];
    fill(m, m + 21, false);
    for (int i = 0; i < 13; i++)
        m[g[i]] = true;
    cout << a(N, m, g) << '\n';
    return 0;
}