#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int N, L, S;
    cin >> N >> L >> S;
    map<int,int> m;
    for (int i = 0; i < N; i++) {
        int a, b, s;
        cin >> a >> b >> s;
        auto ait = m.find(a);
        auto bit = m.find(b + 1);
        if (ait != m.end()) {
            m[a] += s;
        } else {
            m.insert({a, s});
        }
        if (bit != m.end()) {
            m[b + 1] -= s;
        } else {
            m.insert({b + 1, -s});
        }
    }
    int cnt = 0;
    int acm = 0;
    int last = 1;
    for (auto it = m.begin(); it != m.end(); ++it) {
        if (acm < S) {
            cnt += it->first - last;
        }
        acm += it->second;
        last = it->first;
    }
    if (last != L + 1) {
        cnt += L + 1 - last;
    }
    cout << cnt;
    return 0;
}
