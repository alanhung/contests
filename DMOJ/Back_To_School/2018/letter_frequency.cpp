#include <bits/stdc++.h>
using namespace std;
int main()
{
    cin.tie(0)->sync_with_stdio(0);
    string s;
    getline(cin, s);
    int Q;
    cin >> Q;
    int prefix[26][s.length()];
    fill(*prefix, *prefix + 26 * s.length(), 0);
    for (int i = 0; i < s.length(); i++)
        if (s[i])
            prefix[s[i] - 97][i] += 1;
    for (int i = 1; i < s.length(); i++)
        for (int j = 0; j < 26; j++)
            prefix[j][i] += prefix[j][i - 1];
    for (int i = 0; i < Q; i++)
    {
        int f, s;
        char c;
        cin >> f >> s >> c;
        f--;
        s--;
        if (f == 0)
            cout << prefix[c - 97][s] << '\n';
        else
            cout << prefix[c - 97][s] - prefix[c - 97][f - 1] << '\n';
    }
    return 0;
}

