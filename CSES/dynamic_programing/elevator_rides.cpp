// Elevator Rides
// https://cses.fi/problemset/task/1653
#include <bits/stdc++.h>
using namespace std;
int main() {
    int n, x;
    cin >> n >> x;
    pair<int, int> best[1 << n];
    int w[n];
    for (int i = 0; i < n; i++)
        cin >> w[i];
    best[0] = {1, 0};
    for (int s = 1; s < (1 << n); s++) {
        best[s] = {n+1, 0};
        for (int p = 0; p < n; p++) {
            if (s & (1 << p)) {
                pair<int, int> option = best[s^(1<<p)];
                if (option.second + w[p] <= x)
                    option.second += w[p];
                else {
                    option.first++;
                    option.second = w[p];
                }
                best[s] = min(best[s], option);
            }
        }
    }
    cout << best[(1<<n) - 1].first;
    return 0;
}

