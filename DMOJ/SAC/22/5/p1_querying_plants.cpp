#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, c;
    cin >> n >> c;
    for (int i = 0; i < n; i++) {
        int x;
        cin >> x;
        cout << x + 2 * c << ' ';
    }
    cout << '\n';
    return 0;
}