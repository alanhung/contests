import re

n = input()

happy = re.findall(":-\\)", n)
sad = re.findall(":-\\(", n)

if len(happy) > len(sad):
  print("happy")
elif len(happy) < len(sad):
  print("sad")
elif len(happy) == len(sad) and len(happy) != 0:
  print("unsure")
else:
  print("none")