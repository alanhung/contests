// A Math Contest P1 - Arrays
// https://dmoj.ca/problem/mathp1
#include <bits/stdc++.h>
#include <numeric>

using namespace std;

int main() {
    int N;
    cin >> N;
    int val = 0;
    for (int i = 0; i < N; i++) {
        int temp;
        cin >> temp;
        val = gcd(val, temp);
    }
    cout << val;
    return 0;
}

