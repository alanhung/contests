#include <iostream>
#include <algorithm>
#include <vector>

int main()
{
  int question, number_of_citizens;

  std::cin >> question >> number_of_citizens;

  std::vector<int> dmoj;
  std::vector<int> peg;

  for (int i = 0; i < number_of_citizens; i++)
  {
    int citizen;
    std::cin >> citizen;
    dmoj.push_back(citizen);
  }
  for (int i = 0; i < number_of_citizens; i++)
  {
    int citizen;
    std::cin >> citizen;
    peg.push_back(citizen);
  }

  std::sort(dmoj.begin(), dmoj.end());
  std::sort(peg.begin(), peg.end());

  int sum = 0;

  for (int i = 0; i < number_of_citizens; i++)
  {
    sum += std::max(dmoj[i], peg[question == 1 ? i : number_of_citizens - i - 1]);
  }

  std::cout << sum << std::endl;

  return 0;
}