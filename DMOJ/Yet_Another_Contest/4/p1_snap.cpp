// Snap
// https://dmoj.ca/problem/yac4p1
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n;
    cin >> n;
    int arr[2*n];
    for (int i = 0; i < 2*n; i++)
        cin >> arr[i];
    int cnt = 0;
    for (int i = 0; i < n; i++) {
        if (arr[i] == arr[i+n])
            cnt++;
    }
    cout << cnt;
    return 0;
}