import java.util.Scanner;
import java.util.Arrays;

public class Main {

  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    
    int g = sc.nextInt();

    for (int i = 0; i < g; i++) {
      int n = sc.nextInt();
      int[] arr = new int[n];
      for (int j = 0; j < n; j++) {
        arr[j] = sc.nextInt();
      }
      Arrays.sort(arr);
      System.out.printf("%d %d%n", arr[0], arr[arr.length - 1]);
    }
    sc.close();

  }
}