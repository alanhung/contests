#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, m;
    cin >> n >> m;
    bool prime[(int)floor(sqrt(m)) + 1];
    fill(prime, prime + (int)floor(sqrt(m)) + 1, true);
    bool res[m - n + 1];
    fill(res, res + m - n + 1, true);
    for (int i = 2; i * i <= m; i++) {
        if (prime[i])  {
            for (int j = i * i; j <= (int)floor(sqrt(m)) ; j += i)
                prime[j] = false;
            for (int j = max((int) ceil((double) n / i) * i, i * i); j <= m; j += i)
                res[j - n] = false;
        }
    }
    if (n == 1)
        res[0] = false;
    for (int i = 0; i < m - n + 1; i++) {
        if (res[i])
            cout << i + n << '\n';
    }
    return 0;
}

