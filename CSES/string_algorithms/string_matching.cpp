// String Matching
// https://cses.fi/problemset/task/1753
#include <bits/stdc++.h>
using namespace std;
vector<int> prefix_function(string s) {
    vector<int> pi(s.size());
    for (int i = 1; i < s.size(); i++) {
        int j = pi[i-1];
        while (j > 0 && s[i] != s[j])
            j = pi[j-1];
        if (s[i] == s[j])
            j++;
        pi[i] = j;
    }
    return pi;
}
int main() {
    cin.tie(0)->sync_with_stdio(0);
    string text, s;
    cin >> text >> s;
    vector<int> pre = prefix_function(s + '#' + text);
    int cnt = 0;
    for (int i = s.length() + 1; i < pre.size(); i++) if (pre[i]==s.size()) cnt++;
    cout << cnt;
    return 0;
}
