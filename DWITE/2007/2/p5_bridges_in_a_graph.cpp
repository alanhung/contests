#include <bits/stdc++.h>
using namespace std;

int dfs(int u, int p, vector<int> adj[], int low[], bool visited[], int &timer) {
    visited[u] = true;
    low[u] = timer++;
    int cnt = 0;
    for (int v : adj[u]) {
        if (v == p)
            continue;
        if (visited[v])
            low[u] = min(low[u], low[v]);
        else {
            cnt += dfs(v, u, adj, low, visited, timer);
            low[u] = min(low[u], low[v]);
            if (low[v] > low[u])
                cnt++;
        }
    }
    return cnt;
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    int T = 5;
    while(T--) {
        int n, m;
        cin >> n >> m;
        vector<int> adj[n];
        for (int i = 0; i < m; i++) {
            int x, y;
            cin >> x >> y;
            x--; y--;
            adj[x].push_back(y);
            adj[y].push_back(x);
        }
        int low[n];
        bool visited[n];
        fill(visited, visited + n, false);
        int timer = 0;
        cout << dfs(0, -1, adj, low, visited, timer) << '\n';
    }
    return 0;
}