// CCC '21 S4 - Daily Commute
// https://dmoj.ca/problem/ccc21s4
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, w, d;
    cin >> n >> w >> d;
    vector<int> adj[n];
    for (int i = 0; i < w; i++) {
        int a, b;
        cin >> a >> b;
        adj[b-1].push_back(a-1);
    }
    bool visited[n];
    fill(visited, visited + n, 0);
    int dist[n];
    fill(dist, dist + n, -1);
    queue<int> q;
    visited[n-1]=1;
    dist[n-1]=0;
    q.push(n-1);
    while(!q.empty()) {
        int u = q.front();
        q.pop();
        for(int v : adj[u]) {
            if (!visited[v]) {
                visited[v]=1;
                dist[v] = dist[u] + 1;
                q.push(v);
            }
        }
    }
    int sub[n];
    multiset<int> res;
    for (int i = 0; i < n; i++) {
        cin >> sub[i];
        sub[i]--;
        if (dist[sub[i]] != -1)
            res.insert(i + dist[sub[i]]);
    }
    while(d--) {
        int x, y;
        cin >> x >> y;
        x--;
        y--;
        if (dist[sub[x]] != -1) {
            res.erase(res.find(x + dist[sub[x]]));
            res.insert(y + dist[sub[x]]);
        }
        if (dist[sub[y]] != -1) {
            res.erase(res.find(y + dist[sub[y]]));
            res.insert(x + dist[sub[y]]);
        }
        swap(sub[x], sub[y]);
        cout << *res.begin() << '\n';
    }
    return 0;
}