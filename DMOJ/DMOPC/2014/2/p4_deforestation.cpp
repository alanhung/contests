// DMOPC '14 Contest 2 P4 - Deforestation
// https://dmoj.ca/problem/dmopc14c2p4
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, q;
    cin >> n;
    int arr[n + 1];
    arr[0]=0;
    for (int i = 1; i <= n; i++) {
        cin >> arr[i];
        arr[i] += arr[i-1];
    }
    cin >> q;
    while(q--) {
        int a, b;
        cin >> a >> b;
        a++;
        b++;
        cout << arr[b] - arr[a-1] << '\n';
    }
    return 0;
}