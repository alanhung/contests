// Trailing Zeros
// https://cses.fi/problemset/task/1618
#include <bits/stdc++.h>
using namespace std;
int main() {
    int n;
    cin >> n;
    int f = 0;
    int t = 0;
    for (int i = 2; i <= n; i *= 2)
        t+= n/i;
    for (int i = 5; i <=n; i *= 5)
        f+=n/i;
    cout << min(f, t);
    return 0;
}
