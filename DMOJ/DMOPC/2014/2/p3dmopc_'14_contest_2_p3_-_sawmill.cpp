// DMOPC '14 Contest 2 P3 - Sawmill
// https://dmoj.ca/problem/dmopc14c2p3
#include <bits/stdc++.h>
using namespace std;
int main() {
    int N;
    cin >> N;
    int e[N];
    int l[N];
    for (int i = 0; i < N; i++)
        cin >> e[i];
    for (int i = 0; i < N; i++)
        cin >> l[i];
    sort(e, e + N);
    sort(l, l + N);
    long long sum = 0;
    for (int i = 0; i < N; i++) {
        sum += e[i] * l[N - i - 1];
    }
    cout << sum;
    return 0;
}