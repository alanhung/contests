// CCC '02 S4 - Bridge Crossing (Hard)
// https://dmoj.ca/problem/ccc02s4hard
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int m, q;
    cin >> m >> q;
    string que[q + 1];
    int st[q + 1][8];
    que[0] = "";
    st[0][0] = 0;
    for (int i = 1; i <= q; i++) {
        cin >> que[i];
        cin >> st[i][0];
    }
    for (int x = 1; (1 << x) <= q + 1; x++) {
        for (int i = 0; i < q + 1; i++)
            st[i][x] = max(st[i][x-1], st[i + (1 << (x-1))][x-1]);
    }
    long long dp[q + 1];
    dp[0] = 0;
    pair<pair<int,int>,int> ref[q + 1];
    ref[0] = {{-1, -1}, -1};
    for (int i = 1; i <= q; i++) {
        dp[i] = LONG_LONG_MAX;
        // cout << "CUR: " << i <<  ' ' << que[i] << '\n';
        for (int j = i - 1; j >= 0 && i - j <= m; j--) {
            // cout << "j: " << j << '\n';
            int k = log2(i-j);
            int mi = max(st[j+1][k], st[i-(1 << k)+1][k]);
            // cout << k << ' ' << mi <<'\n';
            if (dp[i] > dp[j] + mi) {
                dp[i] = dp[j] + mi;
                ref[i] = {{j + 1, i}, j};
            }
        }
    }
    cout << "Total Time: " << dp[q] << '\n';
    int cur = q;
    vector<vector<int>> res;
    while (cur != 0) {
        res.push_back(vector<int>());
        for (int i = ref[cur].first.first; i <= ref[cur].first.second; i++)
            res.back().push_back(i);
        cur = ref[cur].second;
    }
    reverse(res.begin(), res.end());
    for (int i = 0; i < res.size(); i++) {
        for (int j = 0; j < res[i].size(); j++)
            cout << que[res[i][j]] << " \n"[j==res[i].size()-1];
    }
    return 0;
}