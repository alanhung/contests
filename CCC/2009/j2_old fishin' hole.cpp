// CCC '09 J2 - Old Fishin' Hole
// https://dmoj.ca/problem/ccc09j2
#include <bits/stdc++.h>
using namespace std;
int main() {
    int a, b, c, t;
    cin >> a >> b >> c >> t;
    int cnt = 0;
    for (int i = 0; i * a <= t; i++) {
        for (int j = 0; i * a + j * b <= t; j++) {
            for (int k = 0; i * a + j * b + k * c <= t; k++) {
                if (i + j + k != 0) {
                    cout << i << " Brown Trout, " << j << " Northern Pike, " << k << " Yellow Pickerel\n";
                    cnt++;
                }
            }
        }
    }
    cout << "Number of ways to catch fish: " << cnt;
    return 0;
}