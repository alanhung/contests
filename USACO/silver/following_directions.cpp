#include <bits/stdc++.h>
using namespace std;

int calc(vector<vector<bool>> &right, vector<vector<int>> &cost, int row[], int col[], int n) {
  int res =0;
  for (int i = n-1; i >= 0; i--) {
    for (int j = n-1; j >= 0; j--) {
      cost[i][j] = right[i][j] * cost[i][j + 1] + !right[i][j] * cost[i + 1][j];
      res += cost[i][j];
    }
  }
  return res;
}

int main() {
  cin.tie(0)->sync_with_stdio(0);
  int n;
  cin >> n;
  vector<vector<bool>> right(n, vector<bool>(n));
  int row[n];
  int col[n];
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < n; j++) {
      char c;
      cin >> c;
      right[i][j] = c == 'R';
    }
    cin >> row[i];
  }
  for (int i = 0; i < n; i++)
    cin >> col[i];
  vector<vector<int>> cost(n+1, vector<int>(n+1, 0));
  for (int i = 0; i < n; i++) {
    cost[i][n] = row[i];
    cost[n][i] = col[i];
  }
  cout << calc(right, cost, row, col, n) << '\n';
  int q;
  cin >> q;
  while(q--) {
    int i, j;
    cin >> i >> j;
    i--;
    j--;
    right[i][j] = !right[i][j];
    cout << calc(right, cost, row, col, n) << '\n';
  }
  return 0;
}
