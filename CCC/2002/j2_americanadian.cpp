import java.util.Scanner;

public class Main {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);

    while (true) {
      String command = sc.nextLine();
      if (command.equals("quit!")) {
        break;
      } else if (command.length() > 4) {
        String i = command.replaceAll("([^aeiouy])or$", "$1our");
        System.out.println(i);
      } else {
        System.out.println(command);
      }
    }
    sc.close();
  }
}