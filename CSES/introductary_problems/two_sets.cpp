// Two Sets
// https://cses.fi/problemset/task/1092
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    long long n;
    cin >> n;
    if (n % 4 == 0) {
        cout << "YES\n";
        vector<int> a, b;
        for (int i = 1; i <= n; i++) {
            if (i % 4 == 1 || i % 4 == 0) a.push_back(i);
            else b.push_back(i);
        }
        cout << a.size() << '\n';
        for (int u : a) cout << u << ' ';
        cout << '\n';
        cout << b.size() << '\n';
        for (int u : b) cout << u << ' ';
        cout << '\n';
    } else if (n % 4 == 3) {
        cout << "YES\n";
        vector<int> a, b;
        for (int i = 1; i <= n; i++) {
            if (i % 4 == 1 || i % 4 == 2) a.push_back(i);
            else b.push_back(i);
        }
        cout << a.size() << '\n';
        for (int u : a) cout << u << ' ';
        cout << '\n';
        cout << b.size() << '\n';
        for (int u : b) cout << u << ' ';
        cout << '\n';
    } else 
        cout << "NO\n";
    return 0;
}
