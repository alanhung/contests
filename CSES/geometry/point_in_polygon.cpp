// Point in Polygon
// https://cses.fi/problemset/task/2192
#include <bits/stdc++.h>
using namespace std;

bool cmp(complex<long long> &x, complex<long long> &y) {
    return x.real() < y.real() || (x.real() == y.real() && x.imag() < y.imag());
}

long long cross(complex<long long>x, complex<long long> y, complex<long long> z) {
    return (conj(z - x) * (z - y)).imag();
}

long long mid(complex<long long>x, complex<long long> y, complex<long long>z) {
    vector<complex<long long>> v = {x, y, z};
    sort(v.begin(), v.end(), cmp);
    return (v[1] == z);
}

bool opo(long long x, long long y) {
    return ((x > 0) - (x < 0)) != ((y > 0) - (y < 0));
}

bool boundary(complex<long long>x, complex<long long>y, complex<long long> z) {
    long long c = cross(x, y, z);
    if  (c == 0 && mid(x, y, z))
        return 1;
    return 0;
}

bool intersect(complex<long long>a, complex<long long>b, complex<long long>c, complex<long long>d) {
    long long c1 = cross(a, b, c);
    long long c2 = cross(a, b, d);
    long long c3 = cross(c, d, a);
    long long c4 = cross(c, d, b);
    if ((c1 == 0 && mid (a, b, c)) || (c2 == 0 && mid(a, b, d)) || (c3 == 0 && mid(c, d, a)) || (c4 == 0 && mid(c, d, b)))
        return 1;
    if (opo(c1, c2) && opo(c3, c4))
        return 1;
    return 0;
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, m;
    cin >> n >> m;
    complex<long long> arr[n];
    for (int i = 0; i < n; i++) {
        long long x, y;
        cin >> x >> y;
        arr[i]  = {x, y};
    }
    for (int i = 0; i < m; i++) {
        long long x, y;
        cin >> x >> y;
        complex<long long> p = {x, y};
        int cnt = 0;
        bool g = 0;
        for (int i = 0; i < n; i++) {
            if (boundary(arr[i], arr[(i + 1)%n], p)) {
                g = 1;
                break;
            }
            if (intersect(p, {1000000002, 1000000001}, arr[i], arr[(i+1)%n]))
                cnt++;
        }
        if (g) cout << "BOUNDARY\n";
        else if (cnt & 1) cout << "INSIDE\n";
        else cout << "OUTSIDE\n";
    }
    return 0;
}
