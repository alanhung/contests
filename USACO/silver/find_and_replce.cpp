#include <bits/stdc++.h>
using namespace std;
bool dfs(unsigned char x, unsigned char trans[128], bool visited[128], int stk[128], int d, int &c) {
  if (!visited[x]) {
    visited[x] = 1;
    d++;
    stk[x] = d;
    bool res = trans[(unsigned char)x] != x && dfs(trans[x], trans, visited, stk, d, c);
    stk[x] = 0;
    return res;
  }
  if (stk[x]) {
    c = d - stk[x] + 1;
  }
  return stk[x] != 0;
}
int main() {
  cin.tie(0)->sync_with_stdio(0);
  int T;
  cin >> T;
  while(T--) {
    string a, b;
    cin >> a >> b;
    unsigned char trans[128];
    fill(trans, trans + 128, 0);
    int cnt = 0;
    bool bad = false;
    for (int i = 0; i < (int)a.size(); i++) {
      if (trans[(unsigned char)a[i]]) {
        if (trans[(unsigned char)a[i]] != b[i]) {
          bad = true;
          break;
        }
      } else {
        cnt += (a[i] != b[i]);
        trans[(unsigned char)a[i]] = b[i];
      }
    }
    if (bad) {
      cout << -1 << '\n';
      continue;
    }
    bool looped = false;

    bool visited[128];
    fill(visited, visited + 128, 0);
    int stk[128];
    fill(stk, stk + 128, 0);
    for (int i = 0; i < (int) a.size(); i++) {
      int c = 0;
      if ( (trans[(unsigned char)a[i]] != (unsigned char)a[i]) && dfs(a[i], trans, visited, stk, 1, c)) {
        bad = c == 52;
        looped = true;
        break;
      }
      if (looped)
        break;
    }
    if (bad) {
      cout << -1 << '\n';
      continue;
    }
    cout << cnt + looped << '\n';
  }
  return 0;
}
