// CCC '08 J4 - From Prefix to Postfix
#include <iostream>
#include <stack>
#include <string>

std::string solve(std::string e, int l, int r)
{

    std::string result = "";

    if (r - l == 0)
    {
        return std::string(1, e[l]);
    }

    char op = e[l];

    int fstop = l + 2;
    std::stack<char> s;

    while (true)
    {
        if (e[fstop] == '+' || e[fstop] == '-')
        {
            s.push(e[fstop]);
            fstop += 2;
        }
        else
        {
            if (s.empty())
            {
                break;
            }
            else
            {
                s.pop();
				fstop += 2;
            }
        }
    }

    return solve(e, l + 2, fstop) + ' ' + solve(e, fstop + 2, r) + ' ' + op;
}

int main()
{

	std::ios::sync_with_stdio(0);
	std::cin.tie(0);
    while (true)
    {
        std::string e;
        std::getline(std::cin, e);

        if (e.size() == 1 && e[0] == '0')
        {
            break;
        }
        else
        {
            std::cout << solve(e, 0, e.size() - 1) << '\n';
        }
    }

    return 0;
}