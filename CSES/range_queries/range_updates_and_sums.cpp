// Range Updates and Sums
// https://cses.fi/problemset/task/1735
#include <bits/stdc++.h>
using namespace std;
void push(long long tree[], long long lazy[], int marked[], int u, int l, int r) {
    if (marked[u]) {
        marked[2 * u] = marked[2 * u + 1] = marked[u];
        int d = (l + r) / 2;
        tree[2 * u] = (d - l + 1) * (long long)marked[u];
        tree[2 * u + 1] = (r - (d + 1) +1) * (long long)marked[u];
        lazy[2 * u] = lazy[2 * u + 1] = 0;
        marked[u]=0;
    }
}
long long sum(long long tree[], long long lazy[], int marked[], int u, int l, int r, int a, int b) {
    if (b < l || a > r) return 0;
    if (a <= l && b >= r)
        return tree[u] + (r-l+1)*lazy[u];
    push(tree, lazy, marked, u, l, r);
    tree[u] += (r-l+1)*lazy[u];
    lazy[2 * u] += lazy[u];
    lazy[2 * u + 1] += lazy[u];
    lazy[u]=0;
    int d = (l + r) / 2;
    return sum(tree, lazy, marked, 2 * u, l, d, a, b) + sum(tree, lazy, marked, 2 * u + 1, d + 1, r, a, b);
}

long long assign(long long tree[], long long lazy[], int marked[], int u, int l, int r, int a, int b, long long val) {
    if (b < l || a > r) return tree[u] + (r - l + 1) * lazy[u];
    if (a <= l && b >= r) {
        tree[u] = (r - l + 1) * val;
        lazy[u]=0;
        marked[u]=val;
        return tree[u];
    } 
    push(tree, lazy, marked, u, l, r);
    int d = (l + r) / 2;
    lazy[2 * u] += lazy[u];
    lazy[2 * u + 1] += lazy[u];
    lazy[u]=0;
    tree[u] = assign(tree, lazy, marked, 2 * u, l, d, a, b, val) + assign(tree, lazy, marked, 2 * u + 1, d + 1, r, a, b, val);

    return tree[u];
}
void update(long long tree[], long long lazy[], int marked[], int u, int l, int r, int a, int b, long long val) {
    if (b < l || a > r)
        return;
    if (a <= l && b >= r)
        lazy[u] += val;
    else {
        push(tree, lazy, marked, u, l, r);
        int d = (l + r) / 2;
        tree[u] += (min(b, r) - max(a, l) + 1) * val;
        update(tree, lazy, marked, 2 * u, l, d, a, b, val);
        update(tree, lazy, marked, 2 * u + 1, d + 1, r, a, b, val);
    }
}

long long tree[1000005];
long long lazy[1000005];
int marked[1000005];
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, q;
    cin >> n >> q;
    for (int i = 0; i < n; i++) {
        long long x;
        cin >> x;
        update(tree, lazy, marked, 1, 0, n-1, i, i, x);
    }
    while(q--) {
        int i, a, b;
        cin >> i >> a >> b;
        a--;
        b--;
        if (i == 1) {
            long long x;
            cin >> x;
            update(tree, lazy, marked, 1, 0, n-1, a, b, x);
        } else if (i == 2) {
            long long x;
            cin >> x;
            assign(tree, lazy, marked, 1, 0, n-1, a, b, x);
        } else
            cout << sum(tree, lazy, marked, 1, 0, n-1, a, b) << '\n';
    }
    return 0;
}
