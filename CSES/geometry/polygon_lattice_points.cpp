// Polygon Lattice Points
// https://cses.fi/problemset/task/2193
// https://stackoverflow.com/questions/23729244/algorithm-to-calculate-the-number-of-lattice-points-in-a-polygon
#include <bits/stdc++.h>
using namespace std;
long long cross(complex<long long> x, complex<long long> y) {
    return (conj(x) * y).imag();
}

long long area(complex<long long> arr[], int n) {
    long long res = 0;
    for (int i = 0; i < n - 1; i++) {
        res += cross(arr[i], arr[i + 1]);
    }
    res += cross(arr[n-1], arr[0]);
    return abs(res);
}

long long boundary(complex<long long> arr[], int n) {
    long long res = 0;
    for (int i = 0; i < n - 1; i++)
        res += abs(gcd(arr[i + 1].real() - arr[i].real(), arr[i + 1].imag() - arr[i].imag()));
    res += abs(gcd(arr[0].real() - arr[n - 1].real(), arr[0].imag() - arr[n-1].imag()));
    return res;
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n;
    cin >> n;
    complex<long long> arr[n];
    for (int i = 0; i < n; i++) {
        long long x, y;
        cin >> x >> y;
        arr[i] = {x, y};
    }
    long long b = boundary(arr, n);
    cout << (area(arr, n) - b + 2) / 2 << ' ' << b;
    return 0;
}

