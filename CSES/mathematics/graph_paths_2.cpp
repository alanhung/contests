// Graph Paths 2
// https://cses.fi/problemset/task/1723
#include <bits/stdc++.h>
using namespace std;
vector<vector<long long>> mul(vector<vector<long long>>&a, vector<vector<long long>>&b) {
    vector<vector<long long>> c(a.size(), vector<long long>(b[0].size(), LONG_LONG_MAX / 2));
    for (int i = 0; i < a.size(); i++) {
        for (int j = 0; j < b[0].size(); j++) {
            for (int k = 0; k < a[0].size(); k++) {
                c[i][j] = min(c[i][j], a[i][k] + b[k][j]);
            }
        }
    }
    return c;
}
vector<vector<long long>> pow(vector<vector<long long>> base, int exp) {
    vector<vector<long long>> res(base.size(), vector<long long>(base.size()));
    bool f = 1;
    while (exp > 0) {
        if (exp & 1)  {
            if (f) {
                res = base;
                f = 0;
            }
            else res = mul(res, base);
        }
        exp >>= 1;
        base = mul(base, base);
    }
    return res;
}
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, m, k;
    cin >> n >> m >> k;
    vector<vector<long long>> res(n, vector<long long>(n, LONG_LONG_MAX / 2));
    while(m--) {
        int a, b;
        long long c;
        cin >> a >> b >> c;
        res[a-1][b-1] = min(res[a-1][b-1], c);
    }
    res = pow(res, k);
    cout << (res[0][n-1] == LONG_LONG_MAX / 2 ? -1 : res[0][n-1]);
    return 0;
}

