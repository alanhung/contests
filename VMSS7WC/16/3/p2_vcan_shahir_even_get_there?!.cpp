#include <bits/stdc++.h>
using namespace std;
int main() {
    int N, M, A, B;
    cin >> N >> M >> A >> B;
    A--;
    B--;
    vector<int> adj[N];
    while(M--) {
        int x, y;
        cin >> x >> y;
        x--;
        y--;
        adj[x].push_back(y);
        adj[y].push_back(x);
    }
    bool visited[N];
    fill(visited, visited + N, 0);
    queue<int> q;
    visited[A] = true;
    q.push(A);
    while(!q.empty()) {
        int u = q.front();
        q.pop();
        for (int v : adj[u]) {
            if (!visited[v]) {
                visited[v] = true;
                q.push(v);
                if (visited[B])
                    break;
            }
        }
        if (visited[B])
            break;
    }
    if (visited[B]) {
        cout << "GO SHAHIR!\n";
    } else {
        cout << "NO SHAHIR!\n";
    }
    return 0;
}