#include <iostream>
#include <stack>
#include <utility>
using namespace std;
int main()
{
    cin.tie(0)->sync_with_stdio(0);
    int N, f;
    cin >> N >> f;
    stack<pair<int, int>> st;
    st.push({f, 1});
    cout << 0 << ' ';
    for (int i = 1; i < N; i++)
    {
        int c;
        cin >> c;
        int m = 0;
        if (c < st.top().first)
        {
            m++;
            st.push({c, 1});
        }
        else if (c == st.top().first)
        {
            m += st.top().second;
            if (st.size() >= 2)
                m++;
            st.top().second += 1;
        }
        else
        {
            while (!st.empty() && c >= st.top().first)
            {
                m += st.top().second;
                st.pop();
            }
            st.push({c, m + 1});
            if (st.size() >= 2)
                m++;
        }
        cout << m << ' ';
    }
    return 0;
}
