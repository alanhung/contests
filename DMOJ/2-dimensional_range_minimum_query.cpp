// 2-Dimensional Range Minimum Query
// https://dmoj.ca/problem/2drmq
#include <bits/stdc++.h>
using namespace std;
int st[11][1000][11][1000];
void init(vector<vector<int>> arr) {
  for (int i = 0; i < (int)(arr.size()); i++) {
    for (int j = 0; j < (int)arr[0].size(); j++)
      st[0][i][0][j] = arr[i][j];
    for (int jh = 1; (1 << jh) <= (int)arr[0].size(); jh++)
      for (int j = 0; j + (1 << jh) <= (int)arr[0].size(); j++)
        st[0][i][jh][j] =
            min(st[0][i][jh - 1][j], st[0][i][jh - 1][j + (1 << (jh - 1))]);
  }
  for (int ih = 1; (1 << ih) <= (int)arr.size(); ih++)
    for (int i = 0; i + (1 << ih) <= (int)arr.size(); i++)
      for (int jh = 0; (1 << jh) <= (int)arr[0].size(); jh++)
        for (int j = 0; j + (1 << jh) <= (int)arr[0].size(); j++)
          st[ih][i][jh][j] =
              min(st[ih - 1][i][jh][j], st[ih - 1][i + (1 << (ih - 1))][jh][j]);
}
int query(int a, int b, int c, int d) {
  int ih = log2(b - a + 1);
  int jh = log2(d - c + 1);
  return min({st[ih][a][jh][c], st[ih][b - (1 << ih) + 1][jh][c],
              st[ih][a][jh][d - (1 << jh) + 1],
              st[ih][b - (1 << ih) + 1][jh][d - (1 << jh) + 1]});
}
