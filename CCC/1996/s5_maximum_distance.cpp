// CCC '96 S5 - Maximum Distance
// https://dmoj.ca/problem/ccc96s5
#include <bits/stdc++.h>
using namespace std;
int dist(int i, int x, int j, int y) {
    if (j >= i && y >= x)
        return j - i;
    return 0;
}
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int t;
    cin >> t;
    while(t--) {
        int n;
        cin >> n;
        int a[n];
        int b[n];
        for (int i = 0; i < n; i++)
            cin  >> a[i];
        for (int i = 0; i < n; i++)
            cin  >> b[i];
        int i = 0;
        int j = 0;
        int ma = 0;
        while (i < n && j < n) {
            if (b[j] >= a[i]) {
                ma = max(ma, dist(i, a[i], j, b[j]));
                j++;
            } else
                i++;
        }
        cout << "The maximum distance is " << ma << '\n';
    }
    return 0;
}