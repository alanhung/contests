// CCC '15 J5 - π-day
// https://dmoj.ca/problem/ccc15j5
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, k;
    cin >> n >> k;
    int dp[n + 1][k + 1];
    fill(*dp, *dp + (n + 1) * (k + 1), 0);
    for (int i = 1; i <= n; i++) {
        dp[i][1]=1;
        for (int j = 2; j <= min(i, k); j++)
            dp[i][j] = dp[i-1][j-1] + dp[i-j][j];
        dp[i][i]=1;
    }
    cout << dp[n][k];
    return 0;
}