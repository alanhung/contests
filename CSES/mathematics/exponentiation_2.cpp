// Exponentiation
// https://cses.fi/problemset/task/1095
#include <bits/stdc++.h>
using namespace std;
unsigned long long modpow(unsigned long long base, unsigned long long exp, unsigned long long m) {
    unsigned long long res = 1;
    base %= m;
    while(exp > 0) {
        if (exp & 1) res = (res * base) % m;
        base = (base * base) % m;
        exp >>= 1;
    }
    return res;
}
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n;
    cin >> n;
    while(n--) {
        unsigned long long a, b, c;
        cin >> a >> b >> c;
        cout << modpow(a, modpow(b, c, 1000000006ull), 1000000007ull) << '\n';
    }
    return 0;
}
