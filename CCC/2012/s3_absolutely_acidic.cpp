// CCC '12 S3 - Absolutely Acidic

#include <iostream>
#include <vector>
#include <map>
#include <utility>
#include <algorithm>
#include <set>

int main()
{
  //std::ios::sync_with_stdio(0);
  //std::cin.tie(0);
  int N;
  std::cin >> N;

  std::vector<std::pair<int, int>> sensors;
  std::map<int, int> map;

  for (int i = 0; i < N; i++)
  {
    int R;
    std::cin >> R;

    if (map.find(R) == map.end())
    {
      sensors.push_back(std::make_pair(R, 1));
      map.insert({R, sensors.size() - 1});
    }
    else
    {
      sensors[map[R]].second++;
    }
  }

  std::sort(sensors.begin(), sensors.end(), [](auto &left, auto &right) {
    return left.second > right.second;
  });

  int highest = sensors[0].second;
  std::set<int> largest;
  largest.insert(sensors[0].first);

  for (int i = 1; i < sensors.size(); i++)
  {
    if (sensors[i].second < highest)
    {
      break;
    }

    largest.insert(sensors[i].first);
  }

  if (largest.size() > 1)
  {
    std::cout << std::abs(*largest.rbegin() - *largest.begin());
  }
  else
  {
    std::pair<int, int> secLargest = sensors[1];
    int diff = std::abs(sensors[0].first - secLargest.first);

    for (int i = 1; i < sensors.size(); i++)
    {
      if (sensors[i].second < secLargest.second)
      {
        break;
      }

      int currentDiff = std::abs(sensors[0].first - sensors[i].first);

      if (currentDiff > diff)
      {
        diff = currentDiff;
      }
    }
    std::cout << diff;
  }

  //for (int i = 0; i < sensors.size(); i++)
  //{
  //std::cout << sensors[i].first << ' ' << sensors[i].second << '\n';
  //}

  return 0;
}