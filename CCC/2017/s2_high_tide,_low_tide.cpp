// https://dmoj.ca/problem/ccc17s2

#include <bits/stdc++.h>

using namespace std;

int main() {
    cin.tie(0)->sync_with_stdio(0);
   int N; 
   cin >> N;
   int arr[N];
   for (int i = 0; i < N; i++) {
       cin >> arr[i];
   }
   sort(arr, arr + N);
   for (int i = 0; i < N / 2; i++) {
       cout << arr[N / 2 - i - (N & 1 ? 0 : 1)] << ' ' << arr[N / 2 + i + (N & 1 ? 1: 0)] << ' ';
   }
   if (N & 1)
       cout << arr[0];
}