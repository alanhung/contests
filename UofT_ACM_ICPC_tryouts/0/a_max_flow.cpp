import java.util.Scanner;

public class Main {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int t = Integer.parseInt(sc.nextLine());
    for (int i = 0; i < t; i++) {
      int n = Integer.parseInt(sc.nextLine());
      int[] a = new int[n];
      for (int j = 0; j < n; j++) {
        a[j] = Integer.parseInt(sc.nextLine());
      }
      System.out.println(getMax(a));
    }
    sc.close();
  }
  public static int getMax(int[] arr) {
    int maxValue = arr[0];
    for (int i = 0; i < arr.length; i++) {
      if (arr[i] > maxValue) maxValue = arr[i];
    }
    return maxValue;
  }
}