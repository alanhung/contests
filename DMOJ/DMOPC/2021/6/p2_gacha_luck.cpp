#include <bits/stdtr1c++.h>
using namespace std;
int main() {
    int N, K;
    cin >> N >> K;
    string b;
    cin >> b;
    vector<int> v;
    int cnt = 0;
    for (int i = 0; i < N; i++) {
        if (b[i] == '0') 
            cnt++;
        else {
            if (cnt != 0)
                v.push_back(cnt);
            cnt = 0;
        }
    }
    if (cnt != 0)
        v.push_back(cnt);
    sort(v.begin(), v.end(), greater<int>());
    int r = 0;
    int i = 0;
    int j = 0;
    while(i < v.size() && j < K) {
        r += v[i];
        i++;
        j++;
    }
    cout << r;
    return 0;
}