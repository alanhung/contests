// DMOPC '14 Contest 3 P3 - Not Enough Personnel!
// https://dmoj.ca/problem/dmopc14c3p3
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, q;
    cin >> n;
    vector<string> names(n);
    vector<pair<int,int>> arr(n);
    for (int i = 0; i < n; i++) {
        string s;
        int skill;
        cin >> s >> skill;
        names[i] = s;
        arr[i] = {skill, i};
    }
    sort(arr.begin(), arr.end());
    cin >> q;
    for (int i = 0; i < q; i++) {
        int skill, d;
        cin >> skill >> d;
        vector<pair<int,int>>::iterator it = lower_bound(arr.begin(), arr.end(), skill, [](pair<int,int> const & p, int sk ) { return p.first < sk ;} );
        if (it != arr.end() && it->first <= skill + d)
            cout << names[it->second] << '\n';
        else
            cout << "No suitable teacher!\n";
    }
    return 0;
}