#include <bits/stdc++.h>
using namespace std;
void f(int u, int A[], bool visited[], vector<int> &v) {
    v.push_back(u);
    visited[u] = true;
    if (!visited[A[u]]) {
        f(A[u], A, visited, v);
    }
}
int main() {
    int N;
    long long K;
    cin >> N >> K;
    vector<vector<int>> cycles;
    int A[N];
    for (int i = 0; i < N; i++) {
        int c;
        cin >> c;
        A[i] = c-1;
    }
    bool visited[N];
    fill(visited, visited + N, 0);
    for (int i = 0; i < N; i++) {
        if (!visited[A[i]]) {
            vector<int> c;
            f(A[i], A, visited, c);
            cycles.push_back(move(c));
        }
    }
    int res[N];
    for (int i = 0; i < cycles.size(); i++) {
        for (int j = 0; j < cycles[i].size(); j++) {
            res[cycles[i][(((K % (int)cycles[i].size() + j) - 1) + (int)cycles[i].size()) % (int)cycles[i].size()]] = cycles[i][j];
        }
    }
    for (int i = 0; i < N; i++)
        cout << res[i] + 1<< ' ';
    cout << '\n';
    return 0;
}