// Bubble Sort

#include <iostream>
#include <vector>

using namespace std;

void printArr(vector<int> arr, const int &N)
{
  for (int i = 0; i < N; i++)
  {
    cout << arr[i] << ' ';
  }
  cout << '\n';
}

void mySort(vector<int> arr, const int &N)
{
  for (int i = 0; i < N - 1; i++)
  {
    bool swapped = false;

    for (int j = 0; j < N - i - 1; j++)
    {
      if (arr[j] > arr[j + 1])
      {
        swap(arr[j], arr[j + 1]);
        swapped = true;
        printArr(arr, N);
      }
    }

    if (!swapped)
    {
      break;
    }
  }
}

int main()
{
  ios::sync_with_stdio(0);
  cin.tie(0);
  int N;
  cin >> N;
  vector<int> arr;
  for (int i = 0; i < N; i++)
  {
    int current;
    cin >> current;
    arr.push_back(current);
  }
  printArr(arr, N);
  mySort(arr, N);
  return 0;
}

