#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, m;
    cin >> n >> m;
    long long cost[n + 1];
    long long height[n + 1];
    for (int i = 1; i <= n; i++)
        cin >> cost[i];
    for (int i = 1; i <= n; i++)
        cin >> height[i];
    // piller count, number of changes applied
    // stores the smallest cost so far
    long long dp[n + 1][m + 1];
    for (int i = 0; i <= m; i++)
        dp[0][i] = 0;

    long long cost_wall[n + 1][n + 1];
    for (int i = 1; i <= n; i++) {
        cost_wall[i][0] = 0;
        for (int j = 1; j <= n; j++)
            cost_wall[i][j] = cost_wall[i][j-1] + cost[j] * abs(height[i] - height[j]);
    }
    // loop throgh pillers
    for (int i = 1; i <= n; i++) {
        dp[i][0] = dp[i-1][0] + cost[i] * height[i];
        // loop through possible changes applied
        for (int j = 1; j <= m; j++) {
            dp[i][j] = LONG_LONG_MAX;
            // loop through possible height
            for (int l = 1; l <= n; l++) {
                // loop through possible starting position of wall to end at i
                for (int k = 1; k <= i; k++) {
                    dp[i][j] = min(dp[i][j], dp[k-1][j-1] + cost_wall[l][i] - cost_wall[l][k-1]);
                }
            }
        }
    }
    long long best = LONG_LONG_MAX;
    for (int i = 0; i <= m; i++)
        best = min(best, dp[n][i]);
    cout << best << '\n';
    return 0;
}