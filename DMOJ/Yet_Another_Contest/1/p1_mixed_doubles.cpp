#include <bits/stdc++.h>
#define MOD 1000000007ull
using namespace std;
int main() {
    unsigned int N, K;
    cin >> N >> K;
    unsigned int men[N];
    unsigned int women[N];
    for (unsigned int i = 0; i < N; i++)
        cin >> men[i];
    for (unsigned int i = 0; i < N; i++)
        cin >> women[i];
    sort(men, men + N);
    sort(women, women + N);
    if (men[N - 1] > women[N - 1]) {
        if (men[N - 1] - women[N - 1] >= K) {
            women[N-1] += K;
            K = 0;
        } else {
            K -= men[N - 1] - women[N - 1];
            women[N-1] += men[N - 1] - women[N - 1];
        }
    } else if (men[N-1] < women[N - 1]) {
        if (women[N - 1] - men[N - 1] >= K) {
            men[N-1] += K;
            K = 0;
        } else {
            K -= women[N - 1] - men[N - 1];
            men[N-1] += women[N - 1] - men[N - 1];
        }
    }
    unsigned int half = K / 2;
    women[N-1] += half;
    men[N-1] += K - half;
    unsigned long long res = 0;
    for (unsigned int i = 0; i < N; i++) {
        unsigned long long t = (unsigned long long)men[i] * (unsigned long long)women[i] % MOD;
        res = (res + t) % MOD;
    }
    cout << res << '\n';
    return 0;
}