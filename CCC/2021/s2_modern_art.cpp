// CCC '21 S2 - Modern Art
// TLE
#include <iostream>
#include <map>

using namespace std;

int main()
{
  ios::sync_with_stdio(0);
  cin.tie(0);
  int M, N, K;
  cin >> M >> N >> K;

  int row = 0;
  int col = 0;

  map<int, bool> rowMap;
  map<int, bool> colMap;

  for (int i = 0; i < K; i++)
  {
    char c;
    int index;
    cin >> c >> index;

    if (c == 'R')
    {
      map<int, bool>::iterator it = rowMap.find(index);
      if (it == rowMap.end())
      {
        row++;
        rowMap.insert({index, true});
      }
      else
      {
        if (it->second)
        {
          row--;
          it->second = false;
        }
        else
        {
          row++;
          it->second = true;
        }
      }
    }
    else
    {
      map<int, bool>::iterator it = colMap.find(index);

      if (it == colMap.end())
      {
        col++;
        colMap.insert({index, true});
      }
      else
      {
        if (it->second)
        {
          col--;
          it->second = false;
        }
        else
        {
          col++;
          it->second = true;
        }
      }
    }
  }

  cout << (row * N + col * M) - 2 * (row * col);

  return 0;
}