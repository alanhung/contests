#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int T;
    cin >> T;
    while(T--) {
        unsigned long long g, t;
        cin >> g >> t;
        unsigned long long v = ceil((double) g / t * 100);
        if (v > 100ull)
            cout << "sus";
        else if (v == 100ull)
            cout << "average";
        else if (v >= 98ull && v <= 99ull)
            cout << "below average";
        else if (v >= 95ull && v <= 97ull)
            cout << "can't eat dinner";
        else if (v >= 90ull && v <= 94ull)
            cout << "don't come home";
        else
            cout << "find a new home";
        cout <<'\n';
    }
    return 0;
}

