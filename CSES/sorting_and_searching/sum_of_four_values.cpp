// Sum of Four Values
// https://cses.fi/problemset/task/1642
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, x;
    cin >> n >> x;
    pair<int,int> a[n];
    for (int i = 0; i < n; i++) {
        int temp;
        cin >> temp;
        a[i] = {temp, i + 1};
    }
    sort(a, a + n);
    for (int l = 0; l < n - 3; l++) {
        for (int k = l + 1; k < n - 2; k++) {
            int i = k + 1;
            int j = n - 1;
            while(i < j) {
                if (a[i].first + a[j].first + a[l].first + a[k].first > x)
                    j--;
                else if (a[i].first + a[j].first + a[l].first + a[k].first < x)
                    i++;
                else {
                    cout << a[i].second << ' ' << a[j].second << ' ' << a[l].second << ' ' << a[k].second;
                    return 0;
                }
            }
        }
    }
    cout << "IMPOSSIBLE";
    return 0;
}
