// Repeating Substrings
// https://cses.fi/problemset/task/2106
#include <bits/stdc++.h>
using namespace std;
vector<int> suffix_construction(string s) {
    s += "$";
    int n = s.size();
    vector<int> p(n), c(n);
    iota(p.begin(), p.end(), 0);
    sort(p.begin(), p.end(), [&](int i, int j) {return s[i] < s[j] ;} );
    c[p[0]] = 0;
    for (int i = 1; i < n; i++)
        c[p[i]] = c[p[i - 1]] + (s[p[i]] != s[p[i-1]]);
    for (int h = 0; ( 1 << h) < n; h++) {
        vector<pair<int ,int>> temp(n);
        for (int &i : p)
            temp[i] = {c[i], c[(i + (1 << h)) % n]};
        sort(p.begin(), p.end(), [&](int i, int j) {return temp[i] < temp[j]; });
        c[p[0]] = 0;
        for (int i = 1; i < n; i++)
            c[p[i]] = c[p[i - 1]] + (temp[p[i]] != temp[p[i-1]]);
    }
    p.erase(p.begin());
    return p;
}

vector<int> lcp_construction(string const &s, vector<int> const &p) {
    int n = s.size();
    vector<int> rank(n), lcp(n-1, 0);
    for (int i = 0; i < n; i++)
        rank[p[i]] = i;
    int k = 0;
    for (int i = 0; i < n; i++) {
        if (rank[i] == n - 1) {
            k = 0;
            continue;
        }
        int j = p[rank[i] + 1];
        while (i + k < n && j + k < n && s[i + k] == s[j + k])
            k++;

        lcp[rank[i]] = k;

        if (k)
            k--;
    }
    return lcp;
}

int main() {
    string s;
    cin >> s;
    vector<int> p = suffix_construction(s);
    vector<int> lcp = lcp_construction(s, p);

    int j = 0;
    int l = 0;
    for (int i = 0; i < lcp.size(); i++) {
        if (lcp[i]) {
            if (lcp[i] > l) {
                l = lcp[i];
                j = p[i];
            }
        }
    }
    if (l) cout << s.substr(j, l);
    else cout << -1;
    return 0;
}
