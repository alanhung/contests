#include <bits/stdc++.h>
using namespace std;
void solve(string &a, string &b, string &res, int x, int y, int k, set<string> &s) {
    if (k == a.length() + b.length()) {
        if (s.find(res) == s.end()) {
            s.insert(res);
            cout << res << '\n';
        }
        return;
    }
    if (x < a.length()) {
        res[k] = a[x];
        solve(a, b, res, x + 1, y, k + 1, s);
    }
    if (y < b.length()) {
        res[k] = b[y];
        solve(a, b, res, x, y + 1, k + 1, s);
    }
}
int main() {
    cin.tie(0)->sync_with_stdio(0);
    string a, b, res;
    cin >> a >> b;
    res.resize(a.length() + b.length());
    set<string> s;
    solve(a, b, res, 0, 0, 0, s);
    return 0;
}

