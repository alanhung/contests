#include <iostream>

int main()
{
	std::ios::sync_with_stdio(0);
	std::cin.tie(0);
	int N;
	std::cin >> N;

	int f, s, t;

	std::cin >> f >> s >> t;

	for (int i = 1; i < N; i++)
	{
		int temp1, temp2, temp3;
		std::cin >> temp1 >> temp2 >> temp3;

		temp1 += std::max(s, t);
		temp2 += std::max(f, t);
		temp3 += std::max(f, s);

		f = temp1;
		s = temp2;
		t = temp3;
	}

	std::cout << std::max(std::max(f, s), t);

	return 0;
}