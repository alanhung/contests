// Word Scrambler

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

vector<string> solve(string s)
{
  if (s == "")
  {
    vector<string> temp = {""};
    return temp;
  }

  vector<string> result;

  for (int i = 0; i < s.length(); i++)
  {
    string nextS = s;
    nextS.erase(i, 1);
    vector<string> nextLevel = solve(nextS);

    for (int j = 0; j < nextLevel.size(); j++)
    {
      result.push_back(nextLevel[j] + s[i]);
    }
  }

  return result;
}

int main()
{
  ios::sync_with_stdio(0);
  cin.tie(0);
  string s;
  cin >> s;
  vector<string> result = solve(s);
  sort(result.begin(), result.end());

  for (int i = 0; i < result.size(); i++)
  {
    cout << result[i] << '\n';
  }
  return 0;
}

