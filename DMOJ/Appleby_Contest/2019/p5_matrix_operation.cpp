// Matrix Operation
//
#include <algorithm>
#include <iostream>
#include <utility>


int ref[2250000];
std::pair<int, int> grid[2250000];
int dp[2250000];

int main()
{
	std::ios::sync_with_stdio(0);
	std::cin.tie(0);
    int N;
    std::cin >> N;
    for (int i = 0; i < N * N; i++)
    {
        int c;
        std::cin >> c;
        grid[i] = std::make_pair(i, c);
        ref[i] = c;
    }

    std::sort(grid, grid + N*N, [](std::pair<int, int> f, std::pair<int, int> s) { return f.second > s.second; });

	//for (int i = 0; i < N * N; i++)
	//{
		//std::cout << grid[i].first << ' ' << grid[i].second << '\n';
	//}

	int largest = 0;

	for (int i = 0; i < N * N; i++)
	{
		std::pair<int,int> c = grid[i];
		dp[c.first] = 0;

		if (c.first - N >= 0)
		{
			if (ref[c.first - N] > c.second)
			{
				dp[c.first] = std::max(dp[c.first], dp[c.first - N]);
			}
		}

		if (c.first + N < N*N)
		{
			if (ref[c.first + N] > c.second)
			{
				dp[c.first] = std::max(dp[c.first], dp[c.first + N]);
			}
		}

		if (c.first / N == (c.first - 1) / N)
		{
			
			if (ref[c.first - 1] > c.second)
			{
				dp[c.first] = std::max(dp[c.first], dp[c.first - 1]);
			}
		}

		if (c.first / N == (c.first + 1) / N)
		{
			
			if (ref[c.first + 1] > c.second)
			{
				dp[c.first] = std::max(dp[c.first], dp[c.first + 1]);
			}
		}
		dp[c.first] += 1;

		largest = std::max(dp[c.first], largest);
	}

	std::cout << largest - 1;

    return 0;
}