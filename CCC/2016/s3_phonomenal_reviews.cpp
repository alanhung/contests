// CCC '16 S3 - Phonomenal Reviews
// https://dmoj.ca/problem/ccc16s3
#include <bits/stdc++.h>
using namespace std;
// node, dist
pair<int,int> dfs(int u, int p, set<int> adj[]) {
    int d = 0;
    int a = u;
    for (set<int>::iterator it = adj[u].begin(); it != adj[u].end(); ++it)
        if (*it != p) {
            int na, nd;
            tie(na, nd) = dfs(*it, u, adj);
            nd++;
            if(nd > d) {
                d = nd;
                a = na;
            }
        } 
    return {a, d};
}
int dfs2(int u, int p, set<int> adj[]) {
    int s = 0;
    for (set<int>::iterator it = adj[u].begin(); it != adj[u].end(); ++it) {
        if (*it != p)
            s += dfs2(*it, u, adj) + 1;
    }
    return s;
}
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, m;
    cin >> n >> m;
    bool p[n];
    fill(p, p + n, 0);
    vector<int> pho(m);
    for (int i = 0; i < m; i++) {
        cin >> pho[i];
        p[pho[i]]=1;
    }
    set<int> adj[n];
    for (int i = 0; i < n-1; i++) {
        int a, b;
        cin >> a >> b;
        adj[a].insert(b);
        adj[b].insert(a);
    }
    vector<int> leafs;
    for (int i = 0; i < n; i++)
        if (adj[i].size() == 1)
            leafs.push_back(i);
    queue<int> q;
    for (int u : leafs)
        q.push(u);
    while(!q.empty()) {
        int u = q.front();
        q.pop();
        if (!p[u] && adj[u].size() == 1) {
            q.push(*adj[u].begin());
            adj[*adj[u].begin()].erase(u);
            adj[u].erase(adj[u].begin());
        }
    }
    int a = *pho.begin();
    pair<int, int> bp = dfs(a, -1, adj);
    pair<int, int> cp = dfs(bp.first, -1, adj);
    /* cout << "L " << l << ' ' << dia << '\n'; */
    cout << 2 * dfs2(a, -1, adj) - cp.second << '\n';
    return 0;
}