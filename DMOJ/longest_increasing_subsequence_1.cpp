// https://dmoj.ca/problem/dp1p3
#include <iostream>
#include <vector>
int search(std::vector<int> &v, int l, int r, int key)
{
	while (r - l > 1)
	{
		int m = l + (r - l) / 2;
		{
			if (v[m] >= key)
			{
				r = m;
			}
			else

			{
				l = m;
			}
		}
	}

	return r;
}

int main()
{
	std::ios::sync_with_stdio(0);
	std::cin.tie(0);
	int N;
	std::cin >> N;

	int length = 1;
	std::vector<int> tail(N, 0);
	
	int first;
	std::cin >> first;
	tail[0] = first;

	for (int i = 1; i < N; i++)
	{
		int current;
		std::cin >> current;
		
		if (current < tail[0])
		{
			tail[0] = current;
		}
		
		else if (current > tail[length - 1])
		{
			tail[length++] = current;
		}

		else
		{
			tail[search(tail, -1, length - 1, current)] = current;
		}

	}

	std::cout << length;

	return 0;
}

