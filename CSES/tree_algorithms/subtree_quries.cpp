// Subtree Queries
// https://cses.fi/problemset/task/1137
#include <bits/stdc++.h>
using namespace std;
void update(long long tree[], int n, int i, long long x) {
    while(i <= n) {
        tree[i] += x;
        i += i & (-i);
    }
}
long long query(long long tree[], int i) {
    long long res = 0;
    while(i > 0) {
        res += tree[i];
        i -= i & (-i);
    }
    return res;
}

long long sum(long long tree[], int a, int b) {
    if (a == 1) return query(tree, b);
    return query(tree, b) - query(tree, a - 1);
}

void dfs(int u, int p, vector<int> adj[], int ids[], int size[], int &i) {
    ids[u]=i;
    size[i]=1;
    for (int v : adj[u]) {
        if (v != p) {
            i++;
            dfs(v, u, adj, ids, size, i);
            size[ids[u]] += size[ids[v]];
        }
    }
}
int main () {
    cin.tie(0)->sync_with_stdio(0);
    int n, q;
    cin >> n >> q;
    int arr[n + 1];
    for (int i = 1; i <= n; i++)
        cin >> arr[i];
    vector<int> adj[n + 1];
    for (int i = 1; i < n; i++) {
        int a, b;
        cin >> a >> b;
        adj[a].push_back(b);
        adj[b].push_back(a);
    }
    int ids[n + 1];
    int size[n + 1];
    int x = 1;
    dfs(1, 0, adj, ids, size, x);
    long long tree[n + 1];
    fill(tree, tree + (n + 1), 0);
    for (int i = 1; i <= n; i++) update(tree, n, ids[i], arr[i]);
    while(q--) {
        int ind;
        cin >> ind;
        if (ind == 1) {
            int s;
            long long x;
            cin >> s >> x;
            update(tree, n, ids[s], x - arr[s]);
            arr[s]=x;
        } else {
            int s;
            cin >> s;
            cout << sum(tree, ids[s], ids[s] + size[ids[s]] - 1) << '\n';
        }
    }
    return 0;
}
