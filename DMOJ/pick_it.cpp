#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    while(true) {
        int n;
        cin >> n;
        if (!n)
            break;
        int num[n];
        for (int i = 0; i < n; i++)
            cin >> num[i];
        int dp[n][n];
        fill(*dp, *dp + n * n, 0);
        for (int gap = 2; gap < n; gap++)
            for (int i = 0; i < n - gap; i++)
                for (int m = i + 1; m < i + gap; m++)
                    dp[i][i + gap] = max(dp[i][i + gap], dp[i][m] + dp[m][i + gap] + num[i] + num[m] + num[i + gap]);
        cout << dp[0][n - 1] << '\n';
    }
    return 0;
}
