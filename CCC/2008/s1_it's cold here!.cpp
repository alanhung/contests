import java.util.Scanner;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Collections;

public class Main {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);

    HashMap<Integer, String> map = new HashMap<Integer, String>();
    ArrayList<Integer> temps = new ArrayList<Integer>();

    String city = "";
    int count = 0;
    do {
      city = sc.next();
      int t = sc.nextInt();
      map.put(t, city);
      temps.add(t);
    } while (!city.equals("Waterloo"));

    Collections.sort(temps);

    System.out.println(map.get(temps.get(0)));
    sc.close();
  }
}