#include <bits/stdc++.h>
using namespace std;
int main() {  
  cin.tie(0)->sync_with_stdio(0);
  int n;
  cin >> n;
  // for H counts
  int prefix[n + 1];
  int gcount = 0;
  int hcount = 0;
  char breed[n + 1];
  prefix[0] = 0;
  for (int i = 1; i <=n ; i++) {
    char c;
    cin >> c;
    breed[i] = c;
    prefix[i] = (c == 'H');
    prefix[i] += prefix[i-1];
    gcount += (c == 'G');
    hcount += (c == 'H');
  }
  int es[n+ 1];
  for(int i = 1; i <= n;i++)
    cin >> es[i];
  int hdp[n + 2];
  int gdp[n + 2];
  hdp[n+1]=0;
  gdp[n+1]=0;
  bool dp[n + 1];
  fill(dp, dp + n + 1, 0);
  for (int i = n; i >= 1; i--) {
    hdp[i] = hdp[i+1];
    gdp[i] = gdp[i+1];
    if (breed[i] == 'H') {
      int count = prefix[es[i]] - prefix[i-1];
      hdp[i] += (count == hcount || ((gdp[i] - gdp[es[i]+1]) > 0 ));
    } else {
      int count = (es[i] - i + 1) - (prefix[es[i]] - prefix[i-1]);
      gdp[i] += (count == gcount || ((hdp[i] - hdp[es[i]+1]) > 0));
    }
  }
  // cout << "DP\n";
  // for (int i = 1; i <= n; i++)
  //   cout << dp[i] << ' '; 
  // cout << '\n';
  cout << gdp[1] * hdp[1] << '\n';
  return 0;
}
