// Better Censor
// https://dmoj.ca/problem/regex2
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n;
    cin >> n;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');
    regex re("\\b[a-zA-Z0-9]{4}\\b");
    string line;
    while(n--) {
        getline(cin, line);
        cout << regex_replace(line, re, "****") << '\n';
    }
    return 0;
}
