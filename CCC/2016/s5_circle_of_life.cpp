// CCC '16 S5 - Circle of Life
// https://dmoj.ca/problem/ccc16s5
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    long long n;
    long long t;
    cin >> n >> t;
    bitset<100000> cell;
    for (int i = 0; i < n; i++) {
        char c;
        cin >> c;
        if (c == '1')
            cell.flip(i);
    }
    long long p = 1;
    while (t > 0) {
        if (t & 1) {
            bitset<100000> ncell;
            for (int i = 0; i < n; i++) {
                ncell[i] = cell[((i - p) % n + n) %n ] ^ cell[(i + p)%n];
            }
            cell = ncell;
        }
        p *= 2;
        t >>= 1;
    }
    for (int i = 0; i < n; i++)
        cout << cell[i];
    return 0;
}