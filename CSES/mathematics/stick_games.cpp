// Stick Games
// https://cses.fi/problemset/task/1729
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, k;
    cin >> n >> k;
    int arr[k];
    for (int i = 0; i < k; i++)
        cin >> arr[i];
    bool dp[n + 1];
    dp[0]=0;
    for (int i = 1; i <= n; i++) {
        dp[i]=0;
        for (int j = 0; j < k; j++) {
            if (dp[i]) break;
            dp[i] = (i - arr[j] >= 0) && (!dp[i - arr[j]]);
        }
    }
    for (int i = 1; i <= n; i++)
        cout << (dp[i] ? 'W' : 'L');
    return 0;
}
