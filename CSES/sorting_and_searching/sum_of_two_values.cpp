// Sum of Two Values
// https://cses.fi/problemset/task/1640
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, x;
    cin >> n >> x;
    pair<int, int> ns[n];
    for (int i = 0; i < n ; i++) {
        int temp;
        cin >> temp;
        ns[i] = {temp, i};
    }
    sort(ns, ns + n);
    int i = 0;
    int j = n - 1;
    while(i < j) {
        if (ns[i].first + ns[j].first > x)
            j--;
        else if (ns[i].first + ns[j].first < x)
            i++;
        else {
            cout << ns[i].second  + 1 << ' ' << ns[j].second + 1;
            return 0;
        }
    }
    cout << "IMPOSSIBLE";
    return 0;
}
