// CCC '01 S2 - Spirals - exact
#include <iostream>
#include <cmath>
#include <algorithm>
#include <string>

int main()
{
  int T;
  std::cin >> T;
  for (int total = 0; total < T; total++)
  {
    int x, y;
    std::cin >> x >> y;
    int diff = y - x + 1;
    int largestLength = std::to_string(y).length();
    int side = std::ceil(std::sqrt(diff));

    int spiral[side][side];
    std::fill(*spiral, *spiral + side * side, -1);

    int xPos;
    int yPos;

    if (side % 2 == 0)
    {
      xPos = side / 2 - 1;
      yPos = side / 2 - 1;
    }
    else
    {
      xPos = side / 2;
      yPos = side / 2;
    }

    int standard = 2;
    int count = 0;
    int direction = 0;

    for (int num = x; num <= y; num++)
    {
      spiral[yPos][xPos] = num;

      count++;

      if (direction == 0)
      {
        yPos++;
      }
      else if (direction == 1)
      {
        xPos++;
      }
      else if (direction == 2)
      {
        yPos--;
      }
      else
      {
        xPos--;
      }

      if (count == standard / 2 || count == standard)
      {
        direction = (direction + 1) % 4;
      }

      if (count == standard)
      {
        standard += 2;
        count = 0;
      }
    }
    bool leftEmtpy = false;
    int leftemptyCount = 0;
    bool rightEmpty = false;
    int rightEmptyCount = 0;

    for (int i = 0; i < side; i++)
    {
      if (spiral[i][0] == -1)
      {
        leftemptyCount++;
      }
      if (spiral[i][side - 1] == -1)
      {
        rightEmptyCount++;
      }
    }

    leftEmtpy = leftemptyCount == side ? true : false;
    rightEmpty = rightEmptyCount == side ? true : false;
    int minusSide = 0;
    int compareSide = side;

    if (leftEmtpy)
    {
      minusSide++;
      compareSide--;
    }
    if (rightEmpty)
    {
      compareSide--;
    }
    int maxLength[side];
    for (int i = 0; i < side; i++)
    {
      int m = 0;
      for (int j = 0; j < side; j++)
      {
        int current = std::to_string(spiral[j][i]).length();
        if (spiral[j][i] != -1)
        {
          if (current > m)
          {
            m = current;
          }
        }
      }
      maxLength[i] = m;
    }
    for (int i = 0; i < side; i++)
    {
      for (int j = 0; j < side; j++)
      {
        int current = spiral[i][j];
        if ((leftEmtpy && j == 0) || (rightEmpty && j == (side - 1)))
        {
          continue;
        }
        if (current != -1)
        {
          for (int k = 0; k < maxLength[j] - std::to_string(current).length(); k++)
          {
            std::cout << ' ';
          }
          std::cout << spiral[i][j];
          if (j - minusSide != (compareSide - 1))
          {
            std::cout << ' ';
          }
        }
        else
        {
          int length = maxLength[j];
          if (j - minusSide == (compareSide - 1))
          {
            length--;
          }
          for (int k = 0; k <= length; k++)
          {
            std::cout << ' ';
          }
        }
      }
      std::cout << '\n';
    }

    if (total != T - 1)
    {
      std::cout << '\n';
    }
  }
  return 0;
}