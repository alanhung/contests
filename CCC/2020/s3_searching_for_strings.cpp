// CCC '20 S3 - Searching for Strings
// https://dmoj.ca/problem/ccc20s3
#include <bits/stdc++.h>
using namespace std;
bool same(int nfreq[], int hfreq[]) {
    for (int i = 0; i < 26; i++) {
        if (nfreq[i] != hfreq[i])
            return false;
    }
    return true;
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    string n, h;
    cin >> n >> h;
    if (n.size() > h.size()) {
        cout << 0;
        return 0;
    }
    
    const long long p = 31;
    const long long m = 1e9 + 9;
    const long long a = 131;
    const long long b = 1e9 + 7;

    vector<long long> pw(h.size());
    pw[0]=1;
    for (int i = 1; i < (int)h.size(); i++)
        pw[i] = (pw[i-1]*p)%m;
    vector<long long> bw(h.size());
    bw[0]=1;
    for (int i = 1; i < (int)h.size(); i++)
        bw[i] = (bw[i-1]*a)%b;
    vector<long long> hsh(h.size());
    hsh[0]= h[0] - 'a' + 1;
    for (int i = 1; i < (int)h.size(); i++)
        hsh[i] = (hsh[i-1] + (h[i] - 'a' + 1) * pw[i] % m) % m;
    vector<long long> bsh(h.size());
    bsh[0]=h[0] - 'a' + 1;
    for (int i = 1; i < (int)h.size(); i++)
        bsh[i] = (bsh[i-1] + (h[i] - 'a' + 1) * bw[i] % b) % b;

    int nfre[26];
    fill(nfre, nfre + 26, 0);
    for (int i = 0; i < (int)n.size(); i++) nfre[n[i] - 97]++;

    int hfre[26];
    fill(hfre, hfre + 26, 0);
    for (int i = 0; i < (int)n.size(); i++) hfre[h[i] - 97]++;

    set<pair<long long, long long>> pos;
    if (same(nfre, hfre)) {
        pos.insert({hsh[n.size()-1] * pw[h.size() - n.size()] % m, bsh[n.size() - 1] * bw[h.size() - n.size()] % b });
        /* cout << "HA: " << hsh[n.size() - 1] * pw[h.size() - n.size()] % m << '\n'; */
        /* cout << "HB: " << bsh[n.size() - 1] * bw[h.size() - n.size()] % b << '\n'; */
    }
    for (int i = 0; i + n.size() < h.size(); i++) {
        hfre[h[i]-97]--;
        hfre[h[i + n.size()]-97]++;
        if (same(nfre, hfre)) {
            pos.insert({(hsh[i + n.size()] + m - hsh[i]) %m * pw[h.size() - (i + n.size()) - 1] %m, (bsh[i + n.size()] + b - bsh[i]) %b * bw[h.size() - (i + n.size()) - 1] %b});
            /* cout << i << ' ' << i + n.size() << '\n'; */
            /* cout << "HA: " << (hsh[i + n.size()] + m - hsh[i]) %m * pw[h.size() - (i + n.size()) - 1] %m << '\n'; */
            /* cout << "HB: " << (bsh[i + n.size()] + b - bsh[i]) %b * bw[h.size() - (i + n.size()) - 1] %b << '\n'; */
        }
    }
    /* for (set<pair<long long, long long>>::iterator it = pos.begin(); it != pos.end(); ++it) */
    /*     cout << it->first << ',' << it->second  << ' '; */
    /* cout << '\n'; */
    cout << pos.size() << '\n';
    return 0;
}