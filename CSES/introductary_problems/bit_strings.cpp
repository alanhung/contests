// Bit Strings
// https://cses.fi/problemset/task/1617
#include <bits/stdc++.h>
using namespace std;
unsigned long long modpow(unsigned long long base, unsigned long long exp, unsigned long long m) {
    unsigned long long res = 1;
    base %= m;
    while(exp > 0) {
        if (exp & 1) res = res * base % m;
        base = base * base % m;
        exp >>= 1;
    }
    return res;
}
int main() {
    unsigned long long n;
    cin >> n;
    cout << modpow(2, n, 1000000007ull);
    return 0;
}
