#include <iostream>
#include <limits>
#include <queue>
#include <vector>
using namespace std;
int main()
{
    cin.tie(0)->sync_with_stdio(0);
    int N;
    cin >> N;
    vector<int> adj[N];
    for (int i = 0; i < N; i++)
    {
        int M;
        cin >> M;
        for (int j = 0; j < M; j++)
        {
            int c;
            cin >> c;
            adj[i].push_back(c - 1);
        }
    }
    bool visited[N];
    int dist[N];
    fill(visited, visited + N, false);
    fill(dist, dist + N, 0);
    visited[0] = true;
    dist[0] = 1;
    queue<int> q;
    q.push(0);
    int shortest = numeric_limits<int>::max();
    bool all = true;
    while (!q.empty())
    {
        int u = q.front();
        q.pop();
        for (const int &n : adj[u])
        {
            if (!visited[n])
            {
                visited[n] = true;
                q.push(n);
                dist[n] = dist[u] + 1;
            }
        }
        if (adj[u].size() == 0)
        {
            shortest = min(shortest, dist[u]);
        }
    }
    for (int i = 0; i < N; i++)
    {
        if (!visited[i])
        {
            all = false;
            break;
        }
    }
    cout << (all ? 'Y' : 'N') << '\n';
    cout << shortest << '\n';
    return 0;
}