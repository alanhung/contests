// Exponentiation
// https://cses.fi/problemset/task/1095
#include <bits/stdc++.h>
#define MOD 1000000007ull
using namespace std;
unsigned long long modpow(unsigned long long base, unsigned long long exp, unsigned long long m) {
    unsigned long long res = 1;
    base %= m;
    while(exp > 0) {
        if (exp & 1) res = (res * base) % m;
        base = (base * base) % m;
        exp >>= 1;
    }
    return res;
}
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n;
    cin >> n;
    while(n--) {
        unsigned long long a, b;
        cin >> a >> b;
        cout << modpow(a, b, MOD) << '\n';
    }
    return 0;
}
