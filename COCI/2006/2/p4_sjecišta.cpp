#include <iostream>

using namespace std;

int b(int n, int k)
{
    int c[k + 1];
    fill(c, c + k + 1, 0);

    c[0] = 1;

    for (int i = 0; i <= n; i++)
    {
        for (int j = min(i, k); j > 0; j--)
        {
            c[j] = c[j] + c[j - 1];
        }

    }

    return c[k];
}

int main()
{
    int N;
    cin >> N;
    cout << b(N, 4);

    return 0;
}