#include <iostream>
#include <string>

int main()
{
  int start_day, days_in_month;
  std::cin >> start_day >> days_in_month;

  std::string result;

  result += "Sun Mon Tue Wed Thr Fri Sat\n";

  for (int i = 0; i < start_day - 1; i++)
  {
    result += "    ";
  }

  int count = start_day;
  int i = 1;

  do
  {
    std::string current = std::to_string(i);
    for (int i = 0; i < 3 - current.length(); i++)
    {
      result += " ";
    }
    result += current;

    if (count == 7)
    {
      result += "\n";
      count = 0;
    }
    else
    {
      result += " ";
    }
    i++;
    count++;

  } while (i <= days_in_month);

  result = result.substr(0, result.size() - 1);
  result += "\n";

  std::cout << result;

  return 0;
}