#include <iostream>
#include <map>
#include <string>
using namespace std;
int main()
{
    cin.tie(0)->sync_with_stdio(0);
    int N;
    cin >> N;
    string strs[N];
    for (int i = 0; i < N; i++)
        cin >> strs[i];
    map<string, string> fm;
    map<string, string> sm;
    bool good = true;
    for (int i = 0; i < N; i++)
    {
        string f;
        cin >> f;
        auto fit = fm.find(strs[i]);
        auto sit = sm.find(f);
        if (f == strs[i])
        {
            good = false;
            break;
        }
        if ((fit != fm.end()) ^ sit != (sm.end()))
        {
            good = false;
            break;
        }

        if (fit != fm.end() && sit != sm.end())
        {
            if (fit->first != sit->second || fit->second != sit->first)
            {
                good = false;
                break;
            }
        }
        fm.insert({f, strs[i]});
        sm.insert({strs[i], f});
    }
    if (good)
        cout << "good";
    else
        cout << "bad";
    return 0;
}