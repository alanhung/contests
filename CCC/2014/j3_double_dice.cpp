import java.util.Scanner;

public class Main {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);

    int n = sc.nextInt();
    int antoniaScore = 100;
    int davidScore = 100;

    for (int i = 0; i < n; i++) {
      int antonia = sc.nextInt();
      int david = sc.nextInt();
      
      if (antonia > david) {
        davidScore -= antonia;
      } else if (antonia < david) {
        antoniaScore -= david;
      }
    }
    System.out.println(antoniaScore);
    System.out.println(davidScore);
    sc.close();
  }
}