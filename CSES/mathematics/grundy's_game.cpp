// Grundy's Game
// https://cses.fi/problemset/task/2207
// https://oeis.org/A036685
int dp[1223];
#include <bits/stdc++.h>
using namespace std;
int mex(vector<int> &v) {
    sort(v.begin(), v.end());
    int x = 0;
    for (int i = 0; i < v.size(); i++) {
        if (v[i] == x)
            x++;
        else if (v[i] > x)
            break;
    }
    return x;
}
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int t;
    cin >> t;
    for (int i = 3; i <= 1222; i++) {
        vector<int> v;
        for (int j = 1; i - j > j; j++) {
            v.push_back(dp[j] ^ dp[i-j]);
        }
        dp[i] = mex(v);
    }
    while (t--) {
        int n;
        cin >> n;
        if (n >= 1223) cout << "first\n";
        else cout << (dp[n] ? "first" : "second") << '\n';
    }
    return 0;
}
