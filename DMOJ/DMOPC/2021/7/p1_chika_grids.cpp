#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int N, M;
    cin >> N >> M;
    int grid[N][M];
    bool g = true;
    for (int i = 0; i < N; i++)
        for (int j = 0; j < M; j++) 
            cin >> grid[i][j];
    if (!grid[0][0])
        grid[0][0] = 1;
    for (int i = 1; i < M; i++) {
        if (grid[0][i]) {
            if (grid[0][i] <= grid[0][i - 1]) {
                g = false;
                break;
            }
        } else
            grid[0][i] = grid[0][i - 1] + 1;
    }
    if (!g) {
        cout << -1 << '\n';
        return 0;
    }
    for (int i = 1; i < N; i++) {
        if (grid[i][0]) {
            if (grid[i][0] <= grid[i - 1][0]) {
                g = false;
                break;
            }
        } else
            grid[i][0] = grid[i - 1][0] + 1;
        for (int j = 1; j < M; j++) {
            if (grid[i][j]) {
                if (grid[i][j] <= grid[i - 1][j] || grid[i][j] <= grid[i][j - 1]) {
                    g = false;
                    break;
                }
            } else
                grid[i][j] = max(grid[i][j - 1], grid[i - 1][j]) + 1;
        }
        if (!g)
            break;
    }
    if (!g) {
        cout << -1 << '\n';
        return 0;
    }
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < M; j++) 
            cout << grid[i][j] << ' ';
        cout << '\n';
    }
    return 0;
}