// DMOPC '14 Contest 2 P2 - Cutting Logs
// https://dmoj.ca/problem/dmopc14c2p2
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int l;
    cin >> l;
    string s;
    cin >> s;
    int last = 0;
    int cnt = 0;
    vector<pair<int,int>> res;
    for (int i = 0; i < l; i++) {
        if (s[i] == 'X') {
            if (cnt) res.push_back({last, cnt});
            last = i + 1;
            cnt = 0;
        }
        else
            cnt++;
    }
    if (cnt) res.push_back({last, cnt});
    cout << res.size() << '\n';
    for (int i = 0; i < (int)res.size(); i++)
        cout << s.substr(res[i].first, res[i].second) << '\n';
    return 0;
}