// CCC '12 S1 - Don't pass me the ball!
// https://dmoj.ca/problem/ccc12s1
#include <bits/stdc++.h>
using namespace std;
int main() {
    int j;
    cin >> j;
    if (j < 4) {
        cout << 0;
        return 0;
    }
    cout << (j - 1) * (j - 2) * (j - 3) / 6;
    return 0;
}