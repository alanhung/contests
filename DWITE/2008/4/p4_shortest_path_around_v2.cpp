#include <iostream>
#include <limits>
#include <queue>
#include <vector>

int main()
{
    std::ios::sync_with_stdio(0);
    std::cin.tie(0);
    for (int round = 0; round < 5; round++)
    {
        std::vector<int> adj[64];
        char board[8][8];

        int start = -1;
        int end = -1;

        for (int i = 0; i < 8; i++)
        {

            for (int j = 0; j < 8; j++)
            {
                char c;
                std::cin >> c;
                board[i][j] = c;

                if (c == '#')
                {
                    continue;
                }
                else if (c == 'A')
                {
                    start = i * 8 + j;
                }
                else if (c == 'B')
                {
                    end = i * 8 + j;
                }

                if (j - 1 >= 0 && board[i][j - 1] != '#')
                {
                    adj[8 * i + j].push_back(8 * i + j - 1);
                    adj[8 * i + j - 1].push_back(8 * i + j);
                }

                if (i - 1 >= 0 && board[i - 1][j] != '#')
                {
                    adj[8 * i + j].push_back(8 * (i - 1) + j);
                    adj[8 * (i - 1) + j].push_back(8 * i + j);
                }

                if (j - 1 >= 0 && i - 1 >= 0 && board[i - 1][j - 1] != '#')
                {
                    adj[8 * i + j].push_back(8 * (i - 1) + j - 1);
                    adj[8 * (i - 1) + j - 1].push_back(8 * i + j);
                }

                if (j + 1 < 8 && i - 1 >= 0 && board[i - 1][j + 1] != '#')
                {
                    adj[8 * i + j].push_back(8 * (i - 1) + j + 1);
                    adj[8 * (i - 1) + j + 1].push_back(8 * i + j);
                }
            }
        }

        int dist[64];
        bool visited[64];
        std::fill(dist, dist + 64, std::numeric_limits<int>::max());
        std::fill(visited, visited + 64, false);
        dist[start] = 0;
        visited[start] = true;

        std::queue<int> q;

        q.push(start);

        while (!q.empty())
        {
            int s = q.front();
            q.pop();

            for (int i = 0; i < adj[s].size(); i++)
            {
                if (!visited[adj[s][i]])
                {
                    dist[adj[s][i]] = dist[s] + 1;
                    visited[adj[s][i]] = true;
                    q.push(adj[s][i]);

                    if (adj[s][i] == end)
                    {
                        break;
                    }
                }
            }
        }

        std::cout << dist[end] << '\n';

    }
    return 0;
}