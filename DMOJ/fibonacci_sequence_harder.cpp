// Fibonacci Sequence (Harder)
// https://dmoj.ca/problem/fibonacci2
#include <bits/stdc++.h>
using namespace std;
vector<vector<long long>> mul(vector<vector<long long>> const &a, vector<vector<long long>> const &b, long long m) {
    vector<vector<long long>>res(a.size(), vector<long long>(b[0].size()));
    for (int i = 0; i <(int)a.size(); i++) {
        for (int j = 0; j < (int)b[0].size(); j++) {
            res[i][j]=0;
            for (int k = 0; k < (int)a[0].size(); k++)
                res[i][j] = (res[i][j] + a[i][k] * b[j][k] % m) % m;
        }
    }
    return res;
}
vector<vector<long long>> modpow(vector<vector<long long>> base, string exp, long long m) {
    vector<vector<long long>> res(base.size(), vector<long long>(base.size()));
    for (int i = 0; i < (int)base.size(); i++)
        res[i][i] = 1;
    while (exp.size() > 0) {
        for (int i = 0; i < exp[exp.size()-1] - 48; i++)
            res = mul(res, base, m);
        vector<vector<long long>> temp = base;
        for (int i = 0; i < 9; i++)
            temp = mul(temp, base, m);
        base = temp;
        exp.pop_back();
    }
    return res;
}
int main(){
    string s;
    cin >> s;
    vector<vector<long long>> f = {{0, 1}, {1, 1}};
    f = modpow(f, s, 1000000007);
    cout << f[0][1] << '\n';
    return 0;
}

