#include <bits/stdc++.h>
using namespace std;

bool cy(int u, vector<int> adj[], bool stk[], bool visited[])
{
    if (!visited[u])
    {
        visited[u] = true;
        stk[u] = true;

        for (int v : adj[u])
        {
            if (stk[v])
                return true;
            else if (cy(v, adj, stk, visited))
                return true;
        }
    }
    stk[u] = false;
    return false;
}

int main()
{
    cin.tie(0)->sync_with_stdio(0);
    int N;
    cin >> N;
    vector<int> adj[N];
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            bool c;
            cin >> c;
            if (c)
                adj[i].push_back(j);
        }
    }
    bool visited[N];
    bool stk[N];
    fill(visited, visited + N, 0);
    fill(stk, stk + N, 0);
    for (int i = 0; i < N; i++)
    {
        if (cy(i, adj, stk, visited))
        {
            cout << "NO";
            return 0;
        }
    }
    cout << "YES";
    return 0;
}

