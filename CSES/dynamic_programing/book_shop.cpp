// Book Shop
// https://cses.fi/problemset/task/1158
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, x;
    cin >> n >> x;
    int wt[n];
    int val[n];
    for (int i = 0; i < n; i++) cin >> wt[i];
    for (int i = 0; i < n; i++) cin >> val[i];
    int dp[x + 1];
    fill(dp, dp + x + 1, 0);
    for (int i =0; i < n; i++) {
        for (int j = x; j >= wt[i]; j--) {
            dp[j] = max(dp[j], dp[j-wt[i]] + val[i]);
        }
    }
    cout << dp[x];
    return 0;
}
