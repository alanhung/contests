#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios::sync_with_stdio(0);
    cin.tie(0);

    int M, N;
    cin >> M >> N;

    int r = 0;
    int b = 0;
    int y = 0;

    for (int i = 0; i < N; i++)
    {
        int p = 1;

        for (int j = 0; j < M; j++)
        {
            char c;
            cin >> c;

            int n = 1;

            switch (c)
            {
            case 'R':
                n = 2;
                break;
            case 'U':
                n = 3;
                break;
            case 'Y':
                n = 5;
                break;
            case 'O':
                n = 10;
                break;
            case 'G':
                n = 15;
                break;
            case 'P':
                n = 6;
                break;
            case 'B':
                n = 30;
                break;
            }

            //cout << n << ' ' << p << '\n';
			//cout << n % 2 << ' ' << p % 2 << '\n';
            if (n % 2 == 0 && p % 2 != 0)
            {
                r++;
            }

            if (n % 3 == 0 && p % 3 != 0)
            {
                b++;
            }

            if (n % 5 == 0 && p % 5 != 0)
            {
                y++;
            }

            p = n;
            //cout << r << ' ' << y << ' ' << b;
        }
    }

    cout << r << ' ' << y << ' ' << b;

    return 0;
}