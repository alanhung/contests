#include <bits/stdc++.h>
using namespace std;
int main()
{
    cin.tie(0)->sync_with_stdio(0);
    int r, c;
    cin >> r >> c;
    char board[r][c];
    vector<pair<int, int>> dots;
    for (int i = 0; i < r; i++)
        for (int j = 0; j < c; j++)
        {
            cin >> board[i][j];
            if (board[i][j] == '.')
                dots.push_back({i, j});
        }

    bool visited[r][c];
    fill(*visited, *visited + r * c, 0);

    int cnt = 0;
    queue<pair<int, int>> q;

    for (auto dot : dots)
    {
        if (!visited[dot.first][dot.second])
        {
            cnt++;
            visited[dot.first][dot.second] = true;
            q.push({dot.first, dot.second});
            while (!q.empty())
            {
                int f = q.front().first;
                int s = q.front().second;
                q.pop();
                if (f - 1 >= 0 && !visited[f - 1][s] && board[f - 1][s] != 'X')
                {
                    visited[f - 1][s] = true;
                    q.push({f - 1, s});
                }
                if (f + 1 < r && !visited[f + 1][s] && board[f + 1][s] != 'X')
                {
                    visited[f + 1][s] = true;
                    q.push({f + 1, s});
                }
                if (s - 1 >= 0 && !visited[f][s - 1] && board[f][s - 1] != 'X')
                {
                    visited[f][s - 1] = true;
                    q.push({f, s - 1});
                }
                if (s + 1 < c && !visited[f][s + 1] && board[f][s + 1] != 'X')
                {
                    visited[f][s + 1] = true;
                    q.push({f, s + 1});
                }
            }
        }
    }
    cout << cnt;
    return 0;
}

