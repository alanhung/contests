#include <bits/stdc++.h>
using namespace std;
void f(int idx, char c[], int k) {
    if (idx > 3 || !k) {
        cout << c[0] << c[1] << c[2] << c[3] << '\n';
        return;
    }
    int cur = c[idx] - 97;
    for (int i = 0; i < 26; i++) {
        int d = min(abs(cur - i), 26 - abs(cur - i));
        if (k >= d) {
            int orig = c[idx];
            c[idx] = i + 97;
            f(idx + 1, c, k - d);
            c[idx] = orig;
        }
    }
}
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int K;
    char c[4];
    cin >> K >> c[0] >> c[1] >> c[2] >> c[3];
    f(0, c, K);
    return 0;
}