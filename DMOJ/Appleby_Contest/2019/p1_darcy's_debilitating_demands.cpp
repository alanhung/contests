// Appleby Contest '19 P1 - Darcy's Debilitating Demands
// https://dmoj.ca/problem/ac19p1
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int t;
    cin >> t;
    while(t--) {
        int n, a, b, c;
        cin >> n >> a >> b >> c;
        vector<int>sol(3, 0);
        if (n >= c) {
            n -= c;
            sol[2] = c;
        } else {
            sol[2] = n;
            n = 0;
        }
        if (n >= b) {
            n -= b;
            sol[1] = b;
        } else {
            sol[1] = n;
            n = 0;
        }
        if (n >= a) {
            n -= a;
            sol[0] = a;
        } else {
            sol[0] = n;
            n = 0;
        }
        if (!n) cout << sol[0] << ' ' << sol[1] << ' ' << sol[2] << '\n';
        else cout << -1 << '\n';
    }
    return 0;
}