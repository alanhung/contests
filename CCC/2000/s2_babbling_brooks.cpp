// CCC '00 S2 - Babbling Brooks
#include <iostream>
#include <vector>
#include <cmath>

int main()
{
  int initial;
  std::cin >> initial;

  std::vector<double> streams;

  for (int i = 0; i < initial; i++)
  {
    double stream;
    std::cin >> stream;
    streams.push_back(stream);
  }

  while (true)
  {
    int indicator;
    std::cin >> indicator;
    if (indicator == 77)
    {
      for (int i = 0; i < streams.size(); i++)
      {
        std::cout << (int)std::round(streams[i]) << ' ';
      }
      break;
    }
    else if (indicator == 99)
    {
      int splitIndex, percentage;
      std::cin >> splitIndex >> percentage;
      double original = streams[splitIndex - 1];
      streams[splitIndex - 1] = original * (percentage / 100.0);
      streams.insert(streams.begin() + splitIndex, original * ((100 - percentage) / 100.0));
    }
    else if (indicator == 88)
    {
      int joinIndex;
      std::cin >> joinIndex;
      streams[joinIndex - 1] += streams[joinIndex];
      streams.erase(streams.begin() + joinIndex);
    }
  }
  return 0;
}