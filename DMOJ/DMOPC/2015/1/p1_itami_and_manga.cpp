// DMOPC '15 Contest 1 P1 - Itami and Manga
// https://dmoj.ca/problem/dmopc15c1p1
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n;
    cin >> n;
    vector<string> b(n);
    vector<double> rating(n);
    for (int i = 0; i < n; i++)
        cin >> b[i] >> rating[i];
    cout << b[max_element(rating.begin(), rating.end()) - rating.begin()] << '\n';
    return 0;
}