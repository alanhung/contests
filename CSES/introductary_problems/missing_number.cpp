// Missing Number 
// https://cses.fi/problemset/task/1083
#include <bits/stdc++.h>
using namespace std;
int main() {
    int n;
    cin >> n;
    bool a[n];
    fill(a, a +n, 0);
    for (int i = 0; i < n - 1; i++) {
        int c;
        cin >> c;
        a[c - 1] = true;
    }
    int num;
    for (int i = 0; i < n; i++) {
        if (!a[i])
            num = i + 1;
    }
    cout << num;
    return 0;
}
