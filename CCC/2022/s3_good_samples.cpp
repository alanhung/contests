// CCC '22 S3 - Good Samples
// https://dmoj.ca/problem/ccc22s3
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, m;
    long long k;
    cin >> n >> m >> k;
    int ans[n + 1];
    k -= n;
    if (k < 0) {
        cout << -1;
        return 0;
    }
    ans[0]=0;
    for (int i = 1; i <= n; i++) {
        int val = min({(long long)m-1, (long long)i-1, k});
        if (i - val - 1 == 0) ans[i] = ans[i - 1] + 1;
        else ans[i] = ans[i - val - 1];
        k -= val;
    }
    if (k) {
        cout << -1;
        return 0;
    }
    for (int i = 1; i <= n; i++) cout << ans[i] << ' ';
    return 0;
}