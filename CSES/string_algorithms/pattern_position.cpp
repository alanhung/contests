// Pattern Position
// https://cses.fi/problemset/task/2104
#include <bits/stdc++.h>
using namespace std;

vector<int> suffix_construction(string s) {
    s += "$";
    int n = s.size();
    vector<int> p(n), c(n);
    iota(p.begin(), p.end(), 0);
    sort(p.begin(), p.end(), [&](int i, int j) { return s[i] < s[j]; } );
    c[p[0]] = 0;
    for (int i = 1; i < n; i++)
        c[p[i]] = c[p[i - 1]] + (s[p[i]] != s[p[i - 1]]);
    for (int h = 0; (1 << h) < n; h++) {
        vector<pair<int,int>> temp(n);
        for (int &i : p) 
            temp[i] = {c[i], c[(i + (1 << h)) % n]};
        sort(p.begin(), p.end(), [&](int i, int j) { return temp[i] < temp[j]; });
        c[p[0]] = 0;
        for (int i = 1; i < n; i++)
            c[p[i]] = c[p[i - 1]] + (temp[p[i]] != temp[p[i - 1]]);
    }
    p.erase(p.begin());
    return p;
}

int st[100000][20];
int main() {
    cin.tie(0)->sync_with_stdio(0);
    string s;
    cin >> s;
    vector<int> suffix = suffix_construction(s);
    int n = suffix.size();
    for (int i = 0; i < n; i++)
        st[i][0] = suffix[i];
    for (int x = 1; (1 << x) <= n; x++) {
        for (int i = 0; i + (1 << x) <= n; i++) {
            st[i][x] = min(st[i][x-1], st[i+(1<<(x-1))][x-1]);
        }
    }
    int m;
    cin >> m;
    while(m--) {
        string pattern;
        cin >> pattern;
        vector<int>::iterator l = suffix.begin();
        vector<int>::iterator r = suffix.end();
        for (int i = 0; i < pattern.size(); i++) {
            vector<int>::iterator nl = lower_bound(l, r, pattern[i], [&](int j, char val) { return i + j >= n || s[i + j] < val; } );
            vector<int>::iterator nr = upper_bound(l, r, pattern[i], [&](char val, int j) { return i + j < n && val < s[i + j]; } );
            l = nl;
            r = nr;
        }
        if (l != r) {
            int k = log2(r - l);
            cout << min(st[l-suffix.begin()][k], st[(r-suffix.begin())-(1<<k)][k]) + 1<< '\n';
        }
        else cout << -1 << '\n';
    }
    return 0;
}
