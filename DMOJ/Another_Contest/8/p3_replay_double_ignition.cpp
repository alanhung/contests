// Replay Double Ignition
// https://dmoj.ca/problem/acc8p3
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int m, q;
    cin >> m >> q;
    vector<int> fib = {1, 1};
    string digits = "11";
    while(fib.size() <= 2 || fib[fib.size()-1] != 1 || fib[fib.size() - 2] != 1) {
        fib.push_back((fib[fib.size()-1] + fib[fib.size()-2]) %m);
        digits.append(to_string(fib.back()));
    }
    fib.pop_back();
    fib.pop_back();
    digits.pop_back();
    digits.pop_back();
    while(q--) {
        long long n;
        cin >> n;
        cout << digits[(n - 1) % digits.size()] << '\n';
    }
    return 0;
}