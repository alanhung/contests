#include <bits/stdc++.h>
using namespace std;
void fi(long long N, vector<int> &v) {
    if (N > 9ll) {
        fi(N / 10ll, v);
    }
    v.push_back(N % 10ll);
}
int main() {
    long long N;
    cin >> N;
    vector<int> v;
    fi(N, v);
    int cnt = 0;
    for (size_t i = 0; i < v.size(); i++) {
        if (v[i] < 2) {
            break;
        } else if (v[i] > 3) {
            cnt += pow(2, v.size() - i);
            break;
        }

        if (v[i] == 2 && i == v.size() - 1) {
            cnt++;
        } else if (v[i] == 3) {
            if (i == v.size() - 1)
                cnt += 2;
            else
                cnt += pow(2, v.size() - i - 1);
        }
    }
    for (size_t i = 1; i < v.size(); i++) {
        cnt += pow(2, i);
    }
    cout << cnt;
    return 0;
}

