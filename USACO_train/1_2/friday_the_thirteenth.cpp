/*
TASK: friday
LANG: C++
ID: alan93h1
*/
#include <bits/stdc++.h>
using namespace std;
int main() {
  ofstream fout("friday.out");
  ifstream fin("friday.in");
  int n;
  fin >> n;
  int days[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30 ,31};
  // sunday, monday, tuesday, wednesday, thursday, friday, saturday
  int cnt[7];
  fill(cnt, cnt + 7, 0);
  int day = 13;
  for (int i = 0; i < n; i++) {
    int year = i + 1900; 
    // cout << "YEAR: " << year << '\n';
    if ((!(year % 4) && year % 100) || !(year % 400))
      days[1]=29;
    for (int j = 0; j < 12; j++) {
      cnt[day % 7]++;
      // cout << "MONTH: " << j << ' ' << day << ' ' << day % 7 << '\n';
      day += days[j];
    }
    if ((!(year % 4) && year % 100) || !(year % 400))
      days[1]=28;
  }
  fout << cnt[6] << ' ';
  for (int i = 0; i < 6;i++)
    fout << cnt[i] << " \n"[i==5]; 

  return 0;
}
