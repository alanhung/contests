// Nim Game 2
// https://cses.fi/problemset/task/1098
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int t;
    cin >> t;
    while(t--) {
        int n;
        cin >> n;
        int x = 0;
        while(n--) {
            int k;
            cin >> k;
            x ^= (k%4);
        }
        if (x) cout << "first\n";
        else cout << "second\n";
    }
    return 0;
}
