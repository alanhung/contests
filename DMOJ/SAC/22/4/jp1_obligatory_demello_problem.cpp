#include <bits/stdc++.h>
using namespace std;
int main() {
    string s;
    cin >> s;
    int cnt = 0;
    bool tbl[27];
    fill(tbl, tbl + 27, 0);
    tbl['a' - 97] = true;
    tbl['e' - 97] = true;
    tbl['i' - 97] = true;
    tbl['o' - 97] = true;
    tbl['u' - 97] = true;
    tbl['y' - 97] = true;
    for (int i = 0; i < s.size(); i++) {
        if (tbl[s[i] - 97])
            cnt++;
    }
    if (cnt >= 2)
        cout << "good\n";
    else
        cout << "bad\n";
    return 0;
}