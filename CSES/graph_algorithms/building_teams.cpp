// Building Teams
// https://cses.fi/problemset/task/1668
#include <bits/stdc++.h>
using namespace std;
int main() {
    int n, m;
    cin >> n >> m;
    vector<int> adj[n];
    for (int i = 0; i < m; i++) {
        int a, b;
        cin >> a >> b;
        adj[a-1].push_back(b-1);
        adj[b-1].push_back(a-1);
    }
    int vis[n];
    fill(vis, vis + n, 0);
    queue<int> q;
    bool g = true;
    for (int i = 0; i < n; i++) {
        if (!g)
            break;
        if (!vis[i]) {
            vis[i]=1;
            q.push(i);
            while(!q.empty()) {
                int u = q.front();
                q.pop();
                for (int v : adj[u]) {
                    if (vis[v] == 0) {
                        vis[v]=3-vis[u];
                        q.push(v);
                    } else if (vis[u] == vis[v]) {
                        g = false;
                        break;
                    }
                }
                if(!g)
                    break;
            }
        }
    }
    if (!g)
        cout << "IMPOSSIBLE";
    else {
        for (int i = 0; i < n; i++)
            cout << vis[i] << ' ';
    }
    return 0;
}
