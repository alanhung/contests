// Alchemy 2
// https://dmoj.ca/problem/yac4p2
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n;
    cin >> n;
    pair<int, int> arr[n];
    for (int i = 0; i < n; i++) {
        int x;
        cin >> x;
        arr[i] = {x, i + 1};
    }
    sort(arr, arr + n);
    vector<int> v[n + 1];
    int res[n];
    int j = 0;
    for (int i = 1; i <= n; i++) {
        while (arr[j].first == i) {
            v[i].push_back(arr[j].second);
            j++;
        }
    }
    for (int e : v[1]) {
        res[e-1]=e;
    }
    for (int i = 2; i <= n; i++) {
        if (v[i-1].empty()) {
            if (v[i].size() % i == 0) {
                for (int j = 0; j < v[i].size(); j += i) {
                    for (int k = j; k < j + i - 1; k++) {
                        res[v[i][k]-1] = v[i][k + 1];
                    }
                    res[v[i][j + i - 1]-1] = v[i][j];
                }
            } else {
                cout << -1;
                return 0;
            }
        } else for (int e : v[i]) res[e-1] = v[i-1][0];
    }
    for (int i = 0; i < n; i++)
        cout << res[i] << ' ';
    return 0;
}