#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios::sync_with_stdio(0);
    cin.tie(0);

    int N, Q;
    cin >> N;
    int x[N];
    int y[N];

	int maxX = 0;
	int minX = numeric_limits<int>::max();
	int maxY = 0;
	int minY = numeric_limits<int>::max();
	int maxCA[N];
	int minCA[N];
	int maxWA[N];
	int minWA[N];

    for (int i = 0; i < N; i++)
    {
        cin >> x[i];

		if (x[i] > maxX)
		{
			maxX = x[i];
		}

		if (x[i] < minX)
		{
			minX = x[i];
		}
    }

    for (int i = 0; i < N; i++)
    {
        cin >> y[i];

		if (y[i] > maxY)
		{
			maxY = y[i];
		}

		if (y[i] < minY)
		{
			minY = y[i];
		}
    }

	for (int i = 0; i < N; i++)
	{
		maxWA[i] = max(y[i], maxX);
		minWA[i] = max(y[i], minX);
		maxCA[i] = max(x[i], maxY);
		minCA[i] = max(x[i], minY);
	}

	cin >> Q;

	for (int i = 0; i < Q; i++)
	{
		int key, value;
		cin >> key >> value;
		value--;

		if (key == 1)
		{
			cout << maxCA[value] << '\n';
		}
		else if (key == 2)
		{
			cout << minCA[value] << '\n';
		}
		else if (key == 3)
		{
			cout << maxWA[value] << '\n';
		}
		else 
		{
			cout << minWA[value] << '\n';
		}
	}

    return 0;
}

