// Primes 3
// https://dmoj.ca/problem/primes3
#include <bits/stdc++.h>
using namespace std;
unsigned long long modpow(unsigned long long base, unsigned long long exp, unsigned long long m) {
    unsigned long long res = 1;
    base %= m;
    while(exp > 0) {
        if (exp & 1) res = (__uint128_t) res * base % m;
        base = (__uint128_t) base * base % m;
        exp>>=1;
    }
    return res;
}

bool check(unsigned long long n, unsigned long long a, unsigned long long d, int s) {
    unsigned long long x = modpow(a, d, n);
    if (x == 1 || x == n -1)
        return true;
    for (int r = 1; r < s; r++) {
        x = (__uint128_t) x * x % n;
        if (x == n - 1)
            return true;
    }
    return false;
}

bool rabinmiller(unsigned long long n) {
    if (n < 2)
        return false;

    int s = 0;
    unsigned long long d = n - 1;
    while (!(d & 1)) {
        d >>= 1;
        s++;
    }

    for (unsigned long long a : {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37}) {
        if (n == a)
            return true;
        if (!check(n, a, d, s))
            return false;
    }
    return true;
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    int t;
    cin >> t;
    while(t--) {
        unsigned long long n;
        cin >> n;
        if (rabinmiller(n)) cout << "PRIME\n";
        else cout << "NOT\n";
    }

    return 0;
}

