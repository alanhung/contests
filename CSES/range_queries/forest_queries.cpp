// Forest Queries
// https://cses.fi/problemset/task/1652
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, q;
    cin >> n >> q;
    int grid[n + 1][n + 1];
    fill(grid[0], grid[0] + (n + 1), 0);
    for (int i = 1; i <= n; i++) {
        grid[i][0] = 0;
        for (int j = 1; j <= n; j++) {
            char c;
            cin >> c;
            grid[i][j] = (c == '*');
            grid[i][j] += grid[i-1][j] + grid[i][j-1] - grid[i-1][j-1];
        }
    }
    while(q--) {
        int y1, x1, y2, x2;
        cin >> y1 >> x1 >> y2 >> x2;
        cout << grid[y2][x2] - grid[y2][x1-1] - grid[y1-1][x2] + grid[y1-1][x1-1] << '\n';
    }
    return 0;
}
