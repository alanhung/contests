#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int T;
    cin >> T;
    while(T--) {
        unsigned long long a, b, c, d;
        cin >> a >> b >> c >> d;
        if (b * d > a * d + b * c)
            cout << "YES\n";
        else
            cout << "NO\n";
    }
    return 0;
}