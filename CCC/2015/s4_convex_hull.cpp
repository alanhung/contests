// CCC '15 S4 - Convex Hull
// https://dmoj.ca/problem/ccc15s4
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int k, n, m;
    cin >> k >> n >> m;
    // node, time, wear
    vector<vector<tuple<int,int, int>>> adj(n, vector<tuple<int,int,int>>());
    for (int i = 0; i < m; i++) {
        int a, b, t, h;
        cin >> a >> b >> t >> h;
        adj[a-1].push_back({b-1, t, h});
        adj[b-1].push_back({a-1, t, h});
    }
    int a, b;
    cin >> a >> b;
    a--;
    b--;
    bool visited[n][k];
    fill(*visited, *visited + n * k, 0);
    int dist[n][k];
    fill(*dist, *dist + n * k, INT_MAX);
    // time, wear, node
    priority_queue<tuple<int,int,int>> q;
    q.push({0, 0, a});
    dist[a][0] = 0;
    while(!q.empty()) {
        int h = get<1>(q.top());
        int u = get<2>(q.top());
        q.pop();
        if (visited[u][h])
            continue;
        visited[u][h] = true;
        for (int i = 0; i < adj[u].size(); i++) {
            int v, t, nh;
            tie(v, t, nh) = adj[u][i];
            if (h + nh < k && dist[u][h] + t < dist[v][h + nh]) {
                dist[v][h + nh] = dist[u][h] + t;
                q.push({-dist[v][h + nh], h + nh, v});
            }
        }
    }
    bool found = false;
    int res = INT_MAX;
    for (int i = 0; i < k; i++) {
        if (visited[b][i]) {
            found = true;
            res = min(res, dist[b][i]);
        }
    }
    if (!found) cout << -1;
    else cout << res;
    return 0;
}