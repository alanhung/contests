grid = [
  [1, 2],
  [3, 4]
]

n = list(input())

for i in range(len(n)):
  if n[i] == "H":
    grid.reverse()
  else:
    grid[0].reverse()
    grid[1].reverse()

print(f"{grid[0][0]} {grid[0][1]}")
print(f"{grid[1][0]} {grid[1][1]}")