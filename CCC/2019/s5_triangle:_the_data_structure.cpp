// CCC '19 S5 - Triangle: The Data Structure
// https://dmoj.ca/problem/ccc19s5
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, k;
    cin >> n >> k;
    vector<vector<int>> tri(n);
    for (int i = 0; i < n; i++) {
        vector<int> row(i + 1);
        for (int j = 0; j < i + 1; j++)
            cin >> row[j];
        tri[i] = row;
    }
    int size = 1;
    while (size < k) {
        int newsize = min((int)ceil(size * 3.0 / 2), k);
        int diff = newsize - size;
        vector<vector<int>> newtri(tri.size() - diff);
        for (int i = 0; i < (int)tri.size() - diff; i++) {
            newtri[i] = vector<int>(i + 1);
            for (int j = 0; j < (int)tri[i].size(); j++) {
                newtri[i][j] = max({tri[i][j], tri[i + diff][j], tri[i + diff][j + diff]});
            }
        }
        tri = newtri;
        /* cout << "S " << size << ' ' << newsize << '\n'; */
        /* for (int i = 0; i < (int)tri.size(); i++) { */
        /*     for (int j = 0; j < (int)tri[i].size(); j++) */
        /*         cout << tri[i][j] << ' '; */
        /*     cout << '\n'; */
        /* } */
        /* cout << '\n'; */
        size = newsize;
    }
    long long sum = 0;
    for (int i = 0; i < (int)tri.size(); i++)
        for (int j = 0; j < (int)tri[i].size(); j++)
            sum += tri[i][j];
    cout << sum << '\n';
    return 0;
}