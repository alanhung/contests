#include <iostream>

int main() {
  std::ios::sync_with_stdio(0);
  std::cin.tie(0);
  int N, M;
  std::cin >> N >> M;

  int f[N];
  int s[M];

  for (int i = 0; i < N; i++) {
    std::cin >> f[i];
  }

  for (int i = 0; i < M; i++) {
    std::cin >> s[i];
  }

  int dp[N][M];
  dp[0][0] = f[0] == s[0] ? 1 : 0;

  for (int i = 1; i < M; i++) {
    dp[0][i] = f[0] == s[i] ? 1 : dp[0][i - 1];
  }

  for (int i = 1; i < N; i++) {
    dp[i][0] = f[i] == s[0] ? 1 : dp[i - 1][0];

    for (int j = 1; j < M; j++) {
      if (f[i] == s[j]) {
        dp[i][j] = 1 + dp[i - 1][j - 1];
      } else {
        dp[i][j] = std::max(dp[i][j - 1], dp[i - 1][j]);
      }
    }
  }

  std::cout << dp[N - 1][M - 1];

  return 0;
}
