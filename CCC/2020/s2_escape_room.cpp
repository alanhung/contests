#include <bits/stdc++.h>
using namespace std;
vector<int> factors[1000000];
int main()
{
    int n, m;
    cin >> n >> m;
    for (int i = 1; i * i <= 1000000; i++)
    {
        int sqr = i * i;
        if (i <= n && i <= m)
            factors[sqr - 1].push_back(m * (i - 1) + (i - 1));

        for (int j = sqr + i; j <= 1000000; j += i)
        {
            int s = j / i;
            if (i <= n && s <= m)
                factors[j - 1].push_back(m * (i - 1) + s - 1);

            if (s <= n && i <= m)
                factors[j - 1].push_back(m * (s - 1) + i - 1);
        }
    }
    int grid[n * m];

    for (int i = 0; i < n * m; i++)
        cin >> grid[i];
    bool visited[n * m];
    fill(visited, visited + n * m, false);

    queue<int> q;
    visited[0] = true;
    q.push(0);

    while (!q.empty())
    {
        int n = q.front();
        q.pop();
        for (vector<int>::iterator it = factors[grid[n] - 1].begin(); it != factors[grid[n] - 1].end(); ++it)
        {
            if (!visited[*it])
            {
                visited[*it] = true;
                q.push(*it);
                if (*it == (n * m - 1))
                    break;
            }
        }
    }

    cout << (visited[n * m - 1] ? "yes" : "no");
    return 0;
}