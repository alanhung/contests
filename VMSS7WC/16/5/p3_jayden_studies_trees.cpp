#include <bits/stdc++.h>
using namespace std;
void fill_height(int u, int p, int hei[], vector<int> adj[]) {
    hei[u] = 0;
    for (vector<int>::iterator it = adj[u].begin(); it != adj[u].end(); ++it) {
        if (*it == p)
            continue;
        fill_height(*it, u, hei, adj);
        hei[u] = max(hei[*it] + 1, hei[u]);
    }
}

int dia(int u, int p, int hei[], vector<int> adj[]) {
    int h1 = 0;
    int h2 = 0;
    for (vector<int>::iterator it = adj[u].begin(); it != adj[u].end(); ++it) {
        if (*it == p)
            continue;
        if (hei[*it] + 1 >= h1) {
            h2 = h1;
            h1 = hei[*it] + 1;
        } else if (hei[*it] + 1 > h2) {
            h2 = hei[*it] + 1;
        }
    }
    int maxd = 0;
    for (vector<int>::iterator it = adj[u].begin(); it != adj[u].end(); ++it) {
        if (*it == p)
            continue;
        int c = dia(*it, u, hei, adj);
        if (c > maxd) 
            maxd = c;
    }
    return max(maxd, h1 + h2);
}

int main() {
    int N;
    cin >> N;
    vector<int> adj[N];
    for (int i = 0; i < N - 1; i++) {
        int f, s;
        cin >> f >> s;
        f--;
        s--;
        adj[f].push_back(s);
        adj[s].push_back(f);
    }
    int hei[N];
    fill_height(0, -1, hei, adj);
    cout << dia(0, -1, hei, adj) << '\n';
    return 0;
}