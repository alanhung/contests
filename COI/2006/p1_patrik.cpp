#include <iostream>
#include <stack>
#include <utility>
using namespace std;
int main()
{
    cin.tie()->sync_with_stdio(0);
    int N, f;
    cin >> N >> f;
    stack<pair<int, int>> st;
    long long count = 0;
    st.push({f, 1});
    for (int i = 1; i < N; i++)
    {
        int c;
        cin >> c;
        if (c < st.top().first)
        {
            count++;
            st.push({c, 1});
        }
        else if (c == st.top().first)
        {
            count += st.top().second;
            st.top().second++;
            if (st.size() >= 2)
                count++;
        }
        else
        {
            while (!st.empty() && c > st.top().first)
            {
                count += st.top().second;
                st.pop();
            }
            if (!st.empty())
            {
                if (c == st.top().first)
                {
                    count += st.top().second;
                    st.top().second++;
                    if (st.size() >= 2)
                        count++;
                }
                else
                {
                    st.push({c, 1});
                    count++;
                }
            }
            else
                st.push({c, 1});
        }
    }
    cout << count;
    return 0;
}

