// Polygon Area
// https://cses.fi/problemset/task/2191
#include <bits/stdc++.h>
using namespace std;
long long cross(complex<long long>x, complex<long long> y) {
    return (conj(x) * y).imag();
}
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n;
    cin >> n;
    complex<long long> arr[n];
    for (int i = 0; i < n; i++) {
        long long a, b;
        cin >> a >> b;
        arr[i] = {a, b};
    }
    // shoelace formula
    long long res = 0;
    for (int i = 0; i < n - 1; i++)
        res += cross(arr[i], arr[i+1]);
    res += cross(arr[n-1], arr[0]);
    cout << abs(res);
    return 0;
}
