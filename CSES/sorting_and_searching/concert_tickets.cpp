// Concert Tickets
// https://cses.fi/problemset/task/1091
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, m;
    cin >> n >> m;
    multiset<int> tic;
    while(n--) {
        int cur;
        cin >> cur;
        tic.insert(cur);
    }
    while(m--) {
        int t;
        cin >> t;
        multiset<int>::iterator it = tic.upper_bound(t);
        if (it == tic.begin())
            cout << -1 << '\n';
        else {
            cout << *(--it) << '\n';
            tic.erase(it);
        }

    }
    return 0;
}
