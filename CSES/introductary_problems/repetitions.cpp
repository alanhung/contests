// Repetitions
// https://cses.fi/problemset/task/1069
#include <bits/stdc++.h>
using namespace std;
int main() {
    string s;
    cin >> s;
    int longest = 0;
    for (int i = 0; i < s.length(); i++) {
        int j = i + 1;
        while (j < s.length() && s[j] == s[i]) {
            j++;
        }
        longest = max(longest, j - i);
        i = j - 1;
    }
    cout << longest;
    return 0;
}

