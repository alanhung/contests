// CCC '18 S3 - RoboThieves
// https://dmoj.ca/problem/ccc18s3
#include <bits/stdc++.h>
using namespace std;

bool isbelt(char c) {
    return c == 'L' || c == 'R' || c == 'U' || c == 'D';
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, m;
    cin >> n >> m;
    char grid[n][m];
    for (int i = 0; i < n; i++)
        for (int j = 0; j < m; j++)
            cin >> grid[i][j];
    vector<pair<int,int>> adj[n][m];
    vector<pair<int,int>> dots;
    bool camera[n][m];
    fill(*camera, *camera + n * m, 0);
    pair<int,int> s;
    for (int i = 1; i < n -1; i++) {
        for (int j = 1; j < m - 1; j++) {
            if (grid[i][j] == '.' || grid[i][j] == 'S') {
                if (grid[i][j] == '.')
                    dots.push_back({i,j});
                else
                    s = {i, j};
                if (grid[i-1][j] != 'W')
                    adj[i][j].push_back({i-1, j});
                if (grid[i+1][j] != 'W')
                    adj[i][j].push_back({i+1, j});
                if (grid[i][j-1] != 'W')
                    adj[i][j].push_back({i, j-1});
                if (grid[i][j+1] != 'W')
                    adj[i][j].push_back({i, j+1});
            } else if (grid[i][j] == 'C') {
                for (int k = i; k < n; k++) {
                    if (grid[k][j] == 'W')
                        break;
                    if (!isbelt(grid[k][j])) camera[k][j]=1;
                }
                for (int k = i; k >=0 ; k--) {
                    if (grid[k][j] == 'W')
                        break;
                    if (!isbelt(grid[k][j])) camera[k][j]=1;
                }
                for (int k = j; k < m; k++) {
                    if (grid[i][k] == 'W')
                        break;
                    if (!isbelt(grid[i][k])) camera[i][k]=1;
                }
                for (int k = j; k >= 0; k--) {
                    if (grid[i][k] == 'W')
                        break;
                    if (!isbelt(grid[i][k])) camera[i][k]=1;
                }
            }
        }
    }
    int dist[n][m];
    fill(*dist, *dist + n * m, -1);
    if (!camera[s.first][s.second]) {
        queue<pair<int,int>> q;
        q.push(s);
        dist[s.first][s.second]=0;
        while(!q.empty()) {
            int i = q.front().first;
            int j = q.front().second;
            q.pop();
            // cout << i << ' ' << j << '\n';
            for (pair<int,int> v : adj[i][j]) {
                if (dist[v.first][v.second] == -1 && !camera[v.first][v.second]) {
                    dist[v.first][v.second] = dist[i][j] + 1;
                    bool p = true;
                    while (isbelt(grid[v.first][v.second])) {
                        if (grid[v.first][v.second] == 'R') {
                            if (dist[v.first][v.second + 1] == -1 && grid[v.first][v.second + 1] != 'W' && !camera[v.first][v.second + 1]) {
                                dist[v.first][v.second + 1] = dist[v.first][v.second];
                                v = {v.first, v.second + 1};
                            } else {
                                p = false;
                                break;
                            }
                        } else if (grid[v.first][v.second] == 'L') {
                            if (dist[v.first][v.second - 1] == -1 && grid[v.first][v.second - 1] != 'W' && !camera[v.first][v.second - 1]) {
                                dist[v.first][v.second - 1] = dist[v.first][v.second];
                                v = {v.first, v.second - 1};
                            } else {
                                p = false;
                                break;
                            }
                        } else if (grid[v.first][v.second] == 'U') {
                            if (dist[v.first-1][v.second] == -1 && grid[v.first - 1][v.second] != 'W' && !camera[v.first - 1][v.second]) {
                                dist[v.first - 1][v.second] = dist[v.first][v.second];
                                v = {v.first - 1, v.second};
                            } else {
                                p = false;
                                break;
                            }
                        } else if (grid[v.first][v.second] == 'D') {
                            if (dist[v.first+1][v.second] == -1 && grid[v.first + 1][v.second] != 'W' && !camera[v.first + 1][v.second]) {
                                dist[v.first + 1][v.second] = dist[v.first][v.second];
                                v = {v.first + 1, v.second};
                            } else {
                                p  =false;
                                break;
                            }
                        }
                    }
                    if (p) q.push(v);
                }
            }
        }
    }
    for (pair<int,int> &d : dots)
        cout << dist[d.first][d.second] << '\n';
    return 0;
}