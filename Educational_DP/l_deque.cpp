// Deque
#include <iostream>

int main()
{
    int N;
    std::cin >> N;

    long long dp[N][N];

    for (int i = 0; i < N; i++)
    {
        int c;
        std::cin >> c;
        dp[i][i] = c;
    }

    for (int gap = 1; gap < N; gap++)
    {
        for (int i = 0, j = gap; j < N; i++, j++)
        {
            dp[i][j] = std::max(dp[i][i] + -(dp[i + 1][j]), dp[j][j] + -(dp[i][j - 1]));
        }
    }
	std::cout << dp[0][N - 1];
    return 0;
}