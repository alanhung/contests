// Companies Queries 1
// https://cses.fi/problemset/task/1687
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, q;
    cin >> n>> q;
    int ancestor[n + 1][20];
    fill(*ancestor, *ancestor + (n + 1) * 20, 0);
    for (int i = 2; i <= n; i++) {
        cin >> ancestor[i][0];
    }
    for (int i = 1; (1 << i) <= n; i++)  {
        for (int j = 1; j <= n; j++) {
            ancestor[j][i] = ancestor[ancestor[j][i-1]][i-1];
        }
    }
    while(q--) {
        int a, b;
        cin >> a >> b;
        int i = 0;
        while(b >= 1) {
            if (b & 1) a = ancestor[a][i];

            i++;
            b >>= 1;
        }
        if (a) cout << a;
        else cout << -1;
        cout << '\n';
    }
    return 0;
}
