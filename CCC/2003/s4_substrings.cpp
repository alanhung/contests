// CCC '03 S4 - Substrings
// https://dmoj.ca/problem/ccc03s4
#include <bits/stdc++.h>
using namespace std;
vector<int> sc(string s) {
    s += "$";
    int n = s.size();
    vector<int> p(n), c(n);
    iota(p.begin(), p.end(), 0);
    sort(p.begin(), p.end(), [&](int i, int j) { return s[i] < s[j];});
    c[p[0]] = 0;
    for (int i = 1; i < n; i++)
        c[p[i]] = c[p[i-1]] + (s[p[i]] != s[p[i - 1]]);
    for (int h = 0; (1 << h) < n; h++) {
        vector<pair<int,int>> temp(n);
        for (int &i : p)
            temp[i] = {c[i], c[(i + (1 << h)) % n]};
        sort(p.begin(), p.end(), [&](int i, int j) { return temp[i] < temp[j]; });
        c[p[0]]=0;
        for (int i = 1; i < n; i++)
            c[p[i]] = c[p[i-1]] + (temp[p[i]] != temp[p[i - 1]]);
    }
    p.erase(p.begin());
    return p;
}
vector<int> lc(string const &s, vector<int> const &p) {
    int n = s.size();
    vector<int> rank(n), lcp(n-1, 0);
    for (int i = 0; i < n; i++)
        rank[p[i]] = i;
    int k = 0;
    for (int i = 0; i < n; i++) {
        if (rank[i] == n - 1) {
            k = 0;
            continue;
        }
        int j = p[rank[i] + 1];
        while (i + k < n && j + k < n && s[i + k] == s[j + k])
            k++;
        lcp[rank[i]] = k;
        if (k)
            k--;
    }
    return lcp;
}
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int t;
    cin >> t;
    while(t--) {
        string s;
        cin >> s;
        vector<int> suffix = sc(s);
        vector<int> lcp = lc(s, suffix);
        int sum = 0;
        int n = s.size();
        for (int i = 0; i < lcp.size(); i++)
            sum += lcp[i];
        cout << n * (n + 1) / 2 - sum + 1<< '\n';
    }
    return 0;
}