#include <algorithm>
#include <iostream>
#include <stack>
#include <string>
#include <utility>
#include <vector>
using namespace std;
int main()
{
    cin.tie()->sync_with_stdio(0);
    string s;
    getline(cin, s);
    vector<pair<string, int>> v;
    stack<int> st;
    for (int i = 0; i < s.size(); i++)
    {
        if (s[i] == '(')
            st.push(v.size());
        else if (s[i] == ')')
        {
            string m;
            while (i + 1 < s.size() && isdigit(s[i + 1]))
            {
                i++;
                m.push_back(s[i]);
            }
            int am = stoi(m);
            for (int j = st.top(); j < v.size(); j++)
            {
                v[j].second *= am;
            }
            st.pop();
        }
        else
        {
            string c(1, s[i]);
            string m;
            while (i + 1 < s.size() && islower(s[i + 1]))
            {
                i++;
                c.push_back(s[i]);
            }
            while (i + 1 < s.size() && isdigit(s[i + 1]))
            {
                i++;
                m.push_back(s[i]);
            }
            if (m.size() != 0)
            {
                v.push_back({c, stoi(m)});
            }
            else
            {
                v.push_back({c, 1});
            }
        }
    }
    sort(v.begin(), v.end());
    for (int i = 0; i < v.size(); i++)
    {
        int count = v[i].second;
        while (i + 1 < v.size() && v[i].first == v[i + 1].first)
        {
            i++;
            count += v[i].second;
        }
        cout << v[i].first;
        if (count > 1)
        {
            cout << count;
        }
    }
    return 0;
}

