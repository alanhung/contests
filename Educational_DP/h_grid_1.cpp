#include <iostream>

int main()
{
	std::ios::sync_with_stdio(0);
	std::cin.tie(0);
    int M = 1000000007;
    int H, W;
    std::cin >> H >> W;
    char grid[H][W];

    for (int i = 0; i < H; i++)
    {
        for (int j = 0; j < W; j++)
        {
            std::cin >> grid[i][j];
        }
    }

    int dp[W];
    std::fill(dp, dp + W, 0);
    dp[0] = 1;

    for (int i = 1; i < W; i++)
    {
        if (grid[0][i] != '#')
        {
            dp[i] = 1;
        }
        else
        {
            break;
        }
    }

    for (int i = 1; i < H; i++)
    {

        if (grid[i][0] != '#')
        {
            dp[0] = dp[0];
        }
        else
        {
            dp[0] = 0;
        }

        for (int j = 1; j < W; j++)
        {
            if (grid[i][j] != '#')
            {
                dp[j] = (dp[j] % M) + (dp[j - 1] % M);
            }
            else
            {
                dp[j] = 0;
            }
        }
    }

    std::cout << dp[W - 1] % M;

    return 0;
}