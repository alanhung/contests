#include <bits/stdc++.h>
using namespace std;
int main()
{
    cin.tie(0)->sync_with_stdio(0);
    int I, N, J;
    cin >> I >> N >> J;
    int dist[I];
    fill(dist, dist + I, 0);
    for (int i = 0; i < J; i++)
    {
        int f, s, w;
        cin >> f >> s >> w;
        dist[f - 1] += w;
        dist[s] -= w;
    }
    int result = 0;
    int accum = dist[0];
    if (accum < N)
        result++;
    for (int i = 1; i < I; i++)
    {
        accum += dist[i];
        if (accum < N)
            result++;
    }
    cout << result;
    return 0;
}

