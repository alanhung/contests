#include <iostream>
#include <vector>
using namespace std;
bool ts(int u, vector<int> adj[], bool visited[], bool recstack[])
{
    if (!visited[u])
    {
        visited[u] = true;
        recstack[u] = true;
        for (int &i : adj[u])
        {
            if (recstack[i])
                return true;
            if (ts(i, adj, visited, recstack))
                return true;
        }
    }
    recstack[u] = false;
    return false;
}
int main()
{
    cin.tie(0)->sync_with_stdio(0);
    int N, M;
    cin >> N >> M;
    vector<int> adj[N];
    for (int i = 0; i < N; i++)
    {
        int f, s;
        cin >> f >> s;
        adj[f - 1].push_back(s - 1);
    }
    bool visited[N];
    fill(visited, visited + N, false);
    bool recstack[N];
    fill(recstack, recstack + N, false);
    bool cycle = false;
    for (int i = 0; i < N; i++)
    {
        if (ts(i, adj, visited, recstack))
        {
            cycle = true;
            break;
        }
    }
    cout << (cycle ? 'N' : 'Y');
    return 0;
}
