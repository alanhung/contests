// Subarray Sum
// https://cses.fi/problemset/task/1660
#include <bits/stdc++.h>
using namespace std;
int main() {
    int n, x;
    cin >> n >> x;
    int cnt = 0;
    int sum = 0;
    queue<int> q;
    while(n--) {
        int k;
        cin >> k;
        if (k > x) {
            q = queue<int>();
            sum = 0;
            continue;
        }
        while(sum + k > x) {
            sum -= q.front();
            q.pop();
        }
        sum+=k;
        q.push(k);
        if (sum == x)
            cnt++;
    }
    cout << cnt;
    return 0;
}
