#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int X;
    cin >> X;
    vector<pair<string, string>> x;
    for (int i = 0; i < X; i++) {
        string a, b;
        cin >> a >> b;
        x.push_back({a, b});
    }
    int Y;
    cin >> Y;
    vector<pair<string, string>> y;
    for (int i = 0; i < Y; i++) {
        string a, b;
        cin >> a >> b;
        y.push_back({a, b});
    }
    int G;
    cin >> G;
    map<string, int> m;
    for (int i = 0; i < G; i++) {
        string a, b, c;
        cin >> a >> b >> c;
        m.insert({a, i});
        m.insert({b, i});
        m.insert({c, i});
    }
    int cnt = 0;
    for (int i = 0; i < X; i++) {
        auto ita = m.find(x[i].first);
        auto itb = m.find(x[i].second);
        if (ita->second != itb->second)
            cnt++;
    }
    for (int i = 0; i < Y; i++) {
        auto ita = m.find(y[i].first);
        auto itb = m.find(y[i].second);
        if (ita->second == itb->second)
            cnt++;
    }
    cout << cnt << '\n';
    return 0;
}