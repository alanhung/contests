import java.util.Scanner;
import java.util.HashMap;

public class Main {

  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    HashMap<Character, String> covert = new HashMap<Character, String>();
    covert.put('1', "1");
    covert.put('2', "2");
    covert.put('A', "2");
    covert.put('B', "2");
    covert.put('C', "2");
    covert.put('3', "3");
    covert.put('D', "3");
    covert.put('E', "3");
    covert.put('F', "3");
    covert.put('4', "4");
    covert.put('G', "4");
    covert.put('H', "4");
    covert.put('I', "4");
    covert.put('5', "5");
    covert.put('J', "5");
    covert.put('K', "5");
    covert.put('L', "5");
    covert.put('6', "6");
    covert.put('M', "6");
    covert.put('N', "6");
    covert.put('O', "6");
    covert.put('7', "7");
    covert.put('P', "7");
    covert.put('Q', "7");
    covert.put('R', "7");
    covert.put('S', "7");
    covert.put('8', "8");
    covert.put('T', "8");
    covert.put('U', "8");
    covert.put('V', "8");
    covert.put('9', "9");
    covert.put('W', "9");
    covert.put('X', "9");
    covert.put('Y', "9");
    covert.put('Z', "9");
    covert.put('0', "0");

    int t = Integer.parseInt(sc.nextLine());
    
    for (int i = 0; i < t; i++) {
      String rawPhone = sc.nextLine();
      String phone = String.join("", rawPhone.toUpperCase().split("-")).substring(0, 10);
      String result = "";
      for (int j = 0; j < phone.length(); j++) {
        char c = phone.charAt(j);
        result += covert.get(c);
      }
      System.out.println(result.replaceAll("(\\d{3})(\\d{3})(\\d{4})", "$1-$2-$3"));
    }

  }
}