// An Animal Contest 7 P2 - Squirrel Structures
// https://dmoj.ca/problem/aac7p2
#include <bits/stdc++.h>
using namespace std;
void dfs(int u, int p, vector<int> adj[], vector<int> nadj[], int size[]) {
    for (int v : adj[u]) {
        if (v != p) {
            if (p != -1)
                nadj[p].push_back(v);
            dfs(v, u, adj, nadj, size);
            if (p != -1)
                size[p] += size[v];
        }
    }
}
void bfs(int source, vector<int> nadj[]) {
    queue<int> q;
    q.push(source);
    while(!q.empty()) {
        int u = q.front();
        q.pop();
        for (int v : nadj[u]) {
            cout << u + 1 << ' ' << v + 1 << '\n';
            q.push(v);
        }
    }
}
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n;
    cin >> n;
    vector<int> adj[n];
    for (int i = 0; i < n - 1; i++) {
        int u, v;
        cin >> u >> v;
        adj[u-1].push_back(v-1);
        adj[v-1].push_back(u-1);
    }
    vector<int> nadj[n];
    int size[n];
    fill(size, size + n, 1);
    dfs(0, -1, adj, nadj, size);
    cout << 1 + adj[0].size() << '\n';
    cout << size[0] << '\n';
    bfs(0, nadj);
    for (int u : adj[0]) {
        cout << size[u] << '\n';
        bfs(u, nadj);
    }
    return 0;
}