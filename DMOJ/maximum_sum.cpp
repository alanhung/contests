#include <iostream>
#include <vector>

int main()
{
	std::ios::sync_with_stdio(0);
	std::cin.tie(0);
	int N;
	std::cin >> N;

	int dp[N];

	int f, s, t;
	std::cin >> f >> s >> t;

	dp[0] = f;
	dp[1] = std::max(f, s);
	dp[2] = std::max(f + t, s);

	for (int i = 3; i < N; i++)
	{
		int current;
		std::cin >> current;
		dp[i] = std::max(dp[i - 2], dp[i - 3]) + current;
	}

	std::cout << std::max(dp[N - 1], dp[N -2]);

}

