// CCC '19 S2 - Pretty Average Primes
// https://dmoj.ca/problem/ccc19s2
#include <bits/stdc++.h>
using namespace std;
void sieve(bool prime[], int n) {
    fill(prime, prime + n + 1, 1);
    for (int p = 2; p * p <= n; p++) {
        if (prime[p]) {
            for (int i = p * p; i <= n; i += p)
                prime[i] = 0;
        }
    }
}
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int T;
    cin >> T;
    bool prime[2000001];
    sieve(prime, 2000000);
    while(T--) {
        int n;
        cin >> n;
        // n = (A + B) / 2 => n = (2l + 1 + 2k + 1) / 2 => n = l + k + 1
        for (int i = 1; i <= (n - 1) / 2; i++ ) {
            if (prime[2 * i + 1] && prime[2 * (n - 1 - i) + 1]) {
                cout << 2 * i + 1 << ' ' << 2 * (n - 1 - i) + 1 << '\n';
                break;
            }
        }
    }
    return 0;
}