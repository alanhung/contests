// Tree Distance 1
// https://cses.fi/problemset/task/1132
// https://usaco.guide/problems/cses-1132-tree-distances-i/solution
#include <bits/stdc++.h>
using namespace std;
void dfs1(int u, int p, vector<int> adj[], int fmax[], int smax[], int child[]) {
    fmax[u]=0;
    smax[u]=0;
    for (int v : adj[u]) {
        if (v != p) {
            dfs1(v, u, adj, fmax, smax, child);
            if (fmax[v] + 1 > fmax[u]) {
                smax[u] = fmax[u];
                fmax[u] = fmax[v] + 1;
                child[u]=v;
            } else if (fmax[v] + 1 > smax[u]) {
                smax[u] = fmax[v]+1;
            }
        }
    }
}
void dfs2(int u, int p, vector<int> adj[], int fmax[], int smax[], int child[]) {
    for (int v : adj[u]) {
        if (v != p) {
            if (child[u] == v) {
                if (smax[u] + 1 > fmax[v]) {
                    smax[v] = fmax[v];
                    fmax[v] = smax[u] + 1;
                    child[v]=u;
                } else {
                    smax[v] = max(smax[v], smax[u] + 1);
                }
            } else {
                smax[v] = fmax[v];
                fmax[v] = fmax[u] + 1;
                child[v]=u;
            }
            dfs2(v, u, adj, fmax, smax, child);
        }
    }
}
int main () {
    cin.tie(0)->sync_with_stdio(0);
    int n;
    cin >> n;
    vector<int> adj[n];
    for (int i = 1; i < n; i++) {
        int a, b;
        cin >> a >> b;
        adj[a-1].push_back(b-1);
        adj[b-1].push_back(a-1);
    }
    int fmax[n];
    int smax[n];
    int child[n];
    dfs1(0, -1, adj, fmax, smax, child);
    dfs2(0, -1, adj, fmax, smax, child);
    for (int i = 0; i < n; i++) {
        cout << fmax[i] << ' ';
    }
    return 0;
}
