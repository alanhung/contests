/*
ID: alan93h1
LANG: C++
TASK: ride
*/
#include<bits/stdc++.h>
using namespace std;
int main() {
  string comet, group;
  ofstream fout("ride.out");
  ifstream fin("ride.in");
  fin >> comet >> group;
  int c = 1;
  int g = 1;
  for (char s: comet) {
    int n = s- 'A' + 1;
    c*= n;
    c%= 47;
  }
  for (char s: group) {
    int n = s - 'A' + 1;
    g *= n;
    g %= 47;
  }
  if (c == g)
    fout << "GO\n";
  else
    fout << "STAY\n";
  return 0;
}
