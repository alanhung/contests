// DMOPC '15 Contest 1 P3 - Itami and Cipher
// https://dmoj.ca/problem/dmopc15c1p3
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    string s, t;
    cin >> s >> t;
    int ma = 27;
    for (int i = 0; i < (int)s.size(); i++) {
        bool f = true;
        int shift = (s[i] - t[0] + 26) % 26;
        for (int j = 1; j < (int)t.size(); j++) {
            if ( (s[i + j] - t[j] + 26) % 26 != shift ) {
                f = 0;
                break;
            }
        }
        if (f) ma = min(ma, shift);
    }
    cout << ma << '\n';
    for (int i = 0; i  < (int)s.size(); i++)
        cout << (char)((s[i] - 97 - ma + 26) % 26 + 97);
    cout << '\n';
    return 0;
}