#include <iostream>

int main()
{
  int a, b, c;
  std::cin >> a >> b >> c;

  if (a == b)
  {
    std::cout << c - a << std::endl;
  }
  else if (a == c)
  {
    std::cout << b - a << std::endl;
  }
  else
  {
    std::cout << a - b << std::endl;
  }
  return 0;
}

