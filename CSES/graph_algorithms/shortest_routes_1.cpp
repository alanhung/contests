// Shortest Routes 1
// https://cses.fi/problemset/task/1671
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, m;
    cin >> n >> m;
    vector<pair<long long, long long>> adj[n];
    for (int i = 0; i < m; i++) {
        int a, b ,c;
        cin >> a >> b >> c;
        adj[a-1].push_back({b-1, c});
    }
    priority_queue<pair<long long, long long>> q;
    bool v[n];
    fill(v, v+n, 0);
    long long dist[n];
    fill(dist, dist+n, LLONG_MAX);
    dist[0]=0;
    q.push({0, 0});
    while(!q.empty()) {
        int u = q.top().second;
        q.pop();
        if (v[u])
            continue;
        v[u]=1;
        for (int i = 0; i < adj[u].size(); i++) {
            if (dist[u] + adj[u][i].second < dist[adj[u][i].first]) {
                dist[adj[u][i].first] = dist[u] + adj[u][i].second;
                q.push({-dist[adj[u][i].first], adj[u][i].first});
            }
        }
    }
    for (int i = 0; i < n; i++) {
        cout << dist[i] << ' ';
    }
    return 0;
}
