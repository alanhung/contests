#include <bits/stdc++.h>
using namespace std;
struct Node {
    int v;
    Node* n;
    Node* p;
};
int main() {
    int N, Q;
    cin >> N >> Q;
    Node a[N];
    Node* idx[N];
    for (int i = 0 ; i < N; i++) {
        a[i] = Node{.v = i + 1};
        idx[i] = &a[i];
    }
    for (int i = 0; i < N - 1; i++) {
        a[i].n = &a[i + 1];
        a[i + 1].p = &a[i];
    }
    Node start = Node{.v= -1, .n = &a[0], .p = NULL};
    a[0].p = &start;
    Node end = Node{.v = -1, .n = NULL, .p = &a[N - 1]};
    a[N - 1].n = &end;
    while(Q--) {
        int l, r, k;
        cin >> l >> r >> k;
        l--;
        r--;
        if (k) {
            k--;
            idx[l]->p->n = idx[r]->n;
            idx[r]->n->p = idx[l]->p;

            idx[k]->n->p = idx[r];
            idx[r]->n = idx[k]->n;

            idx[l]->p = idx[k];
            idx[k]->n = idx[l];
        } else {
            idx[l]->p->n = idx[r]->n;
            idx[r]->n->p = idx[l]->p;

            start.n->p = idx[r];
            idx[r]->n = start.n;

            start.n = idx[l];
            idx[l]->p = &start;
        }
    }
    Node* it = start.n;
    while(it->v != -1) {
        cout << it->v << ' ';
        it = it->n;
    }
    cout << '\n';
    return 0;
}