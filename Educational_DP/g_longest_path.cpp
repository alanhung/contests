#include <iostream>
#include <stack>
#include <vector>

using namespace std;

void ts(int u, bool visited[], stack<int> &st, vector<int> adj[])
{
    visited[u] = true;

    for (int i = 0; i < adj[u].size(); i++)
    {
        if (!visited[adj[u][i]])
        {
            ts(adj[u][i], visited, st, adj);
        }
    }

    st.push(u);
}

int main()
{
    ios::sync_with_stdio(0);
    cin.tie(0);
    int N, M;
    cin >> N >> M;

    vector<int> adj[N];

    for (int i = 0; i < M; i++)
    {
        int x, y;
        cin >> x >> y;
        adj[x - 1].push_back(y - 1);
    }

    int m = 0;
    int dp[N];
    fill(dp, dp + N, 0);

    bool visited[N];
    fill(visited, visited + N, false);

    stack<int> st;

    for (int i = 0; i < N; i++)
    {
        if (!visited[i])
        {
            ts(i, visited, st, adj);
        }
    }

    while (!st.empty())
    {
        int u = st.top();
        st.pop();

        if (adj[u].size() != 0)
        {
            for (int i = 0; i < adj[u].size(); i++)
            {
                dp[adj[u][i]] = max(dp[adj[u][i]], 1 + dp[u]);
            }
        }
        else
        {
            m = max(m, dp[u]);
        }
    }

    cout << m;

    return 0;
}