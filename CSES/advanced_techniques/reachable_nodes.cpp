// Reachable Nodes
// https://cses.fi/problemset/task/2138/
#include <bits/stdc++.h>
using namespace std;
#pragma GCC target("popcnt")
void tsort(int u, vector<int> adj[], vector<int> &stk, bool visited[]) {
    for (int v : adj[u]) {
        if (!visited[v]) {
            tsort(v, adj, stk, visited);
            visited[v]=true;
        }
    }
    stk.push_back(u);
}
int main() {
    int n, m;
    cin >> n >> m;
    vector<int> adj[n];
    while(m--) {
        int a, b;
        cin >> a >> b;
        adj[a-1].push_back(b-1);
    }
    vector<int> stk;
    bool visited[n];
    fill(visited, visited + n, 0);
    for (int i = 0; i < n; i++) {
        if (!visited[i]) {
            visited[i]=1;
            tsort(i, adj, stk, visited);
        }
    }
    bitset<50000> reach[n];
    for (int i : stk) {
        reach[i][i] = 1;
        for (int u : adj[i]) {
            reach[i] |= reach[u];
        }
    }
    for (int i = 0; i < n; i++)
        cout << reach[i].count() << ' ';
    return 0;
}
