#include <iostream>
#include <string>

int main()
{
    std::ios::sync_with_stdio(0);
    std::cin.tie(0);
    std::string inputs;
    std::getline(std::cin, inputs);

    int sum = 0;

    for (int i = 0; i < inputs.size(); i++)
    {
        std::string pos = "";
        std::string neg = "";

        while (std::isdigit(inputs[i]))
        {
            pos += inputs[i];
            i++;
        }

        if (inputs[i] == '-')
        {
            i++;

            while (std::isdigit(inputs[i]))
            {
                neg += inputs[i];
                i++;
            }
        }

		if (!pos.empty())
		{
			sum += std::stoi(pos);
		}

		if (!neg.empty())
		{
			sum -= std::stoi(neg);
		}
    }

    std::cout << sum;

    return 0;
}