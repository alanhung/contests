#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, k, x;
    long long d, p;
    cin >> n >> d >> k >> x;
    long long a[n];
    for (int i = 0; i < n; i++)
        cin >> a[i];
    cin >> p;
    int cnt = 0;
    for (int i = 0; i < n; i++) {
        if (a[i] >= p) {
            long long s = a[i];
            while(cnt <= k) {
                s = s * (100 - x) / 100;
                cnt++;
                if (s < p)
                    break;
            }
            if (cnt > k)
                break;
        }
    }
    cout << (cnt <= k ? "YES" : "NO") << '\n';
    return 0;
}