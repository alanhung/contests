#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int N;
    cin >> N;
    bool grid[2][N];
    for (int i = 0; i < 2; i++)
        for (int j = 0; j < N; j++) {
            char c;
            cin >> c;
            grid[i][j] = (c == '.');
        }
    vector<pair<int,int>> rooms;

    bool visited[2][N];
    fill(*visited, *visited + 2 * N, 0);
    queue<pair<int,int>> q;
    bool g = false;
    for (int j = 0; j < N; j++) {
        for (int i = 0; i < 2; i++) {
            if (grid[i][j] && !visited[i][j]) {
                int left = j;
                int right = j;
                q.push({i, j});
                visited[i][j]=1;
                while(!q.empty()) {
                    int r = q.front().first;
                    int c = q.front().second;
                    q.pop();
                    // right
                    if (c + 1 < N && grid[r][c+1] && !visited[r][c+1]) {
                        visited[r][c+1]=1;
                        q.push({r, c+1});
                        right=max(right, c + 1);
                    }
                    if (r) {
                        // up
                        if (grid[r-1][c] && !visited[r-1][c]) {
                            visited[r-1][c]=1;
                            q.push({r-1, c});
                        }
                        // right up
                        if (c + 1 < N && grid[r-1][c+1] && !visited[r-1][c+1]) {
                            visited[r-1][c+1]=1;
                            q.push({r-1, c+1});
                            right=max(right, c + 1);
                        }
                    } else {
                        // down
                        if (grid[r+1][c] && !visited[r+1][c]) {
                            visited[r+1][c]=1;
                            q.push({r+1, c});
                        }
                        // right down
                        if (c + 1 < N && grid[r+1][c+1] && !visited[r+1][c+1]) {
                            visited[r+1][c+1]=1;
                            q.push({r+1, c+1});
                            right=max(right, c + 1);
                        }
                    }
                }
                rooms.push_back({left, right});
                if (rooms.size() == 4) {
                    g = true;
                    break;
                }
            }
        }
        if (g)
            break;
    }
    /* cout << "ROOM: " << rooms.size() << '\n'; */
    /* for (int i = 0; i < rooms.size(); i++) */
    /*     cout << "LEFT " << rooms[i].first << " RIGHT " << rooms[i].second << '\n'; */
    if (rooms.size() >= 4)
        cout << "NO";
    else if (rooms.size() == 1)
        cout << "YES";
    else {
        int sep = 0;
        for (int i = 0; i < rooms.size() - 1; i++) {
            sep += (rooms[i+1].first - rooms[i].second - 1);
        }
        if (sep > 2)
            cout << "NO";
        else
            cout << "YES";
    }
    cout << '\n';
    return 0;
}

