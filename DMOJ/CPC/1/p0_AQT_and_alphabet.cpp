#include <bits/stdc++.h>
using namespace std;
int main()
{
    cin.tie(0)->sync_with_stdio(0);
    bool dp[26];
    fill(dp, dp + 26, false);
    for (int i = 0; i < 5; i++)
    {
        char c;
        cin >> c;
        dp[c - 97] = true;
    }
    for (int i = 0; i < 26; i++)
    {
        if (!dp[i])
        {
            cout << (char)(i + 97);
            break;
        }
    }
    return 0;
}

