#include <bits/stdc++.h>
using namespace std;
#define MOD 1000000007
void mul(unsigned long long a[2][2], unsigned long long b[2][2], unsigned long long res[2][2]) {
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            res[i][j]= 0;
            for (int k = 0; k < 2; k++) {
                res[i][j] = (res[i][j] + a[i][k] * b[k][j] % MOD) % MOD;
            }
        }
    }
}
void pow(unsigned long long m[2][2], unsigned long long N, unsigned long long res[2][2]) {
    if (N == 1) {
        res[0][0] = m[0][0];
        res[0][1] = m[0][1];
        res[1][0] = m[1][0];
        res[1][1] = m[1][1];
        return;
    }
    unsigned long long temp[2][2];
    pow(m, N / 2, temp);
    mul(temp, temp, res);
    if (N & 1) {
        mul(res, m, temp);
        res[0][0] = temp[0][0];
        res[0][1] = temp[0][1];
        res[1][0] = temp[1][0];
        res[1][1] = temp[1][1];
    }
}
int main() {
    cin.tie(0)->sync_with_stdio(0);
    unsigned long long N;
    cin >> N;
    unsigned long long m[2][2] = {{1, 1}, {1, 0}};
    unsigned long long res[2][2];
    pow(m, N, res);
    cout << res[0][1];
    return 0;
}



