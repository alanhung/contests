// Counting Necklaces
// https://cses.fi/problemset/task/2209
#include <bits/stdc++.h>
using namespace std;
#define MOD 1000000007ll
long long modpow(long long base, long long exp, long long m) {
    long long res = 1;
    while (exp > 0) {
        if (exp & 1) res = res * base % m;
        base = base * base % m;
        exp >>= 1;
    }
    return res;
}
int main () {
    long long n, m;
    cin >> n >> m;
    long long res = 0;
    // burnside's lemma
    for (int k = 0; k < n; k++) {
        res = (res + modpow(m, gcd((long long)k, n), MOD)) % MOD;
    }
    cout << res * modpow(n, MOD-2, MOD) % MOD;
    return 0;
}
