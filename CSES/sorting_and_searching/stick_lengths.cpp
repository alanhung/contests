// Stick Lengths
// https://cses.fi/problemset/task/1074
#include <bits/stdc++.h>
using namespace::std;
int main() {
    int n;
    cin >> n;
    int ns[n];
    for (int i =0; i < n; i++)
        cin >> ns[i];
    sort(ns, ns + n);
    int med = ns[n/2];
    long long sum = 0;
    for (int i = 0; i < n; i++)
        sum += abs(med-ns[i]);
    cout << sum;
    return 0;
}
