#include <bits/stdc++.h>
using namespace std;
int main() {
    int A, B, N;
    cin >> A >> B >> N;
    int pre[] = {0, 990, 1010, 1970, 2030, 2940, 3060, 3930, 4060, 4970, 5030, 5990, 6010, 7000};
    int cities[14 + N];
    for (int i = 0; i < 14; i++)
        cities[i] = pre[i];
    for (int i = 14; i < N + 14; i++) {
        cin >> cities[i];
    }
    sort(cities, cities + N + 14);
    int tbl[7001];
    int last = 0;
    for (int i = 0; i < 14 + N; i++) {
        fill(tbl + last, tbl + cities[i] + 1, i);
        last = cities[i] + 1;
    }
    unsigned long long dp[14 + N];
    fill(dp, dp + 14 + N, 0);
    dp[0] = 1;
    for (int i = 0; i < 14 + N; i++) {
        if (!dp[i])
            continue;
        int f = cities[i] + A > 7000 ? 14 + N: tbl[cities[i] + A];
        int s = cities[i] + B + 1 > 7000 ? 14 + N: tbl[cities[i] + B + 1];
        for (int j = f; j < s; j++)
            dp[j] += dp[i];
    }
    cout << dp[14 + N - 1] << '\n';
    return 0;
}