#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios::sync_with_stdio(0);
    cin.tie(0);

    int s, x;
    cin >> s >> x;

    cout << (x >= (((s + 2) * 3) + 16) ? "Yes it fits!" : "No, it's too small :(");

    return 0;
}

