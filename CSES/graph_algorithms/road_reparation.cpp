// Road Reparation
// https://cses.fi/problemset/task/1675
#include <bits/stdc++.h>
using namespace std;

int find(int u, vector<int> &parent) {
    if (parent[u] == u)
        return u;
    return parent[u] = find(parent[u], parent);
}

void unite(int a, int b, vector<int> &parent, vector<int> &rank) {
    a = parent[a];
    b = parent[b];
    if (a != b) {
        if (rank[a] < rank[b])
            swap(a, b);
        parent[b] = a;
        rank[a] += rank[b];
    }
}

int main() {
    int n, m;
    cin >> n >> m;
    vector<tuple<int,int,int>> edges(m);
    for (int i = 0; i < m; i++) {
        int a, b, c;
        cin >> a >> b >> c;
        edges.push_back({a-1, b-1, c});
    }
    sort(edges.begin(), edges.end(), [](tuple<int,int,int> &i, tuple<int,int,int> &j) { return get<2>(i) < get<2>(j); });
    vector<int> parent(n);
    vector<int> rank(n);
    for (int i = 0; i < n; i++) {
        parent[i] = i;
        rank[i] = 1;
    }
    long long cnt = 0;
    for (tuple<int,int,int> &e : edges) {
        int a, b, c;
        tie(a, b, c) = e;
        if (find(a, parent) != find(b, parent)) {
            cnt += c;
            unite(a, b, parent, rank);
        }
    }
    set<int> ps;
    for (int i = 0; i < n; i++) {
        ps.insert(find(i, parent));
    }
    if (ps.size() > 1) cout << "IMPOSSIBLE";
    else cout << cnt;
    return 0;
}
