// DMOPC '21 Contest 5 P1 - Permutations & Primes
// https://dmoj.ca/problem/dmopc21c5p1
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n;
    cin >> n;
    if (n == 1)
        cout << 1 << '\n';
    else if (n < 5)
        cout << -1 << '\n';
    else {
        for (int i = 7; i <= n; i += 2) {
            cout << i << ' ';
        }
        for (int i = 1; i <= 5; i += 2) {
            cout << i << ' ';
        }
        for (int i = 4; i <= n; i += 2) {
            cout << i << ' ';
        }
        cout << 2 << '\n';
    }
    return 0;
}