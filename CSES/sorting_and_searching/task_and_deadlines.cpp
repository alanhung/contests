// Task and Deadlines
// https://cses.fi/problemset/task/1630
#include <bits/stdc++.h>
using namespace std;
int main() {
    int n;
    cin >> n;
    pair<int, int> arr[n];
    for (int i = 0; i < n;i ++) {
        int a, d;
        cin >> a >> d;
        arr[i] = {a, d};
    } 
    sort(arr, arr + n);
    long long cnt  =0;
    long long time = 0;
    for (int i = 0 ; i < n; i++) {
        time += arr[i].first;
        cnt += arr[i].second - time;
    }
    cout << cnt;
    return 0;
}
