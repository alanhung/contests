// DMOPC '14 Contest 2 P6 - Selective Cutting
// https://dmoj.ca/problem/dmopc14c2p6
#include <bits/stdc++.h>
using namespace std;
int query(int tree[], int n, int a, int b) {
    a += n;
    b += n;
    int res = 0;
    while(a<=b) {
        if (a & 1) res += tree[a++];
        if (!(b & 1)) res += tree[b--];
        a /= 2;
        b /= 2;
    }
    return res;
}
void update(int tree[], int n, int i, int x) {
    i += n;
    tree[i] += x;
    for (i/= 2; i > 0; i /= 2)
        tree[i] = tree[2 * i] + tree[2 * i + 1];
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, q;
    cin >> n;
    // val, index
    vector<pair<int, int>> ms(n);
    for (int i = 0; i < n; i++) {
        int x;
        cin >> x;
        ms[i] = {x, i};
    }
    sort(ms.begin(), ms.end());
    cin >> q;
    // left, right, q    index
    vector<pair<tuple<int,int,int>, int>> qs(q);
    for (int i = 0; i < q; i++) {
        int a, b, q;
        cin >> a >> b >> q;
        qs[i] = {{a, b, q}, i};
    }
    sort(qs.begin(), qs.end(), [](pair<tuple<int,int,int>, int> &i, pair<tuple<int,int,int>, int> &j) { return get<2>(i.first) < get<2>(j.first) ;});
    vector<int> res(q);
    int tree[2 * n];
    fill(tree, tree + 2 * n, 0);

    int i = n - 1;
    for (int j = q - 1; j >= 0; j--) {
        while(i >= 0 && ms[i].first >= get<2>(qs[j].first)) {
            update(tree, n, ms[i].second, ms[i].first);
            i--;
        }
        res[qs[j].second] = query(tree, n, get<0>(qs[j].first), get<1>(qs[j].first));
    }
    for (int u : res)
        cout << u << '\n';
    return 0;
}