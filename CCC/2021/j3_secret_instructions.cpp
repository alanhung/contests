#include <bits/stdc++.h>

using namespace std;

int main() {
    int last_left = 0;
    while(1) {
        int cur;
        cin >> cur;
        if (cur == 99999)
            break;
        int f = cur / 10000;
        int s = (cur / 1000) % 10;
        
        if (f + s == 0) {
            if (last_left)
                cout << "left ";
            else 
                cout << "right ";
            cout << cur << '\n';
        } else if ((f + s) % 2 == 1) {
            last_left = 1;
            cout << "left ";
            cout << (cur % (f * 10000 + s * 1000)) << '\n';
        } else {
            last_left = 0;
            cout << "right ";
            cout << (cur % (f * 10000 + s * 1000)) << '\n';
        }
    }
    return 0;
}