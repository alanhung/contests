// CCC '18 S2 - Sunflowers
// https://dmoj.ca/problem/ccc18s2
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n;
    cin >> n;
    int grid[n][n];
    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            cin >> grid[i][j];
    bool h = grid[0][1] > grid[0][0];
    bool v = grid[1][0] > grid[0][0];
    if (v && h) // 0 deg 
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                cout << grid[i][j] << " \n"[j==n-1];
    else if (v && !h) // 90 deg counter clockwise
        for (int j = n-1; j >= 0; j--)
            for (int i = 0; i < n; i++)
                cout << grid[i][j] << " \n"[i==n-1];
    else if (!v && h) // 90 deg clockwise
        for (int j = 0; j < n; j++)
            for (int i = n-1; i >= 0; i--)
                cout << grid[i][j] << " \n"[i==0];
    else // 180 deg
        for (int i = n-1; i >= 0; i--)
            for (int j = n-1; j >= 0; j--)
                cout << grid[i][j] << " \n"[j==0];
    return 0;
}