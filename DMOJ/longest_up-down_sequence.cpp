// Longest Up-Down Subsequence
// https://dmoj.ca/problem/olyq1p2
#include <bits/stdc++.h>
using namespace std;
int main() {
    int n;
    cin >> n;
    int arr[n];
    for (int i = 0; i < n; i++) cin >> arr[i];
    // element at which subsequence with length terminates going up
    int up = 1;
    int down = 1;
    for (int i = 1; i < n; i++) {
        if (arr[i] > arr[i - 1])
            up = down + 1;
        else if (arr[i] < arr[i - 1] )
            down = up + 1;
    }
    cout << max(up, down) << '\n';
    return 0;
}

