#include <bits/stdc++.h>
using namespace std;
int main()
{
    int N, T;
    cin >> N >> T;
    int wt[N];
    int val[N];
    for (int i = 0; i < N; i++)
    {
        cin >> val[i];
        cin >> wt[i];
    }
    int dp[T + 1];
    fill(dp, dp + T + 1, 0);
    for (int i = 0; i < N; i++)
        for (int j = T; j >= wt[i]; j--)
            dp[j] = max(dp[j], val[i] + dp[j - wt[i]]);

    cout << dp[T];
    return 0;
}

