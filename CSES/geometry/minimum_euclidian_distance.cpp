// Minimum Euclidian Distance
// https://cses.fi/problemset/task/2194
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n;
    cin >> n;
    pair<long long, long long> arr[n];
    for (int i = 0; i < n; i++) {
        long long x,y;
        cin >> x >> y;
        arr[i] = {x, y};
    }
    long long dd = LONG_LONG_MAX;
    sort(arr, arr + n);
    set<pair<long long, long long>> s = {{arr[0].second, arr[0].first}};
    int j = 0;
    for (int i = 1; i < n; i++) {
        long long d = ceil(sqrt(dd));
        while (j < i && arr[j].first < arr[i].first - d) {
            s.erase({arr[j].second, arr[j].first});
            j++;
        }
        set<pair<long long, long long>>::iterator l = s.lower_bound({arr[i].second - d, 0});
        set<pair<long long, long long>>::iterator r = s.upper_bound({arr[i].second + d, 0});
        for (set<pair<long long, long long>>::iterator it = l; it != r; ++it) {
            dd = min(dd, (it->second - arr[i].first) * (it->second - arr[i].first) + (it->first - arr[i].second) * (it->first - arr[i].second));
        }
        s.insert({arr[i].second, arr[i].first});
    }
    cout << dd;
    return 0;
}
