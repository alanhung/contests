#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, m, q;
    cin >> n >> m;
    char grid[n][m];
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            cin >> grid[i][j];
        }
    }
    cin >> q;
    while(q--) {
        int c;
        cin >> c;
        if (c == 1) {
            for (int j = 0; j < m; j++) {
                for (int i = n-1; i >= 0 ; i--) {
                    if (grid[i][j] == 'X') {
                        if (i != n-1)
                            grid[i+1][j] = 'X';
                        grid[i][j] = '.';
                    }
                }
            }
            /* cout << "X " << '\n'; */
            /* for (int i = 0; i < n; i++) { */
            /*     for (int j = 0; j < m; j++) */
            /*         cout << grid[i][j]; */
            /*     cout << '\n'; */
            /* } */
            while(true) {
                bool moved = false;
                for (int j = 0; j < m; j++) {
                    for (int i = 0; i < n ; i++) {
                        if (grid[i][j] == 'W') {
                            int cj = j;
                            while(true) {
                                if (cj != 0 && grid[i][cj-1] == '.') {
                                    moved = true;
                                    grid[i][cj-1] = 'W';
                                    grid[i][cj] = '.';
                                    cj--;
                                } else
                                    break;
                            }
                        }
                    }
                }
                /* cout << "LEFT " << '\n'; */
                /* for (int i = 0; i < n; i++) { */
                /*     for (int j = 0; j < m; j++) */
                /*         cout << grid[i][j]; */
                /*     cout << '\n'; */
                /* } */
                bool movedto[n][m];
                fill(*movedto, *movedto + n * m, 0);
                for (int j = 0; j < m; j++) {
                    for (int i = 0; i < n ; i++) {
                        if (grid[i][j] == 'W') {
                            if (i != n-1 && !movedto[i][j] && grid[i+1][j] == '.') {
                                moved = true;
                                grid[i+1][j] = 'W';
                                movedto[i+1][j] = 1;
                                grid[i][j] = '.';
                            }
                        }
                    }
                }
                /* cout << "DOWN " << '\n'; */
                /* for (int i = 0; i < n; i++) { */
                /*     for (int j = 0; j < m; j++) */
                /*         cout << grid[i][j]; */
                /*     cout << '\n'; */
                /* } */
                if (!moved)
                    break;
            }
        } else {
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < m; j++)
                    cout << grid[i][j];
                cout << '\n';
            }
        }
    }
    return 0;
}