#include <iostream>

using namespace std;

int main()
{
    cin.tie(0)->sync_with_stdio(0);
    cout.sync_with_stdio(false);
    int cnt = 2;
    int a, b;
    cin >> a >> b;
    while (a - b >= 0)
    {
        cnt++;
        int temp = a;
        a = b;
        b = temp - b;
    }
    cout << cnt;
    return 0;
}