#include <bits/stdc++.h>
using namespace std;
int main(){
    cin.tie(0)->sync_with_stdio(0);
    int N, Q;
    cin >> N >> Q;
    int sum[N];
    cin >> sum[0];
    for (int i = 1; i < N; i++){
        cin >> sum[i];
        sum[i] += sum[i - 1];
    }
    for (int i = 0; i < Q; i++) {
        int f, s;
        cin >> f >> s;
        f--;
        s--;
        cout << (sum[s] - (f == 0 ? 0 : sum[f - 1])) / (s - f + 1) << '\n';
    }
    return 0;
}