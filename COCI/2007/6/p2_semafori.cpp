#include <bits/stdc++.h>
using namespace std;
int main()
{
    cin.tie(0)->sync_with_stdio(0);
    int N, L;
    cin >> N >> L;
    int cnt = 0;
    for (int i = 0; i < N; i++)
    {
        int f, s, t;
        cin >> f >> s >> t;
        int id = (cnt + f) % (s + t);
        cnt += id < s ? (s - id) : 0;
    }
    cout << cnt + L;
    return 0;
}