#include <bits/stdc++.h>
using namespace std;
int main() {
  cin.tie(0)->sync_with_stdio(0);
  int n;  
  cin >> n;
  int arr[n + 1];
  for(int i = 0; i < n; i++)
    cin >> arr[i];
  arr[n]=0;
  int x = 1;
  arr[0] -= 1;
  cout << 'R';
  // went right
  bool right = 1;
  while(arr[0]) {
    // cout << "X: " << x << '\n';
    // cout << " BEFORE\n";
    // for (int i= 0; i < n+1; i++)
    //   cout << arr[i] << ' ';
    // cout << '\n';
    if (right) {
      if (arr[x]) {
        cout << 'R';
        arr[x]--;
        x++;
      } else {
        cout << 'L';
        arr[x-1]--;
        x--;
        right = 0;
      }
    } else {
      if (arr[x-1] == 1 && arr[x]) {
        cout << 'R';
        arr[x]--;
        x++;
        right = 1;
      } else {
        cout << 'L';
        arr[x-1]--;
        x--;
      }
    }
    // cout << " AFTER\n";
    // for (int i= 0; i < n+1; i++)
    //   cout << arr[i] << ' ';
    // cout << '\n';
  }
  cout << '\n';
  return 0;
}
