// CCC 2011 S4 Cookies
// https://dmoj.ca/problem/ccc01s4

/*
 * Involves the Smallest Circle Problem
 * https://en.wikipedia.org/wiki/Smallest-circle_problem
 *
 * Solved using Heron's formula and area of traingle circumscribed by circle
 * http://3.bp.blogspot.com/-9jyJ5Wi5p3c/VK6pDGUgkDI/AAAAAAAAAMg/sDCU10u93bI/s1600/derive%2Bformula-radius%2Bof%2Bcircumcircle.png
 *
 * https://www.geeksforgeeks.org/minimum-enclosing-circle-set-1/
 */
#include <bits/stdc++.h>
using namespace std;
long double dist(complex<long double> x, complex<long double> y) {
    return sqrt( (x.real() - y.real()) * (x.real() - y.real()) + (x.imag() - y.imag()) * (x.imag() - y.imag()));
}
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n;
    cin >> n;
    complex<long double> arr[n];
    for (int i = 0; i < n; i++) {
        long double x, y;
        cin >> x >> y;
        arr[i] = {x, y};
    }
    long double m = 0;
    for (int i = 0; i < n; i++) {
        for (int j = i + 1; j < n; j++) {
            for (int k = j + 1; k < n; k++) {
                long double a = dist(arr[i], arr[j]);
                long double b = dist(arr[j], arr[k]);
                long double c = dist(arr[i], arr[k]);
                if (a * a + b * b <= c * c)
                    m = max(m, c);
                else if (a * a + c * c <= b * b)
                    m = max(m, b);
                else if (b * b + c * c <= a * a)
                    m = max(m, a);
                else
                    m = max(m, 2 * a * b * c / sqrt( (a + b + c) * (a + b - c) * (a + c - b) * (b + c - a)));
            }
        }
    }
    cout << fixed << setprecision(2) << m;
    return 0;
}