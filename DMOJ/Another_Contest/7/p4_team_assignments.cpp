// Another Contest 7 Problem 4 - Team Assignments
// https://dmoj.ca/problem/acc7p4
#include <bits/stdc++.h>
using namespace std;
long long modpow(long long base, long long exp, long long m) {
    long long res = 1;
    base %= m;
    while(exp > 0) {
        if (exp & 1) res = (res * base) % m;
        base = base * base % m;
        exp >>= 1;
    }
    return res;
}

long long choose(long long n, long long k, long long fac[], long long invf[], long long m) {
    return fac[n] * invf[n - k] % m * invf[k] % m;
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    int k;
    long long n;
    cin >> n >> k;
    long long m = 998244353;
    long long fac[k + 1];
    fac[0] = 1;
    for (int i = 1; i <= k; i++)
        fac[i] = fac[i-1] * i % m;
    long long invf[k + 1];
    invf[k] = modpow(fac[k], m - 2, m);
    for (int i = k - 1; i >= 0; i--)
        invf[i] = (i + 1) * invf[i + 1] % m;

    /* for (int i = 0; i <= k; i++) */
    /*     cout << fac[i] << ' '; */
    /* cout << '\n'; */
    /* for (int i = 0; i <= k; i++) */
    /*     cout << invf[i] << ' '; */
    /* cout << '\n'; */

    long long res = 0;
    for (int i = 1; i <= k; i++) {
        long long cur  = i * modpow(i-1, n - 1, m) % m * choose(k, i, fac, invf, m) % m;
        long long alt = (k % 2 == i % 2 ? 1 : -1);
        res = ((res + cur * alt) % m + m) % m;
        /* cout << res << '\n'; */
    }
    cout << res << '\n';
    return 0;
}