#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int N, M, B, Q;
    cin >> N >> M >> B >> Q;
    B--;
    vector<pair<int, int>> adj[N];
    while(M--) {
        int x, y, z;
        cin >> x >> y >> z;
        x--;
        y--;
        adj[x].push_back({z, y});
        adj[y].push_back({z, x});
    }
    bool visited[N];
    fill(visited, visited + N, 0);
    int dist[N];
    fill(dist, dist + N, numeric_limits<int>::max());
    dist[B] = 0;
    priority_queue<pair<int,int>, vector<pair<int,int>>, greater<pair<int,int>>> q;
    q.push({numeric_limits<int>::max(), B});
    while(!q.empty()) {
        int u = q.top().second;
        q.pop();
        if (visited[u])
            continue;
        visited[u] = true;
        for (vector<pair<int,int>>::iterator it = adj[u].begin(); it != adj[u].end(); ++it) {
            if (!visited[it->second]) {
                dist[it->second] = min(dist[it->second], dist[u] + it->first);
                q.push({dist[it->second], it->second});
            }
        }
    }
    while(Q--) {
        int A;
        cin >> A;
        A--;
        cout << (dist[A] == numeric_limits<int>::max() ? -1 : dist[A]) << '\n';
    }
    return 0;
}