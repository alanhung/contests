// CCC '03 S1 - Snakes and Ladders
#include <iostream>

int main()
{
  int position = 1;

  while (true)
  {
    int input;
    std::cin >> input;
    if (!input)
    {
      std::cout << "You Quit!";
      break;
    }

    if (!(position + input > 100))
    {
      position += input;

      if (position == 54)
      {
        position = 19;
      }
      else if (position == 90)
      {
        position = 48;
      }
      else if (position == 99)
      {
        position = 77;
      }
      else if (position == 9)
      {
        position = 34;
      }
      else if (position == 40)
      {
        position = 64;
      }
      else if (position == 67)
      {
        position = 86;
      }
      std::cout << "You are now on square " << position << '\n';
      if (position == 100)
      {
        std::cout << "You Win!";
        break;
      }
    }
    else
    {
      std::cout << "You are now on square " << position << '\n';
    }
  }
  return 0;
}