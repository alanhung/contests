// DMOPC '15 Contest 1 P6 - Lelei and Contest
// https://dmoj.ca/problem/dmopc15c1p6
#include <bits/stdc++.h>
using namespace std;
long long tree[1000000];
long long lazy[1000000];
long long sum(int u, int l, int r, int a, int b, int m) {
    if (b < l || a > r) return 0;
    if (a <= l && b >= r) {
        return (tree[u] + (r-l+1)*lazy[u] % m) % m;
    }
    tree[u] = (tree[u] + (r-l+1)*lazy[u] % m) % m;
    lazy[2 * u] = (lazy[2 * u] + lazy[u]) % m;
    lazy[2 * u + 1] = (lazy[2 * u + 1] + lazy[u]) % m;
    lazy[u]=0;
    int d = (l + r) / 2;
    return (sum(2 * u, l, d, a, b, m) + sum(2 * u + 1, d + 1, r, a, b, m)) % m;

}
void update(int u, int l, int r, int a, int b, long long val, int m) {
    if (b < l || a > r)
        return;
    if (a <= l && b >= r) {
        lazy[u] = (lazy[u] + val) % m;
    }
    else {
        int d = (l + r) / 2;
        tree[u] = (tree[u] + (min(b, r) - max(a, l) + 1) * val % m ) % m;
        update(2 * u, l, d, a, b, val, m);
        update(2 * u + 1, d + 1, r, a, b, val, m);
    }
}
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int m, n, q;
    cin >> m >> n >> q;
    for (int i = 0; i < n; i++) {
        int x;
        cin >> x;
        update(1, 0, n-1, i, i, x, m);
    }
    while(q--) {
        int i, l, r;
        cin >> i >> l >> r;
        l--;
        r--;
        if (i == 1) {
            int x;
            cin >> x;
            update(1, 0, n-1, l, r, x, m);
        } else
            cout << sum(1, 0, n-1, l, r, m) << '\n';
    }
    return 0;
}