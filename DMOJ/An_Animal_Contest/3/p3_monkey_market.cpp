#include <bits/stdc++.h>
using namespace std;
int main() {
    int N;
    cin >> N;
    int a[N];
    for (int i = 0; i < N; i++)
        cin >> a[i];
    sort(a, a + N);
    int r[N];
    char c[N];
    int i = 0;
    int j = N - 1;
    int k = 0;
    while(j - i > 0) {
        r[k] = a[i];
        c[k] = 'B';
        k++;
        r[k] = a[j];
        c[k] = 'S';
        k++;
        j--;
        i++;
    }
    if (N % 2 == 1) {
        r[N - 1] = a[N/2];
        c[N - 1] = 'E';
    }
    cout << r[0];
    for (int i = 1; i < N; i++) {
        cout << ' ' <<  r[i];
    }
    cout << '\n';
    for (int i  =0; i < N; i++) {
        cout << c[i];
    }
    cout << '\n';
    return 0;
}