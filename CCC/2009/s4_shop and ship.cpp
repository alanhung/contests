#include <bits/stdc++.h>
using namespace std;
int main()
{
    cin.tie(0)->sync_with_stdio(0);
    int N, T, K, D;
    cin >> N >> T;
    vector<pair<int, int>> adj[N];
    for (int i = 0; i < T; i++)
    {
        int f, s, w;
        cin >> f >> s >> w;
        f--;
        s--;
        adj[f].push_back({s, w});
        adj[s].push_back({f, w});
    }
    cin >> K;
    pair<int, int> pen[K];
    for (int i = 0; i < K; i++)
    {
        int f, s;
        cin >> f >> s;
        pen[i] = make_pair(f - 1, s);
    }
    cin >> D;
    D--;
    int dist[N];
    fill(dist, dist + N, numeric_limits<int>::max());
    bool visited[N];
    fill(visited, visited + N, 0);
    dist[D] = 0;
    priority_queue<pair<int, int>, vector<pair<int, int>>, greater<pair<int, int>>> q;
    q.push({0, D});
    while (!q.empty())
    {
        int u = q.top().second;
        q.pop();
        if (visited[u])
            continue;
        visited[u] = true;
        for (auto nei : adj[u])
        {
            if (dist[u] + nei.second < dist[nei.first])
            {
                dist[nei.first] = dist[u] + nei.second;
                q.push({dist[nei.first], nei.first});
            }
        }
    }
    int result = numeric_limits<int>::max();
    for (int i = 0; i < K; i++)
        result = min(result, pen[i].second + dist[pen[i].first]);
    cout << result;
    return 0;
}