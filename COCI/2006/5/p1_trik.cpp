#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int a = 0;
    int b = 1;
    int c = 2;
    char ch;
    while(cin >> ch) {
        if (ch == 'A')
            swap(a, b);
        else if (ch == 'B')
            swap(b, c);
        else
            swap(a, c);
    }
    if (!a)
        cout << 1;
    else if (!b)
        cout << 2;
    else
        cout << 3;
    cout << '\n';
    return 0;
}