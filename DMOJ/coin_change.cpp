#include <iostream>
#include <limits>

int main()
{
	std::ios::sync_with_stdio(0);
	std::cin.tie(0);
	int x, n;
	std::cin >> x >> n;

	int coins[n];

	for (int i = 0; i < n; i++)
	{
		std::cin >> coins[i];
	}

	int dp[x + 1];
	std::fill(dp, dp + x + 1, std::numeric_limits<int>::max() - 1);
	dp[0] = 0;

	for (int i = 1; i <= x; i++)
	{
		for (int j = 0; j < n; j++)
		{
			if (coins[j] <= i)
			{
				dp[i] = std::min(dp[i], dp[i - coins[j]] + 1);
			}
		}
	}
	std::cout << dp[x];

	return 0;
}

