#include <iostream>
#include <queue>
#include <vector>

using namespace std;

int main()
{
    ios::sync_with_stdio(0);
    cin.tie(0);
    vector<int> adj[64];

    for (int i = 0; i < 7; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            if (j - 2 >= 0 && i + 1 < 8)
            {
                adj[i * 8 + j].push_back((i + 1) * 8 + j - 2);
                adj[(i + 1) * 8 + j - 2].push_back(i * 8 + j);
            }
            if (j - 1 >= 0 && i + 2 < 8)
            {
                adj[i * 8 + j].push_back((i + 2) * 8 + j - 1);
                adj[(i + 2) * 8 + j - 1].push_back(i * 8 + j);
            }
            if (j + 2 < 8 && i + 1 < 8)
            {
                adj[i * 8 + j].push_back((i + 1) * 8 + j + 2);
                adj[(i + 1) * 8 + j + 2].push_back(i * 8 + j);
            }
            if (j + 1 < 8 && i + 2 < 8)
            {
                adj[i * 8 + j].push_back((i + 2) * 8 + j + 1);
                adj[(i + 2) * 8 + j + 1].push_back(i * 8 + j);
            }
        }
    }

    int kx, ky, fx, fy;
    cin >> kx >> ky >> fx >> fy;
    kx--;
    ky = 8 - ky;
    fx--;
    fy = 8 - fy;

    bool visited[64];
    int dist[64];
    fill(visited, visited + 64, false);
    fill(dist, dist + 64, 0);

    queue<int> q;
    q.push(ky * 8 + kx);
    visited[ky*8+kx] = true;

    while (!q.empty())
    {
        int u = q.front();
        q.pop();
        bool found = false;

        for (int i = 0; i < adj[u].size(); i++)
        {
            if (!visited[adj[u][i]])
            {
                visited[adj[u][i]] = true;
                dist[adj[u][i]] = dist[u] + 1;
                q.push(adj[u][i]);

                if (adj[u][i] == fy * 8 + fx)
                {
                    found = true;
                    break;
                }
            }
        }
        if (found)
        {
            break;
        }
    }
    /* for (int i = 0; i < adj[ky * 8 + kx].size(); i++) */
    /* { */
    /*     cout << adj[ky*8+kx][i] << ' '; */
    /* } */
    /* cout << '\n'; */
    /* cout << ky * 8 + kx << '\n'; */
    /* cout << fy * 8 + fx << '\n'; */
    cout << dist[fy * 8 + fx];

    return 0;
}