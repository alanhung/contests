#include <bits/stdc++.h>
using namespace std;
int main()
{
    cin.tie(0)->sync_with_stdio(0);
    int N, M;
    cin >> N >> M;
    vector<pair<int, int>> adj[N];
    for (int i = 0; i < M; i++)
    {
        int f, s, t;
        cin >> f >> s >> t;
        adj[f - 1].push_back({s - 1, t});
        adj[s - 1].push_back({f - 1, t});
    }
    bool visited[N];
    int dist[N];
    fill(visited, visited + N, false);
    fill(dist, dist + N, numeric_limits<int>::min());
    dist[0] = 0;
    priority_queue<pair<int, int>> q;
    q.push({numeric_limits<int>::max(), 0});
    while (!q.empty())
    {
        pair<int, int> temp = q.top();
        q.pop();
        if (visited[temp.second])
            continue;
        visited[temp.second] = true;
        for (auto elem : adj[temp.second])
        {
            if (min(elem.second, temp.first) > dist[elem.first])
            {
                dist[elem.first] = min(elem.second, temp.first);
                q.push({min(elem.second, temp.first), elem.first});
            }
        }
    }
    cout << 0 << '\n';
    for (int i = 1; i < N; i++)
        cout << dist[i] << '\n';
    return 0;
}