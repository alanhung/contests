#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;
bool tsort(int u, vector<int> &result, bool recstack[], bool visited[], vector<int> adj[])
{
    if (!visited[u])
    {
        visited[u] = true;
        recstack[u] = true;
        for (int i = 0; i < adj[u].size(); i++)
        {
            if (recstack[adj[u][i]])
                return true;
            if (tsort(adj[u][i], result, recstack, visited, adj))
                return true;
        }
        recstack[u] = false;
        result.push_back(u + 1);
    }
    return false;
}
int main()
{
    cin.tie(0)->sync_with_stdio(0);
    vector<int> adj[7];
    adj[0].push_back(6);
    adj[0].push_back(3);
    adj[1].push_back(0);
    adj[2].push_back(3);
    adj[2].push_back(4);
    while (true)
    {
        int f, s;
        cin >> f >> s;
        if (!f)
            break;
        adj[f - 1].push_back(s - 1);
    }
    bool recstack[7];
    fill(recstack, recstack + 7, false);
    bool visited[7];
    fill(visited, visited + 7, false);
    vector<int> result;
    bool bad = false;
    for (int i = 6; i >= 0; i--)
    {
        if (tsort(i, result, recstack, visited, adj))
        {
            bad = true;
            break;
        }
    }
    if (bad)
        cout << "Cannot complete these tasks. Going to bed.";
    else
    {
        for (int i = 6; i >= 0; i--)
            cout << result[i] << ' ';
    }
    return 0;
}