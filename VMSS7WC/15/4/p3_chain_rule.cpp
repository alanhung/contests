#include <bits/stdc++.h>
using namespace std;
int main()
{
    cin.tie(0)->sync_with_stdio(0);
    int N, M;
    cin >> N >> M;
    vector<pair<int, int>> adj[N];
    for (int i = 0; i < M; i++)
    {
        int a, b, w;
        cin >> a >> b >> w;
        adj[a].push_back({b, w});
        adj[b].push_back({a, w});
    }
    int dist[N];
    fill(dist, dist + N, numeric_limits<int>::max());
    bool visited[N];
    fill(visited, visited + N, false);
    priority_queue<pair<int, int>, vector<pair<int, int>>, greater<pair<int, int>>> q;
    q.push({0, 0});
    dist[0] = 0;
    while (!q.empty())
    {
        int u = q.top().second;
        q.pop();
        if (visited[u])
            continue;
        visited[u] = true;
        for (auto nei : adj[u])
        {
            if (dist[u] + nei.second < dist[nei.first])
            {
                dist[nei.first] = dist[u] + nei.second;
                q.push({dist[nei.first], nei.first});
            }
        }
    }
    int result = dist[N - 1];
    int dist2[N];
    fill(dist2, dist2 + N, numeric_limits<int>::max());
    fill(visited, visited + N, false);
    q.push({0, N - 1});
    dist2[N - 1] = 0;
    while (!q.empty())
    {
        int u = q.top().second;
        q.pop();
        if (visited[u])
            continue;
        visited[u] = true;
        for (auto nei : adj[u])
        {
            if (dist2[u] + nei.second < dist2[nei.first])
            {
                dist2[nei.first] = dist2[u] + nei.second;
                q.push({dist2[nei.first], nei.first});
            }
        }
    }
    for (int i = 0; i < N; i++)
        result = max(result, dist[i] + dist2[i]);
    cout << result;
    return 0;
}