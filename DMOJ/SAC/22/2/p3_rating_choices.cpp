#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int N;
    long long R;
    cin >> N >> R;

    // node, rating
    vector<pair<int, int>> adj[(1 << (N+1)) - 1];
    for (int i = 0; i < (1 << N) - 1; i++) {
        int u, v, w, D;
        cin >> u >> v >> w >> D;
        u--;
        v--;
        w--;
        adj[u].push_back({v, D});
        adj[u].push_back({w, 0});
    }
    long long best = 0;
    queue<int> q;
    q.push(0);
    long long dist[(1 << (N+1)) - 1];
    fill(dist, dist + (1 << (N+1)) - 1, LONG_LONG_MIN);
    dist[0]=0;
    while(!q.empty()) {
        int node = q.front();
        q.pop();
        if (!adj[node].size())
            best = max(best, dist[node]);
        for (pair<int,int> child : adj[node]) {
            dist[child.first] = dist[node] + child.second;
            q.push(child.first);
        }
    }
    cout << R + best << '\n';
    return 0;
}