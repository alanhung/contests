// Bracket Sequence 1
// https://cses.fi/problemset/task/2064
#include <bits/stdc++.h>
using namespace std;
#define MOD 1000000007ll
long long modpow(long long base, long long exp, long long m) {
    long long res = 1;
    while (exp > 0) {
        if (exp & 1) res = res * base % m;
        base = (base * base) % m;
        exp >>= 1;
    }
    return res;
}
int main() {
    int n;
    cin >> n;
    if (n & 1) cout << 0;
    else {
        long long num = 1;
        for (int i = 1; i <= n; i++)
            num = num * i % MOD;
        long long den = 1;
        for (int i = 1; i <= n/2; i++) 
            den = den * i % MOD;
        den = den * den % MOD * (n/2+1) % MOD;
        cout << num * modpow(den, MOD-2, MOD) % MOD;
    }
    return 0;
}

