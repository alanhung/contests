// Educational DP Contest AtCoder O - Matching
// https://dmoj.ca/problem/dpo
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n;
    cin >> n;
    int arr[n][n];
    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            cin >> arr[i][j];
    int mod = 1e9+7;
    int dp[1 << n];
    fill(dp, dp + (1 << n), 0);
    dp[0]=1;
    for (int i = 1; i <= n; i++) {
        for (int s = 1; s < (1 << n); s++) {
            if (__builtin_popcount(s) == i) {
                for (int j = 0; j < n; j++) {
                    if ((s & (1 << j)) && arr[i-1][j]) {
                        dp[s] += dp[s^(1 << j)];
                        dp[s] %= mod;
                    }
                }
            }
        }
    }
    cout << dp[(1 << n)-1] << '\n';
    return 0;
}