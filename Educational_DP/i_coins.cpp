#include <bits/stdc++.h>
using namespace std;
int main() {
    int n;
    cin >> n;
    double p[n];
    for (int i = 0; i < n; i++)
        cin >> p[i];
    double dp[n + 1][(n + 1) / 2 + 1];
    for (int i = 0; i < (n + 1) / 2 + 1; i++)
        dp[0][i] = 0;
    for (int i = 0; i < n + 1; i++)
        dp[i][0] = 1;
    for (int i = 1; i < n + 1; i++)
        for (int j = 1; j < (n + 1) / 2 + 1; j++)
            dp[i][j] = p[i-1] * dp[i-1][j-1] + (1-p[i-1]) * dp[i-1][j];
    cout.precision(numeric_limits<double>::max_digits10);
    cout << fixed << dp[n][(n + 1) / 2] << '\n';
    return 0;
}