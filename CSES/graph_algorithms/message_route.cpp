// Message Route
// https://cses.fi/problemset/task/1667
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, m;
    cin >> n >> m;
    vector<int> adj[n];
    for (int i = 0; i < m; i++) {
        int a, b;
        cin >> a >> b;
        adj[a-1].push_back(b-1);
        adj[b-1].push_back(a-1);
    }
    bool visited[n];
    fill(visited, visited + n, 0);
    visited[0]=1;
    int succ[n];
    fill(succ, succ + n, -1);
    queue<int> q;
    q.push(0);
    while(!q.empty()) {
        int u = q.front();
        q.pop();
        for (int v : adj[u]) {
            if (!visited[v]) {
                visited[v]=visited[u]+1;
                succ[v]=u;
                q.push(v);
            }
        }
    }
    if (!visited[n-1]) {
        cout << "IMPOSSIBLE";
    } else {
        vector<int> ans;
        int x = n - 1;
        while(x != 0) {
            ans.push_back(x);
            x = succ[x];
        }
        ans.push_back(0);
        reverse(ans.begin(), ans.end());
        cout << ans.size() << '\n';
        for (int i = 0; i < ans.size(); i++)
            cout << ans[i] + 1 << ' ';
    }
    return 0;
}
