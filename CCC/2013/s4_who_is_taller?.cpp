#include <bits/stdc++.h>
using namespace std;
int main()
{
    cin.tie(0)->sync_with_stdio(0);
    int N, M, start, end;
    cin >> N >> M;
    vector<int> adj[N];
    for (int i = 0; i < M; i++)
    {
        int f, s;
        cin >> f >> s;
        adj[f - 1].push_back(s - 1);
    }
    cin >> start >> end;
    start--;
    end--;
    bool taller = false;
    bool shorter = false;
    for (int i = 0; i < 2; i++)
    {
        bool visited[N];
        queue<int> q;
        fill(visited, visited + N, false);
        visited[(i == 0 ? start : end)] = true;
        q.push((i == 0 ? start : end));
        while (!q.empty())
        {
            int u = q.front();
            q.pop();
            for (int &ne : adj[u])
            {
                if (!visited[ne])
                {
                    visited[ne] = true;
                    q.push(ne);
                    if (ne == (i == 0 ? end : start))
                    {
                        if (i == 0)
                            taller = true;
                        else if (i == 1)
                            shorter = true;
                        break;
                    }
                }
                if (i == 0 && taller)
                    break;
                if (i == 1 && shorter)
                    break;
            }
        }
    }
    if (taller)
        cout << "yes";
    else if (shorter)
        cout << "no";
    else
        cout << "unknown";
    return 0;
}