// DMOPC '15 Contest 1 P5 - Lelei and Dragon Scales
// https://dmoj.ca/problem/dmopc15c1p5
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int w, h, n;
    cin >> w >> h >> n;
    int prefix[h + 1][w + 1];
    fill(*prefix, *prefix + (h + 1) * (w + 1), 0);
    for (int i = 1; i <= h; i++) {
        for (int j = 1; j <= w; j++) {
            cin >> prefix[i][j];
            prefix[i][j] += prefix[i-1][j];
            prefix[i][j] += prefix[i][j-1];
            prefix[i][j] -= prefix[i-1][j-1];
        }
    }
    int ma = 0;
    for (int ch = 1; ch <= min(n, h); ch++) {
        int cw = min(w, n / ch);
        for (int i = 0; i + ch <= h; i++) {
            for (int j = 0; j + cw <= w; j++) {
                ma = max(ma, prefix[i + ch][j + cw] - prefix[i + ch][j] - prefix[i][j + cw] + prefix[i][j]);
            }
        }
    }
    cout << ma << '\n';
    return 0;
}