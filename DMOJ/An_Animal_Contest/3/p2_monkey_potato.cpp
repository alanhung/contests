#include <bits/stdc++.h>
using namespace std;
int main()
{
    cin.tie(0)->sync_with_stdio(0);
    int K, D;
    cin >> K >> D;
    bool zero = false;
    int smallest = 10;
    for (int i = 0; i < D; i++)
    {
        int c;
        cin >> c;
        if (c)
            smallest = min(smallest, c);
        else
            zero = true;
    }
    if (D == 1 && zero)
        cout << -1 << '\n';
    else
    {
        cout << smallest;
        for (int i = 1; i < K - 1; i++)
        {
            if (zero)
                cout << 0;
            else
                cout << smallest;
        }
        if (K != 1)
            cout << smallest << '\n';
        else
            cout << '\n';
    }
    return 0;
}