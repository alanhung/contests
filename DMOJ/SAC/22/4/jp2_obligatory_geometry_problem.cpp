#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int N, Q;
    cin >> N >> Q;
    int poly[N];
    fill(poly, poly + N, -1);
    while(Q--) {
        string f, s;
        int c;
        cin >> f >> s >> c;
        if (f[0] == 's') {
            if (s[0] == 's')
                poly[c-1] = 0;
            else if (s[0] == 'c')
                poly[c-1] = 1;
            else
                poly[c-1] = 2;
        } else {
            if (s[0] == 's')
                cout << (poly[c-1] == 0 ? 1 : 0) << '\n';
            else if (s[0] == 'c')
                cout << (poly[c-1] == 1 ? 1 : 0) << '\n';
            else
                cout << (poly[c-1] == 2 ? 1 : 0) << '\n';
        }
    }
    return 0;
}