// Dice Combinations
// https://cses.fi/problemset/task/1633
#include <bits/stdc++.h>
#define MOD 1000000007
using namespace std;
int main() {
    int n;
    cin >> n;
    int dp[n + 1];
    dp[0] = 1;
    int wt[6] = {1, 2, 3, 4, 5, 6};
    for (int i = 1; i <= n; i++) {
        dp[i]= 0;
        for (int j = 0; j < 6; j++) {
            if (i - wt[j] >= 0) {
                dp[i] += dp[i - wt[j]];
                dp[i] %= MOD;
            }
        }
    }
    cout << dp[n];
    return 0;
}
