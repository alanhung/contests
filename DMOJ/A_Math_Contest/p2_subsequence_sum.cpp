// A Math Contest P2 - Subsequence Sum
// https://dmoj.ca/problem/mathp2
#include <bits/stdc++.h>
#define MOD 1000000007ll
using namespace std;
long long modpow(long long a, long long b, long long m) {
    long long res = 1;
    a %= m;
    while(b > 0) {
        if (b&1)
            res = (res * a) % m;
        a = a * a % m;
        b >>= 1;
    }
    return res;
}
int main() {
    long long N;
    cin >> N;
    long long sum = 0;
    for (int i = 0; i < N; i++) {
        long long c;
        cin >> c;
        sum = (sum + c) % MOD;
    }
    cout << modpow(2, N - 1, MOD) * sum % MOD << '\n';
    return 0;
}

