// Distinct Numbers
// https://cses.fi/problemset/list/
#include <bits/stdc++.h>
using namespace std;
int main() {
    int n;
    cin >> n;
    int a[n];
    for (int i = 0; i < n; i++)
        cin >> a[i];
    sort(a, a + n);
    int cnt = 0;
    for (int i = 0; i < n; i++) {
        int j = i;
        while(j + 1 < n && a[j + 1] == a[i]) {
            j++;
        }
        i = j;
        cnt++;
    }
    cout << cnt;
    return 0;
}
