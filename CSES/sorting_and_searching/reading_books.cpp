// Reading Books
// https://cses.fi/problemset/task/1164
#include <bits/stdc++.h>
using namespace std;
int main() {
    int n;
    cin >> n;
    pair<int,int> arr[n];
    for (int i = 0; i < n; i++) {
        int a, b;
        cin >> a >> b;
        arr[i] = {a, b};
    }
    sort(arr, arr + n);
    priority_queue<pair<int,int>> q;
    for (int i = 0; i < n ; i++) {
        if (q.empty()) {
            q.push({-arr[i].second, 1});
            cout << 1;
        }
        else {
            if (-q.top().first < arr[i].first) {

            }
        }
        cout << ' ';
    }
    return 0;
}
