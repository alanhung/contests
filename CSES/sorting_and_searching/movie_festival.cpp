// Movie Festival
// https://cses.fi/problemset/task/1629
#include <bits/stdc++.h>
using namespace::std;
int main() {
    int n;
    cin >> n;
    pair<int, int> ns[n];
    for (int i = 0; i < n; i++) {
        int a, b;
        cin >> a >> b;
        ns[i] = {b, a};
    }
    sort(ns, ns + n);
    int cnt = 0;
    int time = 0;
    for(int i = 0; i <n ;i++) {
        if (time <= ns[i].second) {
            cnt++;
            time = ns[i].first;
        }
    }
    cout << cnt;
    return 0;
}
