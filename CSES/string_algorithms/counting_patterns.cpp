// Counting Patterns
// https://cses.fi/problemset/task/2103
#include <bits/stdc++.h>
using namespace std;
vector<int> sort_cyclic_shifts(string const &s) {
    int n =s.size();
    int alphabet = 128;
    vector<int> p(n), c(n), cnt(max(alphabet, n), 0);
    for(int i = 0; i < n; i++)
        cnt[s[i]]++;
    for (int i  = 1; i < alphabet; i++)
        cnt[i] += cnt[i-1];
    for (int i  = 0; i < n; i++)
        p[--cnt[s[i]]] = i;
    c[p[0]] = 0;
    int classes = 1;
    for (int i  = 1; i < n; i++) {
        if (s[p[i]] != s[p[i-1]])
            classes++;
        c[p[i]] = classes - 1;
    }
    vector<int> pn(n), cn(n);
    for (int h = 0; (1 << h) < n; h++) {
        for (int i = 0; i < n; i++) {
            pn[i] = p[i] - (1 << h);
            if (pn[i] < 0) pn[i] += n;
        }
        fill(cnt.begin(), cnt.begin() + classes, 0);
        for (int i = 0; i < n; i++)
            cnt[c[pn[i]]]++;
        for (int i = 1; i < classes; i++)
            cnt[i] += cnt[i-1];
        for (int i = n- 1; i >= 0; i--)
            p[--cnt[c[pn[i]]]] = pn[i];
        cn[p[0]] = 0;
        classes = 1;
        for (int i = 1; i < n; i++) {
            pair<int,int> cur = {c[p[i]], c[(p[i] + (1 << h)) % n]};
            pair<int,int> prev = {c[p[i-1]], c[(p[i-1] + (1 << h)) % n]};
            if (cur != prev)
                ++classes;
            cn[p[i]] = classes - 1;
        }
        c.swap(cn);
    }
    return p;
}
vector<int> suffix_array_construction(string s) {
    s += "$";
    vector<int> c = sort_cyclic_shifts(s);
    c.erase(c.begin());
    return c;
}
int main() {
    cin.tie(0)->sync_with_stdio(0);
    string t;
    cin >> t;
    int n;
    vector<int> suffix = suffix_array_construction(t);
    cin >> n;
    while(n--) {
        string s;
        cin >> s;
        vector<int>::iterator l = suffix.begin();
        vector<int>::iterator r = suffix.end();
        for (int i = 0; i < s.size(); i++) {
            vector<int>::iterator nl = lower_bound(l, r, s[i], [&](int j, char val) { return j + i >= suffix.size() || t[j + i] < val; });
            vector<int>::iterator nr = upper_bound(l, r, s[i], [&](char val, int j) { return j + i < suffix.size() && val < t[j + i]; });
            l = nl;
            r = nr;
        }
        cout << (r - l) << '\n';
    }
    return 0;
}
