// DMOPC '14 Contest 1 P3 - New Students
// https://dmoj.ca/problem/dmopc14c1p3
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int I, S;
    cin >> I;
    int sum = 0;
    for (int i = 0; i < I; i++) {
        int x;
        cin >> x;
        sum += x;
    }
    cin >> S;
    cout << fixed << setprecision(3);
    for (int i = 0; i < S; i++) {
        int x;
        cin >> x;
        sum += x;
        cout << ((double)sum / (i + 1 + I)) << '\n';
    }
    return 0;
}