// Hamming Distance
// https://cses.fi/problemset/list
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, k;
    cin >> n >> k;
    int a[n];
    for (int i = 0; i < n; i++) {
        string s;
        cin >> s;
        a[i] = 0;
        for (int j = 0; j < s.size(); j++) 
            a[i] += (s[s.size() - 1 - j] == '1') * (1<<j);
    }
    int res = 32;
    for (int i = 0; i < n; i++) {
        for (int j = i + 1; j < n; j++) {
            res = min(res, (int)__builtin_popcount(a[i]^a[j]));
        }
    }
    cout << res;
    return 0;
}
