// a Plus B (Hard)
// wa
#include <algorithm>
#include <iostream>
#include <string>

using namespace std;

string sum(string &a, string &b)
{
    if (a.length() > b.length())
    {
        swap(a, b);
    }

    bool neg = (a[0] == '-' && b[0] == '-');

    string result = "";

    int n1 = a.length();
    int n2 = b.length();

    int stop = neg ? 1 : 0;

    int diff = n2 - n1;

    int carry = 0;

    for (int i = n1 - 1; i >= stop; i--)
    {
        int sum = ((a[i] - '0') + (b[i + diff] - '0') + carry);
        result.push_back(sum % 10 + '0');

        carry = sum / 10;
    }

    for (int i = diff - 1 + stop; i >= stop; i--)
    {
        int sum = ((b[i] - '0') + carry);
        result.push_back(sum % 10 + '0');
        carry = sum / 10;
    }

    if (carry)
    {
        result.push_back(carry + '0');
    }

    if (neg)
    {
        result.push_back('-');
    }

    reverse(result.begin(), result.end());

    return result;
}

bool isSmaller(const string &a, const string &b)
{
    int n1 = a.length();
    int n2 = b.length();

    int skip1 = 0;
    int skip2 = 0;

    if (a[0] == '-')
    {
        n1--;
        skip1++;
    }
    else if (b[0] == '-')
    {
        n2--;
        skip2++;
    }

    if (n1 < n2)
    {
        return true;
    }

    if (n2 < n1)
    {
        return false;
    }

    for (int i = 0; i < n1; i++)
    {
        if (a[i + skip1] < b[i + skip2])
        {
            return true;
        }
        else if (a[i + skip1] > b[i + skip2])
        {
            return false;
        }
    }

    return false;
}

string diff(string &a, string &b)
{
    if (isSmaller(a, b))
    {
        swap(a, b);
    }

    bool bigNeg = (a[0] == '-');

    string result = "";

    int n1 = a.length();
    int n2 = b.length();

    int n1Stop = 0;
    int n2Stop = 0;

    if (bigNeg)
    {
        n1Stop++;
    }
    else
    {
        n2Stop++;
    }

    int diff = n1 - n2;

    int carry = 0;

    for (int i = n2 - 1; i >= n2Stop; i--)
    {
        int sub = ((a[i + diff] - '0') - (b[i] - '0') - carry);

        if (sub < 0)
        {
            sub = sub + 10;
            carry = 1;
        }
        else
        {
            carry = 0;
        }
        result.push_back(sub + '0');
    }

    for (int i = n1 - n2 - 1 + n2Stop; i >= n1Stop; i--)
    {
        if (a[i] == '0' && carry)
        {
            result.push_back('9');
            continue;
        }

        int sub = ((a[i] - '0') - carry);

        result.push_back(sub + '0');

        carry = 0;
    }

    for (int i = result.size() - 1; i >= 0; i--)
    {
        if (i == 0)
        {
            break;
        }
        if (result[i] == '0')
        {
            result.pop_back();
        }
        else
        {
            break;
        }
    }

    if (bigNeg)
    {
        if (result.size() == 1)
        {
            if (result[0] != '0')
            {
                result.push_back('-');
            }
        }
        else
        {

            result.push_back('-');
        }
    }
    reverse(result.begin(), result.end());
    return result;
}

int main()
{
    ios::sync_with_stdio(0);
    cin.tie(0);
    int N;
    cin >> N;

    for (int i = 0; i < N; i++)
    {
        string a;
        string b;
        cin >> a >> b;

        if ((a[0] == '-' && b[0] != '-') || (a[0] != '-' && b[0] == '-'))
        {
            cout << diff(a, b) << '\n';
        }
        else
        {
            cout << sum(a, b) << '\n';
        }
    }
    return 0;
}

