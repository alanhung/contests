// Educational DP Contest AtCoder J - Sushi
// https://dmoj.ca/problem/dpj
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n;
    cin >> n;
    int a = 0;
    int b = 0;
    int c = 0;
    for (int i = 0; i < n; i++) {
        int x;
        cin >> x;
        a += (x == 1);
        b += (x == 2);
        c += (x == 3);
    }
    double dp[n + 1][n + 1][n + 1];
    dp[0][0][0] = 0;
    for (int k = 0; k <= c; k++) {
        for (int j = 0; j <= b + (c - k); j++) {
            for (int i = 0; i <= a + (b - j) + (c - k); i++) {
                if (i + j + k == 0)
                    continue;
                dp[i][j][k] = 0;
                if (i != 0)
                    dp[i][j][k] += ((double)i / n * dp[i-1][j][k]);
                if (j != 0)
                    dp[i][j][k] += ((double)j / n * dp[i+1][j-1][k]);
                if (k != 0)
                    dp[i][j][k] += ((double)k / n * dp[i][j+1][k-1]);
                dp[i][j][k] += 1;
                double p = (double)(n-(i+j+k)) / n;
                dp[i][j][k] /= (1 - p);
            }
        }
    }
    cout << fixed << setprecision(9) << dp[a][b][c] << '\n';
    return 0;
}