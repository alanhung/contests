// Tree Diameter
// https://cses.fi/problemset/task/1131
#include <bits/stdc++.h>
using namespace std;
void dfs(int u, int p, vector<int> adj[], int height[], int diameter[]) {
    int h1 = -1;
    int h2 = -1;
    diameter[u]=0;
    for (int v : adj[u]) {
        if (v != p) {
            dfs(v, u, adj, height, diameter);
            if (height[v] > h1) {
                h2 = h1;
                h1 = height[v];
            } else if (height[v] > h2) {
                h2 = height[v];
            }
            diameter[u] = max(diameter[u], diameter[v]);
        }
    }
    height[u] = max(h1, h2) + 1;
    diameter[u] = max(h1 + h2 + 2, diameter[u]);
}
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n;
    cin >> n;
    vector<int> adj[n];
    for (int i = 1; i < n; i++) {
        int a, b;
        cin >> a >> b;
        adj[a-1].push_back(b-1);
        adj[b-1].push_back(a-1);
    }
    int height[n];
    int diameter[n];
    dfs(0, -1, adj, height, diameter);
    cout << diameter[0] << '\n';
    return 0;
}
