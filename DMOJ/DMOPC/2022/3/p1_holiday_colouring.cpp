#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    long long N, M;
    cin >> N >> M;
    if (N % 2 && M % 2) {
        if (N > M)
            swap(N, M);
        cout << M / 2 * N + N << ' ' << M / 2 * N << '\n';
    } else {
        long long cnt = N * M / 2;
        cout << cnt << ' ' << cnt << '\n';
    }
    return 0;
}