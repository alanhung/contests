// CCC '12 S5 - Mouse Journey'
#include <iostream>
#include <algorithm> 
#include <set>
#include <string>

int main()
{
	std::ios::sync_with_stdio(0);
	std::cin.tie(0);
	int R, C, K;
	std::cin >> R >> C >> K;
	std::set<std::string> gr;

	for (int i = 0; i < K; i++)
	{
		int f, s;
		std::cin >> f >> s;
		gr.insert(std::to_string(f - 1) + ";" + std::to_string(s -1));
	}

	int dp[R][C];
	std::fill(*dp, *dp + R * C, 0);
	dp[0][0] = 1;

	for (int i = 0; i < R; i++)
	{
		for (int j = 0; j < C; j++)
		{
			if (gr.find(std::to_string(i) + ";" + std::to_string(j)) == gr.end())
			{
				if (i - 1 >= 0) dp[i][j] += dp[i - 1][j];
				if (j - 1 >= 0) dp[i][j] += dp[i][j - 1];
			}
		}
	}
	std::cout << dp[R - 1][C - 1];
	return 0;
}