// Hrontario
// https://dmoj.ca/problem/mnyc17p3
#include <bits/stdc++.h>
using namespace std;
vector<int> prefix_function(string s) {
    vector<int> pi(s.size());
    for (int i = 1; i < s.size(); i++) {
        int j = pi[i-1];
        while (j > 0 && s[i] != s[j])
            j = pi[j-1];
        if (s[i] == s[j])
            j++;
        pi[i] = j;
    }
    return pi;
}
int main() {
    cin.tie(0)->sync_with_stdio(0);
    string a, b;
    cin >> a >> b;
    vector<int> p = prefix_function(b + "$" + a);
    for (int i = 0; i < a.size(); i++) {
        cout << a[i];
    }
    for (int i = p[a.size() + b.size()]; i < b.size(); i++) {
        cout << b[i];
    }
    return 0;
}

