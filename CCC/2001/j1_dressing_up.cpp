// CCC '01 J1 - Dressing Up
// https://dmoj.ca/problem/ccc01j1
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int h;
    cin >> h;
    for (int i = 0; i < h / 2; i++) {
        for (int j = 0; j < h; j++)
            cout << " *"[j<(2 * i + 1)];
        for (int j = 0; j < h; j++)
            cout << "* "[j<(h - (2 * i + 1))];
        cout << '\n';
    }
    for (int i = 0; i < 2 * h; i++)
        cout << '*';
    cout << '\n';
    for (int i = h / 2 - 1; i >= 0; i--) {
        for (int j = 0; j < h; j++)
            cout << " *"[j<(2 * i + 1)];
        for (int j = 0; j < h; j++)
            cout << "* "[j<(h - (2 * i + 1))];
        cout << '\n';
    }
    return 0;
}