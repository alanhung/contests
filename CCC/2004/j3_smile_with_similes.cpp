import java.util.Scanner;

public class Main {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int n = sc.nextInt();
    int m = sc.nextInt();
    String[] adjectives = new String[n];
    String[] nouns = new String[m];

    for (int i = 0; i < n; i++) {
      adjectives[i] = sc.next();
    }
    for (int i = 0; i < m; i++) {
      nouns[i] = sc.next();
    }

    sc.close();

    for (int i = 0; i < n; i++) {
      for (int j = 0; j < m; j++) {
        System.out.printf("%s as %s%n", adjectives[i], nouns[j]);
      }
    }

  }
}