// Line Segment Intersections
// https://cses.fi/problemset/task/2190
#include <bits/stdc++.h>
using namespace std;

bool cmp(complex<long long> &x, complex<long long> &y) {
    return x.real() < y.real() || (x.real() == y.real() && x.imag() <  y.imag());
}
long long cross(complex<long long> x, complex<long long> y, complex<long long> z) {
    return (conj(z - x) * (z - y)).imag();
}

long long mid(complex<long long>x, complex<long long> y, complex<long long> z) {
    vector<complex<long long>> v = {x, y, z};
    sort(v.begin(), v.end(), cmp);
    return (v[1] == z);
}

bool opo(long long x, long long y) {
    return ((x > 0) - (x < 0)) != ( (y > 0) - (y < 0)) ;
}

bool solve(complex<long long>a, complex<long long> b, complex<long long> c, complex<long long> d) {
    long long c1 = cross(a, b, c);
    long long c2 = cross(a, b, d);
    long long c3 = cross(c, d, a);
    long long c4 = cross(c, d, b);
    if ((c1 == 0 && mid(a, b, c) ) || (c2 == 0 && mid(a, b, d)) || (c3 == 0 && mid(c, d, a)) || (c4 == 0 && mid(c, d, b))) return 1;
    else if (opo(c1, c2) && opo(c3, c4)) return 1;
    return 0;
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    int t;
    cin >> t;
    while(t--) {
        long long x, y;
        complex<long long> a, b, c, d;
        cin >> x >> y; 
        a = {x, y};
        cin >> x >> y; 
        b = {x, y};
        cin >> x >> y; 
        c = {x, y};
        cin >> x >> y; 
        d = {x, y};
        cout << (solve(a, b, c, d) ? "YES" : "NO") << '\n';
    }
    return 0;
}
