#include <bits/stdc++.h>
using namespace std;
int main() {
    int n;
    cin >> n;
    int num[n];
    int prefix[n + 1];
    int m = 0;
    prefix[0] = 0;
    for (int i = 0; i < n; i++) {
        cin >> num[i];
        prefix[i + 1] = num[i] + prefix[i];
        m = max(m, num[i]);
    }
    bool dp[n][n];
    fill(*dp, *dp + n * n, false);
    for (int i = 0; i < n; i++)
        dp[i][i] = true;
    for (int gap = 1; gap < n; gap++) {
        for (int i = 0; i < n - gap; i++) {
            for (int j = i; j < i + gap; j++) {
                if (dp[i][j] && dp[j + 1][i + gap] && (prefix[j + 1] - prefix[i] == prefix[i + gap + 1] - prefix[j + 1]))
                    dp[i][i + gap] = true;
            }
            int l = i;
            int r = i + gap;
            while(l + 1 < r) {
                if (dp[i][l] && dp[r][i + gap] && (prefix[l + 1] - prefix[i] == prefix[i + gap + 1] - prefix[r])  && dp[l + 1][r - 1] ) {
                    dp[i][i + gap] = true;
                    break;
                }
                if (prefix[l + 1] - prefix[i] < prefix[i + gap + 1] - prefix[r] )
                    l++;
                else
                    r--;
            }
            if (dp[i][i + gap])
                m = max(m, prefix[i + gap + 1] - prefix[i]);
        }
    }
    cout << m << '\n';
    return 0;
}