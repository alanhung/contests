#include <iostream>

using namespace std;

int x(int n)
{
    int mod = n % 4;

    if (mod == 0)
        return n;
    else if (mod == 1)
        return 1;
    else if (mod == 2)
        return n + 1;
    else
        return 0;
}

int x(int l, int r)
{
    return (x(l - 1) ^ x(r));
}

int main()
{
    ios::sync_with_stdio(0);
    cin.tie(0);
    int T;
    cin >> T;
    for (int i = 0; i < T; i++)
    {
        int l, r;
        cin >> l >> r;
        cout << x(l, r) << '\n';
    }

    return 0;
}

