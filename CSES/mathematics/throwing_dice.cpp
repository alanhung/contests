// Throwing Dice
// https://cses.fi/problemset/task/1096
#include <bits/stdc++.h>
using namespace std;
#define MOD 1000000007ll
long long I[6][6] = {{1, 0 , 0 ,0 , 0, 0}, {0, 1, 0, 0, 0, 0}, {0, 0, 1, 0, 0, 0}, {0, 0, 0, 1, 0 , 0}, {0, 0, 0, 0, 1, 0}, {0, 0, 0, 0, 0, 1}};
void mulmod(long long a[6][6], long long b[6][6], long long res[6][6], long long m) {
    for (int i = 0; i < 6; i++) {
        for (int j = 0; j < 6; j++) {
            res[i][j] = 0;
            for (int k = 0; k < 6; k++) {
                res[i][j] = (res[i][j] + a[i][k] * b[k][j] % m) % m;
            }
        }
    }
}
void assign(long long input[6][6], long long output[6][6]) {
    for (int i = 0; i < 6; i++) {
        for (int j = 0; j < 6; j++) {
            output[i][j] = input[i][j];
        }
    }
}
void modpow(long long base[6][6], long long exp, long long res[6][6], long long m) {
    assign(I, res);
    while (exp > 0) {
        if (exp & 1) {
            long long ans[6][6];
            mulmod(base, res, ans, m);
            assign(ans, res);
        }
        long long ans[6][6];
        mulmod(base, base, ans, m);
        assign(ans, base);
        exp >>= 1;
    }
}
int main() {
    cin.tie(0)->sync_with_stdio(0);
    long long n;
    cin >> n;
    long long ma[6][6] = {{0, 1, 0, 0, 0, 0,}, {0, 0, 1, 0, 0, 0}, {0, 0, 0, 1, 0, 0}, {0, 0, 0, 0, 1, 0}, {0, 0, 0, 0, 0, 1}, {1, 1, 1, 1, 1, 1}};
    long long res[6][6];
    modpow(ma, n, res, MOD);
    long long out = 0;
    long long v[6] = {1, 1, 2, 4, 8, 16};
    for (int i = 0; i < 6; i++) {
        out =  (out + res[0][i] * v[i] % MOD ) % MOD;
    }
    cout << out;
    return 0;
}
