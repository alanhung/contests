// Grid Paths
// https://cses.fi/problemset/task/1638
#include <bits/stdc++.h>
using namespace std;
#define MOD 1000000007;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n;
    cin >> n;
    int dp[n + 1][n + 1];
    fill(*dp, *dp + (n + 1) * (n + 1), 0);
    dp[0][1]=1;
    for(int i  =1; i <= n; i++) {
        for (int j = 1; j <=n;j++) {
            char k;
            cin >> k;
            if (k == '.') {
                dp[i][j] = dp[i-1][j]+dp[i][j-1];
                dp[i][j] %= MOD;
            }
        }
    }
    cout << dp[n][n];
    return 0;
}
