#include <algorithm>
#include <iostream>
#include <queue>
#include <vector>

using namespace std;

int main()
{
    ios::sync_with_stdio(0);
    cin.tie(0);
    int n, r, c;
    cin >> n >> r >> c;
    vector<int> floors;
    vector<int> adj[r * c];
    char board[r * c];
    cin >> board[0];
    if (board[0] == '.')
        floors.push_back(0);
    for (int i = 1; i < c; i++)
    {
        cin >> board[i];
        if (board[i] == '.')
        {
            floors.push_back(i);
            if (board[i - 1] == '.')
            {
                adj[i].push_back(i - 1);
                adj[i - 1].push_back(i);
            }
        }
    }
    for (int i = 1; i < r; i++)
    {
        cin >> board[c * i];
        if (board[c * i] == '.')
        {
            floors.push_back(c * i);
            if (board[(i - 1) * c] == '.')
            {
                adj[c * i].push_back((i - 1) * c);
                adj[(i - 1) * c].push_back(c * i);
            }
            if (board[(i - 1) * c + 1] == '.')
            {
                adj[c * i].push_back((i - 1) * c + 1);
                adj[(i - 1) * c + 1].push_back(c * i);
            }
        }
        for (int j = 1; j < c - 1; j++)
        {
            cin >> board[c * i + j];
            if (board[c * i + j] == '.')
            {
                floors.push_back(c * i + j);
                if (board[(i - 1) * c + j] == '.')
                {
                    adj[c * i + j].push_back((i - 1) * c + j);
                    adj[c * (i - 1) + j].push_back(i * c + j);
                }
                if (board[i * c + j - 1] == '.')
                {
                    adj[c * i + j].push_back(i * c + j - 1);
                    adj[c * i + j - 1].push_back(i * c + j);
                }
                if (board[(i - 1) * c + j - 1] == '.')
                {
                    adj[c * i + j].push_back((i - 1) * c + j - 1);
                    adj[c * (i - 1) + j - 1].push_back(i * c + j);
                }
                if (board[(i - 1) * c + j + 1] == '.')
                {
                    adj[c * i + j].push_back((i - 1) * c + j + 1);
                    adj[c * (i - 1) + j + 1].push_back(i * c + j);
                }
            }
        }
        cin >> board[c * (i + 1) - 1];
        if (board[c * (i + 1) - 1] == '.')
        {
            floors.push_back(c * (i + 1) - 1);
            if (board[i * c + -1] == '.')
            {
                adj[c * (i + 1) - 1].push_back(i * c - 1);
                adj[c * i - 1].push_back((i + 1) * c - 1);
            }
            if (board[i * c - 2] == '.')
            {
                adj[c * (i + 1) - 1].push_back(i * c - 2);
                adj[c * i - 2].push_back((i + 1) * c - 1);
            }
            if (board[(i + 1) * c - 2] == '.')
            {
                adj[c * (i + 1) - 1].push_back((i + 1) * c - 2);
                adj[c * (i + 1) - 1].push_back((i + 1) * c - 2);
            }
        }
    }
    vector<int> rooms;
    queue<int> q;
    bool visited[r * c];
    fill(visited, visited + r * c, false);
    for (int i = 0; i < floors.size(); i++)
    {
        if (!visited[floors[i]])
        {
            visited[floors[i]] = true;
            int m = 1;
            q.push(floors[i]);
            while (!q.empty())
            {
                int u = q.front();
                q.pop();
                for (int it = 0; it < adj[u].size(); it++)
                {
                    if (!visited[adj[u][it]])
                    {
                        q.push(adj[u][it]);
                        m++;
                        visited[adj[u][it]] = true;
                    }
                }
            }
            rooms.push_back(m);
        }
    }
    int count = 0;
    sort(rooms.begin(), rooms.end(), greater<int>());
    for (int i = 0; i < rooms.size(); i++)
    {
        if (n - rooms[i] >= 0)
        {
            n -= rooms[i];
            count++;
        }
        else
        {
            break;
        }
    }
    if (count != 1)
    {
        cout << count << " rooms, " << n << " square metre(s) left over" << '\n';
    }
    else
    {
        cout << count << " room, " << n << " square metre(s) left over" << '\n';
    }

    /*     for (int i = 0; i < rooms.size(); i++) */
    /*     { */
    /*         cout << rooms[i] << ' '; */
    /*     } */
    /*     cout << '\n'; */
    /*  */
    /*     for (int i = 0; i < r * c; i++) */
    /*     { */
    /*         cout << i << ":\n"; */
    /*         for (int j = 0; j < adj[i].size(); j++) */
    /*         { */
    /*             cout << '\t' << adj[i][j] << ' '; */
    /*         } */
    /*         cout << '\n'; */
    /*     } */
    /*     cout << '\n'; */
    return 0;
}