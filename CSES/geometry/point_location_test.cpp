// Point Location Test
// https://cses.fi/problemset/task/2189
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int t;
    cin >> t;
    while(t--) {
        long long x1, y1, x2, y2, x3, y3;
        cin >> x1 >> y1 >> x2 >> y2 >> x3 >> y3;
        complex<long long> p1(x1, y1), p2(x2, y2), p3(x3, y3);
        long long c = (conj(p3 - p1) * (p3 - p2)).imag();
        if (c > 0) cout << "LEFT\n";
        else if (c < 0) cout << "RIGHT\n";
        else cout << "TOUCH\n";
    }
    return 0;
}
