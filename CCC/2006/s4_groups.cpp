// CCC '06 S4 - Groups
// https://dmoj.ca/problem/ccc06s4
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    while(true) {
        int n;
        cin >> n;
        if (!n)
            break;
        int f[n][n];
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++) {
                int x;
                cin >> x;
                f[i][j] = x-1;
            }
        // find identity element
        int e = -1;
        for (int i = 0; i < n; i++)
            if (f[0][i] == 0) e = i;
        if (e == -1) {
            cout << "no\n";
            continue;
        }

        // check for identity element
        bool good = true;
        for (int i = 1; i < n; i++) {
            good = f[i][e] == i;
            if (!good)
                break;
        }
        if (!good) {
            cout << "no\n";
            continue;
        }
        for (int i = 0; i < n; i++) {
            good = f[e][i] == i;
            if (!good)
                break;
        }
        if (!good) {
            cout << "no\n";
            continue;
        }

        // check for inverse
        bool found = false;
        for (int i = 0; i < n; i++) {
            found = false;
            for (int j = 0; j < n; j++) {
                found = (f[i][j] == e && f[j][i] == e);
                if (found)
                    break;
            }
        }
        if (!found) {
            cout << "no\n";
            continue;
        }

        // check for associativity
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                for (int k = 0; k < n; k++) {
                    good = f[f[i][j]][k] == f[i][f[j][k]];
                    if (!good)
                        break;
                }
                if (!good)
                    break;
            }
            if (!good)
                break;
        }
        if (!good) cout << "no\n";
        else cout << "yes\n";
    }
    return 0;
}