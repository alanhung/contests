#include <iostream>

int main()
{
	std::ios::sync_with_stdio(0);
	std::cin.tie(0);
    int N, M;
	std::cin >> N >> M;
    int grid[N][M];

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < M; j++)
        {
            std::cin >> grid[i][j];
        }
    }

    int dp[M];
    dp[0] = grid[0][0];

    for (int i = 1; i < M; i++)
    {
        dp[i] = (grid[0][i] + dp[i - 1]);
    }

    for (int i = 1; i < N; i++)
    {
		dp[0] += grid[i][0];

        for (int j = 1; j < M; j++)
        {
			dp[j] = std::min(dp[j - 1], dp[j]) + grid[i][j];
        }
    }

	std::cout << dp[M - 1];
    return 0;
}