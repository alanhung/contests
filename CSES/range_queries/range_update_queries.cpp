// Range Update Queries
// https://cses.fi/problemset/task/1651
#include <bits/stdc++.h>
using namespace std;
long long query(long long tree[], int i) {
    long long res = 0;
    while(i > 0) {
        res += tree[i];
        i -= i & (-i);
    }
    return res;
}
void update(long long tree[], int n, int i, long long x) {
    while(i <= n) {
        tree[i] += x;
        i += i & (-i);
    }
}
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, q;
    cin >> n >> q;
    int arr[n + 1];
    arr[0]=0;
    long long tree[n + 1];
    fill(tree, tree + n + 1, 0);
    for (int i = 1; i <= n; i++) {
        cin >> arr[i];
        update(tree, n, i, arr[i] - arr[i-1]);
    }
    while(q--) {
        int ind;
        cin >> ind;
        if (ind == 1) {
            int a, b;
            long long u;
            cin >> a >> b >> u;
            update(tree, n, a, u);
            update(tree, n, b + 1, -u);
        } else {
            int k;
            cin >> k;
            cout << query(tree, k) << '\n';
        }
    }
    return 0;
}
