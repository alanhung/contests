// Playlist
// https://cses.fi/problemset/task/1141
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n;
    cin >> n;
    vector<int> v;
    map<int, pair<bool, int>> m;
    int best = 0;
    int option = 0;
    int j = 0;
    for (int i = 0; i < n; i++) {
        int k;
        cin >> k;
        v.push_back(k);
        map<int, pair<bool, int>>::iterator it = m.find(k);
        if (it != m.end() && it->second.first)  {
            option = i - it->second.second;
            while(j <= it->second.second) {
                m[v[j]].first = 0;
                j++;
            }
            it->second.first = 1;
            it->second.second = i;
        } else {
            option++;
            if (it == m.end())
                m.insert({k, {1, i}});
            else {
                m[k].first = 1;
                m[k].second = i;
            }
            best = max(best, option);
        }
    }
    cout << best;
    return 0;
}
