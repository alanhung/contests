// CCC '08 S5 - Nukit
// https://dmoj.ca/problem/ccc08s5
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int t;
    cin >> t;
    int moves[][4] = {{2, 1, 0, 2}, {1, 1, 1, 1}, {0, 0, 2, 1}, {0, 3, 0, 0}, {1, 0, 0, 1}};
    bool dp[31][31][31][31];
    fill(***dp, ***dp + 31 * 31 * 31 * 31, 0); 
    for (int i = 0; i < 31; i++) {
        for (int j = 0; j < 31; j++) {
            for (int k = 0; k < 31; k++) {
                for (int l = 0; l < 31; l++) {
                    for (int m = 0; m < 5; m++) {
                        int na = i - moves[m][0];
                        int nb = j - moves[m][1];
                        int nc = k - moves[m][2];
                        int nd = l - moves[m][3];
                        if (na >= 0 && nb >= 0 && nc >= 0 && nd >= 0 && !dp[na][nb][nc][nd]) {
                            dp[i][j][k][l] = 1;
                        }
                    }
                }
            }
        }
    }
    while (t--) {
        int a, b, c, d;
        cin >> a >> b >> c >> d;
        if (dp[a][b][c][d]) cout << "Patrick\n";
        else cout << "Roland\n";
    }
    return 0;
}