// Cyclic Shifts
// https://dmoj.ca/problem/ccc20j4
#include <bits/stdc++.h>
using namespace std;

vector<int> sort_cyclic_shifts(string const &s) {
    int n =s.size();
    vector<int> p(n), c(n), cnt(max(n, 27), 0);
    for(int i = 0; i < n; i++)
        cnt[s[i] - 64]++;
    for (int i  = 1; i < 27; i++)
        cnt[i] += cnt[i-1];
    for (int i  = 0; i < n; i++)
        p[--cnt[s[i] - 64]] = i;
    c[p[0]] = 0;
    int classes = 1;
    for (int i  = 1; i < n; i++) {
        if (s[p[i]] != s[p[i-1]])
            classes++;
        c[p[i]] = classes - 1;
    }
    vector<int> pn(n), cn(n);
    for (int h = 0; (1 << h) < n; h++) {
        for (int i = 0; i < n; i++) {
            pn[i] = p[i] - (1 << h);
            if (pn[i] < 0) pn[i] += n;
        }
        fill(cnt.begin(), cnt.begin() + classes, 0);
        for (int i = 0; i < n; i++)
            cnt[c[pn[i]]]++;
        for (int i = 1; i < classes; i++)
            cnt[i] += cnt[i-1];
        for (int i = n- 1; i >= 0; i--)
            p[--cnt[c[pn[i]]]] = pn[i];
        cn[p[0]] = 0;
        classes = 1;
        for (int i = 1; i < n; i++) {
            pair<int,int> cur = {c[p[i]], c[(p[i] + (1 << h)) % n]};
            pair<int,int> prev = {c[p[i-1]], c[(p[i-1] + (1 << h)) % n]};
            if (cur != prev)
                ++classes;
            cn[p[i]] = classes - 1;
        }
        c.swap(cn);
    }
    return p;
}
vector<int> suffix_array_construction(string s) {
    s += "@";
    vector<int> c = sort_cyclic_shifts(s);
    c.erase(c.begin());
    return c;
}


int main() {
    string t, s;
    cin >> t >> s;
    int n = s.size();
    s = s + s;
    vector<int> suffix = suffix_array_construction(t);
    /* for (int i  = 0; i < suffix.size(); i++) */
    /*     cout << suffix[i] << ' '; */
    /* cout << '\n'; */
    bool good = false;
    for (int i = 0; i < n; i++) {
        vector<int>::iterator l = suffix.begin();
        vector<int>::iterator r = suffix.end();
        /* cout << s.substr(i, n) << '\n'; */
        for (int j = 0; j < n; j++) {
            vector<int>::iterator nl = lower_bound(l, r, s[i + j], [&](int k, char val) { return j + k >= t.size() || t[j + k] < val; } );
            vector<int>::iterator nr = upper_bound(l, r, s[i + j], [&](char val, int k) { return j + k < t.size() && val < t[j + k];});
            /* cout << nl - suffix.begin() << ' ' << nr - suffix.begin() << '\n'; */
            l = nl;
            r = nr;
            if (l == r)
                break;
        }
        /* cout << '\n'; */
        good = l != r;
        if (good)
            break;
    }
    if (good) cout << "yes";
    else cout << "no";
    return 0;
}