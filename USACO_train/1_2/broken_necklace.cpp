/*
TASK: beads
LANG: C++
ID: alan93h1
*/
#include <bits/stdc++.h>
using namespace std;
int main() {
  ios::sync_with_stdio(0);
  ofstream fout("beads.out");
  ifstream fin("beads.in");

  int n;
  string str;
  fin >> n >> str;
  str = str + str;
  int size = str.size();
  int left[size + 1][2];
  int right[size + 1][2];
  left[0][0] = 0;
  left[0][1] = 0;
  right[size][0] = 0;
  right[size][1] = 0;
  for (int i = 1; i <= size; i++) {
    if (str[i - 1] == 'r') {
      left[i][0] = left[i - 1][0] + 1;
      left[i][1] = 0;
    } else if (str[i - 1] == 'b') {
      left[i][1] = left[i - 1][1] + 1;
      left[i][0] = 0;
    } else {
      left[i][0] = left[i - 1][0] + 1;
      left[i][1] = left[i - 1][1] + 1;
    }
  }
  for (int i = size - 1; i >= 0; i--) {
    if (str[i] == 'r') {
      right[i][0] = right[i + 1][0] + 1;
      right[i][1] = 0;
    } else if (str[i] == 'b') {
      right[i][1] = right[i + 1][1] + 1;
      right[i][0] = 0;
    } else {
      right[i][0] = right[i + 1][0] + 1;
      right[i][1] = right[i + 1][1] + 1;
    }
  }
  int res = 0;
  for (int i = 0; i <= size; i++) {
    res = max(res, max(left[i][0], left[i][1]) + max(right[i][0], right[i][1]));
  }
  fout << min(res, n) << '\n';
  return 0;
}
