// DMOPC '15 Contest 1 P2 - Itami and Squad
// https://dmoj.ca/problem/dmopc15c1p2
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, l, r;
    cin >> n >> l >> r;
    int sol[n];
    for (int i = 0; i < n; i++) cin >> sol[i];
    sort(sol, sol + n, greater<int>());
    r--;
    int sum = 0;
    for (int i = r; i < n; i += l)
        sum += sol[i];
    cout << sum << '\n';
    return 0;
}