// Another Contest 3 Problem 3 - Lexicographically Largest Common Subsequence
// https://dmoj.ca/problem/acc3p3
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n;
    cin >> n;
    vector<string> arr(n);
    vector<int> pos(n, 0);
    for (int i = 0; i < n; i++) cin >> arr[i];
    char c = 'z';
    string res;
    while (c >= 'a') {
        bool good = true;
        vector<int> npos = pos;
        for (int i = 0; i < n; i++) {
            int j = pos[i];
            while (j < (int)arr[i].size() && arr[i][j] != c)
                j++;
            if (j == (int)arr[i].size()) {
                good = false;
                break;
            }
            npos[i] = j + 1;
        }
        if (good) {
            res.push_back(c);
            pos = npos;
        }
        else
            c--;

    }
    if (res.empty()) cout << -1 << '\n';
    else cout << res << '\n';
    return 0;
}