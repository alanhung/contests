#include <bits/stdc++.h>
using namespace std;

int find(int x, int parent[])
{
    while (x != parent[x])
        x = parent[x];
    return x;
}

bool same(int a, int b, int parent[])
{
    return find(a, parent) == find(b, parent);
}

void unite(int a, int b, int parent[], int size[])
{
    a = find(a, parent);
    b = find(b, parent);
    if (size[a] < size[b])
        swap(a, b);
    size[a] += size[b];
    parent[b] = a;
}

int main()
{
    cin.tie(0)->sync_with_stdio(0);
    int N, Q;
    cin >> N >> Q;
    int parent[N];
    int size[N];
    for (int i = 0; i < N; i++)
    {
        parent[i] = i;
        size[i] = 1;
    }
    for (int i = 0; i < Q; i++)
    {
        int c;
        cin >> c;
        if (c == 1)
        {
            int a, b;
            cin >> a >> b;
            if (!same(a - 1, b - 1, parent))
                unite(a - 1, b - 1, parent, size);
        }
        else
        {
            int a;
            cin >> a;
            cout << size[find(a - 1, parent)] << '\n';
        }
    }
    return 0;
}