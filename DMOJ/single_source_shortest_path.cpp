// Single Source Shortest Path
#include <iostream>
#include <vector>
#include <limits>
#include <tuple>

int main()
{
	std::ios::sync_with_stdio(0);
	std::cin.tie(0);
	int N, M;
	std::cin >> N >> M;

	std::vector<std::tuple<int,int,int>> edges;

	for (int i = 0; i < M; i++)
	{
		int u, v, w;
		std::cin >> u >> v >> w;
		edges.push_back({u - 1, v - 1, w});
		edges.push_back({v - 1, u - 1, w});
	}

	int dist[N];

	for (int i = 0; i < N; i++)
	{
		dist[i] = std::numeric_limits<int>::max();
	}
	dist[0] = 0;

	for (int round = 0; round < N - 1; round++)
	{
		for (int i = 0; i < edges.size(); i++)
		{
			int a, b, w;
			std::tie(a, b, w) = edges[i];

			if (dist[a] != std::numeric_limits<int>::max() && dist[a] + w < dist[b])
			{
				dist[b] = dist[a] + w;
			}
		}
	}

	for (int i = 0; i < N; i++)
	{
		std::cout << (dist[i] == std::numeric_limits<int>::max() ? -1 : dist[i]) << '\n';
	}
	return 0;
}
