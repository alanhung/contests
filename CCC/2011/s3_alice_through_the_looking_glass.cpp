#include <iostream>
#include <cmath>
using namespace std;
bool f(int m, int bx, int by, int x, int y) {
    int p = pow(5, m - 1);
    if (x >= bx + p && x < bx + 4 * p && y < by + p || x >= bx + 2 * p && x < bx + 3 * p && y >= by + p && y < by + 2 * p)
        return true;
    if (m == 1)
        return false;
    if (x >= bx + p && x < bx + 2 * p && y >= by + p && y < by + 2 * p)
        return f(m - 1, bx + p, by + p, x, y);
    if (x >= bx + 2 * p && x < bx + 3 * p && y >= by + 2 * p && y < by + 3 * p)
        return f (m - 1, bx + 2 * p, by + 2 * p, x, y);
    if (x >= bx + 3 * p && x < bx + 4 * p && y >= by + p && y < by + 2 * p)
        return f(m - 1, bx + 3 * p, by + p, x, y);
    return false;
}

int main() 
{
    cin.tie(0)->sync_with_stdio(0);
    int T;
    cin >> T;
    for (int i = 0; i< T; i++)
    {
        int m, x, y;
        cin >> m >> x >> y;
        if (f(m, 0, 0, x, y))
            cout << "crystal\n";
        else
            cout << "empty\n";
    }
    return 0;
}