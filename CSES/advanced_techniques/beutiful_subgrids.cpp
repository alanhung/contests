// Beutfiul Subgrids
// https://cses.fi/problemset/task/2137
#include <bits/stdc++.h>
#pragma GCC target("popcnt")
using namespace std;
int n;
long long cnt;
bitset<3000> row[3000];
int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin >> n;
    for (int i = 0; i < n; i++)
        cin >> row[i];
    for (int i = 0; i < n; i++)
        for (int j = i + 1; j < n; j++) {
            long long t = (row[i]&row[j]).count();
            cnt += t * (t - 1);
        }
    cout << (cnt>>1);
    return 0;
}
