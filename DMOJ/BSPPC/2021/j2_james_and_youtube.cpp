#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios::sync_with_stdio(0);
    cin.tie(0);

    int M;
    cin >> M;

    vector<pair<int, int>> classes;

    for (int i = 0; i < M; i++)
    {
        int x, y;
        cin >> x >> y;
        classes.push_back({x, y});
    }

    int N;
    cin >> N;

    for (int i = 0; i < N; i++)
    {
        int a, b;
        cin >> a >> b;

        bool o = false;

        for (int j = 0; j < classes.size(); j++)
        {
            if (!(a > classes[j].second && b > classes[j].second) && !(a < classes[j].first && b < classes[j].first))
            {
                cout << "Break is Over! Stop playing games! Stop watching Youtube!\n";
                o = true;
                break;
            }
        }

        if (!o)
        {
            cout << ":eyy:\n";
        }
    }

    return 0;
}

