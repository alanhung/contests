// Increasing Subsequence
// https://cses.fi/problemset/list/
#include <bits/stdc++.h>
using namespace std;

int lis(vector<int> const & arr) {
    int n = arr.size();
    vector<int> d(n+1, INT_MAX);
    d[0]=INT_MIN;
    for (int i = 0; i < n; i++) {
        int j = upper_bound(d.begin(), d.end(), arr[i]) - d.begin();
        if (d[j-1] < arr[i] && arr[i] < d[j])
            d[j]=arr[i];
    }
    int ans = 0;
    for (int i = 0; i <= n; i++) {
        if (d[i] < INT_MAX)
            ans = i;
    }
    return ans;
}

int main() {
    int n;
    cin >> n;
    vector<int> arr(n);
    for (int i = 0; i < n; i++) cin >> arr[i];
    cout << lis(arr);
    return 0;
}
