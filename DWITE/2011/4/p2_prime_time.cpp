// DWITE '11 R4 #2 - Prime Time
// https://dmoj.ca/problem/dwite11c4p2
#include <bits/stdc++.h>
using namespace::std;
void sieve(int spf[], int n) {
    for (int i = 1; i <= n; i++)
        spf[i] = i;
    for (int p = 2; p * p <= n; p++) {
        if (spf[p] == p) {
            for (int i = p * p; i <= n; i += p) {
                if (spf[i] == i) spf[i] = p;
            }
        }
    }
}
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int spf[10001];
    sieve(spf, 10000);
    int t = 5;
    while(t--) {
        int x;
        cin >> x;
        int cnt[10000];
        fill(cnt, cnt + 10000, 0);
        int biggest = 0;
        for (int i = x; i >= 2; i--) {
            int k = i;
            while (k != 1) {
                int p = spf[k];
                int c = 0;
                while (k % p == 0) {
                    c++;
                    k /= p;
                }
                cnt[p] += c;
                biggest = max(biggest, p);
            }
        }
        for (int i = 2; i < 10000; i++) {
            if (cnt[i]) {
                cout << i << '^' << cnt[i];
                if (i != biggest) cout << " * ";
                else cout << '\n';
            }
        }
    }
    return 0;
}