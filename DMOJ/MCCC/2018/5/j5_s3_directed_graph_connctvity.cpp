#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int N, M;
    cin >> N >> M;
    vector<pair<int,int>> v;
    list<int> adj[N];
    for (int i = 0; i < M; i++) {
        int f, s;
        cin >> f >> s;
        v.push_back({f-1,s-1});
        adj[f-1].push_back(s-1);
    }
    for (int i = 0; i < M; i++) {
        adj[v[i].first].remove(v[i].second);
        bool found = false;
        bool visitied[N];
        fill(visitied, visitied + N, false);
        queue<int> q;
        q.push(0);
        visitied[0] = true;
        while (!q.empty()) {
            int u = q.front();
            q.pop();
            for (list<int>::iterator it = adj[u].begin(); it != adj[u].end(); ++it) {
                if (!visitied[*it]) {
                    visitied[*it]=true;
                    if (*it == N - 1) {
                        found = true;
                        break;
                    }
                    q.push(*it);
                }
            }
            if (found)
                break;
        }
        adj[v[i].first].push_back(v[i].second);
        if (found)
            cout << "YES\n";
        else
            cout << "NO\n";
    }
    return 0;
}

