// IOI '94 P1 - The Triangle
#include <iostream>
#include <vector>
#include <tuple>
#include <map>

int search(int currentIndex, const std::vector<std::tuple<int, int, int>> &triangle, std::map<int, int> &memo)
{
  std::tuple<int, int, int> current = triangle[currentIndex];
  int max = std::get<0>(current);
  if (memo.find(currentIndex) != memo.end())
  {
    return memo[currentIndex];
  }

  if (std::get<1>(current) == -1)
  {
    memo.insert({currentIndex, max});
    return memo[currentIndex];
  }

  int left = search(std::get<1>(current), triangle, memo);

  int right = search(std::get<2>(current), triangle, memo);

  if (left > right)
  {
    max += left;
  }
  else
  {
    max += right;
  }

  memo.insert({currentIndex, max});
  return memo[currentIndex];
}

int main()
{
  int N;
  std::cin >> N;
  std::vector<std::tuple<int, int, int>> triangle;

  int first;
  std::cin >> first;
  triangle.push_back(std::make_tuple(first, -1, -1));

  int index = 0;
  for (int i = 2; i <= N; i++)
  {
    int left;
    std::cin >> left;
    triangle.push_back(std::make_tuple(left, -1, -1));
    index++;
    std::get<1>(triangle[index - (i - 1)]) = index;
    for (int j = 1; j < i - 1; j++)
    {
      int current;
      std::cin >> current;
      triangle.push_back(std::make_tuple(current, -1, -1));
      index++;
      std::get<1>(triangle[index - (i - 1)]) = index;
      std::get<2>(triangle[index - i]) = index;
    }
    int right;
    std::cin >> right;
    triangle.push_back(std::make_tuple(right, -1, -1));
    index++;
    std::get<2>(triangle[index - i]) = index;
  }

  std::map<int, int> memo;

  //for (int i = 0; i < triangle.size(); i++)
  //{
  //std::cout << i << '(' << std::get<0>(triangle[i]) << ") -> " << std::get<1>(triangle[i]) << ", " << std::get<2>(triangle[i]) << '\n';
  //}

  std::cout << search(0, triangle, memo);

  return 0;
}