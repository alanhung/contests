#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int A, K;
    cin >> A >> K;
    pair<int,string> arr[A];
    for (int i = 0; i < A; i++) {
        cin >> arr[i].first;
        cin >> arr[i].second;
    }
    bool checked[A];
    fill(checked, checked + A, 0);
    while(K--) {
        int x;
        cin >> x;
        for (int i = 0; i < A; i++) {
            if (!checked[i] && x >= arr[i].first) {
                cout << arr[i].second << '\n';
                checked[i]=1;
            }
        }
    }
    return 0;
}