// Convex Hull
// https://cses.fi/problemset/task/2195
#include <bits/stdc++.h>
using namespace std;
bool cmp(complex<long long> &x, complex<long long> &y) {
    return (x.real() < y.real()) || (x.real() == y.real() && x.imag() < y.imag());
}

long long cross(complex<long long> a, complex<long long> b, complex<long long> c) {
    return (conj(c - a) * (c - b)).imag();
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n;
    cin >> n;
    complex<long long> arr[n];
    for (int i = 0; i < n; i++) {
        long long x, y;
        cin >> x >> y;
        arr[i] = {x, y};
    }
    sort(arr, arr + n, cmp);
    vector<complex<long long>> hull;
    long long s = 0;
    for (int t = 0; t < 2; t++) {
        for (int i = 0; i < n; i++) {
            while (hull.size() - s >= 2) {
                long long c = cross(hull[hull.size()-2], hull[hull.size()-1], arr[i]);
                if (c <= 0)
                    break;
                hull.pop_back();
            }
            hull.push_back(arr[i]);
        }
        hull.pop_back();
        s = hull.size();
        reverse(arr, arr + n);
    }
    cout << hull.size() << '\n';
    for (int i = 0; i < hull.size(); i++) cout << hull[i].real() << ' ' << hull[i].imag() << '\n';
    return 0;
}
