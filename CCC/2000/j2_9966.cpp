// CCC '00 J2 - 9966
// https://dmoj.ca/problem/ccc00j2
#include <bits/stdc++.h>
using namespace std;

bool zoe(int d) {
    return d == 0 || d == 8 || d == 1;
}

bool sn(int a, int b) {
    return (a == 6 && b == 9) || (a == 9 && b == 6);
}


bool check(int x, int p) {
    if (p < 0)
        return true;
    if (p == 0)
        return zoe(x);

    int r = x % 10;
    int l = x / pow(10, p);

    if ( (l == r && zoe(l)) || sn(l, r))
        return check((x - l * pow(10, p)) / 10, p - 2);
    return false;
}

int main() {
    int m, n;
    cin >> m >> n;
    int cnt = 0;
    for (int i = m; i <= n; i++) {
        int x = i;
        int p = 0;
        while (x / 10 != 0) {
            x /= 10;
            p++;
        }
        if (check(i, p))
            cnt++;
    }
    cout << cnt;
    return 0;
}