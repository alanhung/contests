// Tower of Hanoi
// https://cses.fi/problemset/task/2165
#include <bits/stdc++.h>
using namespace std;
void hanoi(int start, int end, int n) {
    if (n == 1) {
        cout << start << ' ' << end << '\n';
        return;
    }
    hanoi(start, 6 - start - end, n - 1);
    cout << start << ' ' << end << '\n';
    hanoi(6 - start - end, end, n - 1);
}
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n;
    cin >> n;
    cout << pow(2, n) - 1 << '\n';
    hanoi(1, 3, n);
    return 0;
}
