#include <bits/stdc++.h>
using namespace std;
int main() {
    int N;
    cin >> N;
    int s[N];
    for (int i = 0; i < N; i++)
        cin >> s[i];
    int i = 0;
    int j =  N - 1;
    int a = s[i];
    int b = s[j];
    while (j - i > 1) {
        if (a <= b) {
            i++;
            a += s[i];
        } else {
            j--;
            b += s[j];
        }
    }
    if (a == b)
        cout << i + 1 << '\n';
    else
        cout << "Andrew is sad.\n";
    return 0;
}