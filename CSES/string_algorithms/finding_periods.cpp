// Finding Periods
// https://cses.fi/problemset/task/1733
#include <bits/stdc++.h>
using namespace std;
vector<int> zalgo(string s) {
    int n = s.size();
    vector<int> z(n);
    int l = 0;
    int r = 0;
    for (int i = 1; i < n; i++) {
        if (i <= r)
            z[i] = min(r - i + 1, z[i - l]);
        while (i + z[i] < n && s[z[i]] == s[i + z[i]])
            ++z[i];
        if (i + z[i]  - 1 > r) {
            l = i;
            r = i + z[i]-1;
        }
    }
    return z;
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    string s;
    cin >> s;
    vector<int> z = zalgo(s);
    int n = s.size();
    for (int i = 1; i < n; i++)
        if (i + z[i] == n) cout << i << ' ';
    cout << n;
    return 0;
}
