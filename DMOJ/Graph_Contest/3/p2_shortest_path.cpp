#include <iostream>
#include <limits>
#include <tuple>
#include <vector>

int main()
{
    int N, M;
    std::cin >> N >> M;

    std::vector<std::tuple<int, int, int>> edges;

    for (int i = 0; i < M; i++)
    {
        int a, b, c;
        std::cin >> a >> b >> c;
        edges.push_back({a - 1, b - 1, c});
    }

    int dist[N];

    std::fill(dist, dist + N, std::numeric_limits<int>::max());

    dist[0] = 0;

    for (int round = 0; round < N - 1; round++)
    {

        for (int i = 0; i < edges.size(); i++)
        {
            int a, b, c;
            std::tie(a, b, c) = edges[i];

            if (dist[a] != std::numeric_limits<int>::max() && dist[a] + c < dist[b])
            {
                dist[b] = dist[a] + c;
            }
        }
    }

    std::cout << dist[N - 1];
    return 0;
}

