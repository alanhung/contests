// CCC '02 S1 - The Students' Council Breakfast
// https://dmoj.ca/problem/ccc02s1
#include <bits/stdc++.h>
using namespace std;
int main() {
    int a, b, c, d, t;
    cin >> a >> b >> c >> d >> t;
    int cnt = 0;
    int la = INT_MAX;
    for (int i = 0; i * a <= t; i++) {
        for (int j = 0; i * a + j * b <= t; j++) {
            for (int k = 0; i * a + j * b + k * c <= t; k++) {
                for (int l = 0; i * a + j * b + k * c + l * d <= t; l++) {
                    if (i * a + j * b + k * c + l * d == t) {
                        cout << "# of PINK is " << i << " # of GREEN is " << j << " # of RED is " << k << " # of ORANGE is " << l << '\n';
                        cnt++;
                        la = min(la, i + j + k + l);
                    }
                }
            }
        }
    }
    cout << "Total combinations is " << cnt << ".\n";
    cout << "Minimum number of tickets to print is " << la << ".\n";
    return 0;
}