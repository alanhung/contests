// Another Contest 8 Problem 2 - Unnecessary Trash Push
// https://dmoj.ca/problem/acc8p2
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int t;
    cin >> t;
    while(t--) {
        int n, k;
        cin >> n >> k;
        int acc = 0;
        int cnt = 0;
        for (int i = 0; i < n; i++) {
            int x;
            cin >> x;
            acc += x;
            if (acc >= k) {
                cnt++;
                acc = 0;
            }
        }
        cout << cnt << '\n';
    }
    return 0;
}