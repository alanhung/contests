// An Animal Contest 7 P1 - Squirrnect 4
// https://dmoj.ca/problem/aac7p1
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int t;
    cin >> t;
    while(t--) {
        int w, h;
        cin >> w >> h;
        if ( (w < 4 && h < 4 ) || (w == 1) || (h == 1 && w < 7) ) cout << "bad\n";
        else cout << "good\n";
    }
    return 0;
}