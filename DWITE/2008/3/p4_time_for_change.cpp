#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int Q = 5;
    while(Q--) {
        int m, n;
        cin >> m >> n;
        int wt[n];
        for (int i = 0; i < n; i++)
            cin >> wt[i];

        int dp[m + 1];
        fill(dp, dp + m + 1, INT_MAX - 1);
        dp[0] = 0;
        for (int i = 0; i < n; i++)
            for (int j = wt[i]; j <= m; j++)
                dp[j] = min(dp[j], dp[j - wt[i]] + 1);
        cout << dp[m] << '\n';
    }
    return 0;
}