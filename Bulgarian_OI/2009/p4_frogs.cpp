#include <bits/stdc++.h>
using namespace std;
int main()
{
    cin.tie(0)->sync_with_stdio(0);
    int N;
    cin >> N;
    int bales[N];
    int jumps[N];
    int out[N];
    for (int i = 0; i < N; i++)
        cin >> bales[i];
    for (int i = 0; i < N; i++)
        cin >> jumps[i];
    vector<int> st;
    st.push_back(bales[N - 1]);
    if (jumps[N - 1])
        out[N - 1] = -1;
    else
        out[N - 1] = bales[N - 1];
    for (int i = N - 2; i >= 0; i--)
    {
        while (!st.empty() && bales[i] >= st[st.size() - 1])
            st.pop_back();
        if (jumps[i] > st.size())
            out[i] = -1;
        else if (jumps[i] == 0)
            out[i] = bales[i];
        else
            out[i] = st[st.size() - jumps[i]];
        st.push_back(bales[i]);
    }
    for (int i = 0; i < N; i++)
    {
        cout << out[i] << ' ';
    }
    return 0;
}

