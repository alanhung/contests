#include <bits/stdc++.h>
using namespace std;
int main()
{
    int N;
    cin >> N;
    pair<int, int> arr[N];
    for (int i = 0; i < N; i++)
    {
        int f, s;
        cin >> f >> s;
        arr[i] = make_pair(f, s);
    }
    sort(arr, arr + N);
    double m = numeric_limits<double>::min();
    for (int i = 0; i < N - 1; i++)
    {
        if (arr[i + 1].second > arr[i].second)
            m = max(m, ((double)(arr[i + 1].second - arr[i].second)) / (arr[i + 1].first - arr[i].first));
        else
            m = max(m, ((double)(arr[i].second - arr[i + 1].second)) / (arr[i + 1].first - arr[i].first));
    }
    cout << fixed << setprecision(17) << m;
    return 0;
}