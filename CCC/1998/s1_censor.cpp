// CCC '98 S1 - Censor
// https://dmoj.ca/problem/ccc98s1
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n;
    cin >> n;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');
    string line;
    while (n--) {
        getline(cin, line);
        stringstream ss(line);
        string token;
        while (ss >> token) {
            if (token.size() == 4)
                cout << "**** ";
            else
                cout << token << ' ';
        }
        cout << '\n';
    }
    return 0;
}