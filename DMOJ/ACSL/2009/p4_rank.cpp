// Rank
#include <iostream>
#include <vector>
#include <set>

bool cycle(std::vector<int> adj[], int v, bool visited[], bool recStack[], std::set<int> &u)
{
	bool c = false;
	if (recStack[v])
	{
		return true;
	}

	if (!visited[v])
	{
		visited[v] = true;
		recStack[v] = true;

		std::vector<int>::iterator i;
		for (i = adj[v].begin(); i != adj[v].end(); ++i)
		{
			if (cycle(adj, *i, visited, recStack, u))
			{
				c = true;
			}
		}
	}
	
	if (c)
	{
		u.insert(v);
	}

	recStack[v] = false;
	return c;
}

int main() 
{
	std::ios::sync_with_stdio(0);
	std::cin.tie(0);
	int N, K;

	std::cin >> N >> K;
	std::vector<int> adj[N];

	for (int i = 0; i < K; i++)
	{
		int a, b, sa, sb;
		std::cin >> a >> b >> sa >> sb;

		if (sa > sb)
		{
			adj[a - 1].push_back(b - 1);
		}
		else
		{
			adj[b - 1].push_back(a - 1);
		}
	}

	bool visited[N];
	bool recStack[N];

	std::set<int> u;

	for (int i = 0; i < N; i++)
	{
		visited[i] = false;
		recStack[i] = false;
	}

	for (int i = 0; i < N; i++)
	{
		cycle(adj, i, visited, recStack, u);
	}

	std::cout << u.size();

	return 0;
}

