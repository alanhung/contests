#include <bits/stdc++.h>
using namespace std;

void func(int u, int parent, vector<int> adj[], bool stk[], bool visited[], int low[], int &cnt, bool ap[],
          vector<int> &paths, stack<int> &s, int disc[])
{
    if (u == 1)
    {
        for (int i = 0; i < 26; i++)
            if (stk[i])
                paths.push_back(i);
    }
    visited[u] = true;
    stk[u] = true;
    low[u] = disc[u] = cnt++;
    s.push(u);
    for (int v : adj[u])
    {
        if (v == parent)
            continue;

        if (!visited[v])
        {
            func(v, u, adj, stk, visited, low, cnt, ap, paths, s, disc);
            low[u] = min(low[u], low[v]);
            if (low[v] > low[u])
            {
                ap[u] = true;
                ap[v] = true;
            }
        }
        else if (stk[v])
            low[u] = min(low[u], low[v]);
    }
    if (low[u] == disc[u])
    {
        int w = s.top();
        while (w != u)
        {
            stk[u] = false;
            s.pop();
            w = s.top();
        }
        stk[w] = false;
        s.pop();
    }
}

int main()
{
    cin.tie(0)->sync_with_stdio(0);
    vector<int> adj[26];
    while (true)
    {
        char a, b;
        cin >> a >> b;
        if (a == '*')
            break;
        adj[a - 'A'].push_back(b - 'A');
        adj[b - 'A'].push_back(a - 'A');
    }

    bool stk[26];
    bool visited[26];
    int low[26];
    bool ap[26];
    int disc[26];
    stack<int> s;
    for (int i = 0; i < 26; i++)
    {
        stk[i] = false;
        visited[i] = false;
        low[i] = numeric_limits<int>::max();
        ap[i] = false;
        disc[i] = -1;
    }
    int cnt = 0;
    vector<int> paths;
    func(0, -1, adj, stk, visited, low, cnt, ap, paths, s, disc);
    fill(visited, visited + 26, 0);
    vector<pair<char, char>> result;
    int cnt2 = 0;
    for (int u : paths)
    {
        if (ap[u])
        {
            visited[u] = true;
            for (int v : adj[u])
            {
                if (!visited[v] && ap[v])
                {
                    cnt2++;
                    cout << ((char)(u + 'A')) << ((char)(v + 'A')) << '\n';
                }
            }
        }
    }
    cout << "There are " << cnt2 << " disconnecting roads.\n";
    /*  */
    /*     for (int i = 0; i < paths.size(); i++) */
    /*         cout << paths[i] << ' '; */
    /*     cout << '\n'; */
    /*     for (int i = 0; i < 26; i++) */
    /*         cout << ap[i] << ' '; */
    /*     cout << '\n'; */
    /*     for (int i = 0; i < 26; i++) */
    /*         cout << low[i] << ' '; */
    /*     cout << '\n'; */
    return 0;
}