// DMOPC '14 Contest 2 P5 - Sawmill Scheme
// https://dmoj.ca/problem/dmopc14c2p5
#include <bits/stdc++.h>
using namespace std;
int main() {
    int n, m;
    cin >> n >> m;
    vector<int> adj[n];
    for (int i = 0; i < m; i++) {
        int a, b;
        cin >> a >> b;
        adj[a-1].push_back(b-1);
    }
    bool sawmill[n];
    fill(sawmill, sawmill + n, 1);
    double dp[n];
    fill(dp, dp + n, 0);
    dp[0]=1;
    for (int i = 0; i < n; i++) {
        int n = adj[i].size();
        for (int u : adj[i])
            dp[u] += dp[i]*(1.0/n);
        if (n) sawmill[i]=0;
    }
    cout << fixed << setprecision(17);
    for (int i = 0; i < n; i++)
        if (sawmill[i]) cout << dp[i] << '\n';
    return 0;
}