import java.util.Scanner;

public class Main {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int t = Integer.parseInt(sc.nextLine());
    for (int i = 0; i < t; i++) {
      int n = Integer.parseInt(sc.nextLine());
      String first = sc.nextLine();
      String seccond = sc.nextLine();
      String result = "";
      for (int j = n - 1; j >= 0; j--) {
        result += seccond.charAt(j);
        result += first.charAt(j);
      }
      System.out.println(result);
    }
    sc.close();
  }
}