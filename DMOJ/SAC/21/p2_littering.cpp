#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios::sync_with_stdio(0);
    cin.tie(0);
    int N, K;
    vector<long> l;
    cin >> N >> K;

    for (int i = 0; i < N; i++)
    {
        long current;
        cin >> current;
        l.push_back(current);
    }
    sort(l.begin(), l.end(), greater<long>());
    long sum = 0;

    for (int i = 0; i < K; i++)
    {
        sum += l[i];
    }
    cout << sum;
    return 0;
}