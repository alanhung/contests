// CCC '09 J4 - Signage
#include <iostream>
#include <string>
#include <vector>

int main()
{
    int w;
    std::cin >> w;

    std::vector<std::string> texts = {"WELCOME", "TO", "CCC", "GOOD", "LUCK", "TODAY"};

    std::vector<std::string> resut;

    int temp = w;

    bool f = true;

    for (int i = 0; i < texts.size(); i++)
    {
        int n = texts[i].length();
        int c = f ? 0 : 1;
        // std::cout << "LEVEL" << '\n';
        // std::cout << "temp " << temp << '\n';
        // std::cout << "n " << n << '\n';
        // std::cout << "c " << c << '\n';
        if (temp - n - c >= 0)
        {
            if (!f)
            {
                resut.push_back(".");
            }

            resut.push_back(texts[i]);

            temp = temp - n - c;

            f = false;
        }
        else
        {
            i--;

            if ((resut.size() == 1) && (temp > 0))
            {
                resut.push_back(".");
                temp--;
            }
            int count = 1;
            while (temp != 0)
            {
                resut[count] += ".";
                temp--;
                count = (count + 2) % (resut.size() % 2 == 1 ? resut.size() - 1 : resut.size());
            }

            f = true;
            temp = w;

            for (int j = 0; j < resut.size(); j++)
            {
                std::cout << resut[j];
            }
            std::cout << '\n';

            resut.clear();
        }
    }

    if (resut.size() == 1)
    {
        resut.push_back(".");
        temp--;
    }

    int count = 1;
    while (temp != 0)
    {
        resut[count] += ".";
        temp--;
        count = (count + 2) % (resut.size() % 2 == 1 ? resut.size() - 1 : resut.size());
    }

    for (int i = 0; i < resut.size(); i++)
    {
        std::cout << resut[i];
    }
    std::cout << '\n';

    return 0;
}