// CCC '00 S1 - Slot Machines
// https://dmoj.ca/problem/ccc00s1
#include <bits/stdc++.h>
using namespace std;
int check(int &m, int &n, int l, int p) {
    m++;
    n--;
    if (m == l) {
        n += p; 
        m = 0;
    }
    return 1;
}
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n;
    int m[3];
    cin >> n >> m[0] >> m[1] >> m[2];
    int cnt = 0;
    while (n > 0) {
        cnt += check(m[0], n, 35, 30);
        if (n == 0)
            break;
        cnt += check(m[1], n, 100, 60);
        if (n == 0)
            break;
        cnt += check(m[2], n, 10, 9);
    }
    cout << "Martha plays " << cnt << " times before going broke.";
    return 0;
}