// COCI '06 Contest 1 #5 Bond
// https://dmoj.ca/problem/coci06c1p5
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n;
    cin >> n;
    int arr[n][n];
    for (int i=0;i<n;i++)
        for (int j=0;j<n;j++)
            cin >> arr[i][j];
    double dp[1<<n];
    fill(dp,dp+(1<<n),0);
    dp[0]=1;
    for(int i=0;i<n;i++) {
        for (int s=1;s<(1<<n);s++) {
            if (__builtin_popcount(s)==(i+1)) {
                for (int j =0; j < n; j++) {
                    if (s & (1 << j) && arr[i][j])
                        dp[s] = max(dp[s], arr[i][j]/100.0*dp[s^(1<<j)]);
                }
            }
        }
    }
    cout << fixed << setprecision(6) << dp[(1<<n)-1]*100 << '\n';
    return 0;
}