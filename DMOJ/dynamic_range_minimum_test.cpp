// https://dmoj.ca/problem/segtree
#include <bits/stdc++.h>
using namespace std;

int query(int tree[], int n, int a, int b) {
    a += n;
    b += n;
    int res = INT_MAX;
    while(a <= b) {
        if(a & 1) res = min(res, tree[a++]);
        if (!(b&1)) res = min(res, tree[b--]);
        a /= 2;
        b /= 2;
    }
    return res;
}

void update(int tree[], int n, int i, int x) {
    i += n;
    tree[i] = x;
    for (i /= 2; i > 0; i/= 2)
        tree[i] = min(tree[2 *i], tree[2 * i + 1]);
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, m;
    cin >> n >> m;
    int tree[2 * n];
    for (int i = 0; i < n; i++) {
        int x;
        cin >> x;
        update(tree, n, i, x);
    }
    while(m--) {
        char c;
        int a, b;
        cin >> c >> a >> b;
        if (c == 'M')
            update(tree, n, a, b);
        else
            cout << query(tree, n, a, b) << '\n';
    }
    return 0;
}


