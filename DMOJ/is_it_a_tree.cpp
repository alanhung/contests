#include <bits/stdc++.h>
using namespace std;

bool cy(int u, int parent, bool visited[], vector<int> adj[])
{
    visited[u] = true;
    for (int v : adj[u])
    {
        if (!visited[v])
        {
            if (cy(v, u, visited, adj))
                return true;
        }
        else if (v != parent)
            return true;
    }
    return false;
}
int main()
{
    cin.tie(0)->sync_with_stdio(0);
    vector<int> adj[4];
    for (int i = 0; i < 4; i++)
    {
        for (int j = 0; j < 4; j++)
        {
            bool c;
            cin >> c;
            if (c)
                adj[i].push_back(j);
        }
    }
    bool visited[4];
    fill(visited, visited + 4, 0);
    if (cy(0, -1, visited, adj))
    {
        cout << "No";
        return 0;
    }

    for (int i = 0; i < 4; i++)
    {
        if (!visited[i])
        {
            cout << "No";
            return 0;
        }
    }
    cout << "Yes";
    return 0;
}

