// String Functions
// https://cses.fi/problemset/task/2107
#include <bits/stdc++.h>
using namespace std;
vector<int> prefix_function(string const &s) {
    int n = s.size();
    vector<int> pi(n, 0);
    for (int i = 1; i < n; i++) {
        int j = pi[i-1];
        while(j > 0 && s[i] != s[j])
            j = pi[j - 1];
        if (s[i] == s[j])
            j++;
        pi[i] = j;
    }
    return pi;
}
vector<int> z_function(string const &s) {
    int n = s.size();
    vector<int> z(n);
    z[0] = 0;
    int l = 0;
    int r = 0;
    for (int i = 1; i < n; i++) {
        if (i <= r)
            z[i] = min(r - i + 1, z[i - l]);
        while (i + z[i] < n && s[z[i]] == s[i + z[i]])
            ++z[i];
        if (i + z[i] - 1 > r) {
            l = i;
            r = i + z[i] - 1;
        }
    }
    return z;
}
int main() {
    string s;
    cin >> s;
    vector<int> pi = prefix_function(s);
    vector<int> z = z_function(s);
    for (int i = 0; i < s.size(); i++)
        cout << z[i] << " \n"[i==s.size()-1];
    for (int i = 0; i < s.size(); i++)
        cout << pi[i] << " \n"[i==s.size()-1];
    return 0;
}
