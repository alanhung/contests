// CCC '11 S4 - Blood Distribution
// https://dmoj.ca/problem/ccc11s4
#include <bits/stdc++.h>
using namespace std;
long long bfs(int s, int t, vector<int> &parent, vector<vector<int>> &adj, vector<vector<long long>> &capacity) {
    fill(parent.begin(), parent.end(), -1);
    parent[s] = -2;
    queue<pair<int, long long>> q;
    q.push({s, LONG_LONG_MAX});
    while (!q.empty()) {
        int u = q.front().first;
        long long flow = q.front().second;
        q.pop();
        for (int v : adj[u]) {
            if (parent[v] == -1 && capacity[u][v]) {
                parent[v] = u;
                long long new_flow = min(flow, capacity[u][v]);
                if (v == t)
                    return new_flow;
                q.push({v, new_flow});
            }
        }
    }
    return 0;
}

long long maxflow(int s, int t, int n, vector<vector<int>> &adj, vector<vector<long long>> &capacity) {
    long long flow = 0;
    vector<int> parent(n);
    int new_flow;

    while ((new_flow = bfs(s, t, parent, adj, capacity))) {
        flow += new_flow;
        int cur = t;
        while (cur != s) {
            int prev = parent[cur];
            capacity[prev][cur] -= new_flow;
            capacity[cur][prev] += new_flow;
            cur = prev;
        }
    }
    return flow;
}

int main() {
    int b[8];
    int p[8];
    for (int i = 0; i < 8; i++)
        cin >> b[i];
    for (int i = 0; i < 8; i++)
        cin >> p[i];
    vector<vector<int>> adj(10);
    vector<vector<long long>> capacity(10, vector<long long>(10));
    for (int i = 0; i < 10; i++)
        for (int j= 0; j < 10; j++)
            capacity[i][j]=0;
    for (int i = 0; i < 8; i++) {
        adj[0].push_back(i + 1);
        adj[i + 1].push_back(0);
        capacity[0][i + 1] = b[i];
    }
    for (int i = 0; i < 8; i++) {
        adj[i + 1].push_back(9);
        adj[9].push_back(i + 1);
        capacity[i + 1][9] = p[i];
    }
    // On
    for (int i = 2; i <= 8; i++) {
        adj[1].push_back(i);
        adj[i].push_back(1);
        capacity[1][i] = LONG_LONG_MAX;
    }
    // Op
    for (int i = 4; i <= 8; i += 2) {
        adj[2].push_back(i);
        adj[i].push_back(2);
        capacity[2][i] = LONG_LONG_MAX;
    }
    // An
    adj[3].push_back(4);
    adj[4].push_back(3);
    capacity[3][4] = LONG_LONG_MAX;
    adj[3].push_back(7);
    adj[7].push_back(3);
    capacity[3][7] = LONG_LONG_MAX;
    adj[3].push_back(8);
    adj[8].push_back(3);
    capacity[3][8] = LONG_LONG_MAX;
    // Ap
    adj[4].push_back(8);
    adj[8].push_back(4);
    capacity[4][8] = LONG_LONG_MAX;
    // Bn
    adj[5].push_back(6);
    adj[6].push_back(5);
    capacity[5][6] = LONG_LONG_MAX;
    adj[5].push_back(7);
    adj[7].push_back(5);
    capacity[5][7] = LONG_LONG_MAX;
    adj[5].push_back(8);
    adj[8].push_back(5);
    capacity[5][8] = LONG_LONG_MAX;
    // Bp
    adj[6].push_back(8);
    adj[8].push_back(6);
    capacity[6][8] = LONG_LONG_MAX;
    // ABn
    adj[7].push_back(8);
    adj[8].push_back(7);
    capacity[7][8] = LONG_LONG_MAX;
    cout << maxflow(0, 9, 10, adj, capacity);
    return 0;
}