// Counting Towers
// https://cses.fi/problemset/task/2413
#include <bits/stdc++.h>
using namespace std;
#define MOD 1000000007ll
long long dp[1000001][2];
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int t;
    cin >> t;
    dp[1][0] = 1;
    dp[1][1] = 1;
    for (int i = 2; i <= 1000000; i++) {
        dp[i][0] = (dp[i-1][0]*4%MOD + dp[i-1][1])%MOD;
        dp[i][1] = (dp[i-1][0] + dp[i-1][1]*2%MOD)%MOD;
    }
    while(t--) {
        int n;
        cin >> n;
        cout << (dp[n][1] + dp[n][0])%MOD << '\n';
    }
    return 0;
}
