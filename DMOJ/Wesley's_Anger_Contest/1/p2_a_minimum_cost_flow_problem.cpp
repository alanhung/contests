#include <bits/stdc++.h>
using namespace std;
void dfs(int u, vector<int> adj[], bool visited[]) {
    for (int v : adj[u]) {
        if (!visited[v]) {
            visited[v] = true;
            dfs(v, adj, visited);
        }
    }
}
int main() {
    int n, m, k;
    cin >> n >> m >> k;
    vector<int> adj[n];
    for (int i = 0; i < m; i++) {
        int x, y;
        cin >> x >> y;
        x--; y--;
        adj[x].push_back(y);
        adj[y].push_back(x);
    }
    int cpn = 0;
    bool visited[n];
    fill(visited, visited + n, false);
    for (int i = 0; i < n; i++) {
        if (!visited[i]) {
            cpn++;
            dfs(i, adj, visited);
        }
    }
    cout << max((cpn - 1) - min(m - ((n - 1 ) - (cpn - 1)), k), 0)  << '\n';
    return 0;
}