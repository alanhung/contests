// Fibonacci Numbers
// https://cses.fi/problemset/task/1722
#include <bits/stdc++.h>
using namespace std;
#define MOD 1000000007ll
void mulmod(long long a[2][2], long long b[2][2], long long res[2][2], long long m) {
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            res[i][j] = 0;
            for (int k = 0; k < 2; k++) {
                res[i][j] = (res[i][j] + a[i][k] * b[k][j] % m) % m;
            }
        }
    }
}
void assign(long long input[2][2], long long output[2][2]) {
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            output[i][j] = input[i][j];
        }
    }
}

void modpow(long long base[2][2], long long exp, long long res[2][2], long long m) {
    res[0][0] = 1;
    res[0][1] = 0;
    res[1][0] = 0;
    res[1][1] = 1;
    while (exp > 0) {
        if (exp & 1) {
            long long ans[2][2];
            mulmod(base, res, ans, m);
            assign(ans, res);
        }
        long long ans[2][2];
        mulmod(base, base, ans, m);
        assign(ans, base);
        exp >>= 1;
    }
}
int main() {
    cin.tie(0)->sync_with_stdio(0);
    long long n;
    cin >> n;
    long long ma[2][2] = {{0, 1}, {1, 1}};
    long long res[2][2];
    modpow(ma, n, res, MOD);
    cout << res[0][1];
    return 0;
}
