// Sum of Three Values
// https://cses.fi/problemset/task/1641
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, x;
    cin >> n >> x;
    pair<int ,int > arr[n];
    for (int i = 0;  i< n; i++) {
        int temp;
        cin >> temp;
        arr[i] = {temp, i};
    }
    sort(arr, arr + n);
    for (int k = 0; k < n - 2; k++) {
        int i = k + 1;
        int j = n - 1;
        while (i < j) {
            if (arr[i].first + arr[j].first + arr[k].first> x)
                j--;
            else if (arr[i].first + arr[j].first + arr[k].first< x)
                i++;
            else {
                cout << arr[k].second + 1 << ' ' << arr[i].second + 1 << ' ' << arr[j].second + 1;
                return 0;
            }
                
        }
    }
    cout << "IMPOSSIBLE";
    return 0;
}
