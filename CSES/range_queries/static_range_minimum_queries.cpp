// Static Range Minimum Queries
// https://cses.fi/problemset/task/1647
#include <bits/stdc++.h>
using namespace std;
int table[200000][20];
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, q;
    cin >> n >> q;
    for (int i = 0; i < n; i++) {
        cin >> table[i][0];
    }
    for (int x = 1; (1<<x) <= n; x++) {
        for (int i = 0; i + (1 << x) <= n; i++) {
            table[i][x] = min(table[i][x-1], table[i+(1<<(x-1))][x-1]);
        }
    }
    while(q--) {
        int a, b;
        cin >> a >> b;
        a--;
        b--;
        int k = log2(b-a+1);
        cout << min(table[a][k], table[b-(1<<k)+1][k]) << '\n';
    }
    return 0;
}
