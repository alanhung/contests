// Finding Boarders
// https://cses.fi/problemset/task/1732
#include <bits/stdc++.h>
using namespace std;

vector<int> prefix_function(string s) {
    vector<int> pi(s.size());
    for (int i = 1 ; i < s.size(); i++) {
        int j = pi[i-1];
        while (j > 0 && s[i] != s[j])
            j = pi[j-1];
        if (s[i] == s[j])
            j++;
        pi[i]=j;
    }
    return pi;
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    string s;
    cin >> s;
    vector<int> pi = prefix_function(s);
    vector<int> res;
    int j = pi[s.size()-1];
    while (j > 0) {
        res.push_back(j);
        j = pi[j-1];
    }
    sort(res.begin(), res.end());
    for (int e : res) cout << e << ' ';
    return 0;
}
