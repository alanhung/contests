// An Animal Contest 4 P1 - Dasher's Digits
// https://dmoj.ca/problem/aac4p1
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, m;
    cin >> n >> m;
    string s;
    vector<int> pos;
    for (int i = 0; i < n; i++) {
        char c;
        cin >> c;
        if (c == '0')
            pos.push_back(i);
        s.push_back(c);
    }
    vector<int> val(m);
    for (int i = 0; i < m; i++) cin >> val[i];
    int j = 0;
    int ma = 0;
    for (int i = 0; i < (int)val.size(); i++) {
        if (val[i] >= ma) {
            ma = val[i];
            j = i;
        }
    }
    j = pos[j];
    int i = (j + 1) % s.size();
    while (i != j) {
        if (s[i] != '0')
            cout << s[i];
        i++;
        i %= s.size();
    }
    cout << '\n';
    return 0;
}