// Knapsack 1

#include <iostream>
#include <vector>

using namespace std;

long solve(const int &N, const int &W, const vector<int> &wt, const vector<int> &val)
{
  long dp[W + 1];
  fill(dp, dp + W + 1, 0);

  for (int i = 0; i < N; i++)
  {
    for (int j = W; j >= wt[i]; j--)
    {
      dp[j] = max(dp[j], (long)val[i] + (long)dp[j - wt[i]]);
    }
  }

  return dp[W];
}

int main()
{
  ios::sync_with_stdio(0);
  cin.tie(0);
  int N, W;
  cin >> N >> W;
  vector<int> wt;
  vector<int> val;

  for (int i = 0; i < N; i++)
  {
    int w, v;
    cin >> w;
    cin >> v;
    wt.push_back(w);
    val.push_back(v);
  }

  cout << solve(N, W, wt, val);
  return 0;
}