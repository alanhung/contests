// Apartments
// https://cses.fi/problemset/task/1084
#include <bits/stdc++.h>
using namespace std;
int main() {
    int n, m, k;
    cin >> n >> m >> k;
    int ns[n];
    int ms[m];
    for (int i = 0; i < n; i++)
        cin >> ns[i];
    for (int i = 0; i < m; i++)
        cin >> ms[i];
    sort(ns, ns + n);
    sort(ms, ms + m);
    int cnt = 0;
    int j = 0;
    int i = 0;
    while(i < n && j < m) {
        if (ms[j] < ns[i] - k)
            j++;
        else if (ms[j] > ns[i] + k)
            i++;
        else {
            i++;
            j++;
            cnt++;
        }
    }
    cout << cnt;
    return 0;
}
