#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, q;
    cin >> n >> q;
    int cookies[n][n];
    int sprinkles[n][n];
    fill(*cookies, *cookies + n * n, 0);
    fill(*sprinkles, *sprinkles + n * n, 0);
    while(q--) {
        int type;
        cin >> type;
        if (type == 1) {
            int x, y;
            cin >> x >> y;
            cookies[x-1][y-1] += 1;
        } else {
            int x1, y1, x2, y2;
            cin >> x1 >> y1 >> x2 >> y2;
            x1--;
            y1--;
            x2--;
            y2--;
            for (int i = x1; i <= x2; i++) {
                for (int j = y1; j <= y2; j++) {
                    sprinkles[i][j] += cookies[i][j];
                }
            }
        }
    }
    long long res = 0;
    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            res += sprinkles[i][j];
    cout << res << '\n';
    return 0;
}