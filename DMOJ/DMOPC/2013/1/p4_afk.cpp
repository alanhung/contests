#include <bits/stdc++.h>
using namespace std;
int main()
{
    cin.tie(0)->sync_with_stdio(0);
    int t;
    cin >> t;
    for (int test = 0; test < t; test++)
    {
        int l, w;
        cin >> l >> w;
        pair<int, int> start, end;
        char board[w][l];
        for (int i = 0; i < w; i++)
            for (int j = 0; j < l; j++)
            {
                cin >> board[i][j];
                if (board[i][j] == 'C')
                    start = make_pair(i, j);
                if (board[i][j] == 'W')
                    end = make_pair(i, j);
            }
        int dist[w][l];
        fill(*dist, *dist + l * w, -1);
        bool visited[w][l];
        fill(*visited, *visited + l * w, false);
        queue<pair<int, int>> q;
        dist[start.first][start.second] = 0;
        visited[start.first][start.second] = true;
        q.push({start.first, start.second});
        while (!q.empty())
        {
            int a = q.front().first;
            int b = q.front().second;
            q.pop();
            if (a - 1 >= 0 && !visited[a - 1][b] && board[a - 1][b] != 'X')
            {
                visited[a - 1][b] = true;
                dist[a - 1][b] = dist[a][b] + 1;
                q.push({a - 1, b});
            }
            if (a + 1 < w && !visited[a + 1][b] && board[a + 1][b] != 'X')
            {
                visited[a + 1][b] = true;
                dist[a + 1][b] = dist[a][b] + 1;
                q.push({a + 1, b});
            }
            if (b + 1 < l && !visited[a][b + 1] && board[a][b + 1] != 'X')
            {
                visited[a][b + 1] = true;
                dist[a][b + 1] = dist[a][b] + 1;
                q.push({a, b + 1});
            }
            if (b - 1 >= 0 && !visited[a][b - 1] && board[a][b - 1] != 'X')
            {
                visited[a][b - 1] = true;
                dist[a][b - 1] = dist[a][b] + 1;
                q.push({a, b - 1});
            }
        }
        if (dist[end.first][end.second] >= 60 || dist[end.first][end.second] == -1)
            cout << "#notworth\n";
        else
            cout << dist[end.first][end.second] << '\n';
    }
    return 0;
}