// Pennies in the Ring
// https://dmoj.ca/problem/ccc08s2
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    while(1) {
        int r;
        cin >> r;
        if (!r)
            break;
        int cnt = 0;
        int x = 1;
        int y = r-1;
        while(y != 0) {
            if (x * x + y * y <= r * r) {
                cnt += y;
                x++;
            } else
                y--;
        }
        cout << 4 * (r + cnt) + 1 << '\n';
    }
    return 0;
}