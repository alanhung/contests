// Graph Paths 1
// https://cses.fi/problemset/task/1723
#include <bits/stdc++.h>
using namespace std;
#define MOD 1000000007ll
vector<vector<long long>> mul(vector<vector<long long>>a, vector<vector<long long>> b) {
    vector<vector<long long>> c(a.size(), vector<long long>(b[0].size()));
    for (int i = 0; i < a.size(); i++) {
        for (int j = 0; j < b[0].size(); j++) {
            for (int k = 0; k < a[0].size(); k++) {
                c[i][j] = (c[i][j] + a[i][k] * b[k][j] % MOD) % MOD;
            }
        }
    }
    return c;
}
vector<vector<long long>> pow(vector<vector<long long>> base, int exp) {
    vector<vector<long long>> res(base.size(), vector<long long>(base.size()));
    for (int i = 0; i < base.size(); i++) res[i][i] = 1;
    while (exp > 0) {
        if (exp & 1) res = mul(res, base);
        exp >>= 1;
        base = mul(base, base);
    }
    return res;
}
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, m, k;
    cin >> n >> m >> k;
    vector<vector<long long>> res(n, vector<long long>(n));
    while(m--) {
        int a, b;
        cin >> a >> b;
        res[a-1][b-1]++;
    }
    res = pow(res, k);
    cout << res[0][n-1];
    return 0;
}
