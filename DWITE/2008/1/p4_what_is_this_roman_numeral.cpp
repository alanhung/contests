#include <bits/stdc++.h>
using namespace std;
int val(char u)
{
    if (u == 'I')
        return 1;
    if (u == 'V')
        return 5;
    if (u == 'X')
        return 10;
    if (u == 'L')
        return 50;
    if (u == 'C')
        return 100;
    if (u == 'D')
        return 500;
    if (u == 'M')
        return 1000;
    return -1;
}
int main()
{
    cin.tie(0)->sync_with_stdio(0);
    int t = 5;
    while (t--)
    {
        string s;
        getline(cin, s);
        int res = 0;
        for (int i = 0; i < s.length(); i++)
        {
            int s1 = val(s[i]);
            if (i + 1 < s.length())
            {
                int s2 = val(s[i + 1]);
                if (s1 >= s2)
                    res += s1;
                else
                {
                    res += (s2 - s1);
                    i++;
                }
            }
            else
                res += s1;
        }
        cout << res << '\n';
    }
    return 0;
}