#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    double l, h, w, d;
    cin >> l >> h >> w >> d;
    cout << fixed;
    cout << setprecision(2);
    cout << l * w * h - (M_PI * (d/2) * (d/2) * h) << '\n';
    return 0;
}