// There's a Hole in the Bucket
#include <iostream>
#include <limits>

int main()
{
	std::ios::sync_with_stdio(0);
	std::cin.tie(0);
    while (true)
    {
        int M, N;
        std::cin >> M >> N;
        if (!M)
        {
            break;
        }

        int dp[M + 1];
        std::fill(dp, dp + M + 1, std::numeric_limits<int>::max() - 1);
		dp[0] = 0;

        int wt[N];

        for (int i = 0; i < N; i++)
        {
            std::cin >> wt[i];
        }

        for (int i = 1; i <= M; i++)
        {
            for (int j = 0; j < N; j++)
            {
                if (wt[j] <= i)
                {
                    dp[i] = std::min(dp[i], dp[i-wt[j]] + 1);
                }
            }
        }

		std::cout << ((dp[M] < std::numeric_limits<int>::max() - 1) ? dp[M] : -1) << '\n';
    }
    return 0;
}

