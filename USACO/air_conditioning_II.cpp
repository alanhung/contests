#include <bits/stdc++.h>
using namespace std;
int main() {
  cin.tie(0)->sync_with_stdio(0);
  int N, M;
  cin >> N >> M;
  int s[N], t[N], c[N];
  int a[M], b[M], p[M], m[M] ;
  for (int i = 0; i < N; i++) {
    cin >> s[i] >> t[i] >> c[i];
    s[i]--;
    t[i]--;
  }
  for (int i = 0; i < M; i++) {
    cin >> a[i] >> b[i] >> p[i] >> m[i];
    a[i]--;
    b[i]--;
  }
  int temp[101];
  fill(temp, temp + 101, 0);
  for (int i = 0; i < N; i++) {
    temp[s[i]] -= c[i];
    temp[t[i]+1] += c[i];
  }

  int res = INT_MAX;
  for (int i = 0; i < (1 << M); i++) {
    int arr[101];
    for (int j = 0; j < 101; j++)
      arr[j] = temp[j];
    int cost = 0;
    for (int j = 0; j < M; j++) {
      if (i & (1 << j)) {
        cost += m[j];
        arr[a[j]] += p[j];
        arr[b[j]+1] -= p[j];
      }
    }
    bool bad = 0;
    if (arr[0] < 0)
      bad = 1;
    if (!bad){
      for (int j = 1; j < 100; j++) {
        arr[j] += arr[j-1];
        if (arr[j] < 0) {
          bad = 1;
          break;
        }
      }
    }  
    if (!bad)
      res = min(res, cost);
  }
  cout << res << '\n';
  return 0;
}
