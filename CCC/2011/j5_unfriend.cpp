#include <bits/stdc++.h>
using namespace std;
int f(int u, vector<int> adj[], bool visited[]) {
    visited[u] = true;
    if (adj[u].size() == 0)
        return 2;
    int temp = 1;
    for (vector<int>::iterator it = adj[u].begin(); it != adj[u].end(); ++it) {
        temp *= f(*it, adj, visited);
    }
    return 1 + temp;
}
int main() {
    int N;
    cin >> N;
    vector<int> adj[N];
    for (int i = 0; i < N - 1; i++) {
        int c;
        cin >> c;
        c--;
        adj[c].push_back(i);
    }
    bool visited[N];
    fill(visited, visited + N, false);
    int cnt = 1;
    for (int i = N - 2; i >= 0; i--) {
        if (!visited[i])
            cnt *= f(i, adj, visited);
    }
    cout << cnt << '\n';
    return 0;
}