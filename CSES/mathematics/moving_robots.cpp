// Movin Robots
// https://cses.fi/problemset/task/1726
#include <bits/stdc++.h>
using namespace std;
int main() {
    int k;
    cin >> k;
    double dp[k + 1][64];
    double res[64];
    fill(res, res + 64, 1);
    for (int r = 0; r < 64; r++) {
        fill(*dp, *dp + (k + 1) * 64, 0);
        dp[0][r] = 1;
        for (int i = 0; i < k; i++) {
            for (int u = 0; u < 64; u++) {
                vector<int> moves;
                if (u >= 8) moves.push_back(u-8);
                if (u < 56) moves.push_back(u+8);
                if (u % 8) moves.push_back(u-1);
                if (u % 8 != 7) moves.push_back(u+1);
                for (int m : moves)
                    dp[i+1][m] += (dp[i][u] / moves.size());
            }
        }
        for (int u = 0; u < 64; u++)
            res[u] *= (1 - dp[k][u]);
    }
    double E = 0;
    for (int i = 0; i < 64; i++)
        E += res[i];
    cout << fixed << setprecision(6) << E << '\n';
    return 0;
}
