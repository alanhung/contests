// CCC '01 J2 - Mod Inverse
// https://dmoj.ca/problem/ccc01j2
#include <bits/stdc++.h>
using namespace std;

pair<int,int> xgcd(int a, int b) {
    if (!b)
        return {1, 0};
    int x, y;
    tie(x, y) = xgcd(b, a % b);
    return {y, x - y * (a / b)};
}

int main() {
    int x, m;
    cin >> x >> m;
    if (gcd(x, m) != 1) {
        cout << "No such integer exists.";
        return 0;
    }
    int res, k;
    tie(res, k) = xgcd(x, m);
    cout << (res + m) % m;
    return 0;
}