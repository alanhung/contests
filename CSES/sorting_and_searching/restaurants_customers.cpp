// Restaurants Customers
// https://cses.fi/problemset/task/1619
#include <bits/stdc++.h>
using namespace std;
int main() {
    int n;
    cin >> n;
    vector<pair<int,bool>> ns;
    for(int i = 0; i < n; i++) {
        int a, b;
        cin >> a >> b;
        ns.push_back({a, 1});
        ns.push_back({b, 0});
    }
    sort(ns.begin(), ns.end());
    int best = 0;
    int sum = 0;
    for (int i = 0; i < ns.size(); i++) {
        if (ns[i].second)
            sum++;
        else
            sum--;
        best = max(best, sum);
    }
    cout << best;
    return 0;
}
