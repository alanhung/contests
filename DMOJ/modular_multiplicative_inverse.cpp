// Modular Multiplicative Inverse
// https://dmoj.ca/problem/modinv
#include <bits/stdc++.h>
using namespace std;
unsigned long long gcd(unsigned long long a, unsigned long long b,
                       unsigned long long &x, unsigned long long &y) {
  if (!b) {
    x = 1;
    y = 0;
    return a;
  }
  unsigned long long x1, y1;
  unsigned long long d = gcd(b, a % b, x1, y1);
  x = y1;
  y = x1 - y1 * (a / b);
  return d;
}
int main() {
  unsigned long long n, m;
  cin >> n >> m;
  unsigned long long x, k;
  gcd(n, m, x, k);
  cout << (x + m) % m;
  return 0;
}
