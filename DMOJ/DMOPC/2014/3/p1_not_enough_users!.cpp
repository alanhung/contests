// DMOPC '14 Contest 3 P1 - Not Enough Users!
// https://dmoj.ca/problem/dmopc14c3p1
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, k, d;
    cin >> n >> k >> d;
    for (int i = 0; i < d; i++)
        n *= k;
    cout << n << '\n';
    return 0;
}