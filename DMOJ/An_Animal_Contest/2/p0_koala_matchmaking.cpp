#include <bits/stdc++.h>

using namespace std;

int main()
{
	ios::sync_with_stdio(0);
	cin.tie(0);
	long N;
	cin >> N;

	cout << fixed << (N + (N - 2)) / 2;

	return 0;
}