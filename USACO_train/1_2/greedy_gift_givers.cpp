/*
ID: alan93h1
LANG: C++
TASK: gift1
*/
#include<bits/stdc++.h>
using namespace std;
int main() {
  ofstream fout("gift1.out");
  ifstream fin("gift1.in");
  int np;
  fin >> np;
  map<string, int> m;
  string names[np];
  int bank[np];
  for (int i = 0; i < np; i++) {
    fin >> names[i];
    bank[i]=0; 
    m.insert({names[i], i});
  }
  for (int i = 0; i < np; i++) {
    string name;
    fin >> name;
    int amount, ng;
    fin >> amount >> ng;
    bank[m[name]]-=amount;
    if (ng) {
      int give_amount = amount / ng; 
      amount -= ng * give_amount;
      for (int i = 0; i < ng; i++) {
        string receiver;
        fin >> receiver;
        bank[m[receiver]] += give_amount;
      }
    }
    bank[m[name]] += amount;
  }
  for (int i = 0; i < np; i++) 
    fout << names[i] << ' ' << bank[i] << '\n';
  
  return 0;
}
