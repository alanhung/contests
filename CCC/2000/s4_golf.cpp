// CCC '00 S4 - Golf
//
#include <iostream>
#include <limits>

int main()
{
    std::ios::sync_with_stdio(0);
    std::cin.tie(0);
    int N, C;
    std::cin >> N >> C;

    int clubs[C];

    for (int i = 0; i < C; i++)
    {
        std::cin >> clubs[i];
    }

    int dp[N + 1];
    std::fill(dp, dp + N + 1, std::numeric_limits<int>::max() - 1);
    dp[0] = 0;

    for (int i = 1; i <= N; i++)
    {
        for (int j = 0; j < C; j++)
        {
            if (clubs[j] <= i)
            {
                dp[i] = std::min(dp[i], 1 + dp[i - clubs[j]]);
            }
        }
    }

    if (dp[N] >= std::numeric_limits<int>::max() - 1)
    {
        std::cout << "Roberta acknowledges defeat.";
    }
    else
    {
        std::cout << "Roberta wins in " << dp[N] << " strokes.";
    }
    return 0;
}