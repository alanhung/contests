// Dynamic Range Sum Queries
// https://cses.fi/problemset/task/1648
#include <bits/stdc++.h>
using namespace std;
long long query(int i, long long tree[]) {
    long long res = 0;
    while(i > 0) {
        res += tree[i];
        i -= i & (-i);
    }
    return res;
}
long long sum(int a, int b, long long tree[]) {
    if (a == 1) return query(b, tree);
    return query(b, tree) - query(a-1, tree);
}
void update(int i, long long x, long long tree[], int n) {
    while(i <= n) {
        tree[i] += x;
        i += i & (-i);
    }
}
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, q;
    cin >> n >> q;
    long long tree[n + 1];
    long long arr[n + 1];
    fill(tree, tree + n + 1, 0);
    arr[0]=0;
    for (int i = 1; i <= n; i++) {
        cin >> arr[i];
        update(i, arr[i], tree, n);
    }
    while(q--) {
        int a, b, c;
        cin >> a >> b >> c;
        if (a == 1) {
            update(b, (long long)c - arr[b], tree, n);
            arr[b] = c;
        }
        else
            cout << sum(b, c, tree) << '\n';
    }
    return 0;
}
