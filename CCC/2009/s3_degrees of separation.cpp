// CCC '09 S3 - Degrees Of Separation
// https://dmoj.ca/problem/ccc09s3
#include <bits/stdc++.h>
using namespace std;
int main(){
    cin.tie(0)->sync_with_stdio(0);
    set<int> adj[51] = {{}, {6}, {6}, {4,5,6,15}, {3,5,6}, {3,4,6}, {1,2,3,4,5,7}, {6,8}, {7,9}, {8,10,12}, {9,11}, {10,12}, {9,11,13}, {12,14,15}, {13}, {3,13}, {17,18}, {16,18}, {16,17}};
    while(true){
        char c;
        cin >> c;
        if (c == 'i') {
            int x, y;
            cin >> x >> y;
            set<int>::iterator it = adj[x].find(y);
            if (it == adj[x].end()) {
                adj[x].insert(y);
                adj[y].insert(x);
            }
        } else if (c == 'd') {
            int x, y;
            cin >> x >> y;
            set<int>::iterator it = adj[x].find(y);
            if (it != adj[x].end()) {
                adj[x].erase(it);
                adj[y].erase(x);
            }
        } else if (c == 'n') {
            int x;
            cin >> x;
            cout << adj[x].size() << '\n';
        } else if (c == 'f') {
            int x;
            cin >> x;
            bool visited[51];
            fill(visited, visited + 51, 0);
            int cnt = 0;
            visited[x]=1;
            for (set<int>::iterator it = adj[x].begin(); it != adj[x].end(); ++it)
                visited[*it]=1;
            for (set<int>::iterator it = adj[x].begin(); it != adj[x].end(); ++it) {
                for (set<int>::iterator jt = adj[*it].begin(); jt != adj[*it].end(); ++jt) {
                    if (!visited[*jt]) {
                        visited[*jt]=1;
                        cnt++;
                    }
                }
            }
            cout << cnt << '\n';
        } else if (c == 's') {
            int x, y;
            cin >> x >> y;
            int dist[51];
            fill(dist, dist + 51, -1);
            queue<int> q;
            q.push(x);
            dist[x]=0;
            while(!q.empty()) {
                int u = q.front();
                q.pop();
                for (set<int>::iterator it = adj[u].begin(); it != adj[u].end(); ++it) {
                    if (dist[*it] == -1) {
                        dist[*it] = dist[u] + 1;
                        q.push(*it);
                    }
                }
            }
            if (dist[y] == -1) cout << "Not connected\n";
            else cout << dist[y] << '\n';
        } else
            break;
    }
    return 0;
}