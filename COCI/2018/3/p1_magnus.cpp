#include <bits/stdc++.h>
using namespace std;
int main() {
    string n;
    cin >> n;
    int cnt = 0;
    char look = 'H';
    for (char &c : n) {
        if (c == look) {
            if (c == 'H')
                look = 'O';
            else if (c == 'O')
                look = 'N';
            else if (c == 'N')
                look = 'I';
            else {
                look = 'H';
                cnt++;
            }
        }
    }
    cout << cnt << '\n';
    return 0;
}