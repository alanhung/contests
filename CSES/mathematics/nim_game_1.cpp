// Nim Game 1
// https://cses.fi/problemset/task/1730
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int t;
    cin >> t;
    while(t--) {
        int n;
        cin >> n;
        int x = 0;
        for (int i = 0; i  < n; i++) {
            int k;
            cin >> k;
            x ^= k;
        }
        if (x) cout << "first\n";
        else cout << "second\n";
    }
    return 0;
}
