#include <bits/stdc++.h>
using namespace std;

int find(int x, int link[]) {
  while (x != link[x])
    x = link[x];
  return x;
}

bool same(int a, int b, int link[]) { return find(a, link) == find(b, link); }

void unite(int a, int b, int link[], int size[]) {
  a = find(a, link);
  b = find(b, link);
  if (size[a] < size[b])
    swap(a, b);
  size[a] += size[b];
  link[b] = a;
}

int main() {
  cin.tie(0)->sync_with_stdio(0);
  int N, M;
  cin >> N >> M;
  vector<tuple<int, int, int>> edges;
  bool visited[N];
  fill(visited, visited + N, false);
  for (int i = 1; i <= M; i++) {
    int f, s;
    cin >> f >> s;
    visited[f - 1] = true;
    visited[s - 1] = true;
    edges.push_back({f - 1, s - 1, i});
  }
  for (int i = 0; i < N; i++) {
    if (!visited[i]) {
      cout << "Disconnected Graph" << '\n';
      return 0;
    }
  }
  int link[N];
  int size[N];
  for (int i = 0; i < N; i++) {
    link[i] = i;
    size[i] = 1;
  }
  vector<int> result;
  for (auto edge : edges) {
    if (!same(get<0>(edge), get<1>(edge), link)) {
      unite(get<0>(edge), get<1>(edge), link, size);
      result.push_back(get<2>(edge));
    }
  }
  for (auto item : result)
    cout << item << '\n';
  return 0;
}
