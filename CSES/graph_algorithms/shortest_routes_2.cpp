// Shohrtest Route 2
// https://cses.fi/problemset/task/1672
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, m, q;
    cin >> n >> m >> q;
    long long dist[n][n];
    fill(*dist, *dist + n * n, LONG_LONG_MAX / 2);
    for (int i = 0; i < m; i++) {
        int a, b;
        long long c;
        cin >> a >> b >> c;
        dist[a-1][b-1]= min(dist[a-1][b-1], c);
        dist[b-1][a-1]= min(dist[a-1][b-1], c);
    }
    for (int i = 0; i < n; i++)
        dist[i][i]=0;
    for (int k = 0; k < n; k++) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                dist[i][j] = min(dist[i][j], dist[i][k] + dist[k][j]);
            }
        }
    } 
    while(q--) {
        int a, b;
        cin >> a >> b;
        a--;
        b--;
        cout << (dist[a][b] == LONG_LONG_MAX / 2 ? -1 : dist[a][b]) << '\n';
    }
    return 0;
}
