#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    string s, res;
    getline(cin, s);
    for (int i = 0; i < s.size(); i++) {
        res.push_back(s[i]);
        if (s[i] == 'a' || s[i] == 'e' || s[i] == 'i' || s[i] == 'o' || s[i] == 'u' ) {
            i += 2;
        }
    }
    cout << res << '\n';
    return 0;
}