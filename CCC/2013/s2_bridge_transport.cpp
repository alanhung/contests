#include <iostream>
using namespace std;
int main()
{
    int W, N, n1, n2, n3;
    cin >> W >> N >> n1;
    int cnt = 0;
    if (W >= n1)
        cnt++;

    if (N >= 2)
    {
        cin >> n2;
        if (W >= n1 + n2)
            cnt++;
    }
    if (N >= 3)
    {
        cin >> n3;
        if (W >= n1 + n2 + n3)
            cnt++;
    }
    for (int i = 4; i <= N; i++)
    {
        int cur;
        cin >> cur;
        if (W >= n1 + n2 + n3 + cur)
            cnt++;
        else
            break;
        n1 = n2;
        n2 = n3;
        n3 = cur;
    }
    cout << cnt;
    return 0;
}