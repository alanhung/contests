import java.util.Scanner;
import java.util.ArrayList;

public class Main {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int n = sc.nextInt();
    ArrayList<Integer> arr = new ArrayList<Integer>();

    int result = 0;

    for (int i = 0; i < n; i++) {
      int num = sc.nextInt();
      if (num == 0) {
        arr.remove(arr.size() - 1);
      } else {
        arr.add(num);
      }
    }

    for (int i = 0; i < arr.size(); i++) {
      result += arr.get(i);
    }
    sc.close();
    System.out.println(result);
  }
}