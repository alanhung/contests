import java.util.Scanner;

public class Main {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    short x = sc.nextShort();
    short y = sc.nextShort();
    System.out.println(x > 0 ? y > 0 ? 1 : 4 : y > 0 ? 2 : 3);
  }
}