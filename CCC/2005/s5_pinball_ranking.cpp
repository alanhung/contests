// CCC '05 S5 - Pinball Ranking
// https://dmoj.ca/problem/ccc05s5
#include <bits/stdc++.h>
using namespace std;

void update(int tree[], int n, int i, int x) {
    while(i <= n) {
        tree[i] += x;
        i += (i & -i);
    }
}

int query(int tree[], int i) {
    int sum = 0;
    while(i > 0) {
        sum += tree[i];
        i -= (i & -i);
    }
    return sum;
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    int t;
    cin >> t;
    vector<pair<int,int>> arr(t);
    for (int i = 0; i < t; i++) {
        int x;
        cin >> x;
        arr[i] = {x, i};
    }
    sort(arr.begin(), arr.end());
    int tree[t + 1];
    fill(tree, tree + t + 1, 0);
    int rank[t];
    fill(rank, rank + t, 0);
    for (int i = 0; i < t; i++)
        rank[arr[i].second] = t - i;
    double total = 0;
    for (int i = 0; i < t; i++) {
        update(tree, t, rank[i], 1);
        total += query(tree, rank[i]);
    }
    cout << fixed << setprecision(2) << total / t << '\n';
    return 0;
}