// Counting Rooms
// https://cses.fi/problemset/list/
#include <bits/stdc++.h>
using namespace std;
int main() {
    int n, m;
    cin >> n >> m;
    char grid[n * m];
    for (int i = 0; i < n * m; i++)
        cin >> grid[i];
    int cnt = 0;
    bool v[n * m];
    fill(v, v+ n * m, 0);
    for (int i = 0; i < n * m; i++) {
        if (grid[i] == '.' && !v[i]) {
            queue<int> q;
            cnt++;
            q.push(i);
            v[i] = true;
            while(!q.empty()) {
                int r = q.front() / m;
                int c = q.front() % m;
                q.pop();
                if (r - 1 >= 0 && grid[(r-1)*m + c] != '#' && !v[(r-1)*m+c]) {
                    q.push((r-1)*m+c);
                    v[(r-1)*m+c]=1;
                }
                if(r+1<n && grid[(r+1)*m+c] != '#' && !v[(r+1)*m+c]) {
                    q.push((r+1)*m+c);
                    v[(r+1)*m+c]=1;
                }
                if(c-1>=0 && grid[r*m+c-1] !='#' && !v[r*m+c-1]) {
                    q.push(r*m+c-1);
                    v[r*m+c-1]=1;
                }
                if(c+1<m&&grid[r*m+c+1]!='#'&&!v[r*m+c+1]) {
                    q.push(r*m+c+1);
                    v[r*m+c+1]=1;
                }
            }
        }
    }
    cout << cnt;
    return 0;
}
