#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int h, a, s;
    cin >> h >> a >> s;
    cout << max(min(h, a) - s, 0) << '\n';
    return 0;
}

