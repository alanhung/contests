#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int N;
    cin >> N;
    set<string> s;
    string a[N];
    bool b[N];
    fill(b, b + N, false);
    cin >> a[0];
    if (a[0].find("yubo") != string::npos) {
        s.insert(a[0]);
        b[0] = true;
    }
    for (int i = 1; i < N; i++) {
        cin >> a[i];
        if (a[i].find("yubo") != string::npos) {
            s.insert(a[i]);
            b[i] = true;
            s.insert(a[i - 1]);
        }
        if (b[i - 1])
            s.insert(a[i]);
    }
    for (set<string>::iterator it = s.begin(); it != s.end(); ++it)
        cout << *it << '\n';
    return 0;
}

