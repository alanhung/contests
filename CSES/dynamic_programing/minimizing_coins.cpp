// Minimizing Coins
// https://cses.fi/problemset/task/1634
#include <bits/stdc++.h>
#include <limits>
using namespace std;
int main() {
    int n, x;
    cin >> n >> x;
    int coin[n];
    for (int i = 0; i <n ;i ++)
        cin >> coin[i];
    long long dp[x + 1];
    dp[0] = 0;
    for (int i = 1; i <= x; i++) {
        dp[i] = numeric_limits<long long>::max();
        for (int j = 0; j < n; j++) {
            if (i - coin[j] >= 0 && dp[i - coin[j]] != numeric_limits<long long>::max())
                dp[i] = min(dp[i], dp[i - coin[j]] + 1);
        }
    }
    cout << (dp[x] == numeric_limits<long long>::max() ? -1 : dp[x]);
    return 0;
}
