// DMOPC '15 Contest 1 P4 - Itami and Candy
// https://dmoj.ca/problem/dmopc15c1p4
#include <bits/stdc++.h>
using namespace std;
void sieve(bool prime[], int n) {
    for (int p = 2; p * p <= n; p++) {
        if (prime[p]) {
            for (int i = p * p; i <= n; i += p)
                prime[i]=0;
        }
    }
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, x;
    cin >> n >> x;
    bool prime[n + 1];
    fill(prime, prime + n + 1, 1);
    sieve(prime, n);
    int cnt = 0;
    for (int i = 2; i <= n; i++) {
        if (prime[i]) {
            int mul = (n - i) / x + 1;
            cnt += 2 * mul;
            if ( (n - i) % x == 0) cnt--;
        }
    }
    cout << cnt << '\n';
    return 0;
}