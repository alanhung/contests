// Range Xor Queries
// https://cses.fi/problemset/task/1650
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, q;
    cin >> n >> q;
    int prefix[n+1];
    prefix[0]=0;
    for(int i = 1; i <= n; i++) {
        cin >> prefix[i];
        prefix[i] ^= prefix[i-1];
    }
    while(q--) {
        int a, b;
        cin >> a >> b;
        cout << (prefix[b] ^ prefix[a-1]) << '\n';
    }
    return 0;
}
