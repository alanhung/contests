#include <bits/stdc++.h>
using namespace std;
int dist(char a, char b, char c) {
  if (b == 'M')
    return -1;
  return (a != 'M') + (c != 'O');
}
int main() {
  cin.tie(0)->sync_with_stdio(0); 
  int q;
  cin >> q;
  for (int i = 0; i < q; i++) {
    string str;
    cin >> str;
    if (str.length() < 3) {
      cout << -1 << '\n';
      continue;
    }
    int cost = str.length() - 3;
    int m = 3;
    bool bad = true;
    for (int j = 0; j < str.length()-2; j++) {
      int d = dist(str[j], str[j+1], str[j+2]);
      if (d >= 0) {
        m = min(m, d);
        bad = false;
      }
    }
    if (!bad)
      cout << cost + m << '\n';
    else
      cout << -1 << '\n';
   }
  return 0;
}
