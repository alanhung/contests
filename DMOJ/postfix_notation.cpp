#include <bits/stdc++.h>
using namespace std;
int main() {
    string raw;
    getline(cin, raw);
    stringstream ss(raw);
    string token;
    stack<double> st;

    while(ss >> token) {
        if (isdigit(token[0]))
            st.push(stod(token));
        else {
            double s = st.top();
            st.pop();
            double f = st.top();
            st.pop();
            switch (token[0]) {
                case '+':
                    st.push(f + s);
                    break;
                case '-':
                    st.push(f - s);
                    break;
                case '*':
                    st.push(f * s);
                    break;
                case '/':
                    st.push(f / s);
                    break;
                case '%':
                    st.push(fmod(f, s));
                    break;
                case '^':
                    st.push(pow(f, s));
                    break;

            }
        }
    }
    cout << fixed << setprecision(1) << st.top() << '\n';
    return 0;
}

