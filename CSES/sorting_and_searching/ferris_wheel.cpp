// Ferris Wheel
// https://cses.fi/problemset/task/1090
#include <bits/stdc++.h>
using namespace std;
int main() {
    int n, x;
    cin >> n >> x;
    int ps[n];
    for (int i = 0; i < n; i++)
        cin >> ps[i];
    sort(ps, ps + n);
    int cnt = 0;
    int i = 0;
    int j = n - 1;
    while(i < j) {
        if (ps[i] + ps[j] <= x) {
            i++;
            j--;
        } else {
            j--;
        }
        cnt++;
    }
    if (i == j)
        cnt++;
    cout << cnt;
    return 0;
}
