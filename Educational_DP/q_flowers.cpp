#include <bits/stdc++.h>
using namespace std;
long long query(long long tree[], int n, int a, int b) {
    a += n;
    b += n;
    long long ans = LONG_LONG_MIN;
    while(a <= b) {
        if (a&1) ans = max(ans, tree[a++]);
        if (!(b&1)) ans = max(ans, tree[b--]);
        a /= 2;
        b /= 2;
    }
    return ans;
}
void update(long long tree[], int n, int i, long long x) {
    i += n;
    tree[i] = x;
    for (i /= 2; i >= 1; i /= 2) {
        tree[i] = max(tree[2*i], tree[2*i+1]);
    }
}
int main() {
    int N;
    cin >> N;
    int height[N];
    int beuty[N];
    for (int i = 0; i < N; i++)
        cin >> height[i];
    for (int i = 0; i < N; i++)
        cin >> beuty[i];
    long long tree[2 * (N+1)];
    /* long long dp[N]; */
    fill(tree, tree + 2 * (N+1), LONG_LONG_MIN);
    /* fill(dp, dp + N, LONG_LONG_MIN); */
    update(tree, N+1, 0, 0);
    for (int i = 0; i < N; i++) {
        /* cout << "Q: " << query(tree, N+1, 0, height[i]) << " B: " << beuty[i] << '\n'; */
        /* dp[i] = query(tree, N+1, 0, height[i]) + beuty[i]; */
        /* update(tree, N+1, height[i], dp[i]); */
        update(tree, N +1, height[i], query(tree, N+1, 0 , height[i]) + beuty[i]);
    }
    /* cout << dp[N-1] << '\n'; */
    cout << query(tree, N+1, 0, N) << '\n';
    return 0;
}