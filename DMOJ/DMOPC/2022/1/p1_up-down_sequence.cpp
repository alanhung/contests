// DMOPC '22 Contest 1 P1 - Up-down Sequence
// https://dmoj.ca/problem/dmopc22c1p1
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int t;
    cin >> t;
    while(t--) {
        int n;
        cin >> n;
        vector<int> arr(n);
        vector<pair<int,int>> pos;
        vector<int> ud(n, -1);
        for (int i = 0; i < n; i++)
            cin >> arr[i];
        bool g = 1;
        for (int i = 0; i < n-1; i++) {
            if (!arr[i]) {
                if (pos.size() && pos[pos.size()-1].second + 1== i) {
                    pos[pos.size()-1].second++;
                }
                else
                    pos.push_back({i, i});
            } else if (arr[i + 1]) {
                if (arr[i] == arr[i+1]) {
                    g=0;
                    break;
                }
                if (arr[i+1] > arr[i])
                    ud[i]=1;
                else
                    ud[i]=0;
                if (i > 0 && ud[i-1] != -1) {
                    if (ud[i] == ud[i-1]) {
                        g=0;
                        break;
                    }
                }
            } else if (pos.size() && pos[pos.size()-1].second + 1 == i)
                pos[pos.size()-1].second++;

        }
        /* cout << "UD\n"; */
        /* for (int i = 0; i < n; i++) { */
        /*     cout << ud[i] << ' '; */
        /* } */
        /* cout << '\n'; */
        for (int i = 0; i < (int)pos.size(); i++) {
            if ((pos[i].second - pos[i].first + 1) % 2) {
                if (pos[i].first - 2 >= 0 && pos[i].second + 1< n && ud[pos[i].first - 2] != -1 && ud[pos[i].second + 1] != -1 && ud[pos[i].first - 2] == ud[pos[i].second + 1]) {
                    g = false;
                    break;
                }
            } else {
                if (pos[i].first - 2 >= 0 && pos[i].second + 1< n && ud[pos[i].first - 2] != -1 && ud[pos[i].second + 1] != -1 && ud[pos[i].first - 2] != ud[pos[i].second + 1]) {
                    g = false;
                    break;
                }
            }
        }
        if (g)
            cout << "YES\n";
        else
            cout << "NO\n";
        
    }
    return 0;
}