import java.util.Scanner;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Collections;

public class Main {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int score = 0;

    int n = sc.nextInt();

    String[] responses = new String[n];
    String[] anwsers = new String[n];

    for (int i = 0; i < n; i++) {
      responses[i] = sc.next();
    }
    for (int i = 0; i < n; i++) {
      anwsers[i] = sc.next();
    }
    for (int i = 0; i < n; i++) {
      if (responses[i].equals(anwsers[i])) score++;
    }
    System.out.println(score);
    sc.close();
  }
}