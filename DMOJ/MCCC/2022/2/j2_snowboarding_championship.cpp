#include <bits/stdc++.h>
using namespace std;
int main() {
    int N, A, B;
    cin >> N >> A >> B;
    int a = 0;
    int b = 0;
    for (int i =0; i < N; i++) {
        int f, s;
        cin >> f >> s;
        if (f < A)
            a += 1;
        else
            a += 2;
        if (s < B)
            b += 1;
        else
            b += 2;
    }
    if (a < b)
        cout << "Andrew wins!\n";
    else if (b < a)
        cout << "Tommy wins!\n";
    else
        cout << "Tie!\n";
    return 0;
}