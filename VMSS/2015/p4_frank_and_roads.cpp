#include <bits/stdc++.h>
using namespace std;
int main()
{
    cin.tie(0)->sync_with_stdio(0);
    int T, N, M, G;
    cin >> T >> N >> M >> G;
    int goals[G];
    for (int i = 0; i < G; i++)
        cin >> goals[i];
    vector<pair<int, int>> adj[N + 1];
    for (int i = 0; i < M; i++)
    {
        int f, s, w;
        cin >> f >> s >> w;
        adj[f].push_back({s, w});
    }
    bool visited[N + 1];
    fill(visited, visited + N + 1, 0);
    int dist[N + 1];
    fill(dist, dist + N + 1, numeric_limits<int>::max());
    priority_queue<pair<int, int>, vector<pair<int, int>>, greater<pair<int, int>>> q;
    q.push({0, 0});
    dist[0] = 0;
    while (!q.empty())
    {
        int u = q.top().second;
        q.pop();
        if (visited[u])
            continue;
        visited[u] = true;
        for (auto nei : adj[u])
        {
            if (dist[u] + nei.second < dist[nei.first])
            {
                dist[nei.first] = dist[u] + nei.second;
                q.push({dist[nei.first], nei.first});
            }
        }
    }
    int result = 0;
    for (int i = 0; i < G; i++)
        if (dist[goals[i]] <= T)
            result++;

    cout << result;
    return 0;
}

