#include <iostream>
#include <queue>
#include <sstream>
#include <string>
#include <vector>

using namespace std;

bool solve(int u, vector<int> adj[], bool recStack[], bool visited[], int val[])
{
    if (!visited[u])
    {
        visited[u] = true;
        recStack[u] = true;
        int m = 0;
        for (int i = 0; i < adj[u].size(); i++)
        {
            if (recStack[adj[u][i]])
            {
                val[u] = -1;
                return true;
            }
            else if (val[adj[u][i]] == -1)
            {
                val[u] = -1;
                return true;
            }
            else if (solve(adj[u][i], adj, recStack, visited, val))
            {
                val[u] = -1;
                return true;
            }
            else
            {
                m += val[adj[u][i]];
            }
        }
        val[u] += m;
    }
    recStack[u] = false;
    return false;
}
void tokenize(const string &str, vector<string> &out)
{
    stringstream ss(str);
    string s;
    while (getline(ss, s, '+'))
    {
        out.push_back(s);
    }
}
int main()
{
    cin.tie(0)->sync_with_stdio(0);
    vector<int> adj[90];
    int val[90];
    fill(val, val + 90, 0);
    for (int i = 0; i < 10; i++)
    {
        for (int j = 0; j < 9; j++)
        {
            string s;
            cin >> s;
            if (isdigit(s[0]))
            {
                val[i * 9 + j] = stoi(s);
            }
            else
            {
                vector<string> out;
                tokenize(s, out);
                for (const string &token : out)
                {
                    adj[i * 9 + j].push_back((token[0] - 'A') * 9 + (token[1] - '0') - 1);
                }
            }
        }
    }
    /* cout << "debug:\n"; */
    /* for (int i = 0; i < 9; i++) */
    /* { */
    /*     cout << i << ": "; */
    /*     for (int j = 0; j < adj[i].size(); j++) */
    /*     { */
    /*         cout << adj[i][j] << ' '; */
    /*     } */
    /*     cout << '\n'; */
    /* } */
    bool visited[90];
    bool recStack[90];
    fill(visited, visited + 90, false);
    fill(recStack, recStack + 90, false);
    for (int i = 0; i < 90; i++)
    {
        solve(i, adj, recStack, visited, val);
    }
    for (int i = 0; i < 10; i++)
    {
        for (int j = 0; j < 9; j++)
        {
            if (val[i * 9 + j] == -1)
            {
                cout << "* ";
            }
            else
            {
                cout << val[i * 9 + j] << ' ';
            }
        }
        cout << '\n';
    }
    return 0;
}