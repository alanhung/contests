// Summer Institute '17 Contest 1 P5 - Crazy Math
// https://dmoj.ca/problem/si17c1p5
#include <bits/stdc++.h>
using namespace std;
vector<vector<long long>> mul(vector<vector<long long>> a,
                              vector<vector<long long>> b, long long m) {
  vector<vector<long long>> res(a.size(), vector<long long>(b[0].size()));
  for (int i = 0; i < (int)a.size(); i++)
    for (int j = 0; j < (int)b[0].size(); j++)
      for (int k = 0; k < (int)b.size(); k++)
        res[i][j] = (res[i][j] + a[i][k] * b[k][j] % m) % m;

  return res;
}
vector<vector<long long>> modpow(vector<vector<long long>> base, long long exp,
                                 long long m) {
  vector<vector<long long>> res(base.size(), vector<long long>(base.size()));
  for (int i = 0; i < (int)base.size(); i++)
    res[i][i] = 1;
  while (exp > 0) {
    if (exp & 1)
      res = mul(res, base, m);
    base = mul(base, base, m);
    exp >>= 1;
  }
  return res;
}
int main() {
  cin.tie(0)->sync_with_stdio(0);
  int a, b;
  long long n;
  long long m = 1000000000;
  cin >> a >> b >> n;
  vector<vector<long long>> f = {{0, 1}, {1, 1}};
  f = modpow(f, n, m);
  cout << (a * f[0][0] % m + b * f[0][1] % m) % m << '\n';
  return 0;
}
