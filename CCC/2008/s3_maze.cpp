// CCC '08 S3 - Maze
#include <iostream>
#include <queue>
#include <vector>

int main()
{
    std::ios::sync_with_stdio(0);
    std::cin.tie(0);
    int t;
    std::cin >> t;

    for (int test = 0; test < t; test++)
    {
        int r, c;
        std::cin >> r >> c;

        char grid[r][c];
        std::vector<int> adj[r * c];

        for (int i = 0; i < r; i++)
        {
            for (int j = 0; j < c; j++)
            {
                std::cin >> grid[i][j];
                int index = i * c + j;
                if (grid[i][j] == '+')
                {
                    if (j + 1 < c)
                        adj[index].push_back(index + 1);

                    if (j - 1 >= 0)
                        adj[index].push_back(index - 1);

                    if (i - 1 >= 0)
                        adj[index].push_back((i - 1) * c + j);

                    if (i + 1 < r)
                        adj[index].push_back((i + 1) * c + j);
                }
                else if (grid[i][j] == '-')
                {
                    if (j + 1 < c)
                        adj[index].push_back(index + 1);

                    if (j - 1 >= 0)
                        adj[index].push_back(index - 1);
                }
                else if (grid[i][j] == '|')
                {

                    if ((i - 1) >= 0)
                        adj[index].push_back((i - 1) * c + j);

                    if ((i + 1) < r)
                        adj[index].push_back((i + 1) * c + j);
                }
            }
        }

        //for (int i = 0; i < r * c; i++)
        //{
            //std::cout << i << ": ";
            //for (int j = 0; j < adj[i].size(); j++)
            //{
                //std::cout << adj[i][j] << ' ';
            //}
            //std::cout << '\n';
        //}

        std::queue<int> q;

        bool visited[r * c];
        int dist[r * c];

        std::fill(visited, visited + r * c, 0);

        visited[0] = true;
        dist[0] = 1;

        bool found = false;
        q.push(0);

        while (!q.empty())
        {

            int u = q.front();
            q.pop();

            for (int i = 0; i < adj[u].size(); i++)
            {
                if (!visited[adj[u][i]])
                {
                    visited[adj[u][i]] = true;

                    if (grid[adj[u][i] / c][adj[u][i] % c] != '*')
                    {

                        dist[adj[u][i]] = dist[u] + 1;
                        q.push(adj[u][i]);

                        if (adj[u][i] == r * c - 1)
                        {
                            found = true;
                            break;
                        }
                    }
                }
            }

            if (found)
            {
                break;
            }
        }

		if (r == 1 && c == 1)
		{
			std::cout << 1 << '\n';
		}
        else if (!found)
        {
            std::cout << -1 << '\n';
        }
        else
        {
            std::cout << dist[r * c - 1] << '\n';
        }
    }
    return 0;
}