// Minimal Rotation
// https://cses.fi/problemset/stats/1110/
#include <bits/stdc++.h>
using namespace::std;

// Lyndon Factorization and Duval algorithm
// https://cp-algorithms.com/string/lyndon_factorization.html
string min_rotation(string s) {
    int n = s.size();
    s += s;
    int i  = 0;
    int ans = 0;
    while (i < n) {
        ans = i;
        int j = i + 1;
        int k = i;
        while (j < 2 * n && s[k] <= s[j]) {
            if (s[k] < s[j])
                k = i;
            else
                k++;
            j++;
        }
        while (i <= k)
            i += j - k;
    }
    return s.substr(ans, n);
}
int main() {
    cin.tie(0)->sync_with_stdio(0);
    string s;
    cin >> s;
    cout << min_rotation(s);
    return 0;
}
