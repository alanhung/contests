// CCC '09 S5 - Wireless 
// https://dmoj.ca/problem/ccc09s5
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int m, n, k;
    cin >> m >> n >> k;
    vector<int> cost(m);
    vector<tuple<int,int,int,int>> station;
    while(k--) {
        int x, y, r, b;
        cin >> x >> y >> r >> b;
        station.push_back({x-1,y-1,r,b});
    }
    int ma = 0;
    int cnt= 0;
    for (int j = 0; j < n; j++) {
        fill(cost.begin(), cost.end(), 0);
        for (tuple<int,int,int,int> &node : station) {
            int x, y, r, b;
            tie(x, y, r, b) = node;
            int dd = r * r - (x - j) * (x - j);
            if (dd < 0)
                continue;
            int d = sqrt(dd);
            cost[max(0, y - d)] += b;
            if (y + d + 1 < m)
                cost[y + d + 1] += -b;
        }
        int sum = cost[0];
        if (sum > ma) {
            ma = sum;
            cnt = 1;
        } else if (ma == sum)
            cnt++;
        for (int i = 1; i < m; i++) {
            sum += cost[i];
            if (sum > ma) {
                ma = sum;
                cnt = 1;
            } else if (ma == sum)
                cnt++;
        }
    }
    cout << ma << '\n';
    cout << cnt << '\n';
    return 0;
}