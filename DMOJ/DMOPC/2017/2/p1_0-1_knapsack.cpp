#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n;
    cin >> n;
    long long res = 0;
    while(n--) {
        int c, v;
        cin >> c >> v;
        if (v > 0)
            res += c;
    }
    cout << res << '\n';
    return 0;
}