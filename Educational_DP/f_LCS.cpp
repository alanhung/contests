// LCS
// TLE

#include <iostream>
#include <string>

int main()
{
	std::ios::sync_with_stdio(0);
	std::cin.tie(0);
	std::string s;
	std::string t;

	std::getline(std::cin ,s);
	std::getline(std::cin ,t);


	std::string dp[2][t.size()];

	if (s[0] == t[0])
	{
		dp[0][0] = s[0];
	}

	for (int i = 1; i < t.size(); i++)
	{
		if (s[0] == t[i])
		{
			dp[0][i] = t[i];
		}
		else
		{
			dp[0][i] = dp[0][i - 1];
		}
	}

	for (int i = 1; i < s.size(); i++)
	{
		if (s[i] == t[0])
		{
			dp[1][0] = t[0]; 
		}
		else
		{
			dp[1][0] = dp[0][0];
		}

		for (int j = 1; j < t.size(); j++)
		{
			if (s[i] == t[j])
			{
				std::swap(dp[1][j], dp[0][j - 1]);
				dp[1][j].push_back(t[j]);
			}
			else 
			{
				if (dp[0][j].size() > dp[1][j-1].size())
				{
					dp[1][j] = dp[0][j];
				}
				else
				{
					dp[1][j] = dp[1][j - 1];
				}
			}

			std::swap(dp[0][j - 1], dp[1][j - 1]); 
		}

		std::swap(dp[0][t.size() - 1], dp[1][t.size() - 1]);
	}

	//for (int i = 0; i < s.size(); i++)
	//{
		//for (int j = 0; j < t.size(); j++)
		//{
			//std::cout << dp[i][j] << ' ';
		//}
		//std::cout << '\n';
	//}

	std::cout << dp[0][t.size() -1];

	return 0;
}