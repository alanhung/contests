#include <bits/stdc++.h>
using namespace std;
int main() {
    int n, m;
    cin >> n >> m;
    int a[n];
    for (int i = 0; i < n; i++)
        cin >> a[i];
    sort(a, a + n);
    int b[n];
    int l = 0;
    int r = n - 1;
    int k = 0;
    while(l < r) {
        b[k] = a[l];
        b[k + 1] = a[r];
        k += 2;
        l++;
        r--;
    }
    if (n % 2 == 1)
        b[n - 1] = a[n / 2];
    bool g = true;
    for (int i = 0; i < n - 1; i++)
        if (b[i] + b[i + 1] < m) {
            g = false;
            break;
        }
    if (g) {
        for (int i = 0; i < n; i++)
            cout << b[i] << ' ';
        cout << '\n';
    } else 
        cout << -1 << '\n';
    return 0;
}