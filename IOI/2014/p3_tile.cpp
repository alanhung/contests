// IOI '14 Practice Task 3 - Tile
// https://dmoj.ca/problem/ioi14pp3
#include <bits/stdc++.h>
using namespace std;

// top left, top right, bottom left, bottom right
// x, y
pair<int, int> dent[4] = {{1, 1}, {0, 1}, {0, 0}, {1, 0}};

void corner(int n, int x, int y, int m) {
    if (n == 1) {
        for (int i = 0; i < 4; i++)
            if (m != i)
                cout << x + dent[i].first << ' ' << y + dent[i].second << ' ';
        cout << '\n';
        return;
    }
    pair<int,int> arr[3];
    int cnt = 0;
    for (int i = 0; i < 4; i++) {
        if (m == i)
            corner(n-1,x+dent[i].first*(1<<(n-1)),y+dent[i].second*(1<<(n-1)),i);
        else {
            corner(n-1,x+dent[i].first*(1<<(n-1)),y+dent[i].second*(1<<(n-1)),(i+2)%4);
            arr[cnt] = {x+(1<<(n-1)) - dent[(i+2)%4].first, y+(1<<(n-1))-dent[(i+2)%4].second };
            cnt++;
        }
    }
    for (int i = 0; i < 3; i++)
        cout << arr[i].first << ' ' << arr[i].second << ' ';
    cout << '\n';
}
void block(int n, int x, int y, int bx, int by) {
    if (n == 1) {
        for (int i = 0; i < 4; i++) {
            if (!(x + dent[i].first == bx && y + dent[i].second == by) )
                cout << x + dent[i].first << ' ' << y + dent[i].second  << ' ';
        }
        cout << '\n';
        return;
    }
    pair<int,int> arr[3];
    int cnt = 0;
    for (int i = 0; i < 4; i++) {
        if (bx >= x+dent[i].first*(1<<(n-1)) && bx < x+dent[i].first*(1<<(n-1)) + (1<<(n-1)) && by >= y+dent[i].second*(1<<(n-1)) && by < y +dent[i].second*(1<<(n-1)) + (1<<(n-1)) ) 
            block(n-1, x + dent[i].first*(1<<(n-1)), y + dent[i].second * (1<<(n-1)), bx, by);
        else {
            corner(n-1,x+dent[i].first*(1<<(n-1)),y+dent[i].second*(1<<(n-1)),(i+2)%4);
            arr[cnt] = {x+(1<<(n-1)) - dent[(i+2)%4].first, y+(1<<(n-1))-dent[(i+2)%4].second };
            cnt++;
        }
    }
    for (int i = 0; i < 3; i++)
        cout << arr[i].first << ' ' << arr[i].second << ' ';
    cout << '\n';
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, x, y;
    cin >> n >> x >> y;
    block(n, 0, 0, x, y);
    return 0;
}