#include <bits/stdc++.h>
using namespace std;
int main()
{
    cin.tie(0)->sync_with_stdio(0);
    int c, r, d;
    cin >> c >> r >> d;
    vector<pair<int, int>> adj[c];
    vector<int> dest;
    for (int i = 0; i < r; i++)
    {
        int f, s, t;
        cin >> f >> s >> t;
        adj[f - 1].push_back({s - 1, t});
        adj[s - 1].push_back({f - 1, t});
    }
    for (int i = 0; i < d; i++)
    {
        int f;
        cin >> f;
        dest.push_back(f - 1);
    }
    bool visited[c];
    int dist[c];
    fill(visited, visited + c, false);
    fill(dist, dist + c, numeric_limits<int>::min());
    dist[0] = numeric_limits<int>::max();
    int m = numeric_limits<int>::max();
    priority_queue<pair<int, int>> q;
    q.push({numeric_limits<int>::max(), 0});
    while (!q.empty())
    {
        pair<int, int> temp = q.top();
        q.pop();
        if (visited[temp.second])
            continue;
        visited[temp.second] = true;
        for (auto nei : adj[temp.second])
        {
            if (min(nei.second, temp.first) > dist[nei.first])
            {
                dist[nei.first] = min(nei.second, temp.first);
                q.push({min(nei.second, temp.first), nei.first});
            }
        }
    }
    for (int elem : dest)
        m = min(m, dist[elem]);
    cout << m;
    return 0;
}