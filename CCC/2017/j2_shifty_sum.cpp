import java.util.Scanner;
import java.lang.Math;

public class Main {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int n = sc.nextInt();
    int k = sc.nextInt();
    int result = n;
    for (int i = 1; i <= k; i++) {
      result += n * Math.pow(10, i);
    }
    System.out.println(result);
    sc.close();
  }
}