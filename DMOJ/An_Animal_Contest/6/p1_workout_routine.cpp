// An Animal Contest 6 P1 - Workout Routine
// https://dmoj.ca/problem/aac6p1
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, k;
    cin >> n >> k;
    int r = 0;
    for (int i = 1; i <= n - 1; i++) {
        cout << i << ' ';
        r += i;
        r %= k;
    }
    cout << k * (int)ceil((double)n / k) + (k - r) << '\n';
    return 0;
}