#include <bits/stdc++.h>
using namespace std;
int main() {
    int N;
    int wei[3];
    cin >> N >> wei[0] >> wei[1] >> wei[2];
    int dp[N + 1];
    fill(dp, dp + N + 1, 0);
    for (int i = 0; i < 3; i++) {
        dp[wei[i]] = 1;
    }
    for (int i = 0; i < 3; i++) {
        for (int j = wei[i]; j <= N; j++) {
            if (dp[j - wei[i]]) {
                dp[j] = max(dp[j], dp[j - wei[i]] + 1);
            }
        }
    }
    cout << dp[N] << '\n';
    return 0;
}