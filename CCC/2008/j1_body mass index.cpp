import java.util.Scanner;
import java.lang.Math;

public class Main {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    double bmi = sc.nextDouble() / Math.pow(sc.nextDouble(), 2);
    System.out.println(bmi > 25 ? "Overweight" : bmi < 18 ? "Underweight" : "Normal weight");
  }
}