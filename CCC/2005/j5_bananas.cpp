// CCC '05 J5 - Bananas
#include <iostream>
#include <map>
#include <string>
#include <algorithm>
#include <stack>

bool search(std::string word, std::map<std::string, bool> &dictionary)
{
  if (dictionary.find(word) != dictionary.end())
  {
    return dictionary[word];
  }

  if (word[0] == 'A')
  {
    if (word[1] == 'N' && (word.size() > 2))
    {
      if (search(word.substr(2, word.size() - 2), dictionary))
      {
        dictionary.insert({word, true});
        return true;
      }
    }
  }

  if (word[0] == 'B')
  {
    int index = -1;
    std::stack<char> stack;
    for (std::string::iterator it = word.begin(); it != word.end(); ++it)
    {
      if (*it == 'B')
      {
        stack.push('B');
      }
      else if (*it == 'S')
      {
        stack.pop();
        if (stack.empty())
        {
          index = it - word.begin();
          break;
        }
      }
    }
    if (index != -1 && index != 1)
    {
      bool next = search(word.substr(1, index - 1), dictionary);
      if (next)
      {
        if (index == word.size() - 1)
        {
          dictionary.insert({word, true});
          return true;
        }

        if (word[index + 1] == 'N' && word.size() > index + 2)
        {
          dictionary.insert({word, search(word.substr(index + 2), dictionary)});
          return dictionary[word];
        }
      }
    }
  }
  dictionary.insert({word, false});
  return false;
}

int main()
{
  std::map<std::string, bool> dictionary;
  dictionary.insert({"A", true});
  while (true)
  {
    std::string word;
    std::cin >> word;

    if (word == "X")
    {
      break;
    }

    if (search(word, dictionary))
    {
      std::cout << "YES\n";
    }
    else
    {
      std::cout << "NO\n";
    }
  }
  return 0;
}