#include <iostream>
#include <vector>
#include <queue>
#include <utility>
using namespace std;
int main(){
    cin.tie(0)->sync_with_stdio(0);
    int R, C;
    cin >> R >> C;
    vector<pair<int,int>> ms;
    char table[R][C];
    for (int i = 0; i < R; i++)
        for (int j = 0; j < C; j++)
        {
            cin >> table[i][j];
            if (table[i][j] == 'M')
                ms.push_back({i, j});
        }
    int cnt = 0;
    bool visited[R][C];
    fill(*visited, *visited + R * C, false);
    for (int i = 0; i < ms.size(); i++)
    {
        if (!visited[ms[i].first][ms[i].second])
        {
            cnt++;
            queue<pair<int,int>> q;
            q.push(ms[i]);
            visited[ms[i].first][ms[i].second] = true;
            while (!q.empty())
            {
                int r = q.front().first;
                int c = q.front().second;
                q.pop();

                if (table[r + 1][c] != '#' && !visited[r + 1][c])
                {
                    visited[r + 1][c] = true;
                    q.push({r + 1, c});
                }
                if (table[r - 1][c] != '#' && !visited[r - 1][c])
                {
                    visited[r - 1][c] = true;
                    q.push({r - 1, c});
                }
                if (table[r][c + 1] != '#' && !visited[r][c + 1])
                {
                    visited[r][c + 1] = true;
                    q.push({r, c + 1});
                }
                if (table[r][c - 1] != '#' && !visited[r][c - 1])
                {
                    visited[r][c - 1] = true;
                    q.push({r, c - 1});
                }
            }
        }
    }
    cout << cnt;
    return 0;
}