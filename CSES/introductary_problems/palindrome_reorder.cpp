// Palindrome Reordering
//https://cses.fi/problemset/task/1755
#include <bits/stdc++.h>
using namespace std;
int main() {
    string s;
    cin >> s;
    int a[26];
    fill(a ,a + 26, 0);
    for (char c : s) {
        a[c - 65]++;
    }
    int odd = 0;
    pair<int,int> oddpair;
    vector<pair<int,int>> temp; 
    for (int i = 0; i < 26; i++) {
        if (a[i] & 1) {
            odd++;
            oddpair = {i, a[i]};
        }
        else if (a[i] != 0)
            temp.push_back({i, a[i]});
    }
    if (odd > 1) {
        cout << "NO SOLUTION\n";
    } else {
        for (int i = 0; i < temp.size(); i++) {
            for (int j = 0; j < temp[i].second / 2; j++)
                cout << (char)(temp[i].first + 65);
        }
        if (odd == 1) {
            for (int i = 0; i < oddpair.second; i++)
                cout << (char)(oddpair.first + 65);
        }
        for (int i = temp.size() - 1; i >= 0; i--) {
            for (int j = 0; j < temp[i].second / 2; j++)
                cout << (char)(temp[i].first + 65);
        }
    }
    return 0;
}
