#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    int n, w;
    cin >> n >> w;
    int weight[n];
    int value[n];
    int max_value = 0;
    for (int i = 0; i < n; i++) {
        cin >> weight[i];
        cin >> value[i];
        max_value += value[i];
    }
    long long dp[max_value + 1];
    fill(dp, dp + (max_value + 1), INT_MAX);
    dp[0] = 0;
    for (int i = 0; i < n; i++)
        for (int j = max_value; j >= value[i]; j--)
            dp[j] = min(dp[j], dp[j - value[i]] + weight[i]);
    int ans = 0;
    for (int i = 1; i <= max_value; i++) {
        if (dp[i] <= w)
            ans = i;
    }
    cout << ans << '\n';
    return 0;
}