// SGS Programming Challenge P1 - XOR in Computer Class
// https://dmoj.ca/problem/sgspc1p1
#include <bits/stdc++.h>
using namespace std;
int main() {
    cin.tie(0)->sync_with_stdio(0);
    unsigned long long n;
    cin >> n;
    unsigned long long arr[n];
    for (unsigned long long i = 0; i < n; i++)
        cin >> arr[i];
    unsigned long long x = 0;
    for (unsigned long long b = 0; b < log2(*max_element(arr, arr+n)) + 1ULL; b++) {
        unsigned long long cnt0 = 0;
        unsigned long long cnt1 = 0;
        for (unsigned long long i = 0; i < n; i++) {
            cnt0 += ((arr[i] & (1ULL << b)) > 0) * (n - i) * (i + 1ULL);
            cnt1 += (((arr[i] ^ (1ULL << b) ) & (1ULL << b) ) > 0) * (n - i) * (i + 1ULL);

        }
        if (cnt0 > cnt1)
            x |= (1ULL << b);
    }
    cout << x << '\n';
    return 0;
}

