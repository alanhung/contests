// Coin Combinations 1
// https://cses.fi/problemset/task/1635
#include <bits/stdc++.h>
#define MOD 1000000007
using namespace std;
int main() {
    int n, x;
    cin >> n >> x;
    int coin[n];
    for (int i = 0; i < n; i++)
        cin >> coin[i];
    int dp[x + 1];
    dp[0] = 1;
    for(int i = 1; i <= x; i++) {
        dp[i] = 0;
        for (int j = 0; j < n; j++) {
            if (i - coin[j] >= 0 && dp[i - coin[j]]) {
                dp[i] += dp[i - coin[j]];
                dp[i] %= MOD;
            }
        }
    }
    cout << dp[x];
    return 0;
}
