// Increasing Array
// https://cses.fi/problemset/task/1094
#include <bits/stdc++.h>
using namespace std;
int main() {
    int n;
    cin >> n;
    int a[n];
    for (int i =0; i < n; i++)
        cin >> a[i];
    long long t  = 0;
    for (int i = 0; i < n - 1;i++) {
        if (a[i + 1] < a[i]) {
            t += a[i] - a[i + 1];
            a[i + 1] += a[i] - a[i + 1];
        }
    }
    cout << t;
    return 0;
}

