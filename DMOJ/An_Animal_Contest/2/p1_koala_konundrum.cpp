#include <bits/stdc++.h>

using namespace std;

int main()
{
	ios::sync_with_stdio(0);
	cin.tie(0);
	int N;
	cin >> N;
	map<char, int> chars;

	for (int i = 0; i < N; i++)
	{
		char current;
		cin >> current;

		if (chars.find(current) == chars.end())
		{
			chars.insert({current, 1});
		}
		else
		{
			chars[current]++;
		}
	}

	int odd = 0;
	

	for (map<char, int>::iterator it = chars.begin(); it!= chars.end(); ++it)
	{
		if (it->second % 2 != 0)
		{
			odd++;
		}
	}

	if (odd == 0)
	{
		cout << 1;
	}
	else
	{
		cout << odd;
	}

	return 0;
}