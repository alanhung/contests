// DMOPC '14 Contest 1 P2 - Tiles
#include <bits/stdc++.h>
using namespace std;
int main() {
    int w, l, s;
    cin >> w >> l >> s;
    cout << (w / s) * (l / s) << '\n';
    return 0;
}